#!/bin/bash

TRACKLET_PATH=/share/data/segkitti/tracklets/
resultPath=/home-nfs/lcchen/workspace/SegKITTI/codes/opencv/result/
testList=${TRACKLET_PATH}test_list.txt

OPTIONS="setup prepdata train val test eval prepcad evalcad prepmt evalmt quit"
select opt in ${OPTIONS} ; do
    if [[ "${opt}" = "setup" ]] ; then
	rm SetConfig
	make SetConfig
	./SetConfig kittiConfig.xml
    elif [[ "${opt}" = "prepcad" ]] ; then
	rm PreProp
	rm DataProcessor.o
	rm DepthInterpolater.o
	rm MaskCreater.o
	make PreProp
	./PreProp kittiConfig.xml -cad
    elif [[ "${opt}" = "prepmt" ]] ; then
	rm PreProp
	rm DataProcessor.o
	rm DepthInterpolater.o
	rm MaskCreater.o
	make PreProp
	./PreProp kittiConfig.xml -mt
    elif [[ "${opt}" = "prepdata" ]] ; then
	rm PreProp
	rm DataProcessor.o
	rm DepthInterpolater.o
	rm MaskCreater.o
	make PreProp
	./PreProp kittiConfig.xml -dataset
    elif [[ "${opt}" = "train" ]] ; then
	rm StructSVMCPnSlack.o
	rm TrainingSample.o
	rm DepthInterpolater.o	
	#rm GraphCutWrapper.o
	rm ProgNSlack
	make ProgNSlack
	#./ProgNSlack -d ~/dataset/segkitti/tracklets/tracklet_gt -f ~/dataset/segkitti/tracklets/trainval_list.txt -o ../result/model/svm_model.dat -c 1 -i 10 -e 0
	./ProgNSlack -d ~/dataset/segkitti/tracklets/tracklet_gt -f ~/dataset/segkitti/tracklets/trainval_list.txt -o ../result/model/svm_model.dat -c 1 -i 50 -e 0 -m ../result/model/svm_model.dat.9 -n 10
    elif [[ "${opt}" = "val" ]] ; then
	rm IterativeEnergySolver.o
	rm DepthInterpolater.o
	rm MaskCreater.o
	#rm GraphCutWrapper.o
	rm SingleGC
	make SingleGC
	./SingleGC kittiConfig.xml ../result/model/svm_model.dat.9 ~/dataset/segkitti/tracklets/val_list.txt
    elif [[ "${opt}" = "test" ]] ; then
	rm IterativeEnergySolver.o
	rm DepthInterpolater.o
	rm MaskCreater.o
	#rm GraphCutWrapper.o
	rm SingleGC
	make SingleGC
	./SingleGC kittiConfig.xml ../result/model/svm_model.dat.9 ~/dataset/segkitti/tracklets/test_list.txt
    elif [[ "${opt}" = "eval" ]] ; then
	rm EvalScore
	make EvalScore
	./EvalScore kittiConfig.xml ~/dataset/segkitti/tracklets/test_list.txt -dataset scores.txt
    elif [[ "${opt}" = "evalcad" ]] ; then
	rm EvalScore
	make EvalScore
	./EvalScore kittiConfig.xml ~/dataset/segkitti/tracklets/test_list.txt ~/dataset/segkitti/tracklets/tracklet_cadShape_v4_split1p002/ ~/workspace/SegKITTI/codes/opencv/result/scores_CAD_v4_trained_split1p002.txt
	#./EvalScore kittiConfig.xml ~/dataset/segkitti/tracklets/test_list.txt ~/dataset/segkitti/tracklets/tracklet_cadShape/ ~/workspace/SegKITTI/codes/opencv/result/scores_CAD_v4_trained.txt
	#./EvalScore kittiConfig.xml ~/dataset/segkitti/tracklets/test_list.txt ~/dataset/segkitti/tracklets/tracklet_inverse_mask_fuse/ ~/workspace/SegKITTI/codes/opencv/result/scores_inverse_mask_fuse_icp_inlier05.txt
	#./EvalScore kittiConfig.xml ~/dataset/segkitti/tracklets/test_list.txt ~/dataset/segkitti/tracklets/tracklet_inverse_mask/ ~/workspace/SegKITTI/codes/opencv/result/scores_inverse_mask_GauMRF.txt
	#./EvalScore kittiConfig.xml ${testList} ${TRACKLET_PATH}tracklet_cadShape/ ${resultPath}scores_CAD.txt
    elif [[ "${opt}" = "evalmt" ]] ; then
	rm EvalScore
	make EvalScore
	./EvalScore kittiConfig.xml ~/dataset/segkitti/tracklets/test_list.txt ~/dataset/segkitti/tracklets/tracklet_mtShape/ ~/workspace/SegKITTI/codes/opencv/result/scores_MT.txt
	#./EvalScore kittiConfig.xml ${testList} ${TRACKLET_PATH}tracklet_cadShape/ ${resultPath}scores_CAD.txt
    elif [[ "${opt}" = "quit" ]] ; then
	echo done
	exit
    else
	echo bad option
    fi
done