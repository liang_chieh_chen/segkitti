#include "DepthInterpolater.h"
#include "Constants.h"
#include "ContrastSensitivePM.h"
#include "GmmAppearance.h"
#include "Utility.h"

#include <sstream>
#include <fstream>
#include <iostream>
#include <vector>
#include <cmath>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <Eigen/Sparse>
#include <Eigen/Dense>

/* IF use Laplacian MRF
#include <ilcplex/ilocplex.h>
ILOSTLBEGIN 

//namespace Eigen conflicts graphcut namespace!!!!
//using namespace Eigen;  
//*/

using namespace std;
using namespace cv;


typedef Eigen::SparseMatrix<double> SpMat;
typedef Eigen::Triplet<double> Trip;

DepthInterpolater::DepthInterpolater(int _num_itr)
  :m_num_itr(_num_itr),
   target_depth(0),
   gmm_app(0)
{

}



DepthInterpolater::DepthInterpolater(Mat& depth_img, Mat& mask, Rect& _roi, int _num_comp, int _feat_size, int _num_itr)
  :m_num_itr(_num_itr),
   target_depth(0),
   gmm_app(0)
{
  //NOTE the depth_img is within the roi!!
  //         mask is whole image

  Rect roi(0, 0, depth_img.cols, depth_img.rows);

  CV_Assert(roi.width == _roi.width && roi.height == _roi.height);

  //will focus only on roi
  Mat mask_roi = mask(_roi);

  /* for debug
  imshow("d", depth_img);
  imshow("m", mask_roi);
  waitKey();

  double mmin, mmax;
  minMaxLoc(mask, &mmin, &mmax);
  cout << "mask m:" << mmin << "," << mmax << endl;

  for(int row = 0; row < depth_img.rows; ++row){
    for(int col = 0; col < depth_img.cols; ++col){
      if(depth_img.at<float>(row,col) != 0){
	cout << depth_img.at<float>(row,col) << ' ';
      }
    }
  }
  cout << endl;
  //*/


  gmm_app = new GmmAppearance(depth_img, mask_roi, roi, roi, _num_comp, _feat_size);


  /* for debug

  cout << "printing samples..." << endl;
  gmm_app->printBkgSamples();
  gmm_app->printFrgSamples();
 
  //*/


  m_img_row = depth_img.rows;
  m_img_col = depth_img.cols;


}

DepthInterpolater::~DepthInterpolater()
{
  if(gmm_app != 0){
    delete gmm_app;
  }
}

void DepthInterpolater::find_img_diff(vector<Mat>& img_diff, double& total_diff, const Mat& fimg)
{
  //find image difference between neighbors
  Rect roi(0, 0, fimg.cols, fimg.rows);  //dummy roi = whole image
  int conn = 4;  //4-connect neighbors
  ContrastSensitivePM pairwise(roi, conn);
  vector<double> img_lambda;
  
  img_lambda.push_back(0);   img_lambda.push_back(1);
  
  //pairwise.compute_img_diff(fimg, img_lambda, img_diff, total_diff);
  pairwise.compute_img_diff(fimg, img_diff, total_diff);
}

void DepthInterpolater::interpolate(Mat& recon_depth, const Mat& fimg, const Mat& depth, const Mat& indicator, const double lambda_dep, const int method)
{
  //interpolation method
  //1: L1 reconstruction. Laplacian MRF
  //2: L2 reconstruction. Gaussian MRF
  if(method == 1){
    cout << "reconstructing Laplacian MRF ..." << endl;
    laplacian_interpolate(recon_depth, fimg, depth, indicator, lambda_dep);
  }
  else {
    cout << "reconstructing Gaussian MRF ..." << endl;
    gaussian_interpolate(recon_depth, fimg, depth, indicator, lambda_dep);
  }
 
}
 



void DepthInterpolater::laplacian_interpolate(Mat& recon_depth, const Mat& fimg, const Mat& depth, const Mat& indicator, const double lambda_dep)
{
  
  CV_Assert(depth.type() == CV_32FC1);
  CV_Assert(indicator.type() == CV_8UC1);

  vector<Mat> img_diff;
  double total_diff;

  find_img_diff(img_diff, total_diff, fimg);

  //node index
  Mat cur_node_idx(depth.size(), CV_32SC1);

  create_node_matrix(cur_node_idx);

  //create the projection matrix
  int proj_length = 0;
  vector<Trip> coeffs;

  create_mask_triplets(coeffs, proj_length, cur_node_idx, indicator);

  //vectorize the depth image
  Eigen::VectorXd d(proj_length);

  vectorize_matrix(d, depth, indicator);

  //find south neighbor index
  Mat south_ngh_node_idx(depth.size(), CV_32SC1);

  int ngh_type;
  const int img_size = depth.rows * depth.cols;

  ngh_type = 0;   //south neighbor
  create_ngh_node_matrix(cur_node_idx, ngh_type, south_ngh_node_idx);

  //find east neighbor index

  Mat east_ngh_node_idx(depth.size(), CV_32SC1);
  ngh_type = 1;   //east neighbor
  create_ngh_node_matrix(cur_node_idx, ngh_type, east_ngh_node_idx);

  //set up constraints for LP  
  /*   IF use Laplacian MRF
  IloEnv env;

  IloModel model(env);

  IloNumVarArray y(env);
  IloNumVarArray te(env);
  IloNumVarArray ts(env);

  for(int i = 0; i < img_size; ++i){
    y.add(IloNumVar(env));
    te.add(IloNumVar(env));
    ts.add(IloNumVar(env));
  }

  IloNumVarArray t(env);
  
  for(int i = 0; i < proj_length; ++i)
    t.add(IloNumVar(env));

  IloNumArray all_one(env, proj_length);
  IloNumArray ws(env, img_size);
  IloNumArray we(env, img_size);
  
  //create coefficients
  for(int i = 0; i < proj_length; ++i)
    all_one[i] = 1;
  
  float* val_row[2];

  int count = 0;
  for(int row = 0; row < fimg.rows; ++row){
    val_row[0] = img_diff[0].ptr<float>(row);
    val_row[1] = img_diff[1].ptr<float>(row);

    for(int col = 0; col < fimg.cols; ++col){
      ws[count] = lambda_dep * val_row[0][col];
      we[count] = lambda_dep * val_row[1][col];

      count++;
    }
  }

  //setup objective function
  model.add( IloMinimize(env, IloScalProd(all_one, t) + IloScalProd(ws, ts) + IloScalProd(we, te)) );
  
  //setup constraints
  int ind;

  for(int i = 0; i < proj_length; ++i){
    ind = coeffs[i].col();
    model.add( -t[i] <= y[ind] - d(i) );
    model.add( y[ind] - d(i) <= t[i] );
  }

  count = 0;
  int* east_row;
  int* south_row;

  for(int row = 0; row < cur_node_idx.rows; ++row){
    east_row = east_ngh_node_idx.ptr<int>(row);
    south_row = south_ngh_node_idx.ptr<int>(row);

    for(int col = 0; col < cur_node_idx.cols; ++col){
      ind = east_row[col];
      model.add( -te[count] <= y[count] - y[ind] );
      model.add( y[count] - y[ind] <= te[count]  );

      ind = south_row[col];
      model.add( -ts[count] <= y[count] - y[ind] );
      model.add( y[count] - y[ind] <= ts[count]  );

      count++;
    }
  }
 
  //solve the programing
  IloCplex cplex(model);

  // Optimize the problem and obtain solution.
  if ( !cplex.solve() ) {
    env.error() << "Failed to optimize LP" << endl;
    throw(-1);
  }

  IloNumArray y_hat(env);
  env.out() << "Solution status = " << cplex.getStatus() << endl;
  env.out() << "Solution value  = " << cplex.getObjValue() << endl;

  cplex.getValues(y_hat, y);

  //copy the computed result to output
  recon_depth.create(depth.size(), depth.type());
  copy_result(recon_depth, y_hat);

#if DEBUG == 1
  imagesc_bw("recon_depth_l1", recon_depth);
  string save_file_name;
  save_file_name = "../result/recon_depth_l1" + SAVE_IMG_EXT;
  imwrite(save_file_name, recon_depth);
#endif

  env.end();



  //*/

  //test CPLEX
  /*
  IloEnv env;
  IloModel model(env);

  IloNumVarArray x(env);

  for(int i = 0; i < 100; ++i)
    x.add(IloNumVar(env, 0, i));

  IloNumArray c(env);

  for(int i = 0; i < 100; ++i)
    c.add(i+1);

  IloNumVarArray x2(env);
  for(int i = 0; i < 50; ++i)
    x2.add(IloNumVar(env));

  IloNumArray c2(env);
  for(int i = 0; i < 50; ++i)
    c2.add(i+2);

  model.add( IloMinimize(env, IloScalProd(c, x) + IloScalProd(c2,x2)  ) );

  //solve the programing
  IloCplex cplex(model);
  // Optimize the problem and obtain solution.
  if ( !cplex.solve() ) {
    env.error() << "Failed to optimize LP" << endl;
    throw(-1);
  }

  IloNumArray y(env);
  env.out() << "Solution status = " << cplex.getStatus() << endl;
  env.out() << "Solution value  = " << cplex.getObjValue() << endl;

  cplex.getValues(y, x);
  env.out() << "values = " << y << endl;

  env.end();

  exit(-1);

  //*/

}

void DepthInterpolater::gaussian_interpolate(Mat& recon_depth, const Mat& fimg, const Mat& depth, const Mat& indicator, const double lambda_dep)
{
  CV_Assert(depth.type() == CV_32FC1);
  CV_Assert(indicator.type() == CV_8UC1);
 
  string save_file_name;

#if DEBUG == 1
  /*
    {
    fstream fs;
    fs.open("../result/depth.txt", fstream::out);
  
    fs << depth.rows << " " << depth.cols << endl;

    const float* dep_row;

    for(int row = 0; row < depth.rows; ++row){
    dep_row = depth.ptr<float>(row);

    for(int col = 0; col < depth.cols; ++col){
    fs << dep_row[col] << " ";
    }
    fs << endl;
    }
  
    fs.close();
    }
  */
  //cout << "img size:" << fimg.rows*fimg.cols << endl;

  double t;
#endif

  vector<Mat> img_diff;
  double total_diff;

#if DEBUG == 1
  t = (double)getTickCount();
#endif

  find_img_diff(img_diff, total_diff, fimg);
  
#if DEBUG == 1
  t = ((double)getTickCount() - t)/getTickFrequency();
  //cout << "compute_img_diff:" << t << " sec" << endl;
  //cout << "total_diff:" << total_diff << endl;
#endif

  //node index
  Mat cur_node_idx(depth.size(), CV_32SC1);
  Mat ngh_node_idx(depth.size(), CV_32SC1);

#if DEBUG == 1
  t = (double)getTickCount();
#endif

  create_node_matrix(cur_node_idx);

#if DEBUG == 1
  t = ((double)getTickCount() - t)/getTickFrequency();
  //cout << "create_node_matrix:" << t << " sec" << endl;
#endif

  //find south neighbor index
  int ngh_type;
  vector<Trip> coeffs;
  const int img_size = depth.rows * depth.cols;
  
  ngh_type = 0;  //south neighbor

#if DEBUG == 1
  t = (double)getTickCount();
#endif

  create_ngh_node_matrix(cur_node_idx, ngh_type, ngh_node_idx);  


#if DEBUG == 1
  t = ((double)getTickCount() - t)/getTickFrequency();
  //cout << "create_ngh_node_matrix:" << t << " sec" << endl;
#endif

#if DEBUG == 1
  t = (double)getTickCount();
#endif

  create_triplets(coeffs, cur_node_idx, ngh_node_idx, 1.0);

#if DEBUG == 1
  t = ((double)getTickCount() - t)/getTickFrequency();
  //cout << "create_triplets:" << t << " sec" << endl;
#endif

  SpMat Ts(img_size, img_size);

#if DEBUG == 1
  t = (double)getTickCount();
#endif

  Ts.setFromTriplets(coeffs.begin(), coeffs.end());

#if DEBUG == 1
  t = ((double)getTickCount() - t)/getTickFrequency();
  //cout << "setFromTriplets:" << t << " sec" << endl;
  //cout << "Ts:" << endl << Ts << endl;
#endif

  //get image diff for south neighbor
#if DEBUG == 1
  t = (double)getTickCount();
#endif

  create_diag_triplets(coeffs, img_size, lambda_dep*img_diff[0]);  

#if DEBUG == 1
  t = ((double)getTickCount() - t)/getTickFrequency();
  //cout << "create_diag_triplets:" << t << " sec" << endl;
#endif

  SpMat Ws(img_size, img_size);

#if DEBUG == 1
  t = (double)getTickCount();
#endif

  Ws.setFromTriplets(coeffs.begin(), coeffs.end());

#if DEBUG == 1
  t = ((double)getTickCount() - t)/getTickFrequency();
  //cout << "setFromTriplets:" << t << " sec" << endl;
  //  cout << "Ws:" << endl << Ws << endl;
#endif

  //find east neighbor index
  ngh_type = 1;
  create_ngh_node_matrix(cur_node_idx, ngh_type, ngh_node_idx);
  create_triplets(coeffs, cur_node_idx, ngh_node_idx, 1.0);

#if DEBUG == 1
  //  cout << "east:" << endl << ngh_node_idx << endl;
#endif

  SpMat Te(img_size, img_size);
  Te.setFromTriplets(coeffs.begin(), coeffs.end());

#if DEBUG == 1
  //  cout << "Te:" << endl << Te << endl;
#endif

  //get image diff for east neighbor
  create_diag_triplets(coeffs, img_size, lambda_dep*img_diff[1]);

  SpMat We(img_size, img_size);
  We.setFromTriplets(coeffs.begin(), coeffs.end());

#if DEBUG == 1
  //  cout << "We:" << endl << We << endl;
#endif

  //create sparse identity matrix
#if DEBUG == 1
  t = (double)getTickCount();
#endif

  create_diag_triplets(coeffs, img_size, 1.0);

#if DEBUG == 1
  t = ((double)getTickCount() - t)/getTickFrequency();
  //cout << "create identity matrix:" << t << " sec" << endl;
#endif

  SpMat Idt(img_size, img_size);
  
#if DEBUG == 1
  t = (double)getTickCount();
#endif

  Idt.setFromTriplets(coeffs.begin(), coeffs.end());

#if DEBUG == 1
  t = ((double)getTickCount() - t)/getTickFrequency();
  //cout << "idt setFromTriplets:" << t << " sec" << endl;
#endif

  //create the projection matrix
  int proj_length = 0;

#if DEBUG == 1
  t = (double)getTickCount();
#endif

  create_mask_triplets(coeffs, proj_length, cur_node_idx, indicator);

#if DEBUG == 1
  t = ((double)getTickCount() - t)/getTickFrequency();
  //cout << "proj create triplets:" << t << " sec" << endl;
#endif

  SpMat T(proj_length, img_size);
  
  T.setFromTriplets(coeffs.begin(), coeffs.end());

#if DEBUG == 1
  //  cout << "T:" << endl << T << endl;
#endif

  //vectorize the depth image
  Eigen::VectorXd d(proj_length);

#if DEBUG == 1
  t = (double)getTickCount();
#endif

  vectorize_matrix(d, depth, indicator);

#if DEBUG == 1
  t = ((double)getTickCount() - t)/getTickFrequency();
  //cout << "vectorize_matrix:" << t << " sec" << endl;
  //  cout << "d:" << endl << d << endl;
#endif

  //create the problem matrix
  SpMat A(img_size, img_size);
  
#if DEBUG == 1
  t = (double)getTickCount();
#endif

  A = T.transpose() * T + (Idt - Te).transpose() * We * (Idt - Te) + (Idt - Ts).transpose() * Ws * (Idt - Ts);

#if DEBUG == 1
  t = ((double)getTickCount() - t)/getTickFrequency();
  //cout << "compute A:" << t << " sec" << endl;

  t = (double)getTickCount();
#endif

  d = T.transpose() * d;

#if DEBUG == 1
  t = ((double)getTickCount() - t)/getTickFrequency();
  //cout << "compute d:" << t << " sec" << endl;
  //cout << "A:" << endl << A << endl;
  //cout << "d:" << endl << d << endl;
#endif

  /* save A and d
  {
    fstream fs1, fs2;
    fs1.open("A.txt", fstream::out);
    fs2.open("d.txt", fstream::out);

    for(int k = 0; k < A.outerSize(); ++k){
      for(Eigen::SparseMatrix<double>::InnerIterator it(A, k); it; ++it){
	fs1 << it.value() << ' ' << it.row() << ' ' << it.col() << ' ' << it.index() << endl;
      }
    } 

    for(int k = 0; k < d.size(); ++k){
      fs2 << d(k) << endl;
    }

    fs1.close();
    fs2.close();
  }
  //*/

  //solve the linear equation Ay = d by conjugate gradient
  Eigen::VectorXd y(img_size);

  int init_type = 5;  //1:average smooth. 3:same as depth. 5: all zeros
  int ker_size  = 3;  //smoothing kernel size (square matrix) only used when init_type=1
  bool check_error = false;  //check the estimation error at each iteration if set true

#if DEBUG == 1
  t = (double)getTickCount();
#endif

  Eigen::ConjugateGradient<SpMat> cg(A);
  //cg.compute(A);
  //cg.setTolerance(1e-10);
  cg.setMaxIterations(m_num_itr);

#if DEBUG == 1
  t = ((double)getTickCount() - t)/getTickFrequency();
  //cout << "cg compute A:" << t << " sec" << endl;
#endif

  initialize_depth(y, depth, indicator, init_type, ker_size);

  recon_depth.create(depth.size(), depth.type());

#if DEBUG == 1
  {
    t = (double)getTickCount();

    copy_result(recon_depth, y);
   
    t = ((double)getTickCount() - t)/getTickFrequency();
    //cout << "copy result:" << t << " sec" << endl;

    //imagesc_bw("init", recon_depth);
    //save_file_name = "../result/init" + SAVE_IMG_EXT;
    //imwrite(save_file_name, recon_depth);

  }
#endif

  if(check_error){
    //check the error at each iteration
    //note that this is less efficient because the updating function does not consider two consecutive y, since the number of iteration is set to one
    fstream fs;
    fs.open("cg_error.txt", fstream::out);
  
    cg.setMaxIterations(1);
    for(int itr = 0; itr < m_num_itr; ++itr){
      y = cg.solveWithGuess(d, y);
      fs << cg.error() << endl;
      cout << "itr " << itr << ": " << cg.error() << endl;

      //save results at every 50 iterations
      if( itr == 0 || (itr+1) % 50 == 0){
	copy_result(recon_depth, y);

	stringstream ss;
	ss << "../result/itr" << itr << "_estimate_depth" << SAVE_IMG_EXT;
	string file_name;	
	ss >> file_name;

	imwrite(file_name, recon_depth);

      }
    }
  
    fs.close();
  }
  else{
#if DEBUG == 1    
    t = (double)getTickCount();
#endif

    y = cg.solveWithGuess(d, y);

#if DEBUG == 1
    t = ((double)getTickCount() - t)/getTickFrequency();
    cout << "cg solve d:" << t << " sec" << endl;
#endif

    cout << "#iterations:" << cg.iterations() << endl;
    cout << "estimated error:" << cg.error() << endl;
    //cout << "result:" << endl << y << endl;
  }


#if DEBUG == 1
  t = (double)getTickCount();
#endif

  //copy the computed result to output
  copy_result(recon_depth, y);

#if DEBUG == 1
  t = ((double)getTickCount() - t)/getTickFrequency();
  //cout << "copy result:" << t << " sec" << endl;
#endif

  //save results
#if DEBUG == 1
  //imagesc_bw("recon_depth_l2", recon_depth);
  //save_file_name = "../result/recon_depth_l2" + SAVE_IMG_EXT;
  //imwrite(save_file_name, recon_depth);
#endif


}


void DepthInterpolater::initialize_depth(Eigen::VectorXd &y, const Mat& depth, const Mat& indicator, const int type, const int ker_size)
{
  CV_Assert(depth.type() == CV_32FC1);
  CV_Assert(indicator.type() == CV_8UC1);
  CV_Assert(depth.size() == indicator.size());

  //cout << "initializing reconstructed depth value by ";
  if(type == 1){
    //average smoothing
    cout << "average smoothing ..." << endl;

    Mat ker = Mat::ones(ker_size, ker_size, CV_32FC1);

    //cout << ker << endl;

    Mat tmp(depth.size(), depth.type());

    filter2D(depth, tmp, -1, ker);

    float* tmp_row;
    int count = 0;
    for(int row = 0; row < depth.rows; ++row){
      tmp_row = tmp.ptr<float>(row);
      for(int col = 0; col < depth.cols; ++col){
        y(count) = tmp_row[col];
        count++;
      }
    }
  }
  else if(type == 2){
    //copy available depth values to y
    //use naive guess (find closest row-neighbor, and copy the value)
    //cout << "coping closest row value ..." << endl;

    const float* dep_row;
    const int* ind_row;
    int count = 0;
    float prv_val = 0;

    for(int row = 0; row < depth.rows; ++row){
      dep_row = depth.ptr<float>(row);
      ind_row = indicator.ptr<int>(row);

      for(int col = 0; col < depth.cols; ++col){
        if(ind_row[col] == 1)
	  y(count) = dep_row[col];
        else
	  y(count) = prv_val;

        prv_val = y(count);
        count++;
      }
    }
  }
  else if(type == 3){
    //set y to the available depth measurement
    //cout << "coping available depth ..." << endl;

    const float* dep_row;
    int count = 0;

    for(int row = 0; row < depth.rows; ++row){
      dep_row = depth.ptr<float>(row);

      for(int col = 0; col < depth.cols; ++col){
	y(count) = dep_row[col];
        count++;
      }
    }

  }
  else if(type == 4){
    //random initialization
    //cout << "randomg values ..." << endl;

    y = Eigen::VectorXd::Random(depth.rows*depth.cols);
  }
  else{
    //cout << "all zeros ..." << endl;
    y.setZero(depth.rows*depth.cols);
  } 
}

/*    //IF use Laplacian MRF
void DepthInterpolater::copy_result(Mat& recon, const IloNumArray y)
{
  int y_size = y.getSize();

  CV_Assert(recon.type() == CV_32FC1);
  CV_Assert(recon.rows*recon.cols == y_size);

  float* recon_row;
  int count = 0;

  for(int row = 0; row < recon.rows; ++row) {
    recon_row = recon.ptr<float>(row);

    for(int col = 0; col < recon.cols; ++col) {
      recon_row[col] = y[count];
      count++;
    }
  }


}
//*/

void DepthInterpolater::copy_result(Mat& recon, const Eigen::VectorXd& y)
{
  CV_Assert(recon.type() == CV_32FC1);
  CV_Assert(recon.rows*recon.cols == y.size());

  float* recon_row;
  int count = 0;

  for(int row = 0; row < recon.rows; ++row) {
    recon_row = recon.ptr<float>(row);

    for(int col = 0; col < recon.cols; ++col) {
      recon_row[col] = y(count);
      count++;
    }
  }

}

void DepthInterpolater::vectorize_matrix(Eigen::VectorXd& v, const Mat& mat, const Mat& ind)
{
  CV_Assert(mat.size() == ind.size());
  CV_Assert(mat.type() == CV_32FC1);
  CV_Assert(ind.type() == CV_8UC1);

  int pos = 0;

  const float* mat_row;
  const uchar* ind_row;

  for(int row = 0; row < mat.rows; ++row){
    mat_row = mat.ptr<float>(row);
    ind_row = ind.ptr<uchar>(row);
    
    for(int col = 0; col < mat.cols; ++col){
      if(ind_row[col] == 1){
	v(pos) = mat_row[col];
	pos++;
      }
    }

  }

}

void DepthInterpolater::create_diag_triplets(vector<Trip>& coeffs, const int dim, const Mat& val)
{
  CV_Assert(val.rows * val.cols == dim);
  CV_Assert(val.type() == CV_32FC1);

  coeffs.clear();

  int i = 0;
  const float* val_row; 
  for(int row = 0; row < val.rows; ++row){
    val_row = val.ptr<float>(row);

    for(int col = 0; col < val.cols; ++col){
      coeffs.push_back( Trip(i, i, val_row[col]) );
      i++;
    }
  }
}

void DepthInterpolater::create_diag_triplets(vector<Trip>& coeffs, const int dim, const double val)
{
  coeffs.clear();

  for(int i = 0; i < dim; ++i){
    coeffs.push_back( Trip(i, i, val) );
  }

}

void DepthInterpolater::create_triplets(vector<Trip>& coeffs, const Mat& cur_node_idx, const Mat& ngh_node_idx, const double val)
{
  CV_Assert(cur_node_idx.size() == ngh_node_idx.size());
  CV_Assert(cur_node_idx.type() == CV_32SC1 && ngh_node_idx.type() == CV_32SC1);
 
  coeffs.clear();

  const int* cur_row;
  const int* ngh_row;

  for(int row = 0; row < cur_node_idx.rows; ++row){
    cur_row = cur_node_idx.ptr<int>(row);
    ngh_row = ngh_node_idx.ptr<int>(row);

    for(int col = 0; col < cur_node_idx.cols; ++col){
      coeffs.push_back( Trip(static_cast<double>(cur_row[col]), 
                             static_cast<double>(ngh_row[col]),
                             val) ); 
    }
  }
}


 void DepthInterpolater::create_mask_triplets(vector<Trip>& coeffs, int& count, const Mat& cur_node_idx, const Mat& mask)
{
  CV_Assert(cur_node_idx.size() == mask.size());
  CV_Assert(cur_node_idx.type() == CV_32SC1);
  CV_Assert(mask.type() == CV_8UC1);

  coeffs.clear();
  
  const int* cur_row;
  const uchar* mask_row;
  count = 0;

  for(int row = 0; row < cur_node_idx.rows; ++row){
    cur_row = cur_node_idx.ptr<int>(row);
    mask_row = mask.ptr<uchar>(row);

    for(int col = 0; col < cur_node_idx.cols; ++col){
      if(mask_row[col] == 1){
	coeffs.push_back( Trip(count, cur_row[col], 1) );
	count++;
      }

    }
  }

}

/*
void DepthInterpolater::create_triplets(vector<Trip>& coeffs, const Mat& cur_node_idx, const Mat& ngh_node_idx, const Mat& img_diff)
{
  CV_Assert(cur_node_idx.size() == ngh_node_idx.size());
  CV_Assert(cur_node_idx.size() == img_diff.size());
  CV_Assert(cur_node_idx.type() == CV_32SC1);
  CV_Assert(ngh_node_idx.type() == CV_32SC1);
  CV_Assert(img_diff.type()     == CV_32FC1);

  coeffs.clear();

  const int* cur_row;
  const int* ngh_row;
  const float* img_row;

  for(int row = 0; row < img_diff.rows; ++row){
    cur_row = cur_node_idx.ptr<int>(row);
    ngh_row = ngh_node_idx.ptr<int>(row);
    img_row = img_diff.ptr<float>(row);

    for(int col = 0; col < img_diff.cols; ++col){
      coeffs.push_back( Trip(cur_row[col], ngh_row[col], img_row[col]) );
    }
  }

}
*/

void DepthInterpolater::create_ngh_node_matrix(const Mat& nodes, int ngh_type, Mat& ngh)
{  //ngh_type. 0:south, 1:east

  switch(ngh_type){
  case 0:
    //south
    ngh = nodes + nodes.cols;
    nodes.row(nodes.rows-1).copyTo(ngh.row(nodes.rows-1));
    break;
  case 1:
    //east
    ngh = nodes + 1;
    nodes.col(nodes.cols-1).copyTo(ngh.col(nodes.cols-1));
    break;
  default:
    cerr << "Wrong neighbor type. " << endl;
    cerr << "type = 0: south." << endl;
    cerr << "type = 1: east." << endl;

  }
  

}

void DepthInterpolater::create_node_matrix(Mat& nodes)
{
  CV_Assert(nodes.type() == CV_32SC1);

  int id = 0;

  int* mat_row;
  for(int row = 0; row < nodes.rows; ++row){
    mat_row = nodes.ptr<int>(row);

    for(int col = 0; col < nodes.cols; ++col){
      mat_row[col] = id;
      id++;
    }
  }

}

void DepthInterpolater::compute_unary_term(const Mat& recon_depth, const double lambda_r, Mat& r_ue)
{
  //Assume recon_depth is only within roi
  CV_Assert(recon_depth.type() == CV_32FC1);

  ///* Use GMM
  Mat tmp(m_img_row, m_img_col, CV_32FC1, Scalar(0));

  const Rect roi = gmm_app->get_roi();

  recon_depth.copyTo(tmp(roi));

  gmm_app->compute_unary_term(recon_depth, Mat(), r_ue);
  r_ue = r_ue * lambda_r;
  //*/

  /*for debug
  double mmin, mmax;
  minMaxLoc(r_ue, &mmin, &mmax);
  cout << "r_ue in:" << mmin << "," << mmax << endl;

  //*/



  /*
  r_ue.create(2, recon_depth.rows*recon_depth.cols, CV_32FC1);

  float* ue_bkg_row = r_ue.ptr<float>(0);
  float* ue_frg_row = r_ue.ptr<float>(1);

  const float* recon_depth_row;

  int node_id = 0;
  float energy_bkg = 0;
  float energy_frg = 0;

  for(int row = 0; row < recon_depth.rows; ++row){
    recon_depth_row = recon_depth.ptr<float>(row);

    for(int col = 0; col < recon_depth.cols; ++col){
      energy_frg = lambda_r * abs(recon_depth_row[col] - target_depth);

      ue_bkg_row[node_id] = energy_bkg;
      ue_frg_row[node_id] = energy_frg;

      node_id++;
    }
  } 

  //*/

}
