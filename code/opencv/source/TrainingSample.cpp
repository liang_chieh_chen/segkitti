/* 
 * Modified from codes by Alexander G. Schwing
 *
 * Copyright (C) 2010-2013  Alexander G. Schwing  [http://www.alexander-schwing.de/]
 *
 */


#include "AppearanceModel.h"
#include "Constants.h"
#include "ContourCreater.h"
#include "ContrastSensitivePM.h"
#include "DepthInterpolater.h"
#include "DepthPotential.h"
#include "GmmAppearance.h"
#include "GraphCutWrapper.h"
//#include "StarShapeModel.h"
#include "TrainingSample.h"
#include "Utility.h"

#include <fstream>
#include <iostream>
#include <cmath>
#include <opencv2/core/core.hpp>

using namespace std;
using namespace cv;



int TrainingSample::Load(struct SampleParams* sample) 
{
  ////print out some information
  //cout << "NUM_STAR:" << NUM_STAR << endl;
  ////


  this->sample = sample;

  /* feature weights
     w0 for GMM
     w1 for Ising Prior
     w2 for Contrast Sensitive Potts Model
     w3 for simple depth when observed frg point cloud
     w4 for simple depth when observed bkg point cloud
     w5 for recovered depth
     w6 for star shape model
     w7 for cad contour 
     w8 for stereo depth
     w9 for mechanical turker 1
     w10 for turker 2
     w11 for turker 3
   */

  int numFeatures = 12;

  ////assign gt and other required data
  gt.clear();

  //load ground truth image
  Mat gt_img;

  gt_img = imread(sample->fn, CV_LOAD_IMAGE_GRAYSCALE);

  CV_Assert(gt_img.type() == CV_8UC1);

  uchar* gt_row;
  for(int row = 0; row < gt_img.rows; ++row){
    gt_row = gt_img.ptr<uchar>(row);

    for(int col = 0; col < gt_img.cols; ++col){
      //the read-in gt has value {0, 255}
      gt.push_back((int)gt_row[col]/255);  
    }
  }

  labeling.assign(gt.size(), 0);

  phi_gt.assign(numFeatures, 0.0);
  FeatureVector(gt, phi_gt);


  return numFeatures;
}

int TrainingSample::MarginRescaling(std::vector<double>& w, double time) 
{
  //compute margin rescaling and assign labeling
  Mat a_ue, d_ue, r_ue, st_ue, c_ue;
  Mat p_pe, p_pe2, p_pi;
  Mat s_pe, s_pi;

  Mat mask_roi, soft_mask_roi;

  //use three turkers
  vector<Mat> m_ue(3);

  get_model_energies(a_ue, d_ue, r_ue, st_ue, c_ue, m_ue, p_pe, p_pe2, p_pi, s_pe, s_pi, mask_roi, soft_mask_roi);

  //adjust ue based on loss
  float* a_ue_row[2];
  a_ue_row[0] = a_ue.ptr<float>(0);
  a_ue_row[1] = a_ue.ptr<float>(1);

  CV_Assert(a_ue.cols == (int)gt.size());
  
  //find most violating labeling
  for(int col = 0; col < a_ue.cols; ++col){
    if( gt[col] == 0 ){
      a_ue_row[1][col] -= weighted_loss[0];
    } else if( gt[col] == 1) {
      a_ue_row[0][col] -= weighted_loss[1];
    } else{
      cerr << "Wrong gt in MarginRescaling" << endl;
      exit(-1);
    }
  }
 
  //modify the weight of d_ue
  d_ue.row(0) = d_ue.row(0) * w[3];
  d_ue.row(1) = d_ue.row(1) * w[4];

  //combine the energies
  Mat ue, pe, pi;
  ue = (w[0]*a_ue + d_ue + w[5]*r_ue + w[7]*c_ue + w[8]*st_ue + w[9]*m_ue[0] + w[10]*m_ue[1] + w[11]*m_ue[2]);
  
  if(w[6] == 0){
    //no star shape model
    pe = w[1]*p_pe + w[2]*p_pe2;
    pi = p_pi;
  } else {
    combine_binary_energy(w[1]*p_pe + w[2]*p_pe2, p_pi, w[6]*s_pe, s_pi, pe, pi);
  }
 

  /*
  {
    double mmin, mmax;

    minMaxLoc(a_ue, &mmin, &mmax);
    cout << "a_ue m:" << mmin << "," << mmax << endl;
    minMaxLoc(d_ue, &mmin, &mmax);
    cout << "d_ue m:" << mmin << "," << mmax << endl;     
    minMaxLoc(p_pe, &mmin, &mmax);
    cout << "p_pe m:" << mmin << "," << mmax << endl;
    minMaxLoc(p_pe2, &mmin, &mmax);
    cout << "p_pe2 m:" << mmin << "," << mmax << endl;
    minMaxLoc(ue, &mmin, &mmax);
    cout << "ue m:" << mmin << "," << mmax << endl;
  }
  //*/

  //normalize ue?
  /*
  bool do_normalize_ue = false;

  if(do_normalize_ue) {
    double nsum;

    float* ue_bkg_row = ue.ptr<float>(0);
    float* ue_frg_row = ue.ptr<float>(1);

    for(int col = 0; col < ue.cols; ++col){
      nsum = (ue_bkg_row[col] + ue_frg_row[col]) / 2;

      //cout << "ue:" << ue_bkg_row[col] << ", " << ue_frg_row[col] << ", " << sum << endl;

      ue_bkg_row[col] -= nsum;
      ue_frg_row[col] -= nsum;
      //cout << "ue:" << ue_bkg_row[col] << ", " << ue_frg_row[col] << ", " << endl;
    }

  }
  //*/
 


  //compute the labeling
  Mat x_opt;
  double energy;
  GraphCutWrapper* gc = new GraphCutWrapper(ue.cols, pi);

  //normalize ue, pe?
  //if(DO_NORMALIZE_ENERGY){
  //  gc->compute_map(ue/ue.cols, pe/pi.cols, pi, x_opt, energy);
  //}
  //else{
  gc->compute_map(ue, pe, pi, x_opt, energy);
    //}


  /* for debug
   
     if(energy <= 0){
     cout << "energy < 0 (OVERFLOW?): " << energy << endl;
    
     cout << "w:";
       
     for(size_t i = 0; i < w.size(); ++i){
     cout << w[i] << ';';
     }
     cout << endl;
     double mmin, mmax;

     cout << "fn:" << sample->fn << endl;
     minMaxLoc(a_ue, &mmin, &mmax);
     cout << "a_ue m:" << mmin << "," << mmax << endl;
     minMaxLoc(d_ue, &mmin, &mmax);
     cout << "d_ue m:" << mmin << "," << mmax << endl;     
     minMaxLoc(p_pe, &mmin, &mmax);
     cout << "p_pe m:" << mmin << "," << mmax << endl;
     minMaxLoc(p_pe2, &mmin, &mmax);
     cout << "p_pe2 m:" << mmin << "," << mmax << endl;
   
     }
  //*/


  //get the map result
  int* x_row;

  x_row = x_opt.ptr<int>(0);

  for(int col = 0; col < x_opt.cols; ++col){
    labeling[col] = x_row[col];
  }
   

  delete gc;


  return 0;
}

int TrainingSample::FeatureVector(std::vector<int>& y, std::vector<double>& phi) 
{
  //compute feature vector phi from labeling y
  Mat a_ue, d_ue, r_ue, st_ue, c_ue;
  Mat p_pe, p_pe2, p_pi;
  Mat s_pe, s_pi;

  Mat mask_roi, soft_mask_roi;

  //use three turkers
  vector<Mat> m_ue(3);

  get_model_energies(a_ue, d_ue, r_ue, st_ue, c_ue, m_ue, p_pe, p_pe2, p_pi, s_pe, s_pi, mask_roi, soft_mask_roi);

  /*debug
    double mmin, mmax;

    minMaxLoc(a_ue, &mmin, &mmax);
    cout << "a_ue: " << mmin << "," << mmax << endl;
  
    minMaxLoc(p_pe, &mmin, &mmax);
    cout << "p_pe: " << mmin << "," << mmax << endl;
  
    minMaxLoc(p_pe2, &mmin, &mmax);
    cout << "p_pe2: " << mmin << "," << mmax << endl;
  
  //*/

  //find feature from a_ue
  phi[0] = 0;
  
  //find features for the recovered depth potential
  phi[5] = 0;

  //find features for the contour potential
  phi[7] = 0;

  //find features for the stereo depth potential
  phi[8] = 0;

  //find features for contour potential of three Mechanical Turkers
  phi[9]  = 0;
  phi[10] = 0;
  phi[11] = 0;

  //
  float* a_ue_row[2];
  a_ue_row[0] = a_ue.ptr<float>(0);
  a_ue_row[1] = a_ue.ptr<float>(1);

  float* r_ue_row[2];
  r_ue_row[0] = r_ue.ptr<float>(0);
  r_ue_row[1] = r_ue.ptr<float>(1);

  float* c_ue_row[2];
  c_ue_row[0] = c_ue.ptr<float>(0);
  c_ue_row[1] = c_ue.ptr<float>(1);

  float* st_ue_row[2];
  st_ue_row[0] = st_ue.ptr<float>(0);
  st_ue_row[1] = st_ue.ptr<float>(1);

  //turker 1
  float* m_ue_b1_row[2];
  m_ue_b1_row[0] = m_ue[0].ptr<float>(0);
  m_ue_b1_row[1] = m_ue[0].ptr<float>(1);

  //turker 2
  float* m_ue_b2_row[2];
  m_ue_b2_row[0] = m_ue[1].ptr<float>(0);
  m_ue_b2_row[1] = m_ue[1].ptr<float>(1);

  //turker 3
  float* m_ue_b3_row[2];
  m_ue_b3_row[0] = m_ue[2].ptr<float>(0);
  m_ue_b3_row[1] = m_ue[2].ptr<float>(1);

  for(int col = 0; col < a_ue.cols; ++col){
    if( y[col] == 0 ){
      phi[0] += a_ue_row[0][col];
      phi[5] += r_ue_row[0][col];
      phi[7] += c_ue_row[0][col];

      phi[8] += st_ue_row[0][col];

      phi[9]  += m_ue_b1_row[0][col];
      phi[10] += m_ue_b2_row[0][col];
      phi[11] += m_ue_b3_row[0][col];

    } else if( y[col] == 1 ) {
      phi[0] += a_ue_row[1][col];
      phi[5] += r_ue_row[1][col];
      phi[7] += c_ue_row[1][col];

      phi[8] += st_ue_row[1][col];

      phi[9]  += m_ue_b1_row[1][col];
      phi[10] += m_ue_b2_row[1][col];
      phi[11] += m_ue_b3_row[1][col];

    } else{
      cerr << "y:" << y[col] << endl;
      cerr << "Wrong labeling when computing FeatureVector." << endl;
      exit(-1);
    }
  }
  
  //find features for simple depth potential
  phi[3] = 0;
  phi[4] = 0;

  int node_id = 0;
  uchar* mask_row;
  float* soft_row;
  
  for(int row = 0; row < mask_roi.rows; ++row){
    mask_row = mask_roi.ptr<uchar>(row);
    soft_row = soft_mask_roi.ptr<float>(row);

    for(int col = 0; col < mask_roi.cols; ++col){
      if(mask_row[col] == FOREGROUND_LABEL && y[node_id] == 0){
	if(USE_SOFT_MASK)
	  phi[3] += soft_row[col];
	else
	  phi[3]++;

      } else if(mask_row[col] == STRICT_BACKGROUND_LABEL && y[node_id] == 1) {
	if(USE_SOFT_MASK)
	  phi[4] += (-soft_row[col]);
	else
	  phi[4]++;

      }
      
      node_id++;

    }
  }

  //take use of pi, to find feature from p_pe and p_pe2 based on y
  phi[1] = 0;
  phi[2] = 0; 
  int node_start, node_end;

  int* p_pi_row[2];
  p_pi_row[0] = p_pi.ptr<int>(0);
  p_pi_row[1] = p_pi.ptr<int>(1);

  for(int col = 0; col < p_pi.cols; ++col){
    node_start = p_pi_row[0][col];
    node_end   = p_pi_row[1][col];

    if( y[node_start] != y[node_end] ){
      phi[1] += p_pe.at<float>(col);
      phi[2] += p_pe2.at<float>(col);
    }
  }

  //find feature for star shape model
  phi[6] = 0;
  
  int* s_pi_row[2];
  s_pi_row[0] = s_pi.ptr<int>(0);
  s_pi_row[1] = s_pi.ptr<int>(1);

  for(int col = 0; col < s_pi.cols; ++col){
    node_start = s_pi_row[0][col];
    node_end   = s_pi_row[1][col];

    if( y[node_start] == 0 && y[node_end] == 1) {
      phi[6]++; // +=  s_pe.at<float>(1, col);
    }
    
  }
  //*/

  
  /* //normalize phi?
  //if(DO_NORMALIZE_ENERGY){
  phi[0] /= a_ue.cols;
  phi[3] /= d_ue.cols;
  phi[4] /= d_ue.cols;
  phi[5] /= r_ue.cols;

  phi[1] /= p_pi.cols;
  phi[2] /= p_pi.cols;

  phi[6] /= s_pi.cols;
  //  }
  //*/

  /* divide by 10
  phi[0] /= 10;
  phi[3] /= 10;
  phi[4] /= 10;
  phi[5] /= 10;

  phi[1] /= 10;
  phi[2] /= 10;
  //*/


  /* L2 normalization
     {
     double total=0;
     for(size_t i = 0; i < phi.size(); ++i)
     total += phi[i]*phi[i];

     total = sqrt(total);

     for(size_t i = 0; i < phi.size(); ++i)
     phi[i] = phi[i] / total;
     }
  //*/

  /* L1 normalization
     {
     double total = 0;
     for(size_t i = 0; i < phi.size(); ++i)
     total += phi[i];

     for(size_t i = 0; i < phi.size(); ++i)
     phi[i] = phi[i] / total;
     }  
  //*/


  //change the sign of phi!!
  //phi = -Energy (large feature phi = small energy)
  for(size_t i = 0; i < phi.size(); ++i){
    phi[i] *= -1;
  } 

  /*debug
  cout << "phi:";
  for(size_t i = 0; i < phi.size(); ++i){
    cout << phi[i] << ", "; 
  }
  cout << endl;
  //*/ 

  return 0;
}

double TrainingSample::Loss() 
{
  double loss = 0.0;
  //compute loss of labeling w.r.t. gt
  CV_Assert(labeling.size() == gt.size());

  //weighted Hamming loss
  for(size_t i = 0; i < labeling.size(); ++i){
    if(labeling[i] != gt[i]){
      loss += weighted_loss[ gt[i] ];
    }
  }

  /*
  //if(DO_NORMALIZE_ENERGY){
  //      loss /= labeling.size();
    //}
  //*/


  return loss;
}

int TrainingSample::AddFeatureDiffAndLoss(std::vector<double>& featureDiffAndLoss, std::vector<double>& w) 
{
  int numFeatures = int(featureDiffAndLoss.size())-1;
  std::vector<double> phi_y(numFeatures, 0.0);
  FeatureVector(labeling, phi_y);
  for(int k=0;k<numFeatures;++k) {
    featureDiffAndLoss[k] += phi_gt[k] - phi_y[k];
  }
  featureDiffAndLoss[numFeatures] += Loss();
  return 0;
}

void TrainingSample::setup_weighted_loss(const std::vector<double>& wl){
  weighted_loss.clear();

  for(size_t i = 0; i < wl.size(); ++i)
    weighted_loss.push_back(wl[i]);
}  

void TrainingSample::get_model_energies(Mat& a_ue, Mat& d_ue, Mat& r_ue, Mat& st_ue, Mat& c_ue, vector<Mat>& m_ue, Mat& p_pe, Mat& p_pe2, Mat& p_pi, Mat& s_pe, Mat& s_pi, Mat& mask_roi, Mat& soft_mask_roi)
{
  Mat fimg, mask, soft_mask, inv_mask;
  Mat depth_img, recon_depth, stereo_depth;
  vector<Rect> bboxes;
  float target_depth;

  load_all_data_and_star_energy(sample->fn, fimg, bboxes, mask, soft_mask, inv_mask, depth_img, recon_depth, stereo_depth, target_depth, s_pe, s_pi, NUM_STAR);

  mask_roi = mask(bboxes[0]);
  soft_mask_roi = soft_mask(bboxes[0]);

  //load_all_data(sample->fn, fimg, bboxes, mask, depth_img, recon_depth, target_depth);

  ////setup the apperance model
  AppearanceModel* app_model;
 
  int num_components = 5;    //number of mixture components
  int feat_size = 3;         //only rgb color is used

  if(USE_ORIG_MASK){
    app_model = new GmmAppearance(fimg, mask, bboxes[0], bboxes[1], num_components, feat_size);
  } else{
    app_model = new GmmAppearance(fimg, inv_mask, bboxes[0], bboxes[1], num_components, feat_size);
  }

  ////setup the simple depth potential
  DepthPotential* dep_model;

  dep_model = new DepthPotential();

  ////setup the binary model
  int nc = 8;  //4: 4-connected neighbors. 8: 8-connected neighbor

  ContrastSensitivePM* binary_model = new ContrastSensitivePM(bboxes[0], nc);

  //weight for contrast sensitive potts model and simple depth
  vector<double> all_one_weight(2, 1.0);

  ////setup the recovered depth potential
  num_components = 2;
  feat_size = 1;

  DepthInterpolater* dep_int;

  if(USE_ORIG_MASK){
    dep_int = new DepthInterpolater(depth_img, mask, bboxes[0], num_components, feat_size);
  } else{
    dep_int = new DepthInterpolater(depth_img, inv_mask, bboxes[0], num_components, feat_size);
  }

  dep_int->setup_target_depth(target_depth);

  double lambda_recover = 1;

  ////setup the stereo depth potential
  num_components = 2;
  feat_size = 1;
  
  DepthInterpolater* dep_int_stereo;

  if(USE_ORIG_MASK){
    dep_int_stereo = new DepthInterpolater(stereo_depth, mask, bboxes[0], num_components, feat_size);
  } else{
    dep_int_stereo = new DepthInterpolater(stereo_depth, inv_mask, bboxes[0], num_components, feat_size);
  }


  ////setup the contour potential
  string filename;
  string search_pattern = GT_FOLDER;
  string replace_pattern;
  bool use_dt;

  if(USE_AVG_SHAPE){
    use_dt = false;
    replace_pattern = AVG_SHAPE_FOLDER;
  } else{
    use_dt = true;
    replace_pattern = CAD_SHAPE_FOLDER;
  }

  filename = replace_string(sample->fn, search_pattern, replace_pattern);
  
  ContourCreater* cc = new ContourCreater(filename, use_dt);
  

  //only GMM and CSPM take whole image and whole mask
  app_model->compute_unary_term(fimg, mask, a_ue);  //mask is not used...


  if(USE_SOFT_MASK){
    dep_model->compute_unary_term(mask(bboxes[0]), soft_mask(bboxes[0]), d_ue);
  }
  else{
    dep_model->compute_unary_term(mask(bboxes[0]), all_one_weight, d_ue);
  }

  dep_int->compute_unary_term(recon_depth, lambda_recover, r_ue);
  cc->compute_unary_term(c_ue);

  dep_int_stereo->compute_unary_term(stereo_depth, lambda_recover, st_ue);

  binary_model->compute_pe(fimg, mask, all_one_weight, p_pe, p_pe2, p_pi);

  ////compute the contour potential for the turkers
  //int num_turker = 3;
  ContourCreater* cc_m[3];

  //for turker 1
  search_pattern = GT_FOLDER;
  replace_pattern = MT_SHAPE_FOLDER_B1;
  filename = replace_string(sample->fn, search_pattern, replace_pattern);
  cc_m[0] = new ContourCreater(filename);

  //for turker 2
  search_pattern = GT_FOLDER;
  replace_pattern = MT_SHAPE_FOLDER_B2;
  filename = replace_string(sample->fn, search_pattern, replace_pattern);
  cc_m[1] = new ContourCreater(filename);

  //for turker 3
  search_pattern = GT_FOLDER;
  replace_pattern = MT_SHAPE_FOLDER_B3;
  filename = replace_string(sample->fn, search_pattern, replace_pattern);
  cc_m[2] = new ContourCreater(filename);
  
  for(int i = 0; i < 3; ++i){
    if(USE_TURKER){
      cc_m[i]->compute_unary_term(m_ue[i]);
    } else{
      m_ue[i].create(a_ue.size(), a_ue.type());
      m_ue[i].setTo(Scalar(0));
    }
  }


  /* for debug
  double mmin, mmax;

  cout << "fn:" << sample->fn << endl;
  minMaxLoc(a_ue, &mmin, &mmax);
  cout << "a_ue m:" << mmin << "," << mmax << endl;
  minMaxLoc(d_ue, &mmin, &mmax);
  cout << "d_ue m:" << mmin << "," << mmax << endl;
  minMaxLoc(r_ue, &mmin, &mmax);
  cout << "r_ue m:" << mmin << "," << mmax << endl;
  minMaxLoc(p_pe, &mmin, &mmax);
  cout << "p_pe m:" << mmin << "," << mmax << endl;
  minMaxLoc(p_pe2, &mmin, &mmax);
  cout << "p_pe2 m:" << mmin << "," << mmax << endl;

  imshow("img",fimg);
  imshow("mask",mask_roi);

  visualize_ue(a_ue, bboxes[0]);
  waitKey();

  //*/

  delete app_model;
  delete dep_model;
  delete binary_model;
  delete dep_int;
  delete cc;
  delete dep_int_stereo;

  for(int i = 0; i < 3; ++i){
    delete cc_m[i];
  }

}


