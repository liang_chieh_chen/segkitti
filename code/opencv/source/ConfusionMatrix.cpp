#include "ConfusionMatrix.h"

#include <algorithm>
#include <cassert>
#include <iomanip>

using namespace std;

ConfusionMatrix::ConfusionMatrix(const int m)
{
  _matrix.resize(m);

  for(int i = 0; i < m; i++){
    _matrix[i].resize(m, 0);
  }

  ROW_BEGIN = "bkg, frg: |";
  ROW_END = " |";
  COL_SEP = " |";
}

ConfusionMatrix::~ConfusionMatrix()
{}

int ConfusionMatrix::numRows() const
{ 
  return (int) _matrix.size();
}

int ConfusionMatrix::numCols() const
{
  if(_matrix.empty())
    return 0;
  return (int) _matrix[0].size();

}

void ConfusionMatrix::clear()
{
  for(size_t i = 0; i < _matrix.size(); ++i){
    fill(_matrix[i].begin(), _matrix[i].end(), 0);
  }
}

void ConfusionMatrix::accumulate(const int actual, const int predicted)
{
  if( (actual < 0) || (predicted < 0) )
    return;

  _matrix[actual][predicted] += 1;
}

void ConfusionMatrix::accumulate(const ConfusionMatrix& confusion)
{
  assert( confusion._matrix.size() == _matrix.size() );
  
  if(_matrix.empty()) return;

  assert( confusion._matrix[0].size() == _matrix[0].size() );

  for(size_t row = 0; row < _matrix.size(); ++row){
    for(size_t col = 0; col < _matrix[row].size(); ++col){
      _matrix[row][col] += confusion._matrix[row][col];
    }
  }

}

void ConfusionMatrix::printCounts(ostream &os, const char *header) const
{
    if (header == NULL) {
        os << "--- confusion matrix: (actual, predicted) ---" << endl;
    } else {
        os << header << endl;
    }
    for (int i = 0; i < (int)_matrix.size(); i++) {
        os << ROW_BEGIN;
	for (int j = 0; j < (int)_matrix[i].size(); j++) {
            if (j > 0) os << COL_SEP;
	    os << _matrix[i][j];
	}
	os << ROW_END << "\n";
    }
    os << "\n";
}

void ConfusionMatrix::printRowNormalized(ostream &os, const char *header) const
{
    if (header == NULL) {
        os << "--- confusion matrix: (actual, predicted) ---" << endl;
    } else {
        os << header << endl;
    }
    for (int i = 0; i < (int)_matrix.size(); i++) {
	double total = rowSum(i);
        os << ROW_BEGIN;
	for (int j = 0; j < (int)_matrix[i].size(); j++) {
            if (j > 0) os << COL_SEP;
	    os << fixed << setprecision(3) << setw(4)
		 << ((double)_matrix[i][j] / total);
	}
	os << ROW_END << "\n";
    }
    os << "\n";
}

void ConfusionMatrix::printColNormalized(ostream &os, const char *header) const
{
    vector<double> totals;
    for (int i = 0; i < (int)_matrix[0].size(); i++) {
	totals.push_back(colSum(i));
    }

    if (header == NULL) {
        os << "--- confusion matrix: (actual, predicted) ---" << endl;
    } else {
        os << header << endl;
    }
    for (int i = 0; i < (int)_matrix.size(); i++) {
        os << ROW_BEGIN;
	for (int j = 0; j < (int)_matrix[i].size(); j++) {
            if (j > 0) os << COL_SEP;
	    os << fixed << setprecision(3) << setw(4)
		 << ((double)_matrix[i][j] / totals[j]);
	}
	os << ROW_END << "\n";
    }
    os << "\n";
}

void ConfusionMatrix::printNormalized(ostream &os, const char *header) const
{
    double total = totalSum();

    if (header == NULL) {
        os << "--- confusion matrix: (actual, predicted) ---" << endl;
    } else {
        os << header << endl;
    }
    for (int i = 0; i < (int)_matrix.size(); i++) {
        os << ROW_BEGIN;
	for (int j = 0; j < (int)_matrix[i].size(); j++) {
            if (j > 0) os << COL_SEP;
	    os << fixed << setprecision(3) << setw(4)
		 << ((double)_matrix[i][j] / total);
	}
	os << ROW_END << "\n";
    }
    os << "\n";
}

void ConfusionMatrix::printPrecisionRecall(ostream &os, const char *header) const
{
    if (header == NULL) {
        os << "--- class-specific recall/precision ---" << endl;
    } else {
        os << header << endl;
    }

    // recall
    os << ROW_BEGIN;
    for (unsigned i = 0; i < _matrix.size(); i++) {
        if (i > 0) os << COL_SEP;
        double r = (_matrix[i].size() > i) ? (double)_matrix[i][i] / (double)rowSum(i) : 0.0;
        os << fixed << setprecision(3) << setw(4) << r;
    }
    os << ROW_END << "\n";

    // precision
    os << ROW_BEGIN;
    for (unsigned i = 0; i < _matrix.size(); i++) {
        if (i > 0) os << COL_SEP;
        double p = (_matrix[i].size() > i) ? (double)_matrix[i][i] / (double)colSum(i) : 1.0;
        os << fixed << setprecision(3) << setw(4) << p;
    }
    os << ROW_END << "\n";
    os << "\n";
}

void ConfusionMatrix::printF1Score(ostream &os, const char *header) const
{
    if (header == NULL) {
        os << "--- class-specific F1 score ---" << endl;
    } else {
        os << header << endl;
    }

    os << ROW_BEGIN;
    for (unsigned i = 0; i < _matrix.size(); i++) {
        if (i > 0) os << COL_SEP;
        // recall
        double r = (_matrix[i].size() > i) ? (double)_matrix[i][i] / (double)rowSum(i) : 0.0;
        // precision
        double p = (_matrix[i].size() > i) ? (double)_matrix[i][i] / (double)colSum(i) : 1.0;
        os << fixed << setprecision(3) << setw(4) << ((2.0 * p * r) / (p + r));
    }
    os << ROW_END << "\n";
    os << "\n";
}


void ConfusionMatrix::printJaccard(ostream &os, const char *header) const
{
    if (header == NULL) {
        os << "--- class-specific Jaccard coefficient ---" << endl;
    } else {
        os << header << endl;
    }

    os << ROW_BEGIN;
    for (unsigned i = 0; i < _matrix.size(); i++) {
        if (i > 0) os << COL_SEP;
        double p = (_matrix[i].size() > i) ? (double)_matrix[i][i] /
            (double)(rowSum(i) + colSum(i) - _matrix[i][i]) : 0.0;
        os << fixed << setprecision(3) << setw(4) << p;
    }
    os << ROW_END << "\n";
    os << "\n";
}

double ConfusionMatrix::rowSum(int n) const
{
    double v = 0.0;
    for (int i = 0; i < (int)_matrix[n].size(); i++) {
	v += (double)_matrix[n][i];
    }
    return v;
}

double ConfusionMatrix::colSum(int m) const
{
    double v = 0;
    for (int i = 0; i < (int)_matrix.size(); i++) {
	v += (double)_matrix[i][m];
    }
    return v;
}

double ConfusionMatrix::diagSum() const
{
    double v = 0;
    for (int i = 0; i < (int)_matrix.size(); i++) {
	if (i >= (int)_matrix[i].size())
	    break;
	v += (double)_matrix[i][i];
    }
    return v;
}

double ConfusionMatrix::totalSum() const
{
    double v = 0;
    for (int i = 0; i < (int)_matrix.size(); i++) {
	for (int j = 0; j < (int)_matrix[i].size(); j++) {
	    v += (double)_matrix[i][j];
	}
    }
    return v;
}

double ConfusionMatrix::accuracy() const
{
    return (diagSum() / totalSum());
}

double ConfusionMatrix::avgPrecision() const
{
    double totalPrecision = 0.0;
    for (unsigned i = 0; i < _matrix.size(); i++) {
        totalPrecision += (_matrix[i].size() > i) ?
            (double)_matrix[i][i] / (double)colSum(i) : 1.0;
    }

    return totalPrecision /= (double)_matrix.size();
}

double ConfusionMatrix::avgRecall() const
{
    double totalRecall = 0.0;
    int numClasses = 0;
    for (unsigned i = 0; i < _matrix.size(); i++) {
        if (_matrix[i].size() > i) {
            const double classSize = (double)rowSum(i);
            if (classSize > 0.0) {
                totalRecall += (double)_matrix[i][i] / classSize;
                numClasses += 1;
            }
        }
    }

    if (numClasses != (int)_matrix.size()) {
      cerr << "not all classes represented in avgRecall()";
    }

    return totalRecall / (double)numClasses;
}

double ConfusionMatrix::avgJaccard() const
{
    double totalJaccard = 0.0;
    for (unsigned i = 0; i < _matrix.size(); i++) {
        if (_matrix[i].size() <= i) continue;
        const double intersectionSize = (double)_matrix[i][i];
        const double unionSize = (double)(rowSum(i) + colSum(i) - _matrix[i][i]);
        if (intersectionSize == unionSize) // avoid divide by zero
            totalJaccard += 1.0;
        else totalJaccard += intersectionSize / unionSize;
    }

    return totalJaccard / (double)_matrix.size();
}

double ConfusionMatrix::precision(int n) const
{
    assert(_matrix.size() > (unsigned)n);
    return (_matrix[n].size() > (unsigned)n) ?
        (double)_matrix[n][n] / (double)colSum(n) : 1.0;
}

double ConfusionMatrix::recall(int n) const
{
    assert(_matrix.size() > (unsigned)n);
    return (_matrix[n].size() > (unsigned)n) ?
        (double)_matrix[n][n] / (double)rowSum(n) : 0.0;
}

double ConfusionMatrix::jaccard(int n) const
{
    assert((_matrix.size() > (unsigned)n) && (_matrix[n].size() > (unsigned)n));
    const double intersectionSize = (double)_matrix[n][n];
    const double unionSize = (double)(rowSum(n) + colSum(n) - _matrix[n][n]);
    return (intersectionSize == unionSize) ? 1.0 :
        intersectionSize / unionSize;
}

const unsigned& ConfusionMatrix::operator()(int i, int j) const
{
    return _matrix[i][j];
}

unsigned& ConfusionMatrix::operator()(int i, int j)
{
    return _matrix[i][j];
}
