/*
 *
 * The Gaussian Mixture Models are modified from the source codes of OPENCV in /imgproc/src/grabcut.cpp
 *
 *
 * Liang-Chieh@UCLA, May 2013
 *
 */

#include "GmmAppearance.h"
#include "GmmModel.h"
#include "Constants.h"

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <sstream>
#include <fstream>
#include <iostream>

using namespace cv;
using namespace std;


GmmAppearance::GmmAppearance(Mat& img, Mat& mask, Rect& roi, Rect& wb_roi, int _num_comp, int _feat_size)
  :AppearanceModel(mask, roi, wb_roi),
   num_components(_num_comp),
   feat_size(_feat_size)
{
  for(int i = 0; i < 2; i++){
    //background model and foreground model
    gmm[i] = new GmmModel(num_components, feat_size);
  }

  //initialize the GMM models by KMeans

  Mat fimg;
  //convert to the image to doulbe
  if( img.type() == CV_8UC3 || img.type() == CV_8UC1 ){
    img.convertTo(fimg, CV_32F, 1.0/255);
  }
  else{
    fimg = img;
  }

  //const Mat map_result = get_map_result();

  const int kMeansItCount = 10;
  const int kMeansType = KMEANS_PP_CENTERS;

  Mat bkgLabels, frgLabels;

  collect_samples(fimg, mask, bkgSamples, frgSamples);

  int bkg_num_components; 
  int frg_num_components;

  bkg_num_components = (bkgSamples.rows < num_components)? bkgSamples.rows : num_components;

  frg_num_components = (frgSamples.rows < num_components)? frgSamples.rows : num_components;
  
  kmeans(bkgSamples, bkg_num_components, bkgLabels,
         TermCriteria(CV_TERMCRIT_ITER, kMeansItCount, 0.000001), 1, kMeansType);
  kmeans(frgSamples, frg_num_components, frgLabels,
         TermCriteria(CV_TERMCRIT_ITER, kMeansItCount, 0.000001), 1, kMeansType);

  gmm[0]->reset_ss();
  gmm[1]->reset_ss();

  for(int j = 0; j < bkgSamples.rows; j++){
    gmm[0]->accumulate_ss(bkgLabels.at<int>(j,0), bkgSamples.ptr<float>(j) );
  }

  for(int j = 0; j < frgSamples.rows; j++){
    gmm[1]->accumulate_ss(frgLabels.at<int>(j,0), frgSamples.ptr<float>(j));
  }

  gmm[0]->train();
  gmm[1]->train();


  /* for debug
  gmm[0]->print_out_parameters();
  gmm[1]->print_out_parameters();

  //*/
}

GmmAppearance::~GmmAppearance()
{
  for(int i = 0; i < 2; i++)
    delete gmm[i];
}

void GmmAppearance::collect_samples(Mat& img, Mat& mask, Mat& bkgSamples, Mat& frgSamples)
{
  //mask provides the seeds for first iteration
  //map result provides the seeds for the following iterations

  Mat fimg;
  //convert to the image to doulbe
  if( img.type() == CV_8UC3 || img.type() == CV_8UC1 ){
    img.convertTo(fimg, CV_32F, 1.0/255);
  }
  else{
    fimg = img;
  }

  int nChannels = fimg.channels();

  //const Mat map_result = get_map_result();

  bkgSamples.create(mask.rows*mask.cols, feat_size, CV_32FC1);
  frgSamples.create(mask.rows*mask.cols, feat_size, CV_32FC1);
  

  int bkgCounts = 0, frgCounts = 0;

  const Rect roi_wb = get_roi_wb();

  for(int j = roi_wb.y; j < roi_wb.y + roi_wb.height; ++j){
    const float* fimg_row = fimg.ptr<float>(j);
    const uchar* mask_row = mask.ptr<uchar>(j);
    //const uchar* map_row  = map_result.ptr<uchar>(j);

    for(int i = roi_wb.x; i < roi_wb.x + roi_wb.width; ++i){
      if( mask_row[i] == STRICT_BACKGROUND_LABEL || mask_row[i] == WEAK_BACKGROUND_LABEL)
      //if(mask_row[i] == WEAK_BACKGROUND_LABEL)
      {
        //if pxl belongs to background
        float* bkg_sample_row = bkgSamples.ptr<float>(bkgCounts);

        for(int c = 0; c < nChannels; ++c){
          bkg_sample_row[c] = fimg_row[i*nChannels + c];
	  
	}

        bkgCounts++;

      }
      else if(mask_row[i] == FOREGROUND_LABEL)  //only foreground point clouds as foreground seeds
      //else if(mask_row[i] == FOREGROUND_LABEL || mask_row[i] == UNDEFINED_LABEL || mask_row[i] == STRICT_BACKGROUND_LABEL) //set bounding box as foreground seeds
      {
	/*//further reduce the bounding box size for foreground seeds
	if( (j<roi_wb.y+roi_wb.height/4.0) || (j>roi_wb.y+3.0*roi_wb.height/4.0) || (i<roi_wb.x+roi_wb.width/4.0) || (i>roi_wb.x+3.0*roi_wb.width/4.0) ){
	  continue;
	}
	//*/

        //if pxl belongs to foreground
        float* frg_sample_row = frgSamples.ptr<float>(frgCounts);

        for(int c = 0; c < nChannels; ++c){
          frg_sample_row[c] = fimg_row[i*nChannels + c];
	  
	
        }


        frgCounts++;

      } //end if

    } //end col

  } //end row

  /*
#if DEBUG == 1
  cout << "initializing, bkg count, frg count:" << bkgCounts << "," << frgCounts << endl;
#endif
  */

  bkgSamples = bkgSamples.rowRange(0, bkgCounts);
  frgSamples = frgSamples.rowRange(0, frgCounts);

  if(bkgCounts == 0){
    //randomly uniformly generate the same number of samples as frg
    bkgSamples = Mat::zeros(frgSamples.size(), frgSamples.type());
    randu(bkgSamples, 0, 1);
    bkgCounts = bkgSamples.rows;

  }

  if(frgCounts == 0){
    //randomly uniformly generate the same number of samples as frg
    frgSamples = Mat::zeros(bkgSamples.size(), bkgSamples.type());
    randu(frgSamples, 0, 1);
    frgCounts = frgSamples.rows;
  }

    
  CV_Assert(bkgCounts!=0 && frgCounts!=0);


}

void GmmAppearance::update_model_parameters(Mat& img, Mat& mask)
{
  //mask provides the seeds for first iteration
  //map result provides the seeds for the following iterations

  Mat fimg;
  //convert to the image to doulbe
  if( img.type() == CV_8UC3 || img.type() == CV_8UC1 ){
    img.convertTo(fimg, CV_32F, 1.0/255);
  }
  else{
    fimg = img;
  }

  int nChannels = fimg.channels();

  //reset the sufficient statistics
  gmm[0]->reset_ss();
  gmm[1]->reset_ss();

  const Mat map_result = get_map_result();

  const Rect roi_wb = get_roi_wb();

#if DEBUG == 1
  int frgCounts = 0, bkgCounts = 0;
#endif

  //accumulate the ss only within the weak background regions
  for(int j = roi_wb.y; j < roi_wb.y + roi_wb.height; ++j){
    const float* fimg_row = fimg.ptr<float>(j);
    //const uchar* mask_row = mask.ptr<uchar>(j);
    const uchar* map_row  = map_result.ptr<uchar>(j);

    for(int i = roi_wb.x; i < roi_wb.x + roi_wb.width; ++i){
      if(map_row[i] == 0)
      {
        int comp_id = gmm[0]->find_most_likely_component( fimg_row + i*nChannels );
        gmm[0]->accumulate_ss(comp_id, fimg_row+i*nChannels);

#if DEBUG == 1
	bkgCounts++;
#endif

      }
      else if(map_row[i] == 1)
      {
        //if pxl belongs to foreground
        int comp_id = gmm[1]->find_most_likely_component( fimg_row + i*nChannels );
        gmm[1]->accumulate_ss(comp_id, fimg_row + i*nChannels);

#if DEBUG == 1
	frgCounts++;
#endif

      } //end if

    } //end col

  } //end row

  //find the model parameters
  gmm[0]->train();
  gmm[1]->train();

#if DEBUG == 1
  cout << "updating ue, bkg count, frg count:" << bkgCounts << "," << frgCounts << endl;
#endif



}

void GmmAppearance::compute_unary_term(const Mat& img, const Mat& mask, Mat& ue)
{
  //TODO: mask is not used. remove it.

  const Rect roi = get_roi();

  ue.create(2, roi.width*roi.height, CV_32FC1);

  float* ue_bkg_row = ue.ptr<float>(0);
  float* ue_frg_row = ue.ptr<float>(1);

  Mat fimg;
  //convert to the image to doulbe
  if( img.type() == CV_8UC3 || img.type() == CV_8UC1 ){
    img.convertTo(fimg, CV_32F, 1.0/255);
  }
  else{
    fimg = img;
  }

  int nChannels = fimg.channels();

  const Mat fimg_roi = fimg(roi);
  //  const Mat mask_roi = mask(roi);


  float p_frg, p_bkg;
  float ue_frg, ue_bkg;
  float sum;

  int node_id = 0;
  for(int j = 0; j < roi.height; ++j){
    const float* fimg_roi_row = fimg_roi.ptr<float>(j);
    //    const uchar* mask_roi_row = mask_roi.ptr<uchar>(j);

    for(int i = 0; i < roi.width; ++i){
      /*
	ue_bkg = -log( gmm[0]->get_pxl_likelihood( fimg_roi_row + i*nChannels ) );
	ue_frg = -log( gmm[1]->get_pxl_likelihood( fimg_roi_row + i*nChannels ) );

	ue_bkg_row[node_id] = ue_bkg;
	ue_frg_row[node_id] = ue_frg;

	//*/
      ///*
      ue_bkg = gmm[0]->get_pxl_likelihood( fimg_roi_row + i*nChannels );
      ue_frg = gmm[1]->get_pxl_likelihood( fimg_roi_row + i*nChannels );
  
      sum = ue_bkg + ue_frg;

      /* for debug
      if(isnan(sum)){
        cout << "sum:" << sum << "=" << ue_bkg << "+" << ue_frg << endl;
        exit(-1);
      }

      if(isinf(sum)){
        cout << "sum:" << sum << "=" << ue_bkg << "+" << ue_frg << endl;
        exit(-1);
      }
      //*/

      p_bkg = ue_bkg / sum;
      p_frg = ue_frg / sum;


      if(p_bkg < ZERO_PROB){
	p_bkg = ZERO_PROB;
      }
  
      if(p_frg < ZERO_PROB){
	p_frg = ZERO_PROB;
      }
  

      ue_bkg_row[node_id] = -log( p_bkg );
      ue_frg_row[node_id] = -log( p_frg );

      /* for debug
      if(isnan(ue_bkg_row[node_id])){
	cout << "bkg:" << ue_bkg_row[node_id] << "=" << p_bkg << "=" << ue_bkg << "/" << sum << endl;
	exit(-1);
      }

      if(isnan(ue_frg_row[node_id])){
	cout << "frg:" << ue_frg_row[node_id] << "=" << p_frg << "=" << ue_frg << "/" << sum << endl;
	exit(-1);
      }

      
      //*/
      

      node_id++;

    } //end col

  } //end row


  /* for debug
  //double mmin, mmax;
  //minMaxLoc(ue, &mmin, &mmax);
  //cout << "ue:" << mmin << "," << mmax << endl;
  cout << endl;
  gmm[0]->print_out_parameters();
  gmm[1]->print_out_parameters();
  //*/

  //normalize ue?
  ///*
  bool do_normalize_ue = false;

  if(do_normalize_ue) {
    double nsum;

    for(int col = 0; col < roi.width; ++col){
      
      nsum = (ue_bkg_row[col] + ue_frg_row[col]) / 2;
      ue_bkg_row[col] -= nsum;
      ue_frg_row[col] -= nsum;
      

      //nsum = ue_bkg_row[col];
      //ue_frg_row[col] -= nsum;
      //ue_bkg_row[col] -= nsum;
    }

  }
  //*/
  
  
}

void GmmAppearance::save_parameters(const int num_itr, const string& file_name) const
{
  //outupt file name
  stringstream ss;
  string f_name;

  ss << "itr" << num_itr << "_" << file_name;
  ss >> f_name;

  f_name = get_save_path() + f_name;

  //open file
  fstream fs(f_name.c_str(), fstream::out);

  if(!fs.is_open()){
    cerr << "Fail to open " << file_name << endl;
    return;
  }

  //write number of component for each gmm and the feature size
  fs << num_components << endl;
  fs << feat_size << endl;

  //write model parameters
  //each row for each gmm
  //save in this format: [w0, w1, ..., m0, m1, ..., C0, C1, ...]. C0 is scanned in row-order
  for(int ii = 0; ii < 2; ++ii){
    const Mat par = gmm[ii]->get_parameters();
    const float* par_row = par.ptr<float>(0);

    //write model parameters
    for(int k = 0; k < par.cols; ++k)
      fs << par_row[k] << " ";

    fs << endl;
  }

  fs.close();

  //save samples
  /*
  ss.clear();
  ss << "itr" << num_itr << "_samples.dat";
  ss >> f_name;

  f_name = get_save_path() + f_name;

  //open file
  fs.open(f_name.c_str(), fstream::out);

  if(!fs.is_open()){
    cerr << "Fail to open " << file_name << endl;
    return;
  }

  //format: [count; feat_size; samples;]
  fs << bkgSamples.rows << endl;
  fs << bkgSamples.cols << endl;
  for(int j = 0; j < bkgSamples.rows; ++j){
    for(int i = 0; i < bkgSamples.cols; ++i)
      fs << bkgSamples.at<float>(j,i) << " ";
    fs << endl;
  }

  fs << frgSamples.rows << endl;
  fs << frgSamples.cols << endl;
  for(int j = 0; j < frgSamples.rows; ++j){
    for(int i = 0; i < frgSamples.cols; ++i)
      fs << frgSamples.at<float>(j,i) << " ";
    fs << endl;
  }

  fs.close();
  */

}


