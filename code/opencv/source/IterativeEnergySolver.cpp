#include "AppearanceModel.h"
#include "Constants.h"
#include "ContourCreater.h"
#include "ContrastSensitivePM.h"
#include "DepthInterpolater.h"
#include "DepthPotential.h"
#include "EnergySolver.h"
#include "GraphCutWrapper.h"
#include "IterativeEnergySolver.h"
#include "StarShapeModel.h"
#include "Utility.h"

#include <opencv2/core/core.hpp>
#include <string>
#include <vector>
#include <sstream>
#include <iomanip>
#include <fstream>

using namespace cv;
using namespace std;



IterativeEnergySolver::IterativeEnergySolver(const int _num_itr, const string& img_name)
  :EnergySolver(_num_itr, img_name)
{}

IterativeEnergySolver::~IterativeEnergySolver()
{}

void IterativeEnergySolver::run(AppearanceModel* app_model, DepthPotential* dep_model, DepthInterpolater* dep_int, DepthInterpolater* dep_int_stereo, ContrastSensitivePM* binary_model, StarShapeModel* star_model, ContourCreater* cc, vector<ContourCreater*> cc_m, Mat& img, Mat& mask, Mat& soft_mask, Mat& recon_depth, Mat& stereo_depth, vector<double>& w, int gc_max_itr)
{ // w (feature weight)
  //

  const Rect roi = app_model->get_roi();

  vector<double> all_one_weight(2, 1.0);

  Mat a_ue;               //for GMM 
  Mat d_ue;               //for simple depth potential
  Mat r_ue;               //for recovered depth potential
  Mat st_ue;              //for stereo depth potential
  Mat c_ue;               //for contour potential

  Mat p_pe, p_pe2, p_pi;  //for Potts model
  Mat s_pe, s_pi;         //for Star model

  vector<Mat> m_ue(3);    //use three mechanical turkers' shape

  Mat ue, pe, pi;         //final ue, pe, pi
  
  Mat x_opt;              //MAP result
  double energy;          //MAP energy

  GraphCutWrapper* gc;

  string file_name;
  stringstream ss;


  bool use_timer = false;

  double t1,t2;

  fstream fs("../result/inference_time.txt", fstream::out | fstream::app);

  for(int itr = 0; itr < get_num_itr(); ++itr) {

    t1 = (double)getTickCount();


    app_model->compute_unary_term(img, mask, a_ue);  //mask is not used...

    if(USE_SOFT_MASK)
      dep_model->compute_unary_term(mask(roi), soft_mask(roi), d_ue);
    else
      dep_model->compute_unary_term(mask(roi), all_one_weight, d_ue);

    dep_int->compute_unary_term(recon_depth, 1, r_ue);
    cc->compute_unary_term(c_ue);
    dep_int_stereo->compute_unary_term(stereo_depth, 1, st_ue);

    for(size_t ii = 0; ii < cc_m.size(); ++ii){
      if(USE_TURKER){
	cc_m[ii]->compute_unary_term(m_ue[ii]);
      } else{
	m_ue[ii].create(a_ue.size(), a_ue.type());
	m_ue[ii].setTo(Scalar(0));
      }
    }

#if DEBUG ==1
    ////save model parameter
    //app_model->save_parameters(itr, file_name);

    //show the ue for foreground object and save it
    ///*
    //      ss.clear();
    //      ss << get_save_path() <<  get_img_name() + "_ue" << "_itr" << itr << "_ue" + SAVE_IMG_EXT;;
    //      ss >> file_name;

    /*visualize a_ue
      visualize_ue(a_ue, roi, file_name);
      waitKey();
    //*/

    /*visualize r_ue
      visualize_ue(r_ue, Rect(0,0,recon_depth.cols, recon_depth.rows));
      waitKey();
    //*/
#endif
  
    //compute the binary energy
    binary_model->compute_pe(img, mask, all_one_weight, p_pe, p_pe2, p_pi);

    //load star shape energy
    bool load_star_from_file = true;

    star_model->compute_pe(img(roi), mask(roi), s_pe, s_pi, load_star_from_file, get_img_name());


    //weight the depth potential
    d_ue.row(0) = d_ue.row(0) * w[3];
    d_ue.row(1) = d_ue.row(1) * w[4];

    //total ue
    ue = (w[0]*a_ue + d_ue + w[5]*r_ue + w[7]*c_ue + w[8]*st_ue + w[9]*m_ue[0] + w[10]*m_ue[1] + w[11]*m_ue[2]);

    //normalize ue?
    /*
    bool do_normalize_ue = true;

    if(do_normalize_ue) {
      double nsum;

      float* ue_bkg_row = ue.ptr<float>(0);
      float* ue_frg_row = ue.ptr<float>(1);
      for(int col = 0; col < ue.cols; ++col){
      
	nsum = (ue_bkg_row[col] + ue_frg_row[col]) / 2;
	ue_bkg_row[col] -= nsum;
	ue_frg_row[col] -= nsum;
      

	//nsum = ue_bkg_row[col];
	//ue_frg_row[col] -= nsum;
	//ue_bkg_row[col] -= nsum;
      }

    }
    */

    if(w[6] == 0){
      //only potts model
      pe = (w[1] * p_pe + w[2] * p_pe2);
      pi = p_pi;
    } else {
      combine_binary_energy((w[1]*p_pe + w[2]*p_pe2), p_pi, w[6]*s_pe, s_pi, pe, pi);

    }


    /* for debug
    //if(get_img_name() != "000234_tracklet8")
    //  return;

    double dsum = 0;
    for(int col=0; col < a_ue.cols; col++)
    dsum += a_ue.at<float>(0,col);
    cout << "a_ue 0:" << dsum << endl;

    dsum = 0;
    for(int col=0; col < a_ue.cols; col++)
    dsum += a_ue.at<float>(1,col);
    cout << "a_ue 1:" << dsum << endl;

    dsum = 0;
    for(int col=0; col < d_ue.cols; col++)
    dsum += d_ue.at<float>(0,col);
    cout << "d_ue 0:" << dsum << endl;

    dsum = 0;
    for(int col=0; col < d_ue.cols; col++)
    dsum += d_ue.at<float>(1,col);
    cout << "d_ue 1:" << dsum << endl;

    dsum = 0;
    for(int col=0; col < r_ue.cols; col++)
    dsum += r_ue.at<float>(0,col);
    cout << "r_ue 0:" << dsum << endl;

    dsum = 0;
    for(int col=0; col < r_ue.cols; col++)
    dsum += r_ue.at<float>(1,col);
    cout << "r_ue 1:" << dsum << endl;



    double mmin, mmax;
    minMaxLoc(a_ue, &mmin, &mmax);
    cout << "a_ue m:" << mmin << "," << mmax << endl;
    
    minMaxLoc(d_ue, &mmin, &mmax);
    cout << "d_ue m:" << mmin << "," << mmax << endl;
    
    minMaxLoc(ue, &mmin, &mmax);
    cout << "ue m:" << mmin << "," << mmax << endl;
    
    minMaxLoc(p_pe, &mmin, &mmax);
    cout << "p_pe m:" << mmin << "," << mmax << endl;
    
    minMaxLoc(p_pe2, &mmin, &mmax);
    cout << "p_pe2 m:" << mmin << "," << mmax << endl;
    
    
    //*/

    gc = new GraphCutWrapper(ue.cols, pi);

    //normalize ue, pe?
    if(DO_NORMALIZE_ENERGY){
      gc->compute_map( ue / ue.cols, pe/pi.cols, pi, x_opt, energy);
    } else {
      gc->compute_map( ue, pe, pi, x_opt, energy);

      t2 = (double)getTickCount() - t1;
      //cout << "up time: " << t2 / getTickFrequency() << " sec." << endl;
  
      fs << get_img_name() << " " <<  t2 / getTickFrequency() << endl;

    }


    /* for debug. Note that the energy may be less than zero if we include the contour (since we use signed distance transform)

    if(energy < 0){
      cout << "energy < 0 (OVERFLOW?): " << energy << endl;

      double dsum = 0;
      for(int col=0; col < a_ue.cols; col++)
	dsum += a_ue.at<float>(0,col);
      cout << "a_ue 0:" << dsum << endl;

      dsum = 0;
      for(int col=0; col < a_ue.cols; col++)
	dsum += a_ue.at<float>(1,col);
      cout << "a_ue 1:" << dsum << endl;

      dsum = 0;
      for(int col=0; col < d_ue.cols; col++)
	dsum += d_ue.at<float>(0,col);
      cout << "d_ue 0:" << dsum << endl;

      dsum = 0;
      for(int col=0; col < d_ue.cols; col++)
	dsum += d_ue.at<float>(1,col);
      cout << "d_ue 1:" << dsum << endl;

      dsum = 0;
      for(int col=0; col < r_ue.cols; col++)
	dsum += r_ue.at<float>(0,col);
      cout << "r_ue 0:" << dsum << endl;

      dsum = 0;
      for(int col=0; col < r_ue.cols; col++)
	dsum += r_ue.at<float>(1,col);
      cout << "r_ue 1:" << dsum << endl;

      dsum = 0;
      for(int col=0; col < c_ue.cols; col++)
	dsum += c_ue.at<float>(0,col);
      cout << "c_ue 0:" << dsum << endl;

      dsum = 0;
      for(int col=0; col < c_ue.cols; col++)
	dsum += c_ue.at<float>(1,col);
      cout << "c_ue 1:" << dsum << endl;


      double mmin, mmax;
      minMaxLoc(a_ue, &mmin, &mmax);
      cout << "a_ue m:" << mmin << "," << mmax << endl;
    
      minMaxLoc(d_ue, &mmin, &mmax);
      cout << "d_ue m:" << mmin << "," << mmax << endl;

      minMaxLoc(r_ue, &mmin, &mmax);
      cout << "r_ue m:" << mmin << "," << mmax << endl;

      minMaxLoc(c_ue, &mmin, &mmax);
      cout << "c_ue m:" << mmin << "," << mmax << endl;
    
      minMaxLoc(ue, &mmin, &mmax);
      cout << "ue m:" << mmin << "," << mmax << endl;
    
      minMaxLoc(p_pe, &mmin, &mmax);
      cout << "p_pe m:" << mmin << "," << mmax << endl;
    
      minMaxLoc(p_pe2, &mmin, &mmax);
      cout << "p_pe2 m:" << mmin << "," << mmax << endl;
    
    
    }
    //*/

    /* for debug
    //cout << "pe:" << pe2 << endl;
    //cout << w[2]*pe2;
     

    cout << ue;
    cout << "w:" << w[0] << ',' << w[1] << ',' << w[2] << endl;
    exit(-1);
    //*/
 
   
    model_energy.push_back(energy);

    x_opt = x_opt.reshape(0, roi.height);

    //update map result
    app_model->update_map_result_within_roi(x_opt);
 
    //show the map result and save it under the map folder
    file_name = get_save_path() + get_img_name() + SAVE_IMG_EXT;

    imwrite(file_name, x_opt*255);

#if DEBUG == 1
    //visualization takes some time
    //visualize_binary_img(app_model->get_map_result(), roi, file_name);
#endif

    //show the masked image
    file_name = get_save_path() + get_img_name() + "_masked" + SAVE_IMG_EXT;
    visualize_map_result(img, app_model->get_map_result(), roi, file_name, false);

#if DEBUG == 1
    //visualization takes some time
    //visualize_map_result(img, app_model->get_map_result(), roi, file_name);
#endif

    //if run more than one iteration, then update the model
    if(get_num_itr() != 1){
      //stopping criterion
      if( satisfy_stopping_criterion(itr) )
	break;

      app_model->update_model_parameters(img, mask);
    }

  } //end itr


    //  delete pd;
  delete gc;

  if(use_timer){
    t2 = (double)getTickCount() - t1;
    cout << "up time: " << t2 / getTickFrequency() << " sec." << endl;
  }

  //show the model energy for each iteration
  ///*
  if(!model_energy.empty()){
    cout << "done inference for " << get_img_name() << "..." << "energy = ";
    for(size_t i = 0; i < model_energy.size(); ++i)
      cout << setw(15) << model_energy[i] << ", ";
    cout << endl;

    //save_model_energy(file_name);
  }
  else{    
    cout << "model_energy is empty." << endl;
  }
  //*/


  fs.close();

}

  bool IterativeEnergySolver::satisfy_stopping_criterion(const int itr)
  {
    if(itr == 0)
      return false;

    if(abs((model_energy[itr]-model_energy[itr-1])/model_energy[itr-1]) < 1e-4)
      return true;
    else
      return false;
  }

