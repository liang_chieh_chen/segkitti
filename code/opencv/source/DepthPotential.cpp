#include "Constants.h"
#include "DepthPotential.h"

#include <iostream>
#include <vector>
#include <opencv2/core/core.hpp>

using namespace std;
using namespace cv;

DepthPotential::DepthPotential()
{}

DepthPotential::~DepthPotential()
{}

void DepthPotential::compute_unary_term(const Mat& mask, const vector<double>& lambda, Mat& ue)
{
  //lambda_f, lambda_b
  if(lambda.size() == 0){
    cerr << "WARNNING: lambda for depth potential is empty!" << endl;
    return;
  }

  CV_Assert(mask.type() == CV_8UC1);

  ue.create(2, mask.rows * mask.cols, CV_32FC1);
  ue.setTo(0);

  float* ue_bkg_row = ue.ptr<float>(0);
  float* ue_frg_row = ue.ptr<float>(1);

  const uchar* mask_row;

  int node_id = 0;

  for(int row = 0; row < mask.rows; ++row){
    mask_row = mask.ptr<uchar>(row);

    for(int col = 0; col < mask.cols; ++col){
      if(mask_row[col] == FOREGROUND_LABEL) {
	ue_bkg_row[node_id] = lambda[0];
      } else if(mask_row[col] == STRICT_BACKGROUND_LABEL) {
	ue_frg_row[node_id] = lambda[1];
      }

      node_id++;
    }
    
  }
}

void DepthPotential::compute_unary_term(const Mat& mask, const Mat& soft_mask, Mat& ue)
{
  CV_Assert(mask.type() == CV_8UC1);
  CV_Assert(soft_mask.type() == CV_32FC1);

  ue.create(2, mask.rows * mask.cols, CV_32FC1);
  ue.setTo(0);

  float* ue_bkg_row = ue.ptr<float>(0);
  float* ue_frg_row = ue.ptr<float>(1);

  const uchar* mask_row;
  const float* soft_row;

  int node_id = 0;

  for(int row = 0; row < mask.rows; ++row){
    mask_row = mask.ptr<uchar>(row);
    soft_row = soft_mask.ptr<float>(row);

    for(int col = 0; col < mask.cols; ++col){
      if(mask_row[col] == FOREGROUND_LABEL) {
	ue_bkg_row[node_id] = soft_row[col];
      } else if(mask_row[col] == STRICT_BACKGROUND_LABEL) {
	ue_frg_row[node_id] = -soft_row[col];
      }

      node_id++;
    }
    
  }
}
