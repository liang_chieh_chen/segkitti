#include "Constants.h"
#include "PointCloudAccumulator.h"
#include "MaskCreater.h"
#include "tracklets.h"
#include "Utility.h"

#include <cstdlib>
#include <sstream>
#include <iostream>
#include <iomanip> 
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace cv;
using namespace std;


PointCloudAccumulator::PointCloudAccumulator(const string& v_c_file, const string& c_c_file, const string& r_folder,  const string& v_folder, const int c_id, const int e_width)
  : velo_to_cam_filename(v_c_file),
    cam_to_cam_filename(c_c_file),
    raw_data_img_folder(r_folder),
    velo_folder(v_folder),
    camera_id(c_id),
    expand_width(e_width)
{}

PointCloudAccumulator::~PointCloudAccumulator()
{
  for(size_t i = 0; i < mcs.size(); ++i){
    delete mcs[i];
  }
  
  mcs.clear();
}

void PointCloudAccumulator::init(const vector<int>& i_ids)
{
  img_ids = i_ids;

  num_frame = (int)img_ids.size();

  for(int i = 0; i < num_frame; ++i){
    string velo_pc_filename;
    int img_id = img_ids[i];
    stringstream ss;
    ss << setw(10) << setfill('0') << img_id;
    string id;
    ss >> id;

    velo_pc_filename = velo_folder + id + ".bin";  //file ext HARD CODED!!!!!!!!

    mcs.push_back(new MaskCreater(velo_to_cam_filename, cam_to_cam_filename,
				  velo_pc_filename, camera_id, expand_width));
  }


}

void PointCloudAccumulator::clear()
{
  for(size_t i = 0; i < mcs.size(); ++i){
    delete mcs[i];
  }

  mcs.clear();
}

void PointCloudAccumulator::fuse_point_clouds(const Mat& fimg, const Tracklets::tTracklet* car, Mat& fuse_mask, Mat& fuse_depth_img, const string& tracklet_name, const bool use_icp)
{
  //compute masks for all image id
  Mat masks[num_frame];
  Rect rois[num_frame];
  Rect wb_rois[num_frame];
  Mat imgs[num_frame];


  //load raw data images
  string img_filename;
  for(int i = 0; i < num_frame; ++i){
    int img_id = img_ids[i];
    stringstream ss;
    ss << setw(10) << setfill('0') << img_id;
    string id;
    ss >> id;

    img_filename = raw_data_img_folder + id + ".png";  //file ext HARD CODED!!!!!!

    imgs[i] = imread(img_filename, CV_LOAD_IMAGE_COLOR);
    
    //change image to float
    imgs[i].convertTo(imgs[i], CV_32F, 1.0/255);
  }

  //create masks
  int frg_size;
  vector<int> active_imgs;  //record if the tracklet is active for the img_ids

  for(int i = 0; i < num_frame; ++i){

    masks[i].create(fimg.size(), CV_8UC1);
    masks[i].setTo(Scalar(0));

    frg_size = mcs[i]->create_mask(imgs[i], img_ids[i], car, masks[i], rois[i], wb_rois[i]);

    if(frg_size >= 0 && frg_size < FRG_PC_THRESHOLD){
      active_imgs.push_back(0);
      continue;
    }

    if( rois[i].width == 0 && rois[i].height == 0){
      active_imgs.push_back(0);
      continue;
    }

    active_imgs.push_back(1);

  }

  /*debug
  for(int i = 0; i < num_frame; ++i){
    string window_name;
    stringstream ss;
    ss << i;
    ss >> window_name;
    
    window_name += "orig";
    imshow(window_name, masks[i]);
    
  }
  waitKey();
  //*/


  ////fuse masks
  //1. rotate and translate all the other masks to the same coordinate of first image
  Mat rectified_masks[num_frame];
  Mat rectified_depth_imgs[num_frame];

  for(int i = 1; i < num_frame; ++i){
    if(active_imgs[i]){

      if(use_icp){
	mcs[i]->rotate_translate_maski_to_maskj_icp(tracklet_name, car, masks[i], masks[0], mcs[i]->get_depth_img(), mcs[0]->get_depth_img(), rois[i], rois[0], img_ids[i], img_ids[0], imgs[i], imgs[0], rectified_masks[i], rectified_depth_imgs[i]);
      }else {
	mcs[i]->rotate_translate_maski_to_maskj(tracklet_name, car, masks[i], mcs[i]->get_depth_img(), rois[i], rois[0], img_ids[i], img_ids[0], imgs[i], imgs[0], rectified_masks[i], rectified_depth_imgs[i]);
      }

      /*debug
	string window_name;
	stringstream ss;
	ss << i;
	ss >> window_name;
	window_name += "rec";

	imshow(window_name, rectified_masks[i]);

	ss.clear();
	ss << i;
	ss >> window_name;
	window_name += "dep";
    
	imagesc_bw(window_name, mcs[i]->get_depth_img());

	ss.clear();
	ss << i;
	ss >> window_name;
	window_name += "rec dep";

	imagesc_bw(window_name, rectified_depth_imgs[i]);

	waitKey();
      //*/
    }

  }  

  //2. fuse the results to mask. use first index as destination
  fuse_mask = masks[0];
  fuse_depth_img = mcs[0]->get_depth_img().clone();

  Mat counter(fuse_depth_img.size(), CV_32SC1, Scalar(0));

  uchar* target_mask_row;
  uchar source_label;

  int* counter_row;

  float* target_depth_row;
  float source_depth;

  for(int i = 1; i < num_frame; ++i){
    if(!active_imgs[i])
      continue;

    uchar* source_mask_row;
    float* source_depth_row;

    for(int row = 0; row < masks[i].rows; ++row){
      target_mask_row = fuse_mask.ptr<uchar>(row);
      source_mask_row = rectified_masks[i].ptr<uchar>(row);

      target_depth_row = fuse_depth_img.ptr<float>(row);
      source_depth_row = rectified_depth_imgs[i].ptr<float>(row);

      counter_row      = counter.ptr<int>(row);
     
      for(int col = 0; col < masks[i].cols; ++col){
	source_label = source_mask_row[col];
	
	source_depth = source_depth_row[col];

	//only accumulate the car point clouds at this moment, even though the function rotate_translate_maski_to_maskj may transform both car and bkg point clouds
	if(source_label == FOREGROUND_LABEL){// || source_label == STRICT_BACKGROUND_LABEL){
	  target_mask_row[col] = source_label;

	  target_depth_row[col] += source_depth;
	  
	  counter_row[col] += 1;

	}
	
      }
      
    }
    
  }

  //compute the average of target depth
  int count;
  for(int row = 0; row < fuse_depth_img.rows; ++row){
    target_depth_row = fuse_depth_img.ptr<float>(row);
    counter_row      = counter.ptr<int>(row);

    for(int col = 0; col < fuse_depth_img.cols; ++col){
      count = counter_row[col];

      if(count > 1){
	target_depth_row[col] = target_depth_row[col] / ((float)count);
      }
    }
  }


  /*
  imshow("final m", fuse_mask);
  imagesc_bw("final d", fuse_depth_img);
  waitKey();
  //*/

  //3. remove the bkg p.c. that leak to car convex hull
  //since we only accumute the car point clouds at this moment, we do not need to do this



}
