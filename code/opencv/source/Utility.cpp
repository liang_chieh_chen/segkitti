#include "Utility.h"
#include "ExpConfiguration.h"
#include "Constants.h"

//#include "tracklets.h"
//#include "DepthInterpolater.h"

#include <sstream>
#include <fstream>
#include <cstdlib>
#include <cassert>
#include <algorithm>


using namespace std;

/*
int readExpConfigurationFromFile(string filename, ExpConfiguration& config)
{
  FileStorage fs(filename, FileStorage::READ);
  if (!fs.isOpened())
  {
    cerr << "Failed to open " << filename << endl;
    return -1;
  }
  fs["ExpConfiguration"] >> config;

  return 0;
}
*/

void visualize_ue(const Mat& ue, const Rect& roi_rect, const string& file_name)
{
  //visualize log(Pr(x=1)/Pr(x=0))
  //remember ue has been taken negative logrithm (i.e., ue = -log(Pr))
  //

  Mat canvas(roi_rect.height, roi_rect.width, CV_32FC1);

  const float* bkg_ue = ue.ptr<float>(0);
  const float* frg_ue = ue.ptr<float>(1);

  int ind = 0;

  for(int j = 0; j < canvas.rows; ++j){
    float* canvas_row = canvas.ptr<float>(j);

    for(int i = 0; i < canvas.cols; ++i){
      canvas_row[i] = bkg_ue[ind] - frg_ue[ind];

      ind++;
    }
  }

  normalize(canvas, canvas, 0, 1, NORM_MINMAX);

  string fig_name;
  stringstream ss;

  ss << 0;
  ss >> fig_name;
  fig_name = "ue-" + fig_name;


  namedWindow(fig_name.c_str(), 1);
  imshow(fig_name, canvas);

  if(!file_name.empty()){
    imwrite(file_name, 255*canvas);
  }

  //  waitKey();
  //  destroyWindow(fig_name);

}

void visualize_binary_img(const Mat& img, const Rect& roi, const string& file_name)
{
  CV_Assert(img.type() == CV_8UC1);

  Mat canvas = img(roi)*255;

  namedWindow("binary-img", 1);
  imshow("binary-img", canvas);

  if(!file_name.empty()){
    imwrite(file_name, canvas);
  }


//  waitKey();
//  destroyWindow("binary-img");

}

void visualize_map_result(const Mat& img, const Mat& mask, const Rect& roi, const string& file_name, const bool show_result)
{
  //mask is assumed 0 or 1
  Vec3f bkg_color(0.5, 0.5, 0.5);

  CV_Assert(img.type() == CV_32FC3);
  CV_Assert(mask.type() == CV_8UC1);
 
  Mat canvas(img.size(), img.type());

  const uchar* mask_row;

  for(int row = roi.y; row < roi.y+roi.height; ++row){
    mask_row = mask.ptr<uchar>(row);
    for(int col = roi.x; col < roi.x+roi.width; ++col){
      if(mask_row[col] == 1){
	canvas.at<Vec3f>(row,col) = img.at<Vec3f>(row,col);
      }else{
	canvas.at<Vec3f>(row,col) = bkg_color;
      }
    }
  }

  if(show_result){
    namedWindow("map-result", 1);
    imshow("map-result", canvas(roi));
  }

  if(!file_name.empty()){
    imwrite(file_name, 255*canvas(roi));
  }

  //waitKey();
  //destroyWindow("map-result");

}

void visualize_position(const string& name, const Mat& pos, const int rows, const int cols)
{
  //visualize the points specified by the pos matrix on a canvas of size rows-by-cols
  
  //assume pos is int type and has size = num_elements x 2
  CV_Assert(pos.type() == CV_32SC1);
  CV_Assert(pos.cols == 2); 

  Mat canvas(rows, cols, CV_8UC1, Scalar(0));

  const int* pos_row;
  int x, y;

  for(int row = 0; row < pos.rows; ++row){
    pos_row = pos.ptr<int>(row);

    y = pos_row[0];
    x = pos_row[1];

    canvas.at<uchar>(y,x) = 255;
  }

  namedWindow(name, 1);
  imshow(name, canvas);

}

void imagesc_bw(const string& name, const Mat& fimg, const string& filename)
{
    double minVal, maxVal;

    minMaxLoc(fimg, &minVal, &maxVal); //find minimum and maximum intensitie

    //cout << "mm:" << minVal << "," << maxVal << endl;

    Mat draw;

    fimg.convertTo(draw, CV_8U, 255.0/(maxVal - minVal), -minVal * 255.0/(maxVal - minVal));

    namedWindow(name, 1);
    imshow(name, draw);
    //waitKey();
    //    destroyWindow(name);

    if(filename != ""){
      imwrite(filename, draw);
    }

    
}



void color_imshow_bw(const string& name, const Mat& fimg, const string& filename)
{
  CV_Assert(fimg.depth() == CV_32F);

  Mat canvas(fimg.size(), CV_32FC3, Scalar(0,0,0));

  float dep_val;
  int ind;
  for(int row=0; row < canvas.rows; ++row){
    
    for(int col=0; col < canvas.cols; ++col){
      dep_val = fimg.at<float>(row,col);

      if(dep_val != 0){
	ind     = (int)(64*5.0/dep_val + 0.5);

	if(ind < 0)
	  ind = 0;
	if(ind > 64)
	  ind = 63;

	//JET_COLOR saves in RGB order
	canvas.at<Vec3f>(row,col) = Vec3f(JET_COLOR[ind*3+2], JET_COLOR[ind*3+1], JET_COLOR[ind*3]);
      }
    }
  }

  namedWindow(name, 1);
  imshow(name, canvas*255);
  //waitKey();
  //    destroyWindow(name);

  if(filename != ""){
    imwrite(filename, canvas*255);
  }


}

void setup_select_indicator(vector<bool>& ind, const Mat& source, const int dim, const bool larger, const float val)
{
  //ASSUME source is of size feature_dimension x number_of_elements
  CV_Assert(source.type() == CV_32FC1);
  CV_Assert(source.rows > dim);

  ind.clear();

  int num_ele = source.cols;   
  float target_val;
  const float* row = source.ptr<float>(dim);

  for(int i = 0; i < num_ele; ++i){
    target_val = row[i];

    if(larger)
      if( target_val >= val )
	ind.push_back(true);
      else
	ind.push_back(false);
    else
      if( target_val < val )
	ind.push_back(true);
      else
	ind.push_back(false);
  }

}

void negate_indicator(vector<bool>& dest, const vector<bool>& source)
{
  vector<bool> tmp;

  for(size_t i = 0; i < source.size(); ++i){
    if(source[i])
      tmp.push_back(false);
    else
      tmp.push_back(true);
  }

  dest = tmp;
}

void and_indicator(vector<bool>& dest, const vector<bool>& source1, const vector<bool>& source2)
{
  assert(source1.size() == source2.size());

  vector<bool> tmp;

  for(size_t i = 0; i < source1.size(); ++i){
    if(source1[i] && source2[i])
      tmp.push_back(true);
    else
      tmp.push_back(false);
  }
  
  dest = tmp;

}

void select_by_indicator(const vector<bool>& source, const vector<bool>& ind, vector<bool>& dest)
{
  vector<bool> tmp;

  for(size_t i = 0; i < source.size(); ++i){
    if(ind[i])
      tmp.push_back(source[i]);
  }

  dest = tmp;
}

void select_by_indicator(const Mat& source, const vector<bool>& ind, Mat& dest)
{
  //Selct columns in source specified by ind

  Mat tmp(source.size(), source.type());

  int count = 0;
  for(size_t i = 0; i < ind.size(); ++i){
    if(ind[i]){
      source.col(i).copyTo(tmp.col(count));
      count++;
    }
  }

  //dest = tmp(Range::all(), Range(0, count)).clone();
  dest = tmp(Range::all(), Range(0, count));
}

void remove_by_indicator(const Mat& source, const vector<bool>& remove_ind, Mat& dest)
{
  vector<bool> select_ind;

  for(size_t i = 0; i < remove_ind.size(); ++i){
    if(remove_ind[i])
      select_ind.push_back(false);
    else
      select_ind.push_back(true);
  }

  select_by_indicator(source, select_ind, dest);

}

void save_sparse_float_mat(const string& name, const Mat& f_mat, const bool rewrite)
{
  //ONLY SUPPORT number of channel = 1
  CV_Assert(f_mat.type() == CV_32FC1);

  //save format is as follows
  // mat.rows mat.cols
  // (row, col, val) (i.e., in triplet format)
  // 

  fstream fs;
  
  if(rewrite)
    fs.open(name.c_str(), fstream::out | fstream::binary);
  else
    fs.open(name.c_str(), fstream::out | fstream::app | fstream::binary);

  if(!fs.is_open()){
    cerr << "fail to open " << name << " for saving." << endl;
    return;
  }

  int ival;

  ival = f_mat.rows;
  fs.write( (char*)&ival, sizeof(int) );

  ival = f_mat.cols;
  fs.write( (char*)&ival, sizeof(int) );

  const float* f_row;
  float fval;

  for(int row = 0; row < f_mat.rows; ++row){
    f_row = f_mat.ptr<float>(row);

    for(int col = 0; col < f_mat.cols; ++col){
      fval = f_row[col];

      if(fval != 0){
	ival = row;
	fs.write( (char*)&ival, sizeof(int) );

	ival = col;
	fs.write( (char*)&ival, sizeof(int) );

	fs.write( (char*)&fval, sizeof(float) );
      }
    }
  }


  fs.close();

}

void load_sparse_float_mat(const string& name, Mat& f_mat)
{
  fstream fs;

  fs.open(name.c_str(), fstream::in | fstream::binary);

  if(!fs.is_open()){
    cerr << "Fail to load " << name << endl;
    return;
  }

  int row, col;
  float fval;

  //load row and col size
  fs.read( (char*)&row, sizeof(int) );
  fs.read( (char*)&col, sizeof(int) );

  f_mat.create(row, col, CV_32FC1);
  f_mat.setTo(Scalar(0));

  while( !fs.eof() ){
    //load (row, col, val) triplet
    fs.read( (char*)&row, sizeof(int) );
    fs.read( (char*)&col, sizeof(int) );
    fs.read( (char*)&fval, sizeof(float) );

    f_mat.at<float>(row, col) = fval;

  }

  fs.close();
  
}

void save_int_mat_bin(const string& name, const Mat& i_mat, const bool rewrite)
{
  //ONLY SUPPORT number of channel = 1
  CV_Assert(i_mat.type() == CV_32SC1);

  //save format is as follows
  // mat.rows mat.cols
  // row0: val0, val1, val2, ...
  // row1: val0, val1, val2, ...
  // ...
  // row index and newline are not saved

  fstream fs;
  
  if(rewrite)
    fs.open(name.c_str(), fstream::out | fstream::binary);
  else
    fs.open(name.c_str(), fstream::out | fstream::app | fstream::binary);

  if(!fs.is_open()){
    cerr << "fail to open " << name << " for saving." << endl;
    return;
  }

  int ival;

  ival = i_mat.rows;
  fs.write( (char*)&ival, sizeof(int) );

  ival = i_mat.cols;
  fs.write( (char*)&ival, sizeof(int) );

  const int* i_row;

  for(int row = 0; row < i_mat.rows; ++row){
    i_row = i_mat.ptr<int>(row);

    for(int col = 0; col < i_mat.cols; ++col){
      ival = i_row[col];

      fs.write( (char*)&ival, sizeof(int) );

    }
  }


  fs.close();

}

void load_int_mat_bin(const string& name, Mat& i_mat)
{
  fstream fs;

  fs.open(name.c_str(), fstream::in | fstream::binary);

  if(!fs.is_open()){
    cerr << "Fail to load " << name << endl;
    return;
  }

  int row, col;
  int ival;

  //load row and col size
  fs.read( (char*)&row, sizeof(int) );
  fs.read( (char*)&col, sizeof(int) );

  i_mat.create(row, col, CV_32SC1);


  //load val for each row
  int* i_row;

  for(int row = 0; row < i_mat.rows; ++row){
    i_row = i_mat.ptr<int>(row);

    for(int col = 0; col < i_mat.cols; ++col){
      fs.read( (char*)&ival, sizeof(int) );

      i_row[col] = ival;
    }
  }


  fs.close();
  
}

void save_float_mat_bin(const string& name, const Mat& f_mat, const bool rewrite)
{
  //ONLY SUPPORT number of channel = 1
  CV_Assert(f_mat.type() == CV_32FC1);

  //save format is as follows
  // mat.rows mat.cols
  // row0: val0, val1, val2, ...
  // row1: val0, val1, val2, ...
  // ...
  // row index and newline are not saved

  fstream fs;
  
  if(rewrite)
    fs.open(name.c_str(), fstream::out | fstream::binary);
  else
    fs.open(name.c_str(), fstream::out | fstream::app | fstream::binary);

  if(!fs.is_open()){
    cerr << "fail to open " << name << " for saving." << endl;
    return;
  }

  int ival;
  float fval;

  ival = f_mat.rows;
  fs.write( (char*)&ival, sizeof(int) );

  ival = f_mat.cols;
  fs.write( (char*)&ival, sizeof(int) );

  const float* f_row;

  for(int row = 0; row < f_mat.rows; ++row){
    f_row = f_mat.ptr<float>(row);

    for(int col = 0; col < f_mat.cols; ++col){
      fval = f_row[col];

      fs.write( (char*)&fval, sizeof(float) );

    }
  }


  fs.close();

}

int load_float_mat_bin(const string& name, Mat& f_mat)
{
  fstream fs;

  fs.open(name.c_str(), fstream::in | fstream::binary);

  if(!fs.is_open()){
    cerr << "Fail to load " << name << endl;
    return -1;
  }

  int row, col;
  float fval;

  //load row and col size
  fs.read( (char*)&row, sizeof(int) );
  fs.read( (char*)&col, sizeof(int) );

  f_mat.create(row, col, CV_32FC1);


  //load val for each row
  float* f_row;

  for(int row = 0; row < f_mat.rows; ++row){
    f_row = f_mat.ptr<float>(row);

    for(int col = 0; col < f_mat.cols; ++col){
      fs.read( (char*)&fval, sizeof(float) );

      f_row[col] = fval;
    }
  }


  fs.close();
  
  return 1;
}



void save_float_mat(const string& name, const Mat& f_mat, const bool rewrite)
{
  //save format is as follows
  //num_row num_col num_channel
  //data for row 0
  //data for row 1
  //and so on
  //NOTE if f_mat has more than one channel, remember how the opencv stores color images.
  //

  CV_Assert(f_mat.depth() == CV_32F);

  fstream fs;
  
  if(rewrite)
    fs.open(name.c_str(), fstream::out);
  else
    fs.open(name.c_str(), fstream::out | fstream::app);

  if(!fs.is_open()){
    cerr << "fail to open " << name << " for saving." << endl;
    return;
  }

  int num_chan = f_mat.channels();

  fs << f_mat.rows << " " << f_mat.cols << " " << num_chan << endl;

  const float* f_row;

  for(int row = 0; row < f_mat.rows; ++row){
    f_row = f_mat.ptr<float>(row);

    for(int col = 0; col < f_mat.cols*num_chan; ++col){
      fs << f_row[col] << " ";
    }
    fs << endl;
  }
  fs << endl;


  fs.close();

}

void save_uchar_mat(const string& name, const Mat& u_mat, const bool rewrite)
{
  //save format is as follows
  //num_row num_col num_channel
  //data for row 0
  //data for row 1
  //and so on
  //NOTE if u_mat has more than one channel, remember how the opencv stores color images.
  //

  CV_Assert(u_mat.depth() == CV_8U);

  fstream fs;
  
  if(rewrite)
    fs.open(name.c_str(), fstream::out);
  else
    fs.open(name.c_str(), fstream::out | fstream::app);

  if(!fs.is_open()){
    cerr << "fail to open " << name << " for saving." << endl;
    return;
  }

  int num_chan = u_mat.channels();

  fs << u_mat.rows << " " << u_mat.cols << " " << num_chan << endl;

  const uchar* u_row;

  for(int row = 0; row < u_mat.rows; ++row){
    u_row = u_mat.ptr<uchar>(row);

    for(int col = 0; col < u_mat.cols*num_chan; ++col){
      fs << u_row[col] << " ";
    }
    fs << endl;
  }
  fs << endl;


  fs.close();

}

void save_a_bounding_box(const string& name, const Rect& bbox, const bool rewrite)
{
  //save format is as follows
  //x y width height
  //

  fstream fs;
  
  if(rewrite){
    fs.open(name.c_str(), fstream::out);
  }
  else{
    fs.open(name.c_str(), fstream::out | fstream::app);
  }

  if(!fs.is_open()){
    cerr << "fail to open " << name << " for saving." << endl;
    return;
  }

  fs << bbox.x << " " << bbox.y << " " << bbox.width << " " << bbox.height << endl;


  fs.close();

}

void load_weight_from_ssvm_output(const string& filename, vector<double>& w)
{
  fstream fs(filename.c_str(), fstream::in | fstream::binary);

  if(!fs.is_open()){
    cerr << "fail to open " << filename << " when loading weight." << endl;
    return;
  }
  
  int    ival;
  double dval;

  //load number of features
  fs.read( (char*)&ival, sizeof(int) );

  //load each weight
  w.clear();
  
  for(int i = 0; i < ival; ++i){
    fs.read( (char*)&dval, sizeof(double) );

    w.push_back(dval);
  }

  //done loading
  fs.close();

}

void load_vector_double(const string& filename, vector<double>& w)
{
  w.clear();

  fstream fs;

  fs.open(filename.c_str(), fstream::in);

  if(! fs.is_open() ){
    cerr << "Fail to open " << filename << endl;
    exit(-1);
  }

  string buffer;
  stringstream ss;
  string val;
  double dval;

  while(getline(fs, buffer)){
    ss.clear();
    ss << buffer;
    
    ss >> val;
    dval = atof(val.c_str());
    w.push_back(dval);

  }

  fs.close();

}

void load_bounding_boxes_from_file(const string& filename, vector<Rect>& bboxes)
{
  bboxes.clear();

  fstream fs;

  fs.open(filename.c_str(), fstream::in);

  if(! fs.is_open() ){
    cerr << "Fail to open " << filename << endl;
    exit(-1);
  }

  string buffer;
  stringstream ss;
  string val;
  double dval;

  while(getline(fs, buffer)){
    ss.clear();
    ss << buffer;
    
    Rect tmp;
    
    ss >> val;
    dval = atof(val.c_str());
    tmp.x = dval > 0 ? dval + 0.5 : dval;

    ss >> val;
    dval = atof(val.c_str());
    tmp.y = dval > 0 ? dval + 0.5 : dval;

    ss >> val;
    dval = atof(val.c_str());
    tmp.width = dval > 0 ? dval + 0.5 : dval;

    ss >> val;
    dval = atof(val.c_str());
    tmp.height = dval > 0 ? dval + 0.5 : dval;

    bboxes.push_back(tmp);

  }

  fs.close();
  

}

ostream& operator<<(ostream& os, const Rect& rect)
{
  os << rect.x << ',' << rect.y << ';' << rect.width << ',' << rect.height;
  return os;
}

string extract_first_token(const string& name, const string& pattern)
{
  return name.substr(0, name.find_first_of(pattern));
}

void load_all_data(const string& name, const ExpConfiguration* config, Mat& fimg, vector<Rect>& bboxes, Mat& mask, Mat& soft_mask, Mat& inv_mask, Mat& depth_img, Mat& recon_depth, Mat& stereo_depth, float& target_depth)
{
  //find tracklet name
  string target_name;

  target_name = name.substr(0, name.find_last_of("."));

  //find the gt filename
  string filename;

  filename = (*config).detectionDataBaseDir + (*config).trackletsDir + GT_FOLDER + "/" + target_name + (*config).imgExt;

  load_all_data(filename, fimg, bboxes, mask, soft_mask, inv_mask, depth_img, recon_depth, stereo_depth, target_depth);

}


string replace_string(string subject, const string& search, const string& replace) 
{
    size_t pos = 0;
    while((pos = subject.find(search, pos)) != std::string::npos) {
         subject.replace(pos, search.length(), replace);
         pos += replace.length();
    }
    return subject;
}

void load_all_data_and_star_energy(const string& name, Mat& fimg, vector<Rect>& bboxes, Mat& mask, Mat& soft_mask, Mat& inv_mask, Mat& depth_img, Mat& recon_depth, Mat& stereo_depth, float& frg_median_depth, Mat& s_pe, Mat& s_pi, const int num_star)
{
  //IMPLICITLY assume the name is the path + filename for the
  //ground truth image

  load_all_data(name, fimg, bboxes, mask, soft_mask, inv_mask, depth_img, recon_depth, stereo_depth, frg_median_depth);

  string search_pattern;
  string replace_pattern;
  string filename;

  int ext_pos;

  ext_pos = name.find_last_of(".");

  filename = name.substr(0, ext_pos) + "_pe_" + NumberToString(num_star) + ".bin";
  search_pattern = GT_FOLDER;
  replace_pattern = STAR_FOLDER;
  filename = replace_string(filename, search_pattern, replace_pattern);

  load_sparse_float_mat(filename, s_pe);

  search_pattern = "pe";
  replace_pattern = "pi";
  filename = replace_string(filename, search_pattern, replace_pattern);

  load_int_mat_bin(filename, s_pi);

} 

void load_all_data(const string& name, Mat& fimg, vector<Rect>& bboxes, Mat& mask, Mat& soft_mask, Mat& inv_mask, Mat& depth_img, Mat& recon_depth, Mat& stereo_depth, float& frg_median_depth)
{
  //IMPLICITLY assume the name is the path + filename for the
  //ground truth image

  string search_pattern;
  string replace_pattern;
  string filename;

  int ext_pos;

  ext_pos = name.find_last_of(".");

  ////read the image 
  search_pattern  = GT_FOLDER;
  replace_pattern = IMG_FOLDER;
  filename = replace_string(name, search_pattern, replace_pattern);
  
  //assume file extension is .png
  filename = filename.substr(0, filename.find_last_of("_")) + ".png";

  fimg = imread(filename, CV_LOAD_IMAGE_COLOR);

  if(!fimg.data){
    cerr << "Fail to load img " << filename << endl;
    return;
  }

  ///*test lab color
  //cvtColor(fimg, fimg, CV_BGR2Lab);
  //*/


  fimg.convertTo(fimg, CV_32F, 1.0/255);

  ////load roi and wb_roi
  filename = name.substr(0, ext_pos) + ".txt";  
  replace_pattern = ROI_FOLDER;
  filename = replace_string(filename, search_pattern, replace_pattern);
  
  //bboxes;  0:roi, 1:wb_roi
  load_bounding_boxes_from_file(filename, bboxes);
  
  ////load the mask
  replace_pattern = PC_FOLDER;
  filename = replace_string(name, search_pattern, replace_pattern);

  Mat tmp;
  tmp = imread(filename, CV_LOAD_IMAGE_GRAYSCALE);

  mask.create(fimg.size(), CV_8UC1);
  mask.setTo(Scalar(0));
  tmp.copyTo(mask(bboxes[1]));

  ////load the soft mask
  filename = name.substr(0, ext_pos) + ".bin";
  replace_pattern = SOFT_PC_FOLDER;
  filename = replace_string(filename, search_pattern, replace_pattern);
    
  load_float_mat_bin(filename, tmp);
  soft_mask.create(fimg.size(), CV_32FC1);
  soft_mask.setTo(Scalar(0));
  tmp.copyTo(soft_mask(bboxes[1]));

  ////load the inverse mask
  replace_pattern = INVERSE_MASK_FOLDER;
  filename = replace_string(name, search_pattern, replace_pattern);

  tmp = imread(filename, CV_LOAD_IMAGE_GRAYSCALE);

  inv_mask.create(fimg.size(), CV_8UC1);
  inv_mask.setTo(Scalar(0));
  tmp.copyTo(inv_mask(bboxes[0]));

  ////load the depth img
  filename = name.substr(0, ext_pos) + ".bin";
  replace_pattern = DEPTH_FOLDER;
  filename = replace_string(filename, search_pattern, replace_pattern);
  
  load_sparse_float_mat(filename, depth_img);

  ////load the frg_median_depth
  filename = name.substr(0, ext_pos) + "_median_depth.txt";
  replace_pattern = DEPTH_FOLDER;
  filename = replace_string(filename, search_pattern, replace_pattern);

  fstream fs(filename.c_str(), fstream::in);

  if(!fs.is_open()){
    cerr << "Fail to open " << filename << endl;
    exit(-1);
  }

  fs >> frg_median_depth;

  fs.close();
  
  ////load the precomputed recovered depth
  filename = name.substr(0, ext_pos) + "_" + NumberToString(RECOVER_DEPTH_WEIGHT) + ".bin";
  replace_pattern = RECON_DEPTH_FOLDER;
  filename = replace_string(filename, search_pattern, replace_pattern);

  load_float_mat_bin(filename, recon_depth);
  
  ////load the stereo depth img
  filename = name.substr(0, ext_pos) + ".bin";
  replace_pattern = STEREO_FOLDER;
  filename = replace_string(filename, search_pattern, replace_pattern);

  load_float_mat_bin(filename, stereo_depth);


}

void read_filenames(const string& filename, vector<string>& filenames)
{
  filenames.clear();

  fstream fs(filename.c_str(), fstream::in);

  if(!fs.is_open()){
    cerr << "Fail to open " << filename << endl;
    return;
  }

  string buffer;

  while(getline(fs, buffer)){
    filenames.push_back(buffer);
  }
  
  

  fs.close();

}

void combine_binary_energy(const Mat& pe1, const Mat& pi1, const Mat& pe2, const Mat& pi2, Mat& pe, Mat& pi)
{
  CV_Assert(pe1.type() == CV_32FC1);
  CV_Assert(pe2.type() == CV_32FC1);
  CV_Assert(pi1.type() == CV_32SC1);
  CV_Assert(pi2.type() == CV_32SC1);
  CV_Assert(pi1.rows == 2 && pi2.rows == 2);


  pe.create(2, pe1.cols+pe2.cols, CV_32FC1);
  pi.create(2, pi1.cols+pi2.cols, CV_32SC1);

  
  //iterate through pi1
  const int* pi1_row[2];
  const int* pi2_row[2];
  int* pi_row[2];

  for(int i = 0; i < 2; ++i){    
    pi1_row[i] = pi1.ptr<int>(i);
    pi2_row[i] = pi2.ptr<int>(i);
    pi_row[i]  = pi.ptr<int>(i);
  }

  //assume pe1 is un-driectional, while pe2 is directional
  const float* pe1_row;
  const float* pe2_row[2];
  float* pe_row[2];

  pe1_row = pe1.ptr<float>(0);
  for(int i = 0; i < 2; ++i){
    pe2_row[i] = pe2.ptr<float>(i);
    pe_row[i]  = pe.ptr<float>(i);
  }

  //start processing
  int start_ind, end_ind;

  map< pair<int, int>, int> pi1_table;
  map< pair<int, int>, int>::iterator it;
  
  pair<int, int> key;

  int count = 0;


  //iterate through pi1 and pe1
  for(int col = 0; col < pi1.cols; col++){
    start_ind = pi1_row[0][col];
    end_ind   = pi1_row[1][col];


    //create pi1_table mapping (start_ind,end_ind) to column index
    key = make_pair(start_ind, end_ind);
    
    //Strong Assumption:no need to check if the key exists
    pi1_table[key] = col;
    /* find if the key exits
    it = pi1_table.find(key);
    if( it == pi1_table.end() )
      pi1_table[key] = col;
    else{
      cerr << "map to the same val!" << endl;
      exit(-1);
    }
    */

    pi_row[0][col] = start_ind;
    pi_row[1][col] = end_ind;

    //pe1 is un-directional
    pe_row[0][col] = pe1_row[col];
    pe_row[1][col] = pe1_row[col];


  }

  count = pi1.cols;


  //iterate through pi2 and pe2
  int pi1_col_ind;

  map< pair<int, int>, int>::iterator r_it;

  pair<int, int> r_key;  //key of reverse order


  /*
   * BE CAREFUL: there are cases where the star shape model
   * output pi = -1!!. Do not consider those edges.
   */

  for(int col = 0; col < pi2.cols; col++){
    start_ind = pi2_row[0][col];
    end_ind   = pi2_row[1][col];

    //handle the bug case!
    if(start_ind < 0 || end_ind < 0){
      continue;
    }

    key  = make_pair(start_ind, end_ind);
    r_key = make_pair(end_ind, start_ind);


    //check key
    it = pi1_table.find(key);

    if( it == pi1_table.end() ){

      //check r_key
      r_it = pi1_table.find(r_key);

      if(r_it == pi1_table.end()){

	//cannot find both orders in PI1
        //PI2 has entry not in PI1, append it to PI
	pi_row[0][count] = start_ind;
	pi_row[1][count] = end_ind;
	//pe2 is directional
	pe_row[0][count] = pe2_row[0][col];
	pe_row[1][count] = pe2_row[1][col];

	count++;

      } 
      else{
	//find reverse order in PI1
	//PI2 has the same entry in PI1, accumulate the pe's
	pi1_col_ind = (*r_it).second;
      
	//note the order of pe2_row
	pe_row[0][pi1_col_ind] += pe2_row[1][col];
	pe_row[1][pi1_col_ind] += pe2_row[0][col];

      }
      
    }
    else{
      //find forward order in PI1
      //PI2 has the same entry in PI1, accumulate the pe's
      pi1_col_ind = (*it).second;

      pe_row[0][pi1_col_ind] += pe2_row[0][col];
      pe_row[1][pi1_col_ind] += pe2_row[1][col];

    }

  }


  //shrink pe, pi
  pe = pe(Range::all(), Range(0, count));
  pi = pi(Range::all(), Range(0, count));


}

void extract_expID_and_C(const string& path, string& exp_id, string& train_c)
{
  //assume folder encoded as map_expIDx_svmCx_/
  //
  string search_pattern;
  string sep_pattern = "_";  //separator
  size_t pos;
  size_t sep_pos;

  search_pattern = "expID";
  pos = path.find(search_pattern);
  sep_pos = path.find(sep_pattern, pos+1);

  //not include pattern name
  if(pos == string::npos)
    exp_id = "";
  else
    exp_id = path.substr(pos+search_pattern.length(), sep_pos-pos-search_pattern.length());


  search_pattern = "svmC";
  pos = path.find(search_pattern);
  sep_pos = path.find(sep_pattern, pos+1);

  if(pos == string::npos)
    train_c = "";
  else
    train_c = path.substr(pos+search_pattern.length(), sep_pos-pos-search_pattern.length());



}

void find_closest_element(const double query, const double* source, const int source_size, double* val, int *index)
{
  double diff;

  if(source_size < 1){
    return;
  }

  *val   = abs(source[0] - query);
  *index = 0;

  for(int i = 1; i < source_size; ++i){
    diff = abs(source[i] - query);

    if( (*val) > diff ){
      *val = diff;
      *index = i;
    }
  }

  
}
