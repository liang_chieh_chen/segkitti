/*
 * Perform inference for images
 *
 *
 *
 *
 * Liang-Chieh Chen@TTIC, August 2013
 *
 */


#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <sstream>
#include <cmath>
#include <cstdlib>
#include <stdio.h>

#include <dirent.h>
#include <fnmatch.h>

#include "tracklets.h"
#include "Constants.h"
#include "ContourCreater.h"
#include "ContrastSensitivePM.h"
#include "DepthInterpolater.h"
#include "DepthPotential.h"
#include "ExpConfiguration.h"
//#include "FastPDWrapper.h"
#include "GmmAppearance.h"
#include "HistAppearance.h"
#include "AlternateEnergySolver.h"
#include "IterativeEnergySolver.h"
#include "MaskCreater.h"
#include "StarShapeModel.h"
#include "Utility.h"

using namespace cv;
using namespace std;



int num_input_argument = 5;  //including the exe
//int num_feature        = 7;  //number of feature

//NOTE: TraverseDirectory and RemoveFiles are borrowed from Alexander Schwing's StructSVMCPnSlack.cpp
void TraverseDirectory(const string& path, const string& pattern, vector<string>& fileNames);
int RemoveFiles(vector<string>& fileNames, const char* fn);

static void help(char *argv[])
{
  cout << endl
       << "argv[1]: experiment setting (.xml)" << endl
       << "argv[2]: learned weight (.txt)"  << endl
       << "argv[3]: test image list (.txt)" << endl
       << "argv[4]: save folder path" << endl;

}

int main(int argc, char *argv[])
{
  if (argc != num_input_argument){
    help(argv);
    return -1;
  }
 
  int result;

  //read the experiment configuration
  string filename = argv[1];
  
  ExpConfiguration config;
    
  result = readExpConfigurationFromFile(filename, config);

  if(result == -1){
    cerr << "Failed to open " << filename << "in SingleImgGrabcutDemo" << endl;
    exit(-1);
  }

  //read the learned weight
  string weight_filename = argv[2];
  vector<double> w;

  // weight w is formated as below:
  load_weight_from_ssvm_output(weight_filename, w);

  ///* for debug
  cout << "learned weight" << endl;
  for(size_t i = 0; i < w.size(); ++i){
    cout << w[i] << ' ';
  }
  cout << endl;
  //*/
 
  //get test image list
  string test_filename = argv[3];
  vector<string> file_names;
  string path = config.detectionDataBaseDir + config.trackletsDir + GT_FOLDER + "/";
  string pattern = "*" + config.imgExt;

  TraverseDirectory(path, pattern, file_names);
  RemoveFiles(file_names, test_filename.c_str());

  if(file_names.size()==0) {
    cout << "No files to process." << endl;
    return -1;
  }


  cout << "NUM_STAR:" << NUM_STAR << endl;
  cout << "RECOVER_DEPTH_WEIGHT:" << RECOVER_DEPTH_WEIGHT << endl;

  
  //start to find the map for each image based on learned weight
  for(size_t i = 0; i < file_names.size(); ++i){
    string target_name;

    target_name = file_names[i].substr(0, file_names[i].find_last_of("."));


    //    if(target_name != "001072_tracklet3")
    //      continue;

    string img_name;

    img_name = extract_first_token(file_names[i], "_");

    ////load image, mask, roi and wb_roi
    Mat fimg, mask, soft_mask, inv_mask;
    Mat depth_img, recon_depth, stereo_depth;
    vector<Rect> bboxes;
    float target_depth;

    load_all_data(file_names[i], &config, fimg, bboxes, mask, soft_mask, inv_mask, depth_img, recon_depth, stereo_depth, target_depth);


    ////setup the apperance model
    AppearanceModel* app_model;
    
    int num_components = 5;    //number of mixture components
    int feat_size = 3;         //only rgb color is used
    
    if(USE_ORIG_MASK){
      app_model = new GmmAppearance(fimg, mask, bboxes[0], bboxes[1], num_components, feat_size);
    } else{
      app_model = new GmmAppearance(fimg, inv_mask, bboxes[0], bboxes[1], num_components, feat_size);
    }

    app_model->setup_save_path(config.resultDir);

    ////setup the simple depth potential
    DepthPotential* dep_model;

    dep_model = new DepthPotential();

    ////setup the binary model
    int nc = 8;  //4: 4-connected neighbors. 8: 8-connected neighbor

    ContrastSensitivePM* binary_model = new ContrastSensitivePM(bboxes[0], nc);

    ////setup star shape model
    filename = config.detectionDataBaseDir + config.trackletsDir + STAR_FOLDER + "/";
    StarShapeModel* star_model = new StarShapeModel(nc, NUM_STAR, filename);   

    ////setup recovered depth potential
    num_components = 2;
    feat_size = 1;
    
    DepthInterpolater* dep_int;

    if(USE_ORIG_MASK){
      dep_int = new DepthInterpolater(depth_img, mask, bboxes[0], num_components, feat_size);
    } else{
      dep_int = new DepthInterpolater(depth_img, inv_mask, bboxes[0], num_components, feat_size);
    }
    
    dep_int->setup_target_depth(target_depth);

    ////setup stereo depth potential
    num_components = 2;
    feat_size = 1;

    DepthInterpolater* dep_int_stereo;

    if(USE_ORIG_MASK){
      dep_int_stereo = new DepthInterpolater(stereo_depth, mask, bboxes[0], num_components, feat_size);
    } else {
      dep_int_stereo = new DepthInterpolater(stereo_depth, inv_mask, bboxes[0], num_components, feat_size);
    }

    ////setup cad shape contour
    bool use_dt;

    if(USE_AVG_SHAPE){
      use_dt = false;
      filename = config.detectionDataBaseDir + config.trackletsDir + AVG_SHAPE_FOLDER + "/" + file_names[i];
    } else {
      use_dt = true;
      filename = config.detectionDataBaseDir + config.trackletsDir + CAD_SHAPE_FOLDER + "/" + file_names[i];
    }

    ContourCreater* cc = new ContourCreater(filename, use_dt);

    ////setup Mechanical Turker shape contour
    vector<ContourCreater*> cc_m(3);

    filename = config.detectionDataBaseDir + config.trackletsDir + MT_SHAPE_FOLDER_B1 + "/" + file_names[i];
    cc_m[0] = new ContourCreater(filename);

    filename = config.detectionDataBaseDir + config.trackletsDir + MT_SHAPE_FOLDER_B2 + "/" + file_names[i];
    cc_m[1] = new ContourCreater(filename);

    filename = config.detectionDataBaseDir + config.trackletsDir + MT_SHAPE_FOLDER_B3 + "/" + file_names[i];
    cc_m[2] = new ContourCreater(filename);


    ////setup the energy solver
    int em_num_itr = 1;

    int gc_max_itr = 100;  //max number of iteratinos for graphcut

    CV_Assert(em_num_itr == 1);  //only support 1 itr


    EnergySolver* em = new IterativeEnergySolver(em_num_itr, target_name);

    string save_map_path = argv[4];

    em->setup_save_path(save_map_path);
    em->run(app_model, dep_model, dep_int, dep_int_stereo, binary_model, star_model, cc, cc_m, fimg, mask, soft_mask, recon_depth, stereo_depth, w, gc_max_itr);




    delete app_model;
    delete dep_model;
    delete binary_model;
    delete star_model;
    delete dep_int;
    delete cc;
    delete em;
    delete dep_int_stereo;

    for(size_t k = 0; k < cc_m.size(); ++k){
      delete cc_m[k];
    }

    //destroyAllWindows();

   

  }  //end iterate through images
   

  /*
    int result;

    //read the experiment configuration
    string filename = argv[1];
    ExpConfiguration config;
 
    result = readExpConfigurationFromFile(filename, config);

    if(result == -1){
    cerr << "Failed to open " << filename << endl;
    exit(-1);
    }

    //which camera to use
    int cam_id = 2;

    stringstream ss;
    string img_dir;

    ss << "image_" << setw(2) << setfill('0') << cam_id << "/data/";
    ss >> img_dir;

    string save_path = config.workBaseDir + config.outputDir;

    string velo_to_cam_filename, cam_to_cam_filename, velo_pc_filename, tracklet_filename;

    velo_to_cam_filename = config.dataBaseDir + "calib_velo_to_cam.txt";
    cam_to_cam_filename  = config.dataBaseDir + "calib_cam_to_cam.txt";
    tracklet_filename    = config.dataBaseDir + config.dataDir + "tracklet_labels.xml";

    //load tracklets
    int tracklet_id;

    ss.clear();
    ss << argv[2];
    ss >> tracklet_id;

    Tracklets* tracklets = new Tracklets();
    if(!tracklets->loadFromFile(tracklet_filename)){    
    cerr << "Fail to open " << tracklet_filename << endl;
    }

    //extract the tracklet specified by tracklet_id
    Tracklets::tTracklet* car = tracklets->getTracklet(tracklet_id);

    int img_id;

    ss.clear();
    ss << argv[3];
    ss >> img_id;

    //ss.clear();
    //ss << config.imgName;
    //ss >> img_id;

    //for(img_id = car->first_frame; img_id <= car->lastFrame(); img_id++)
    //for(img_id = car->first_frame; img_id <= 0; img_id++)
    {
    //read the image
    Mat img;

    string img_name;

    ss.clear();
    ss << setw(10) << setfill('0') << img_id;
    ss >> img_name;

    filename = config.dataBaseDir + config.dataDir + img_dir + img_name + config.imgExt;

    img = imread(filename, CV_LOAD_IMAGE_COLOR);

    if(!img.data){     
    cerr << "Failed to load img " << filename << endl;
    return -1;
    }

    Mat fimg;
    //convert to the image to float
    if( img.type() == CV_8UC3 || img.type() == CV_8UC1 ){
    img.convertTo(fimg, CV_32F, 1.0/255);
    }
    else{
    fimg = img;
    }

    imshow("img", img);
    filename = save_path + img_name + "_img" + SAVE_IMG_EXT;
    imwrite(filename, img);

    //create mask
    int expand_width = 2;
    double lambda_recover = 0.01;
    int recover_depth_method = 2;   //L1 norm or L2 norm

    velo_pc_filename = config.dataBaseDir + config.dataDir + config.veloDir + img_name + config.veloExt;
    
    MaskCreater* mc = new MaskCreater(velo_to_cam_filename, cam_to_cam_filename, velo_pc_filename, cam_id, expand_width);

    //bool use_recovered_depth = false;
    Mat mask(fimg.size(), CV_8UC1, Scalar(0));
    Rect roi, wb_roi;

    mc->create_mask(fimg, img_id, car, mask, roi, wb_roi);
    mc->setup_depth_var(lambda_recover, recover_depth_method);

    imshow("img-bx", img(roi));
    filename = save_path + img_name + "_imgbx" + SAVE_IMG_EXT;
    imwrite(filename, img(roi));
  
    //contour creater
    ContourCreater* cc = new ContourCreater();
    
    //setup the appearance model
    AppearanceModel* app_model;

    int app_model_type = 2;    //1:histogram, 2:GMM
    float lambda_bkg = atof(argv[4]);   //100;  //very useful to segment out camouflage
    float lambda_frg = atof(argv[5]);   // 10   //not that useful, since unary model (apperance) is strong enough

    if(app_model_type == 1){
    //use Histograms
    int num_bins = 16;

    app_model = new HistAppearance(fimg, mask, roi, wb_roi, num_bins, lambda_bkg, lambda_frg);
    } 
    else if(app_model_type == 2) {
    int num_components = 5;   //number of mixture components
    int feat_size = 3;        //only rgb color is used
    app_model = new GmmAppearance(fimg, mask, roi, wb_roi, num_components, feat_size, lambda_bkg, lambda_frg);
    } 
    else {
    cerr << "Wrong appearance model type!" << endl;
    exit(-1);
    }

    app_model->setup_save_path(save_path);

    //energy weight
    vector<double> lambda(4);
    lambda[0] = atof(argv[6]);   //1;   //ising prior weight
    lambda[1] = atof(argv[7]);   //40;  //weight for pairwise term
    lambda[2] = atof(argv[8]);   //0.5; //weight for depth unary term
    lambda[3] = atof(argv[9]);   //? :  //weight for contour
    //NOTE: pe = (lambda[0]+lambda[1]*exp(-||zi-zj||^2/beta) / dist(i,j)

    //setup the binary model
    int nc = 8;   //4 for 4-connected neighbors. 8 for 8-connected neighbor

    //potts model
    ContrastSensitivePM* binary_model = new ContrastSensitivePM(app_model->get_roi(), nc);

    //star shape
    bool use_star_shape = false;
    int num_frg_seeds = 20;
    StarShapeModel* star_model = new StarShapeModel(app_model->get_roi(), nc, num_frg_seeds);

  
    //setup energy solver
    int em_num_itr = 1;
    bool use_iterative_energy_solver = false;
    
    //max number of iterations to perform graph cut
    int gc_max_itr = 100;
     

    if(use_iterative_energy_solver){
    EnergySolver* em = new IterativeEnergySolver(em_num_itr, img_name, use_star_shape);
      
    em->setup_save_path(save_path);
    em->run(app_model, binary_model, star_model, fimg, mask, lambda, gc_max_itr);

    delete em;
    
    }
    else{
    AlternateEnergySolver* aes = new AlternateEnergySolver(em_num_itr, img_name);

    aes->setup_save_path(save_path);
    aes->run(app_model, binary_model, mc, cc, fimg, mask, lambda, gc_max_itr);

    delete aes;
    }
    //
    
    delete app_model;
    delete binary_model;
    delete star_model;
    
    }

    //show the results
    waitKey();

    //delete
    destroyAllWindows();
  
    delete tracklets;

  */

  return 0;
}


void TraverseDirectory(const string& path, const string& pattern, vector<string>& fileNames)
{
  DIR *dir, *tstdp;
  struct dirent *dp;

  //open the directory
  if((dir  = opendir(path.c_str())) == NULL) {
    cout << "Error opening " << path << endl;
    return;
  }

  int result;
  while ((dp = readdir(dir)) != NULL) {
    tstdp=opendir(dp->d_name);
		
    if(tstdp) {
      closedir(tstdp);
    } else {
      result = fnmatch(pattern.c_str(), dp->d_name, 0);

      if(result==0) {
	fileNames.push_back(dp->d_name);
	//cout << fileNames.back() << endl;
      }
    }
  }

  closedir(dir);
  return;

}

int RemoveFiles(vector<string>& fileNames, const char* fn) 
{
  ifstream ifs(fn, ios_base::in);
  string imageName;
  ifs >> imageName;
  vector<size_t> FoundIndices;
  while(imageName.size()>0) {
    string pattern = imageName.substr(0, imageName.find_last_of("."));
    for(size_t ix=0;ix<fileNames.size();++ix) {
#ifdef USE_ON_WINDOWS
      string tmp = fileNames[ix].substr(fileNames[ix].find_last_of("\\")+1, fileNames[ix].find_last_of(".") - fileNames[ix].find_last_of("\\") - 1);
#else
      string tmp = fileNames[ix].substr(fileNames[ix].find_last_of("/")+1, fileNames[ix].find_last_of(".") - fileNames[ix].find_last_of("/") - 1);
#endif
      if(tmp==pattern) {
	FoundIndices.push_back(ix);
	break;
      }
    }
    if(ifs.eof()) {
      break;
    }
    imageName = "";
    ifs >> imageName;
  }
  ifs.close();
  vector<string> tmp;
  for(size_t ix=0;ix<FoundIndices.size();++ix) {
    tmp.push_back(fileNames[FoundIndices[ix]]);
  }
  fileNames = tmp;
  return 0;
}
