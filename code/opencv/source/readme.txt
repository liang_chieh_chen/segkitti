1. Dependencies:

   To run the code, you need to have 
   1. OpenCV
   2. Boost 
   3. CPLEX (if you want to run Laplacian MRF)
   4. Eigen (if you want to run Gaussian MRF)
   5. gurobi (for parallel structured SVM) 

   See the provided Makefile for reference, and modify the paths correspondingly.
 
   Modify the folder paths in kittiConfig.xml.

2. compile the codes by 
  make

3. run the program as follows. See the bash files for examples.

  inference:  ./SingleGC kittiConfig.xml LEARNED_WEIGHT_FILE IMG_LIST
  training:   ./ProgNSlack -d ~/dataset/segkitti/tracklets/tracklet_gt -f ~/dataset/segkitti/tracklets/trainval_list.txt -o ../result/model/svm_model.dat -c 1 -i 10 -e 0
  evaluation: ./EvalScore kittiConfig.xml ~/dataset/segkitti/tracklets/test_list.txt -dataset scores.txt

