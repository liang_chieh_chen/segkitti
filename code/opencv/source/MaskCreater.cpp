#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <cmath>

#include "icpPointToPlane.h"
#include "icpPointToPoint.h"
#include "tracklets.h"
#include "Utility.h"
#include "MaskCreater.h"
#include "DepthInterpolater.h"
#include "Constants.h"

using namespace cv;
using namespace std;

bool write_point_clouds = false;

MaskCreater::MaskCreater(const string& v_c_file, const string& c_c_file, const string& v_p_file, const int c_id, const int e_width)
  : velo_to_cam_filename(v_c_file),
    cam_to_cam_filename(c_c_file),
    velo_pc_filename(v_p_file),
    camera_id(c_id),
    expand_width(e_width),
    lambda_dep(0.01),
    recover_method(2)
{}

MaskCreater::~MaskCreater()
{}

void MaskCreater::load_depth_from_velodyne(Mat& depth, const Mat& fimg)
{
  //create mask image from velodyne point cloud (camera id = cam_id)
  Mat velo_to_cam[4], K;

  //read calibration
  load_calib_velo_to_cam( velo_to_cam_filename, cam_to_cam_filename, velo_to_cam, K);
  
  string save_file_name;  
  

  ///////////////////////////////////////////////////////
  //compute 3d object rotation in velodyne coordinates///
  ///////////////////////////////////////////////////////
  //VELODYNE COORDINATE SYSTEM:
  //  x -> facing forward
  //  y -> facing left
  //  z -> facing up 

  //////load point clouds: in velodyne coordinate////////
  //load point clouds
  Mat velo_pc;
  load_velo_points(velo_pc_filename, velo_pc);

  //set up if want to clean the point clouds
  //bool do_clean_point_clouds = true;

  /*
    #if DEBUG == 1
    cout << "original pc size:" << velo_pc.size() << endl;
    #endif
  //*/

  /* remove the ground point clouds: 
  //1. avoid car p.c. leak to ground
  //2. but also remove informative background p.c. close to ground
                     
  if(do_clean_point_clouds){
  //remove_ground_point_clouds(velo_pc, bx3D);
  remove_ground_point_clouds(velo_pc);
  #if DEBUG == 1
  cout << "after removing ground point clouds, size:" << velo_pc.size() << endl;
  #endif
  }
  //*/

  remove_behind_point_clouds(velo_pc);

  /*
    #if DEBUG == 1
    cout << "after removing behind image plane point clouds, size:" << velo_pc.size() << endl;
    #endif
  //*/

  ///////////////////////////////////////////////////////
  //////tranforming to camera or image coordinate////////
  ///////////////////////////////////////////////////////
  //change coordinate from velodyne to camera for the 3D bounding box
  //camera coordinates:
  //dim 0: x (right positive), 
  //dim 1: y (down positive), 
  //dim 2: z (depth, point out)

  Mat pc_h;

  //change coordinate from velodyne to camera for point clouds
  vconcat(velo_pc, Mat::ones(1, velo_pc.cols, CV_32FC1), pc_h);
  rotate_translate_matrix(pc_h, velo_to_cam[camera_id], Mat(), pc_h);
  velo_pc = pc_h(Range(0, pc_h.rows-1), Range::all());

  //project the point clouds to image plane
  Mat pc_2D;
  project_points(K, velo_pc, pc_2D);

  //get depth values for every pixel
  depth.create(fimg.size(), CV_32FC1);
  depth.setTo(Scalar(0));
  
  read_depth_values(depth, pc_2D, velo_pc);

}

void MaskCreater::convert_img_to_points(const Mat& depth_img, Mat& points2D, Mat& depth_val, const Mat& mask)
{
  //will convert only those points specified by mask
  CV_Assert(depth_img.type() == CV_32FC1);

  Mat indicator;
  if(mask.empty()){
    indicator = Mat::ones(depth_img.size(), CV_8UC1);
  }else{
    CV_Assert(mask.type() == CV_8UC1);
    indicator = mask.clone();
  }

  int img_size = depth_img.rows * depth_img.cols;

  points2D.create(2, img_size, CV_32FC1);
  points2D.setTo(Scalar(0));

  depth_val.create(1, img_size, CV_32FC1);
  depth_val.setTo(Scalar(0));

  const float* img_row;

  float* points2D_row[2];
  points2D_row[0] = points2D.ptr<float>(0);
  points2D_row[1] = points2D.ptr<float>(1);

  float* depth_row;
  depth_row = depth_val.ptr<float>(0);

  int ind = 0;
  for(int row = 0; row < depth_img.rows; ++row){
    img_row = depth_img.ptr<float>(row);
    for(int col = 0; col < depth_img.cols; ++col){

      if(indicator.at<uchar>(row, col)){
	points2D_row[0][ind] = col;
	points2D_row[1][ind] = row;
	depth_row[ind]       = img_row[col];
	ind++;
      }

    }
  }

  points2D = points2D(Range::all(), Range(0, ind));
  depth_val = depth_val(Range::all(), Range(0, ind));


}

void MaskCreater::convert_points_to_img_binary(const Mat& depth_img, const Mat& points2D, const vector<bool>& frg_ind, Mat& inverse_mask)
{
  CV_Assert(depth_img.type() == CV_32FC1);
  CV_Assert(points2D.type() == CV_32FC1);
  CV_Assert((int)frg_ind.size() == points2D.cols);

  inverse_mask.create(depth_img.size(), CV_8UC1);
  inverse_mask.setTo(Scalar(0));

  const float* points_row[2];
  points_row[0] = points2D.ptr<float>(0);
  points_row[1] = points2D.ptr<float>(1);

  int x, y;

  for(int col = 0; col < points2D.cols; ++col){
    x = points_row[0][col];
    y = points_row[1][col];

    if(frg_ind[col]){
      inverse_mask.at<uchar>(y,x) = 255;
    } else{
      inverse_mask.at<uchar>(y,x) = 0;
    }
    
  }

  

}

void MaskCreater::create_inverse_mask(const string& tracklet_name, const Mat& depth_img, const int img_id, const Rect& roi, const Tracklets::tTracklet* car, Mat& inverse_mask)
{
  //create mask image from velodyne point cloud (camera id = cam_id)
  Mat velo_to_cam[4], K;

  //read calibration
  load_calib_velo_to_cam( velo_to_cam_filename, cam_to_cam_filename, velo_to_cam, K);
  
  string save_file_name;  
  
  Mat bx3D;  //3D bounding box coordinates
  get_3D_boundingbox_coordinate(car->h, car->w, car->l, bx3D);


  int pose_idx;

  pose_idx = img_id - car->first_frame;

  Mat tracklet_T(3, 1, CV_32FC1);
  tracklet_T.at<float>(0) = car->poses[pose_idx].tx;
  tracklet_T.at<float>(1) = car->poses[pose_idx].ty;
  tracklet_T.at<float>(2) = car->poses[pose_idx].tz;
 
  double tracklet_rz = wrap_to_pi(car->poses[pose_idx].rz);

  ///////////////////////////////////////////////////////
  //compute 3d object rotation in velodyne coordinates///
  ///////////////////////////////////////////////////////
  //VELODYNE COORDINATE SYSTEM:
  //  x -> facing forward
  //  y -> facing left
  //  z -> facing up 
  Mat tracklet_R(3, 3, CV_32FC1);
  tracklet_R.at<float>(0,0) = cos(tracklet_rz); tracklet_R.at<float>(0,1) = -sin(tracklet_rz); tracklet_R.at<float>(0,2) = 0;
  tracklet_R.at<float>(1,0) = sin(tracklet_rz); tracklet_R.at<float>(1,1) =  cos(tracklet_rz); tracklet_R.at<float>(1,2) = 0;
  tracklet_R.at<float>(2,0) = 0;                tracklet_R.at<float>(2,1) = 0;                 tracklet_R.at<float>(2,2) = 1;

  //get the 2D points from input depth_img
  Mat points2D, depth_val;

  convert_img_to_points(depth_img, points2D, depth_val);

  //the x, y positions are shifted by roi
  points2D.row(0) = points2D.row(0) + roi.x;
  points2D.row(1) = points2D.row(1) + roi.y;


  //inverse projection of points2D from image plane to camera coordinate
  Mat points3D(points2D.size(), points2D.type());

  for(int i = 0; i < points2D.rows; ++i){
    points3D.row(i) = depth_val.mul(points2D.row(i));
  }
  vconcat(points3D, depth_val, points3D);

  points3D = K.inv() * points3D;

  ///* writing out points in camera coordinate for debug
  string save_path = "../result/matching/";
  fstream fs;
  string filename;

  if(write_point_clouds){
    filename = save_path + tracklet_name + "_points3D_recon.txt";
    fs.open(filename.c_str(), fstream::out);

    for(int row = 0; row < points3D.rows; ++row){
      for(int col = 0; col < points3D.cols; ++col){
	fs << points3D.at<float>(row,col) << ' ';
      }
    }

    fs << endl;

    fs.close();
  }
  Mat points3D_cam = points3D.clone();
  //*/


  //cout << "i: inv p3d in cam: " << points3D << endl;

  //transform the points3D from camera coordinate to Velodyne coordinate
  vconcat(points3D, Mat::ones(1, points3D.cols, CV_32FC1), points3D);
  points3D = velo_to_cam[camera_id].inv() * points3D;
  points3D = points3D(Range(0, points3D.rows-1), Range::all());

  //cout << "v to cam:" << velo_to_cam[camera_id] << endl;

  //cout << "i: p3d in vel:" << points3D << endl;

  /*
    for(int c = 14000; c < 14020; ++c){
    cout << points3D.at<float>(0,c) << ' ' << points3D.at<float>(1,c) << ' ' << points3D.at<float>(2,c) << endl;
    }
  */

  //get the bx3D in velodyne coordinate
  rotate_translate_matrix(bx3D, tracklet_R, tracklet_T, bx3D);

  //cout << "i: b3d in vel:" << bx3D << endl;


  //find which points are within the 3D boundng box
  vector<bool> frg_pc_indicator;
  Mat dist_pc_to_3Dbbox;

  bool remove_car_ground = false;

  find_pc_within_3Dbx(frg_pc_indicator, dist_pc_to_3Dbbox, points3D, bx3D, remove_car_ground, tracklet_R, tracklet_T);

  //shift the positions back by roi
  points2D.row(0) = points2D.row(0) - roi.x;
  points2D.row(1) = points2D.row(1) - roi.y;

  
  //convert points to images
  convert_points_to_img_binary(depth_img, points2D, frg_pc_indicator, inverse_mask);


  ///* writing out points in camera coordinate for debug
  if(write_point_clouds){
    filename = save_path + tracklet_name + "_points3D_recon_car.txt";
    fs.open(filename.c_str(), fstream::out);

    for(int row = 0; row < points3D.rows; ++row){
      for(int col = 0; col < points3D.cols; ++col){
	if(frg_pc_indicator[col])
	  fs << points3D_cam.at<float>(row,col) << ' ';
      }
    }

    fs << endl;

    fs.close();
  }
  //*/



}


void MaskCreater::rotate_translate_maski_to_maskj_icp(const string& tracklet_name, const Tracklets::tTracklet* car, const Mat& mask_i, const Mat& mask_j, const Mat& depth_img_i, const Mat& depth_img_j, const Rect& roi_i, const Rect& roi_j, const int img_id_i, const int img_id_j, const Mat& img_i, const Mat& img_j, Mat& rectified_mask, Mat& rectified_depth_img)
{

  const Mat mask_roi_i      = mask_i(roi_i);
  const Mat depth_img_roi_i = depth_img_i(roi_i);

  const Mat mask_roi_j      = mask_j(roi_j);
  const Mat depth_img_roi_j = depth_img_j(roi_j);

  //create mask image from velodyne point cloud (camera id = cam_id)
  Mat velo_to_cam[4], K;

  //read calibration
  load_calib_velo_to_cam( velo_to_cam_filename, cam_to_cam_filename, velo_to_cam, K);

  //get the indicator from mask. will only work on those points
  Mat indicator_i(mask_roi_i.size(), mask_roi_i.type(), Scalar(0));

  uchar* ind_row;
  const uchar* mask_roi_row;
  for(int row = 0; row < indicator_i.rows; ++row){
    ind_row = indicator_i.ptr<uchar>(row);
    mask_roi_row = mask_roi_i.ptr<uchar>(row);

    for(int col = 0; col < indicator_i.cols; ++col){
      if(mask_roi_row[col] == FOREGROUND_LABEL ){  //|| mask_roi_row[col] == STRICT_BACKGROUND_LABEL){
	ind_row[col] = 1;
      }
    }
  }

  Mat indicator_j(mask_roi_j.size(), mask_roi_j.type(), Scalar(0));

  for(int row = 0; row < indicator_j.rows; ++row){
    ind_row = indicator_j.ptr<uchar>(row);
    mask_roi_row = mask_roi_j.ptr<uchar>(row);

    for(int col = 0; col < indicator_j.cols; ++col){
      if(mask_roi_row[col] == FOREGROUND_LABEL ){  //|| mask_roi_row[col] == STRICT_BACKGROUND_LABEL){
	ind_row[col] = 1;
      }
    }
  }

  //get the 2D points from depth_img_i
  Mat points2D_i, depth_val_i;

  convert_img_to_points(depth_img_roi_i, points2D_i, depth_val_i, indicator_i);

  //the x, y positions are shifted by roi
  points2D_i.row(0) = points2D_i.row(0) + roi_i.x;
  points2D_i.row(1) = points2D_i.row(1) + roi_i.y;

  //get the 2D points from depth_img_j
  Mat points2D_j, depth_val_j;

  convert_img_to_points(depth_img_roi_j, points2D_j, depth_val_j, indicator_j);

  //the x, y positions are shifted by roi
  points2D_j.row(0) = points2D_j.row(0) + roi_j.x;
  points2D_j.row(1) = points2D_j.row(1) + roi_j.y;

  //get the 3D points for ith frame
  //inverse projection of points2D_i from image plane to camera coordinate
  Mat points3D_i(points2D_i.size(), points2D_i.type());

  for(int p = 0; p < points2D_i.rows; ++p){
    points3D_i.row(p) = depth_val_i.mul(points2D_i.row(p));
  }
  vconcat(points3D_i, depth_val_i, points3D_i);

  points3D_i = K.inv() * points3D_i;

  //get the 3D points for jth frame
  Mat points3D_j(points2D_j.size(), points2D_j.type());

  for(int p = 0; p < points2D_j.rows; ++p){
    points3D_j.row(p) = depth_val_j.mul(points2D_j.row(p));
  }
  vconcat(points3D_j, depth_val_j, points3D_j);

  points3D_j = K.inv() * points3D_j;

  ////find the R, T for fitting points3D_j by points3D_i
  ////i.e., points3D_j = R*ponits3D_i + T
  ////will transform only the inliers

  //copy values to fit the format of libicp
  int dim = 3;
  int num_templ = points3D_i.cols;
  int num_model = points3D_j.cols;
  double* templ = (double*)calloc(dim * num_templ, sizeof(double)); 
  double* model = (double*)calloc(dim * num_model, sizeof(double));

  float* points3D_row[3];

  for(int p = 0; p < 3; ++p)
    points3D_row[p] = points3D_i.ptr<float>(p);

  for(int col = 0; col < points3D_i.cols; ++col){
    for(int row = 0; row < 3; ++row){
      templ[col*dim+row] = (double)points3D_row[row][col];
    }
  }

  for(int p = 0; p < 3; ++p)
    points3D_row[p] = points3D_j.ptr<float>(p);

  for(int col = 0; col < points3D_j.cols; ++col){
    for(int row = 0; row < 3; ++row){
      model[col*dim+row] = (double)points3D_row[row][col];
    }
  }

  ///* writing out points in camera coordinate for debug
  string save_path = "../result/matching/";
  fstream fsi, fsj;
  string filename;

  if(write_point_clouds){
    filename = save_path + tracklet_name + "_points3D_i.txt";
    fsi.open(filename.c_str(), fstream::out);
  
    filename = save_path + tracklet_name + "_points3D_j.txt";
    fsj.open(filename.c_str(), fstream::out);

    for(int row = 0; row < points3D_i.rows; ++row){
      for(int col = 0; col < points3D_i.cols; ++col){
	fsi << points3D_i.at<float>(row,col) << ' ';
      }
    }

    fsi << endl;

    for(int row = 0; row < points3D_j.rows; ++row){
      for(int col = 0; col < points3D_j.cols; ++col){
	fsj << points3D_j.at<float>(row,col) << ' ';
      }
    }

    fsj << endl;


    fsi.close();
    fsj.close();
  }
  //*/

  //cout << points3D_i << endl;
  //cout << points3D_j << endl;

 
  //initial transformation
  Matrix R = Matrix::eye(3);
  Matrix t(3,1);

  ///*//use tracklet informatino for the initialization
  int pose_idx_i, pose_idx_j;
  pose_idx_i = img_id_i - car->first_frame;
  pose_idx_j = img_id_j - car->first_frame;

  t.val[0][0] = car->poses[pose_idx_j].tx - car->poses[pose_idx_j].tx;
  t.val[1][0] = car->poses[pose_idx_j].ty - car->poses[pose_idx_j].ty;
  t.val[2][0] = car->poses[pose_idx_j].tz - car->poses[pose_idx_j].tz;

  double tracklet_rz;
  tracklet_rz = wrap_to_pi(car->poses[pose_idx_j].rz) - wrap_to_pi(car->poses[pose_idx_i].rz);
  R.val[0][0] = cos(tracklet_rz); R.val[0][1] = -sin(tracklet_rz);
  R.val[1][0] = sin(tracklet_rz); R.val[1][1] = cos(tracklet_rz);  
  //*/


  //inlier distance
  double inlier_dist = -1;  //unit: meter. <=0: use all points
  vector<int> active;

  //cout << "1" << endl;

  //IcpPointToPlane icp(model, num_model, dim);  //it will crash if use this class
  IcpPointToPoint icp(model, num_model, dim);

  icp.fit(templ, num_templ, R, t, inlier_dist);

  //cout << "2" << endl;
  //cout << "R:" << R << endl << "T:" << t << endl;
  //active = icp.getInliers(templ, num_templ, R, t, inlier_dist);
  //cout << "a:" << active.size() << endl;

  //get the inliers
  //inlier_dist = 1;

  active = icp.getInliers(templ, num_templ, R, t, inlier_dist);
  //cout << "aa:" << active.size() << endl;
  
  if(active.size() == 0){  //0 means use all
    for(int k = 0; k < num_templ; ++k){
      active.push_back(k);
    }
  }


  free(model);
  free(templ);

  ////transform the format
  Mat tracklet_T(3, 1, CV_32FC1);
  Mat tracklet_R(3, 3, CV_32FC1);

  for(int p = 0; p < 3; ++p)
    tracklet_T.at<float>(p) = t.val[p][0];
  
  for(int p = 0; p < 3; ++p)
    for(int q = 0; q < 3; ++q)
      tracklet_R.at<float>(p,q) = R.val[p][q];


  //transform from frame i to j
  Mat points3D_i_to_j;
  rotate_translate_matrix(points3D_i_to_j, tracklet_R, tracklet_T, points3D_i);


  ///*save result in camera coordinate for debuging
  if(write_point_clouds){
    filename = save_path + tracklet_name + "_points3D_i_to_j.txt";
    fsj.open(filename.c_str(), fstream::out);

    for(int row = 0; row < points3D_i_to_j.rows; ++row){
      for(int col = 0; col < points3D_i_to_j.cols; ++col){
	fsj << points3D_i_to_j.at<float>(row,col) << ' ';
      }
    }
    fsj << endl;

    fsj.close();
  }
  //*/



  ////project to image plane
  Mat points2D_i_to_j;
  project_points(K, points3D_i_to_j, points2D_i_to_j);

  //organize output
  rectified_mask.create(mask_i.size(), CV_8UC1);
  rectified_mask.setTo(Scalar(UNDEFINED_LABEL));
  rectified_depth_img.create(depth_img_i.size(), CV_32FC1);
  rectified_depth_img.setTo(Scalar(0));

  float* points2D_i_row[2];
  float* points2D_i_to_j_row[2];

  for(int k = 0; k<2; ++k){
    points2D_i_row[k] = points2D_i.ptr<float>(k);
    points2D_i_to_j_row[k] = points2D_i_to_j.ptr<float>(k);
  }

  int x_i, y_i, x_j, y_j;
  uchar mask_label;
  float depth;

  for(size_t p = 0; p < active.size(); ++p){
    int col = active[p];

    x_i = points2D_i_row[0][col];
    y_i = points2D_i_row[1][col];

    x_j = points2D_i_to_j_row[0][col];
    y_j = points2D_i_to_j_row[1][col];

    if(is_within_image_plane(x_i, y_i, mask_i.rows, mask_i.cols) && 
       is_within_image_plane(x_j, y_j, rectified_mask.rows, rectified_mask.cols)){
    
      if(!is_matching_outlier( img_i, img_j, Point2i(x_i, y_i), Point2i(x_j, y_j) )) {
      
	mask_label = mask_i.at<uchar>(y_i, x_i);
	rectified_mask.at<uchar>(y_j, x_j) = mask_label;

	depth = depth_img_i.at<float>(y_i, x_i);
	rectified_depth_img.at<float>(y_j, x_j) = depth;

      }
    }
  }

  //imshow("rec m", rectified_mask);
  //waitKey();
}



void MaskCreater::rotate_translate_maski_to_maskj(const string& tracklet_name, const Tracklets::tTracklet* car, const Mat& mask_i, const Mat& depth_img_i, const Rect& roi_i, const Rect& roi_j, const int img_id_i, const int img_id_j, const Mat& img_i, const Mat& img_j, Mat& rectified_mask, Mat& rectified_depth_img)
{
  const Mat mask_roi_i      = mask_i(roi_i);
  const Mat depth_img_roi_i = depth_img_i(roi_i);

  //create mask image from velodyne point cloud (camera id = cam_id)
  Mat velo_to_cam[4], K;

  //read calibration
  load_calib_velo_to_cam( velo_to_cam_filename, cam_to_cam_filename, velo_to_cam, K);

  ////get T, R for frame i
  int pose_idx;

  pose_idx = img_id_i - car->first_frame;

  Mat tracklet_T_i(3, 1, CV_32FC1);
  tracklet_T_i.at<float>(0) = car->poses[pose_idx].tx;
  tracklet_T_i.at<float>(1) = car->poses[pose_idx].ty;
  tracklet_T_i.at<float>(2) = car->poses[pose_idx].tz;
 
  double tracklet_rz;
  tracklet_rz = wrap_to_pi(car->poses[pose_idx].rz);

  ///////////////////////////////////////////////////////
  //compute 3d object rotation in velodyne coordinates///
  ///////////////////////////////////////////////////////
  //VELODYNE COORDINATE SYSTEM:
  //  x -> facing forward
  //  y -> facing left
  //  z -> facing up 
  Mat tracklet_R_i(3, 3, CV_32FC1);
  tracklet_R_i.at<float>(0,0) = cos(tracklet_rz); tracklet_R_i.at<float>(0,1) = -sin(tracklet_rz); tracklet_R_i.at<float>(0,2) = 0;
  tracklet_R_i.at<float>(1,0) = sin(tracklet_rz); tracklet_R_i.at<float>(1,1) =  cos(tracklet_rz); tracklet_R_i.at<float>(1,2) = 0;
  tracklet_R_i.at<float>(2,0) = 0;                tracklet_R_i.at<float>(2,1) = 0;                 tracklet_R_i.at<float>(2,2) = 1;

  ////get T, R for frame j
  pose_idx = img_id_j - car->first_frame;

  Mat tracklet_T_j(3, 1, CV_32FC1);
  tracklet_T_j.at<float>(0) = car->poses[pose_idx].tx;
  tracklet_T_j.at<float>(1) = car->poses[pose_idx].ty;
  tracklet_T_j.at<float>(2) = car->poses[pose_idx].tz;
 
  tracklet_rz = wrap_to_pi(car->poses[pose_idx].rz);

  Mat tracklet_R_j(3, 3, CV_32FC1);
  tracklet_R_j.at<float>(0,0) = cos(tracklet_rz); tracklet_R_j.at<float>(0,1) = -sin(tracklet_rz); tracklet_R_j.at<float>(0,2) = 0;
  tracklet_R_j.at<float>(1,0) = sin(tracklet_rz); tracklet_R_j.at<float>(1,1) =  cos(tracklet_rz); tracklet_R_j.at<float>(1,2) = 0;
  tracklet_R_j.at<float>(2,0) = 0;                tracklet_R_j.at<float>(2,1) = 0;                 tracklet_R_j.at<float>(2,2) = 1;

  //get the indicator from mask. will only work on those points
  Mat indicator(mask_roi_i.size(), mask_roi_i.type(), Scalar(0));

  uchar* ind_row;
  const uchar* mask_roi_row;
  for(int row = 0; row < indicator.rows; ++row){
    ind_row = indicator.ptr<uchar>(row);
    mask_roi_row = mask_roi_i.ptr<uchar>(row);

    for(int col = 0; col < indicator.cols; ++col){
      if(mask_roi_row[col] == STRICT_BACKGROUND_LABEL || mask_roi_row[col] == FOREGROUND_LABEL){
	ind_row[col] = 1;
      }
    }
  }

  //get the 2D points from depth_img_i
  Mat points2D_i, depth_val_i;

  convert_img_to_points(depth_img_roi_i, points2D_i, depth_val_i, indicator);


  //the x, y positions are shifted by roi
  points2D_i.row(0) = points2D_i.row(0) + roi_i.x;
  points2D_i.row(1) = points2D_i.row(1) + roi_i.y;


  //get the 3D points for ith frame
  //inverse projection of points2D_i from image plane to camera coordinate
  Mat points3D_i(points2D_i.size(), points2D_i.type());

  for(int i = 0; i < points2D_i.rows; ++i){
    points3D_i.row(i) = depth_val_i.mul(points2D_i.row(i));
  }
  vconcat(points3D_i, depth_val_i, points3D_i);

  points3D_i = K.inv() * points3D_i;

  //shift the positions back by roi
  //points2D_i.row(0) = points2D_i.row(0) - roi_i.x;
  //points2D_i.row(1) = points2D_i.row(1) - roi_i.y;

  //transform the points3D from camera coordinate to Velodyne coordinate
  vconcat(points3D_i, Mat::ones(1, points3D_i.cols, CV_32FC1), points3D_i);
  points3D_i = velo_to_cam[camera_id].inv() * points3D_i;
  points3D_i = points3D_i(Range(0, points3D_i.rows-1), Range::all());


  //get to object frame for i
  Mat points3D_i_orig;
  inverse_rotate_translate_matrix(points3D_i_orig, tracklet_R_i, tracklet_T_i, points3D_i);

  //get to velodyne frame for j
  Mat points3D_j;
  rotate_translate_matrix(points3D_j, tracklet_R_j, tracklet_T_j, points3D_i_orig);


  ////project to image plane j
  //change to camera coordinate
  Mat pc_h;
  vconcat(points3D_j, Mat::ones(1, points3D_j.cols, CV_32FC1), pc_h);
  rotate_translate_matrix(pc_h, velo_to_cam[camera_id], Mat(), pc_h);
  points3D_j = pc_h(Range(0, pc_h.rows-1), Range::all());

  Mat points2D_j;
  project_points(K, points3D_j, points2D_j);

  //organize output
  rectified_mask.create(mask_i.size(), CV_8UC1);
  rectified_mask.setTo(Scalar(UNDEFINED_LABEL));
  rectified_depth_img.create(depth_img_i.size(), CV_32FC1);
  rectified_depth_img.setTo(Scalar(0));

  float* points2D_i_row[2];
  float* points2D_j_row[2];

  for(int k = 0; k<2; ++k){
    points2D_i_row[k] = points2D_i.ptr<float>(k);
    points2D_j_row[k] = points2D_j.ptr<float>(k);
  }

  int x_i, y_i, x_j, y_j;
  uchar mask_label;
  float depth;

  for(int col = 0; col < points2D_j.cols; ++col){
    x_i = points2D_i_row[0][col];
    y_i = points2D_i_row[1][col];

    x_j = points2D_j_row[0][col];
    y_j = points2D_j_row[1][col];

    if(is_within_image_plane(x_i, y_i, mask_i.rows, mask_i.cols) && 
       is_within_image_plane(x_j, y_j, rectified_mask.rows, rectified_mask.cols)){
    
      if(!is_matching_outlier( img_i, img_j, Point2i(x_i, y_i), Point2i(x_j, y_j) )) {
      
	mask_label = mask_i.at<uchar>(y_i, x_i);
	rectified_mask.at<uchar>(y_j, x_j) = mask_label;

	depth = depth_img_i.at<float>(y_i, x_i);
	rectified_depth_img.at<float>(y_j, x_j) = depth;

      }
    }
  }


}

bool MaskCreater::is_matching_outlier(const Mat& img_i, const Mat& img_j, const Point2i& p_i, const Point2i& p_j)
{
  //p_i is matched to p_j. if the color difference is huge, we consider the matching as outlier
  CV_Assert(img_i.type() == CV_32FC3);
  CV_Assert(img_j.type() == CV_32FC3);

  //the tolerance difference for each color channel
  double color_tolerance = 10.0 / 255.0;  
  double threshold       = color_tolerance*color_tolerance*3;  //for three color channels

  Vec3f color_i = img_i.at<Vec3f>(p_i.y, p_i.x);
  Vec3f color_j = img_j.at<Vec3f>(p_j.y, p_j.x);

  Vec3f color_diff = color_i - color_j;

  double diff = color_diff.dot(color_diff);
  
  if(diff > threshold){
    return true;
  }
  else{
    return false;
  }

}

int MaskCreater::create_mask(const Mat& fimg, const int img_id, const Tracklets::tTracklet* car, Mat& mask, Rect& roi, Rect& wb_roi)
{
  int frg_size = 0;

  //create mask image from velodyne point cloud (camera id = cam_id)
  Mat velo_to_cam[4], K;

  //read calibration
  load_calib_velo_to_cam( velo_to_cam_filename, cam_to_cam_filename, velo_to_cam, K);
  
  string save_file_name;  
  
  Mat bx3D;  //3D bounding box coordinates
  get_3D_boundingbox_coordinate(car->h, car->w, car->l, bx3D);


  int pose_idx;

  pose_idx = img_id - car->first_frame;

  Mat tracklet_T(3, 1, CV_32FC1);
  tracklet_T.at<float>(0) = car->poses[pose_idx].tx;
  tracklet_T.at<float>(1) = car->poses[pose_idx].ty;
  tracklet_T.at<float>(2) = car->poses[pose_idx].tz;
 
  double tracklet_rz = wrap_to_pi(car->poses[pose_idx].rz);

  ///////////////////////////////////////////////////////
  //compute 3d object rotation in velodyne coordinates///
  ///////////////////////////////////////////////////////
  //VELODYNE COORDINATE SYSTEM:
  //  x -> facing forward
  //  y -> facing left
  //  z -> facing up 
  Mat tracklet_R(3, 3, CV_32FC1);
  tracklet_R.at<float>(0,0) = cos(tracklet_rz); tracklet_R.at<float>(0,1) = -sin(tracklet_rz); tracklet_R.at<float>(0,2) = 0;
  tracklet_R.at<float>(1,0) = sin(tracklet_rz); tracklet_R.at<float>(1,1) =  cos(tracklet_rz); tracklet_R.at<float>(1,2) = 0;
  tracklet_R.at<float>(2,0) = 0;                tracklet_R.at<float>(2,1) = 0;                 tracklet_R.at<float>(2,2) = 1;

  //get the bx3D in velodyne coordinate
  rotate_translate_matrix(bx3D, tracklet_R, tracklet_T, bx3D);


  //////load point clouds: in velodyne coordinate////////
  //load point clouds
  Mat velo_pc;
  load_velo_points(velo_pc_filename, velo_pc);

  //set up if want to clean the point clouds
  bool do_clean_point_clouds = true;

  /*
    #if DEBUG == 1
    cout << "original pc size:" << velo_pc.size() << endl;
    #endif
  //*/

  /* remove the ground point clouds: 
  //1. avoid car p.c. leak to ground
  //2. but also remove informative background p.c. close to ground
                     
  if(do_clean_point_clouds){
  //remove_ground_point_clouds(velo_pc, bx3D);
  remove_ground_point_clouds(velo_pc);
  #if DEBUG == 1
  cout << "after removing ground point clouds, size:" << velo_pc.size() << endl;
  #endif
  }
  //*/

  remove_behind_point_clouds(velo_pc);

  /*
    #if DEBUG == 1
    cout << "after removing behind image plane point clouds, size:" << velo_pc.size() << endl;
    #endif
  //*/

  //get foreground point clouds indicator
  //extract foreground first, because after changing the 
  //coordinate from velodyne to camera, some
  //ground point clouds leak to the 3D bounding box and are
  //considered as car ponit clouds due to computation inaccuracy
  //  
  vector<bool> frg_pc_indicator;
  Mat dist_pc_to_3Dbbox;

  bool remove_car_ground = false;
  //find_pc_within_3Dbx(frg_pc_indicator, velo_pc, bx3D, remove_car_ground);

  find_pc_within_3Dbx(frg_pc_indicator, dist_pc_to_3Dbbox, velo_pc, bx3D, remove_car_ground, tracklet_R, tracklet_T);

  //main idea:
  //1. project all point clouds to image plane;
  //   work on point clouds within the 2D bounding box
  //2. find out which are foreground point clouds
  //3. find out which are background point clouds 

  ///////////////////////////////////////////////////////
  //////tranforming to camera or image coordinate////////
  ///////////////////////////////////////////////////////
  //change coordinate from velodyne to camera for the 3D bounding box
  //camera coordinates:
  //dim 0: x (right positive), 
  //dim 1: y (down positive), 
  //dim 2: z (depth, point out)

  //cout << "m:bx3d in vel:" << bx3D << endl;

  Mat pc_h;
  vconcat(bx3D, Mat::ones(1, bx3D.cols, CV_32FC1), pc_h);
  rotate_translate_matrix(pc_h, velo_to_cam[camera_id], Mat(), pc_h);
  bx3D = pc_h(Range(0, pc_h.rows-1), Range::all());


  //cout << "v to cam:" << velo_to_cam[camera_id] << endl;
  //cout << "m:bx3d in cam:" << bx3D << endl;


  /* for debug
     for(int col = 0; col < bx3D.cols; col++){
     for(int row = 0; row < bx3D.rows; row++){
     cout << bx3D.at<float>(row, col) << ",";
     }
     cout << endl;
     }
  */

  //project the 3D bounding box to image plane
  Mat bx2D;  //bx2D has size 2xnum_element
  project_points(K, bx3D, bx2D);

  ///* debugging back projection from image to camera
  Mat point_2D;
  point_2D = bx2D.clone();
  Mat depth_2D;

  depth_2D = bx3D.row(2).clone();


  for(int r = 0; r < point_2D.rows; ++r)
    point_2D.row(r) = depth_2D.mul(point_2D.row(r));

  vconcat(point_2D, depth_2D, point_2D);

  Mat point_3D;
  point_3D = K.inv() * point_2D;


  //*/

  
  /* for debug
     cout << "bx2Ds" << endl;
     for(int col = 0; col < bx2D.cols; col++){
     for(int row = 0; row < bx2D.rows; row++){
     cout << bx2D.at<float>(row,col) << ",";
     }
     cout << endl;
     }
  */


  //find the projected bounding box
  box = get_2Drectangle_from_colVectors(bx2D);

  //There are cases where the tracklet is active but the 2D 
  //bounding box is already out of image plane!
  if( bx2D_outside_image_plane(box, fimg.rows, fimg.cols) ){
    roi.x = 0;      wb_roi.x = 0;
    roi.y = 0;      wb_roi.y = 0;
    roi.width  = 0;  wb_roi.width  = 0;
    roi.height = 0;  wb_roi.height = 0;

    return 0;
  }

  //change coordinate from velodyne to camera for point clouds
  vconcat(velo_pc, Mat::ones(1, velo_pc.cols, CV_32FC1), pc_h);
  rotate_translate_matrix(pc_h, velo_to_cam[camera_id], Mat(), pc_h);
  velo_pc = pc_h(Range(0, pc_h.rows-1), Range::all());

  //project the point clouds to image plane
  Mat pc_2D;
  project_points(K, velo_pc, pc_2D);

  //select pc which are within the 2D bounding box
  //will work on these point clouds only
  vector<bool> pc_indicator;
  find_pc_within_2Dbx(pc_indicator, pc_2D, box);
  select_by_indicator(velo_pc, pc_indicator, velo_pc);
  select_by_indicator(pc_2D, pc_indicator, pc_2D);
  select_by_indicator(frg_pc_indicator, pc_indicator, frg_pc_indicator);
  select_by_indicator(dist_pc_to_3Dbbox, pc_indicator, dist_pc_to_3Dbbox);

  //pick 2D points that are foreground
  Mat frg_pc;
  //OLD not right: ground leaks to bx3D
  //find_pc_within_3Dbx(pc_indicator, velo_pc, bx3D);
  select_by_indicator(pc_2D, frg_pc_indicator, frg_pc);

  frg_size = (int)frg_pc.cols;

  if(frg_size <= 0){
    return 0;
  }

  /*
    #if DEBUG == 1
    cout << "frg_pc size:" << frg_pc.size() << endl;
    #endif
  */

  //find foreground median depth

  frg_median_depth = compute_pc_depth(velo_pc, frg_pc_indicator, 3);

  //start to find background point clouds
  //will use pc_indicator for background point cloud 
  //indicator from now on.
  //
  Mat bkg_pc;
  //vector<bool> bkg_pc_indicator;

  frg_shape = Mat::zeros(fimg.size(), CV_8UC1);  
  
  if(do_clean_point_clouds && frg_pc.cols != 0){
    //1. find the shape for the 2D foreground point clouds
    vector< vector<Point> > frg_contour(1);
    find_frg_shape(frg_contour[0], frg_pc);
    drawContours(frg_shape, frg_contour, 0, 255, -1);

#if DEBUG == 1
    //    namedWindow("frg shape", CV_WINDOW_AUTOSIZE);
    //    imshow("frg shape", frg_shape);
#endif
  
    //2. we dilate the frg_shape to remove background point clouds that 
    //   are very close to the frg_shape (in case of ambiguous clouds)
    int num_dilate = 0;
    Mat morph_element = getStructuringElement(MORPH_RECT,
					      Size(3, 1));  //wid,hei

    Mat frg_shape_m;
    dilate(frg_shape, frg_shape_m, morph_element, Point(-1,-1), num_dilate);
    //dilate(frg_shape, frg_shape_m, Mat(), Point(-1,-1), num_dilate);
#if DEBUG == 1
    namedWindow("dilate frg shape", 1);
    imshow("dilate frg shape", frg_shape_m);
#endif

    //3. remove point clouds that are within the foreground shape,
    //   but has larger depth values than foreground object
    double frg_min_depth;
    frg_min_depth = compute_pc_depth(velo_pc, frg_pc_indicator, 0);
    frg_min_depth += 3;  //tolerate some measurement inaccuracy

  
    find_bkg_pc(pc_indicator, frg_pc_indicator, velo_pc, pc_2D, frg_shape_m, frg_min_depth);


  }
  else{
    negate_indicator(pc_indicator, frg_pc_indicator);
  }

  select_by_indicator(pc_2D, pc_indicator, bkg_pc);
 
  /*
    #if DEBUG == 1
    cout << "bkg_pc size:" << bkg_pc.size() << endl;
    #endif
  */

  //find_bkg_pc(bkg_pc, velo_pc, pc_2D, frg_shape_m, frg_min_depth);
  //4. we erode the frg_shape to remove ambiguous foreground point
  //   clouds along object boundaries
  /*  NOT USED
      int num_erode = 1;
      erode(frg_shape, frg_shape_m, morph_element, Point(-1,-1), num_erode);
      refine_pc_within_mask(frg_pc, frg_pc, frg_shape_m);

      #if DEBUG == 1
      namedWindow("erode frg shape", 1);
      imshow("erode frg shape", frg_shape_m);
      #endif
  //*/

  //5. create the 2D mask used for Grabcut
  //5.1 use point clouds (no recover depth)
  create_mask_by_bx(mask, roi, wb_roi, box, frg_pc, bkg_pc, expand_width);

#if DEBUG == 1
  imagesc_bw("mask-point clouds", mask(wb_roi));
  /*  
  save_file_name = "../result/mask_pointclouds" + SAVE_IMG_EXT;
  imwrite(save_file_name, mask(wb_roi));

  save_file_name = "../result/frg_shape" + SAVE_IMG_EXT;
  imwrite(save_file_name, frg_shape(roi));
  //*/
#endif    

  ////get depth values for pixels within only roi, and for only frg/bkg indicators!!! (not whole image), but the size of depth_img is the whole image
  depth_img.create(fimg.size(), CV_32FC1);
  depth_img.setTo(Scalar(0));

  ////remove those outliers that are within the car convex hull but have depth larger than car depth
  bool remove_within_ch_outlier = true;

  if(remove_within_ch_outlier){
    read_depth_values(depth_img, pc_2D, velo_pc, frg_pc_indicator, pc_indicator);
  } else {
  ////keep outliers
    read_depth_values(depth_img, pc_2D, velo_pc);
  }

  /*////create soft mask
  //initialization
  float thre1 = -0.25;
  float thre2 = 0.75;

  soft_mask.create(fimg.size(), CV_32FC1);
  soft_mask.setTo(Scalar(0));

  set_mask_by_soft_mask(mask, velo_pc, pc_2D, frg_pc_indicator, pc_indicator, dist_pc_to_3Dbbox, thre1, thre2);

  //  set_soft_mask_dist(velo_pc, pc_2D, frg_pc_indicator, pc_indicator, dist_pc_to_3Dbbox, thre1, thre2);
  //  set_mask_by_threshold_soft_mask(mask, thre1, thre2);
  //*/

  //return number of frg point clouds
  return frg_size;

}

//functions related to recover depth
/*  HAS ALREADY MOVED to DEPTHINTERPOLATER
void MaskCreater::find_init_depth(Mat& recon_depth_img, const Mat& fimg)
{
  CV_Assert(fimg.type() == CV_32FC3);

  recon_depth_img.create(fimg.size(), CV_32FC1);
  recon_depth_img.setTo(Scalar(0));

  DepthInterpolater dep_int;

  Mat t_fimg, t_depth, t_ind, t_recon;

  //cout << "img:" << fimg.rows << "," << fimg.cols << endl;
  //cout << "box:" << box.y << "," << box.x << ";" << box.height << "," << box.width << endl;

  t_fimg = fimg(box);
  t_depth = depth_img(box);
  //t_ind = depth_ind(box);
  t_recon = recon_depth_img(box);

  dep_int.interpolate(t_recon, t_fimg, t_depth, t_ind, lambda_dep, recover_method);

}

void MaskCreater::build_depth_ue(Mat& d_ue, const Mat& recon_depth, const double lambda_d_u)
{
  CV_Assert(recon_depth.type() == CV_32FC1);

  const Rect roi = box;

  d_ue.create(2, roi.width*roi.height, CV_32FC1);

  float* ue_bkg_row = d_ue.ptr<float>(0);
  float* ue_frg_row = d_ue.ptr<float>(1);

  const Mat recon_depth_roi = recon_depth(roi);
  const float* recon_depth_row;

  int node_id = 0;
  float energy_frg, energy_bkg = 0;

  for(int row = 0; row < recon_depth_roi.rows; ++row){
    recon_depth_row = recon_depth_roi.ptr<float>(row);

    for(int col = 0; col < recon_depth_roi.cols; ++col){
      //energy_bkg = lambda_d_u * min( abs(recon_depth_row[col] - 9), abs(recon_depth_row[col] - 50) );
      //energy_frg = lambda_d_u * abs(recon_depth_row[col] - frg_median_depth) * abs(recon_depth_row[col] - frg_median_depth);
      energy_frg = lambda_d_u * abs(recon_depth_row[col] - frg_median_depth);
      //energy_bkg = -energy_frg;

      ue_bkg_row[node_id] = energy_bkg;
      ue_frg_row[node_id] = energy_frg;

      node_id++;
    }
  } 

  
}
//end functions related to recover depth
//
//
*/

void MaskCreater::get_3D_boundingbox_coordinate(const float h, const float w, const float l, Mat& bx3D)
{
  //OUTPUT: bx3D has size = 3 x 8

  bx3D.create(3, 8, CV_32FC1);

  float height=-1, width=-1, length=-1;

  float* bx3D_row[3];

  for(int i = 0; i < 3; i++)
    bx3D_row[i] = bx3D.ptr<float>(i);

  for(int i = 0; i < 8; i++){   
    switch (i){
    case 0:
      length = l/2;
      width = w/2;
      height = 0;
      break;
    case 1:
      length = l/2;
      width = -w/2;
      height = 0;
      break;
    case 2:
      length = -l/2;
      width = -w/2;
      height = 0;
      break;
    case 3:
      length = -l/2;
      width = w/2;
      height = 0;
      break;
    case 4: 
      length = l/2;
      width = w/2;
      height = h;
      break;
    case 5:
      length = l/2;
      width = -w/2;
      height = h;
      break;
    case 6:
      length = -l/2;
      width = -w/2;
      height = h;
      break;
    case 7:
      length = -l/2;
      width = w/2;
      height = h;
      break;
    default:
      cerr << "wrong i in get_3D_boundingbox" << endl;
      break;
    }

    bx3D_row[0][i] = length;
    bx3D_row[1][i] = width;
    bx3D_row[2][i] = height;

  }

}

int MaskCreater::load_calib_velo_to_cam(string rigid_file_name, string cam_file_name, Mat* velo_to_cam, Mat& K)
{
  int success;

  //get velodyne to camera
  float Tr[16];
  success = load_calib_rigid(rigid_file_name, Tr);
  Mat Tr_velo_to_cam(4, 4, CV_32FC1, Tr);

  //get camera intrinsic and extrinsic calibration
  Calib calib[4];
  success = load_calib_cam_to_cam(cam_file_name, calib, 4);

  Mat R_rect00(4, 4, CV_32FC1, Scalar(0));

  for(int r=0; r<3; r++)
    for(int c=0; c<3; c++)
      R_rect00.at<float>(r,c) = calib[0].R_rect[r*3+c];
  R_rect00.at<float>(3,3) = 1;

  
  //compute extrinsics from first to ith rectified camera
  Mat T[4];

  for(int i = 0; i < 4; i++){
    T[i] = Mat::eye(4,4,CV_32FC1);
    T[i].at<float>(0,3) = calib[i].P_rect[3] / calib[i].P_rect[0];
  }

  //transformation: velodyne -> rectified camera coordinates
  for(int i = 0; i < 4; i++){
    velo_to_cam[i] = T[i] * R_rect00 * Tr_velo_to_cam;
  }

  //calibration matrix after rectification (equal for all cameras)
  K.create(3,3,CV_32FC1);
  //note P_rect has size 3x4
  for(int row = 0; row < 3; row++){
    for(int col = 0; col < 3; col++){
      K.at<float>(row, col) = calib[0].P_rect[row*4+col];
    }
  }
 
  return success;
}

int MaskCreater::load_velo_points(string file_name, Mat& velo)
{
  //OUTPUT: velo has size = 3 x number_point_clouds

  velo.create(3, MAX_NUMBER_POINT_CLOUDS, CV_32FC1);

  fstream fs;

  fs.open(file_name.c_str(), fstream::in | fstream::binary);
  if(!fs.is_open()){
    cerr << "Fail to open " << file_name << endl;
    return -1;
  }

  int num_byte = 4;
  int count = 0;
  float* row[3];

  for(int i = 0; i < 3; ++i)
    row[i] = velo.ptr<float>(i);


  while( !fs.eof() ){
    if(count >= MAX_NUMBER_POINT_CLOUDS){
      cerr << "WARNING: FILE contains more number of point clouds than expected!" << endl;
      count = MAX_NUMBER_POINT_CLOUDS;
      break;
    }

    //char float_data[num_byte];
    float f_data;

    //read (x,y,z)
    for(int i = 0; i < 3; i++){
      fs.read((char *) &f_data, num_byte);

      row[i][count] = f_data;

    }
   
    fs.read((char *) &f_data, num_byte);   //read reflectance (not used)

    count++;

  }
  
  fs.close();

  //shrink the Mat
  velo = velo(Range::all(), Range(0, count));

  return 1;
}

int MaskCreater::load_velo_points(string file_name, vector<Mat>& velo)
{
  velo.clear();

  fstream fs;

  fs.open(file_name.c_str(), fstream::in | fstream::binary);
  if(!fs.is_open()){
    cerr << "Fail to open " << file_name << endl;
    return -1;
  }

  int num_byte = 4;

  while( !fs.eof() ){
    //char float_data[num_byte];
    float f_data;
    Mat point(3, 1, CV_32FC1);

    //read (x,y,z)
    for(int i = 0; i < 3; i++){
      fs.read((char *) &f_data, num_byte);

      point.at<float>(i) = f_data;

    }
   
    fs.read((char *) &f_data, num_byte);   //read reflectance (not used)

    velo.push_back(point);
  }
  
  fs.close();

  return 1;
}

int MaskCreater::load_calib_rigid(string file_name, float* mat)
{//output mat is [R T;0 0 0 1]
  
  fstream fs;

  fs.open(file_name.c_str(), fstream::in);

  if(!fs.is_open()){
    cerr << "Fail to open " << file_name << endl;

    return -1;
  }

  int success;
  string var_id;

  float R[9];
  float T[3];

  var_id = "R";
  success = readVariable(file_name, var_id, 3, 3, R);
    
  if(success == -1){
    cerr << "Fail to read R" << endl;
    return -1;
  }

  var_id = "T";
  success = readVariable(file_name, var_id, 3, 1, T);
  
  if(success == -1){
    cerr << "Fail to read T" << endl;
    return -1;
  }

  //orgainze output and append [0 0 0 1] to last row
  //int num_row = 4;
  int num_col = 4;

  //get R
  for(int row = 0; row < 3; row++){
    for(int col = 0; col < 3; col++){
      mat[row*num_col + col] = R[row*3 + col];
    }
  }

  //get T
  for(int row = 0; row < 3; row++)
    mat[row*num_col + 3] = T[row];

  //append [0 0 0 1] to last row
  for(int col = 0; col < 3; col++)
    mat[3*num_col + col] = 0;
  mat[3*num_col + 3] = 1;

  return 1;
}


int MaskCreater::load_calib_cam_to_cam(string file_name, Calib* calib, int num_cam )
{
  fstream fs;

  fs.open(file_name.c_str(), fstream::in);

  if(!fs.is_open()){
    cerr << "Fail to open " << file_name << endl;

    return -1;
  }

  int success;
  //read corner_dist (same value for all cameras)
  double corner_dist;
  string cam_id;
  string var_id;

  success = readVariable(file_name, "corner_dist", 1, 1, &corner_dist);

  if(success == -1) {
    cerr << "Fail to read corner_dist!" << endl;
    return -1;
  }

  for(int i = 0; i < num_cam; i++){
    calib[i].corner_dist = corner_dist;

    //get camera id
    stringstream ss;
    ss.fill('0');
    ss.width(2);
    ss << i;
    ss >> cam_id;

    //read remaining variables
    var_id = "S_";
    success = readVariable(file_name, var_id + cam_id, 1, 2, calib[i].S);
    
    if(success == -1){
      cerr << "Fail to read S" << endl;
      return -1;
    }

    var_id = "K_";
    success = readVariable(file_name, var_id + cam_id, 3, 3, calib[i].K);
    
    if(success == -1){
      cerr << "Fail to read K" << endl;
      return -1;
    }

    var_id = "D_";
    success = readVariable(file_name, var_id + cam_id, 1, 5, calib[i].D);
    
    if(success == -1){
      cerr << "Fail to read DS" << endl;
      return -1;
    }

    var_id = "R_";
    success = readVariable(file_name, var_id + cam_id, 3, 3, calib[i].R);
    
    if(success == -1){
      cerr << "Fail to read R" << endl;
      return -1;
    }

    var_id = "T_";
    success = readVariable(file_name, var_id + cam_id, 3, 1, calib[i].T);
    
    if(success == -1){
      cerr << "Fail to read T" << endl;
      return -1;
    }

    var_id = "S_rect_";
    success = readVariable(file_name, var_id + cam_id, 1, 2, calib[i].S_rect);
    
    if(success == -1){
      cerr << "Fail to read S_rect" << endl;
      return -1;
    }

    var_id = "R_rect_";
    success = readVariable(file_name, var_id + cam_id, 3, 3, calib[i].R_rect);
    
    if(success == -1){
      cerr << "Fail to read R_rect" << endl;
      return -1;
    }

    var_id = "P_rect_";
    success = readVariable(file_name, var_id + cam_id, 3, 4, calib[i].P_rect);
    
    if(success == -1){
      cerr << "Fail to read P_rect" << endl;
      return -1;
    }

  }


  fs.close();

  return 1;
}

Rect MaskCreater::get_2Drectangle_from_colVectors(const Mat& points2D)
{
  //Assume points2D has size 2 x num_element
  CV_Assert(points2D.type() == CV_32FC1);
  CV_Assert(points2D.rows == 2);

  Rect bx;

  float min_x, max_x, min_y, max_y;

  const float* points_row[2];
  points_row[0] = points2D.ptr<float>(0);
  points_row[1] = points2D.ptr<float>(1);

  min_x = points_row[0][0];
  max_x = min_x;

  min_y = points_row[1][0];
  max_y = min_y;

  float x, y;

  for(int i = 1; i < points2D.cols; i++){
    x = points_row[0][i];
    y = points_row[1][i];
 
    if(min_x > x)
      min_x = x;
    if(max_x < x)
      max_x = x;

    if(min_y > y)
      min_y = y;
    if(max_y < y)
      max_y = y;
  }

  bx.x = static_cast<int>(min_x + 0.5);
  bx.y = static_cast<int>(min_y + 0.5);

  bx.width = static_cast<int>(max_x - min_x + 1.5);
  bx.height = static_cast<int>(max_y - min_y + 1.5);

  return bx;


}


bool MaskCreater::is_inRange(const Mat& point, const Mat& min_val, const Mat& max_val, vector<int>& in_segment)
{//point must be a column vector
  CV_Assert(point.cols == 1);
  CV_Assert(point.type() == CV_32FC1);

  in_segment.clear();  //has value 1, if the corresponding coordinate has lied within the 3d bounding box along the coordinate (e.g., if p.x is within the box's x-coordinate)

  for(int i = 0; i < point.rows; i++){
    if( point.at<float>(i) >= min_val.at<float>(i) &&
	point.at<float>(i) <= max_val.at<float>(i) ){
      in_segment.push_back(1);
    } else{
      in_segment.push_back(0);
    }
    /*
    //if point is < min value, return false
    if(point.at<float>(i) < min_val.at<float>(i))
      return false;
    //if point is > max value, return false
    if(point.at<float>(i) > max_val.at<float>(i))
      return false;
    */
  }

  int result = 0;

  for(size_t i = 0; i < in_segment.size(); ++i){
    result += in_segment[i];
  }

  if(result  == (int)in_segment.size())
    return true;
  else
    return false;

}

void MaskCreater::find_pc_within_3Dbx(vector<bool>& target_indicator, Mat& dist, const Mat& all_pc, const Mat& bx3D, const bool remove_car_ground, const Mat& tracklet_R, const Mat& tracklet_T)
{
  Mat bx3D_orig;

  inverse_rotate_translate_matrix(bx3D_orig, tracklet_R, tracklet_T, bx3D);

  //cout << "fpc: bx3d orig:" << bx3D_orig << endl;

  //transform the point clouds in Velodyne system to 3D bounding box system
  Mat pc_orig;

  inverse_rotate_translate_matrix(pc_orig, tracklet_R, tracklet_T, all_pc);

  //cout << "fpc: pc orig:" << pc_orig << endl;

  find_pc_within_3Dbx(target_indicator, dist, pc_orig, bx3D_orig, remove_car_ground);

  /*
  cout << "ind:";
  for(size_t i = 0; i < target_indicator.size(); ++i){
    if(target_indicator[i])
      cout << "1 ";
    else
      cout << "0 ";
  }
  cout << endl;
  */
}


void MaskCreater::find_dist_to_vertex(Mat& dist, const Mat& point, const Mat& lower, const Mat& upper)
{ //compute the point distance to the lower or upper coordinate of a 3D bounding box
  CV_Assert(point.cols == 1);
  CV_Assert(lower.cols == 1);
  CV_Assert(upper.cols == 1);

  Mat dist1, dist2;

  dist1 = abs(point - lower);
  dist2 = abs(point - upper);

  dist = min(dist1, dist2);

}

float MaskCreater::find_dist(const Mat& dist, vector<int>& in_segment)
{
  CV_Assert(dist.type() == CV_32FC1);
  
  int pos = 0;


  for(size_t i = 0; i < in_segment.size(); ++i){
    pos += in_segment[i];

    //cout << "in seg:" << in_segment[i] << endl;

  }

  double mmin=0, mmax=0;
  float tmp;

  if(pos == 3){
    //point is totally within the 3D bounding box
    minMaxLoc(dist, &mmin, &mmax);
  }else{
    //point has at most two coordinates within the 3D bounding box
    for(size_t i = 0; i < in_segment.size(); ++i){
      if(in_segment[i] == 0){
	tmp = dist.at<float>((int)i);
	mmin += tmp*tmp;
      }
    }

    mmin = sqrt(mmin);
  }

  //cout << "mmin:" << mmin << endl;

  return mmin;
}

void MaskCreater::find_pc_within_3Dbx(vector<bool>& target_indicator, Mat& dist, const Mat& all_pc, const Mat& bx3D, const bool remove_car_ground)
{
  //ASSUME all_pc is of size = feature_dimension x number_of_elements
  //
  target_indicator.clear();
  dist.create(1, all_pc.cols, CV_32FC1);
  dist.setTo(Scalar(0));

  Mat min_val, max_val;

  //find the extreme values of the bounding box
  reduce(bx3D, min_val, 1, CV_REDUCE_MIN);
  reduce(bx3D, max_val, 1, CV_REDUCE_MAX);

  //cout << "min max val:" << min_val << endl << max_val << endl;

  float* dist_row = dist.ptr<float>(0);

  vector<int> in_segment;


  double tolerance = 0;  //3D bounding boxes are not very accurate

  for(int i = 0; i < all_pc.cols; ++i){
    Mat point;

    /*//for debug
    point = all_pc.col(i).clone();
    point.at<float>(0) = 3;
    point.at<float>(1) = 0;
    point.at<float>(2) = 3.3;
    find_dist_to_vertex(p_dist, point, min_val, max_val);

    cout << "p:" << endl << point << endl;
    cout << "p_dist:" << endl << p_dist << endl;

    if( is_inRange(point, min_val, max_val, in_segment) ){
      target_indicator.push_back(true);
    }
    else{
      target_indicator.push_back(false);
    }

    //*/

    //compute the distance between the point and the 3D bounding box
    point = all_pc.col(i);

    Mat p_dist;

    find_dist_to_vertex(p_dist, point, min_val, max_val);

    //cout << "p:" << endl << point << endl;
    //cout << "p_dist:" << endl << p_dist << endl;

    if( is_inRange(point, min_val-tolerance, max_val+tolerance, in_segment) ){
      target_indicator.push_back(true);
    }
    else{
      target_indicator.push_back(false);
    }

    dist_row[i] = find_dist(p_dist, in_segment);


  }


  //  select_by_indicator(all_pc, select_indicator, target_pc);

  //  float min_z = 9999;

  if(remove_car_ground){
    //remove the car point clouds that are close to ground
    for(int i = 0; i < all_pc.cols; ++i){
      //      if(all_pc.at<float>(2,i) < min_z){
      //	min_z = all_pc.at<float>(2,i);
      //      }

      //ASSUME in object coordinate
      if(all_pc.at<float>(2, i) < 0.1){
	target_indicator[i] = false;
      }
    }

  }


  //  cout << "min z:" << min_z << endl;

  /*debug
  int count = 0;
  for(int i = 0; i < target_indicator.size(); ++i){
    if(target_indicator[i])
      count++;
  }

  cout << "count:" << count << endl;
  exit(-1);
  */
}

void MaskCreater::find_vectorPoints_range(const vector<Mat>& points, Mat& min_val, Mat& max_val)
{//points must be a vector of Mat (where Mat is column vectors)

  for(size_t i = 0; i < points.size(); i++){
    if(i==0){
      min_val = points[i].clone();
      max_val = points[i].clone();
    } else{
      for(int j = 0; j < points[i].rows; j++){
	if( min_val.at<float>(j) > points[i].at<float>(j) )
	  min_val.at<float>(j) = points[i].at<float>(j);
	if( max_val.at<float>(j) < points[i].at<float>(j) )
	  max_val.at<float>(j) = points[i].at<float>(j);
      }
    }
  }

}

void MaskCreater::create_mask_by_bx(Mat& mask, Rect& roi, Rect& wb_roi, Rect& bx2D, const Mat& frg_pc, const Mat& bkg_pc, const int expand_width)
{ //create mask. bx2D gives UNDEFINED_LABEL, frg_pc gives FOREGROUND_LABEL, bkg_pc gives STRICT_BACKGROUND_LABEL
  //expanded bx2D gives WEAK_BACKGROUND_LABEL
  //current code is not optimized. many pixels are over-written serveral times  
  //mask default is STRICT_BACKGROUND_LABEL

  //ASSUME frg_pc is of size feature_dimension x number_elements

  CV_Assert(frg_pc.type() == CV_32FC1);
  CV_Assert(bkg_pc.type() == CV_32FC1);
  CV_Assert(mask.type() == CV_8UC1);

  if(frg_pc.cols != 0){
    CV_Assert(frg_pc.rows == 2);
  }
  if(bkg_pc.cols != 0){
    CV_Assert(bkg_pc.rows == 2);
  }

  int max_x, max_y;

  // for debug
  //cout << "bx2D:" << bx2D.x << "," << bx2D.y << ";" << bx2D.width << "," << bx2D.height << endl;

  //shrink bx2D if it is out of image plane
  roi.x = max(bx2D.x, 0);
  roi.y = max(bx2D.y, 0);
  max_x         = min(bx2D.x + bx2D.width,  mask.cols);
  max_y         = min(bx2D.y + bx2D.height, mask.rows);
  roi.width  = max_x - roi.x;
  roi.height = max_y - roi.y;

  //expand the bx2D
  wb_roi.x = max(bx2D.x - expand_width, 0);
  wb_roi.y = max(bx2D.y - expand_width, 0);
  max_x = min(bx2D.x+bx2D.width+expand_width, mask.cols);
  max_y = min(bx2D.y+bx2D.height+expand_width, mask.rows);
  wb_roi.width = max_x - wb_roi.x;
  wb_roi.height = max_y - wb_roi.y;  

  //change bx2D to be roi, since bx2D may be out of image plane
  bx2D = roi;
  wb_box = wb_roi;

  /* for debug
  cout << "wb_roi:" << wb_roi.x << "," << wb_roi.y << ";" << wb_roi.width << "," << wb_roi.height << endl;
  cout << "roi:" << roi.x << "," << roi.y << ";" << roi.width << "," << roi.height << endl;
  */

  //set WEAK_BACKGROUND_LABEL
  mask(wb_roi) = WEAK_BACKGROUND_LABEL;

  //set UNDEFINED_LABEL
  mask(roi) = UNDEFINED_LABEL;

  //set FOREGROUND_LABEL
  const float* x_row = frg_pc.ptr<float>(0);
  const float* y_row = frg_pc.ptr<float>(1);

  int x, y;
  for(int i = 0; i < frg_pc.cols; i++){
    x = static_cast<int>(x_row[i] + 0.5);
    y = static_cast<int>(y_row[i] + 0.5);

    if(is_within_image_plane(x, y, mask.rows, mask.cols)){
      mask.at<uchar>(y, x) = FOREGROUND_LABEL;
    }

  }

  //set BACKROUND_LABEL
  x_row = bkg_pc.ptr<float>(0);
  y_row = bkg_pc.ptr<float>(1);

  for(int i = 0; i < bkg_pc.cols; i++){
    x = static_cast<int>(x_row[i] + 0.5);
    y = static_cast<int>(y_row[i] + 0.5);

    if(is_within_image_plane(x, y, mask.rows, mask.cols)){
      mask.at<uchar>(y, x) = STRICT_BACKGROUND_LABEL;
    }
  }
  

}

void MaskCreater::create_mask_by_recovered_depth(Mat& mask, Rect& roi, Rect& wb_roi, const Rect& bx2D, const Mat& recon_depth, const double frg_depth, const double threshold, const int expand_width)
{ // Create mask by reconstructed depth image
  // will threshold the reconstructed depth image.
  // |d_i - frg_depth| > threshold is considered as background
  // bx2D gives UNDEFINED_LABEL, frg_pc gives FOREGROUND_LABEL,
  // bkg_pc gives STRICT_BACKGROUND_LABEL
  // expanded bx2D gives WEAK_BACKGROUND_LABEL
  //
  // current code is not optimized. many pixels are over-written serveral times  
  //
  //mask default is STRICT_BACKGROUND_LABEL
  //

  //ASSUME frg_pc is of size feature_dimension x number_elements
  CV_Assert(mask.type() == CV_8UC1);
  CV_Assert(recon_depth.type() == CV_32FC1);

  int max_x, max_y;

  //shrink bx2D if it is out of image plane
  roi.x = max(bx2D.x, 0);
  roi.y = max(bx2D.y, 0);
  max_x         = min(bx2D.x + bx2D.width,  mask.cols - 1);
  max_y         = min(bx2D.y + bx2D.height, mask.rows - 1);
  roi.width  = max_x - roi.x;
  roi.height = max_y - roi.y;

  //expand the bx2D
  wb_roi.x = max(bx2D.x - expand_width, 0);
  wb_roi.y = max(bx2D.y - expand_width, 0);
  max_x = min(bx2D.x+bx2D.width+expand_width, mask.cols-1);
  max_y = min(bx2D.y+bx2D.height+expand_width, mask.rows-1);
  wb_roi.width = max_x - wb_roi.x;
  wb_roi.height = max_y - wb_roi.y;  

  //set WEAK_BACKGROUND_LABEL
  mask(wb_roi) = WEAK_BACKGROUND_LABEL;

  //set UNDEFINED_LABEL
  mask(roi) = UNDEFINED_LABEL;

  //set FOREGROUND_LABEL and BACKGROUND_LABEL based on the reconstructed depth values
  const float* depth_row;
  uchar* mask_row;
  float depth_val;
  double diff;

  for(int row = roi.y; row < roi.y + roi.height; row++){

    depth_row = recon_depth.ptr<float>(row);
    mask_row  = mask.ptr<uchar>(row);

    for(int col = roi.x; col < roi.x + roi.width; col++){

      depth_val = depth_row[col];
      diff = abs(depth_val - frg_depth);

      if( diff > 2*threshold){
	mask_row[col] = STRICT_BACKGROUND_LABEL;
      }
      else{
	if(diff < threshold)
	  mask_row[col] = FOREGROUND_LABEL;
	else
	  mask_row[col] = UNDEFINED_LABEL;
      }
    }


  }

    
}


bool MaskCreater::is_within_image_plane(const int x, const int y, const int img_row, const int img_col)
{
  if(x < 0 || y < 0 || x >= img_col || y >= img_row)
     return false;

  return true;

}

double MaskCreater::wrap_to_pi(double alpha)
{
  //wrap to 0 ~ 2*pi
  alpha = fmod(alpha, 2*D_PI);

  //wrap to -pi ~ pi
  if(alpha > D_PI)
    alpha -= 2*D_PI;

  return alpha;

}

void MaskCreater::project_points(const Mat& P, const Mat& points3D, Mat& points2D)
{
  //P is 3x3, assume points3D is of size 3 x num_elements
  CV_Assert(P.rows == 3 && P.cols == 3);
  CV_Assert(points3D.rows == 3);
  CV_Assert(points3D.type() == CV_32FC1);

  Mat prod;

  prod = P * points3D;

  //  cout << "p3:" << points3D << endl;
  // cout << "prod:" << prod << endl;


  int num_element = points3D.cols;

  points2D.create(2, num_element, CV_32FC1);

  divide(prod.row(0), prod.row(2), points2D.row(0));
  divide(prod.row(1), prod.row(2), points2D.row(1));

  //cout << "p2:" << points2D << endl;
 
} 

void MaskCreater::remove_ground_point_clouds(Mat& velo_pc)
{
  //remove point clouds that are likely to be ground
  //pc is in the Velodyne coordinate system
  //

  vector<bool> select_indicator;
  int which_dim = 2;           //third dim is pointing upright
  bool larger = true;
  
  setup_select_indicator(select_indicator, velo_pc, which_dim, larger, GROUND_VAL);
  select_by_indicator(velo_pc, select_indicator, velo_pc);

}

/* INCOMPLETE
void MaskCreater::remove_ground_point_clouds(Mat& velo_pc, const Mat& bx3D)
{
  //remove point clouds that are within bx3D and are likely to
  //be ground.
  //velo_pc is in the Velodyne coordinate system
  //

  vector<bool> in3Dbx_indicator;
  vector<bool> ground_indicator;
  int which_dim = 2;           //third dim is pointing upright
  bool larger = false;
  float ground_val = -1.55;    //ALL CAMERA HEIGHTs = 1.65m

    
  //find pc within the 3D bounding box
  find_pc_within_3Dbx(in3Dbx_indicator, velo_pc, bx3D);
  
  //find the ground point cloud
  setup_select_indicator(ground_indicator, velo_pc, which_dim, larger, ground_val);

  //take intersection of the indicators
  and_indicator(ground_indicator, in3Dbx_indicator, ground_indicator);
  //get result
  remove_by_indicator(velo_pc, ground_indicator, velo_pc);
  
}
*/

void MaskCreater::remove_behind_point_clouds(Mat& velo_pc)
{
  //remove point clouds that are behind the image plane
  //pc is in the Velodyne coordinate system
  //

  vector<bool> select_indicator;
  int which_dim = 0;       //first dim is pointing forward
  bool larger = true;
  float val = 0.1;    //the image plane value (>0)
  
  setup_select_indicator(select_indicator, velo_pc, which_dim, larger, val);
  select_by_indicator(velo_pc, select_indicator, velo_pc);

}

void MaskCreater::inverse_rotate_translate_matrix(Mat& out, const Mat& R, const Mat& T, const Mat& m)
{
  CV_Assert(m.type() == CV_32FC1);
  CV_Assert(R.type() == CV_32FC1);

  Mat tmp = m.clone();

  if(!T.empty()){
    CV_Assert(T.type() == CV_32FC1);
    CV_Assert(T.cols == 1);  //T is a column vector

   for(int row = 0; row < m.rows; row++){
      float* tmp_row = tmp.ptr<float>(row);
      const float* T_row = T.ptr<float>(row);

      for(int col = 0; col < tmp.cols; col++){
	tmp_row[col] = tmp_row[col] - T_row[0];
      }
    } 
  }

  Mat inv_R = R.clone();

  inv_R.at<float>(0,1) = -1 * inv_R.at<float>(0,1);
  inv_R.at<float>(1,0) = -1 * inv_R.at<float>(1,0);

  tmp = inv_R*tmp;

  out = tmp;

}


void MaskCreater::rotate_translate_matrix(Mat& out, const Mat& R, const Mat& T, const Mat& m)
{
  CV_Assert(m.type() == CV_32FC1);
  CV_Assert(R.type() == CV_32FC1);

  if(!T.empty()){
    CV_Assert(T.type() == CV_32FC1);
    CV_Assert(T.cols == 1);  //T is a column vector
  }

  Mat tmp = R*m;

  if(!T.empty()){
    for(int row = 0; row < m.rows; row++){
      float* tmp_row = tmp.ptr<float>(row);
      const float* T_row = T.ptr<float>(row);

      for(int col = 0; col < tmp.cols; col++){
	tmp_row[col] = tmp_row[col] + T_row[0];
      }
    }
      
  }

  out = tmp;

}

void MaskCreater::find_pc_within_2Dbx(vector<bool>& indicator, const Mat& pc_2D, const Rect& bx2D)
{
  //assume pc_2D is of size 2xnum_elements
  CV_Assert(pc_2D.type() == CV_32FC1);
  
  indicator.clear();

  int num_element = pc_2D.cols;
  int x, y;
  
  const float* pc_row[2];
  pc_row[0] = pc_2D.ptr<float>(0);
  pc_row[1] = pc_2D.ptr<float>(1);

  for(int i = 0; i < num_element; ++i){
    x = static_cast<int>(pc_row[0][i] + 0.5);
    y = static_cast<int>(pc_row[1][i] + 0.5);

    if( bx2D.contains( Point(x, y) ) )
      indicator.push_back(true);
    else
      indicator.push_back(false);

  }
  
}

double MaskCreater::compute_pc_depth(const Mat& velo_pc, const vector<bool>& ind, const int type)
{
  //Assume velo_pc is in camera coordinate system
  //dim 0: x (right positive), 
  //dim 1: y (down positive), 
  //dim 2: z (depth, point out)
  
  //assume velo_pc has size 3xnum_elements
  CV_Assert(velo_pc.type() == CV_32FC1);
  CV_Assert(velo_pc.cols == (int)ind.size());

  //type = 0: compute min
  //type = 1: compute max
  //type = 2: compute mean
  //type = 3: compute median

  double result = 0;
  
  int num_element = velo_pc.cols;

  const float* depth_row = velo_pc.ptr<float>(2);

  vector<float> depth_vals;
  
  for(int i = 0; i < num_element; ++i){
    if(ind[i]){
      depth_vals.push_back(depth_row[i]);
    }//end if ind
  }//end for

  
  if(type == 0){
    //find min
    result = ( *min_element(depth_vals.begin(), depth_vals.end()) );
  }
  else if(type == 1){
    //find max
    result = ( *max_element(depth_vals.begin(), depth_vals.end()) );
  }
  else if(type == 2){
    //find mean
    result = accumulate(depth_vals.begin(), depth_vals.end(), 0.0);
    result = result / depth_vals.size();
  }
  else if(type == 3){
    //find median
    nth_element(depth_vals.begin(), depth_vals.begin()+depth_vals.size()/2, depth_vals.end());

    if(depth_vals.size() != 0)
      result = depth_vals[ depth_vals.size()/2 ];
    else
      result = 0;
  }
  

  return result;
}

void MaskCreater::find_frg_shape(vector<Point>& contour, const Mat& pc)
{
  //find the shape for the foreground specifidy by the point cloud
  //so far, we only find its convex hull

  CV_Assert(pc.type() == CV_32FC1);
  CV_Assert(pc.rows == 2);   //assume pc has size 2xnum_elements

  vector< vector<Point> > cntr(1);

  int num_element = pc.cols;
  int x, y;

  const float* pc_row0 = pc.ptr<float>(0);
  const float* pc_row1 = pc.ptr<float>(1);

  for(int i = 0; i < num_element; ++i){

    x = static_cast<int>(pc_row0[i] + 0.5);
    y = static_cast<int>(pc_row1[i] + 0.5);

    cntr[0].push_back(Point(x,y));
  }

  
  convexHull(cntr[0], contour);


}

void MaskCreater::find_bkg_pc(vector<bool>& bkg_ind, const vector<bool>& frg_ind, const Mat& pc_3D, const Mat& pc_2D, const Mat frg_shape, const double frg_min_depth)
{
  //find the background point clouds that are not within the 
  //foreground shape mask and do not have depth larger than 
  //foreground's min depth

  CV_Assert(pc_3D.type() == CV_32FC1);
  CV_Assert(pc_3D.rows == 3);
  CV_Assert(pc_2D.type() == CV_32FC1);
  CV_Assert(pc_2D.rows == 2);
  CV_Assert(pc_3D.cols == pc_2D.cols);
  CV_Assert(frg_shape.type() == CV_8UC1);
  
  bkg_ind.clear();

  //assume pc_3D has size 3 x num_elements
  //assume pc_2D has size 2 x num_elements

  //Assume pc_3D is in camera coordinate
  //dim 0: x (right positive), 
  //dim 1: y (down positive), 
  //dim 2: z (depth, point out)

  //vector<bool> bkg_ind;

  int num_elements = pc_3D.cols;
  int x, y;
  float depth;
  //float height;

  //const float* height_row = pc_3D.ptr<float>(1);
  const float* depth_row = pc_3D.ptr<float>(2);
  const float* x_row = pc_2D.ptr<float>(0);
  const float* y_row = pc_2D.ptr<float>(1);

  for(int i = 0; i < num_elements; ++i){
    x = static_cast<int>(x_row[i] + 0.5);
    y = static_cast<int>(y_row[i] + 0.5);
   
    if(!frg_ind[i] && is_within_image_plane(x, y, frg_shape.rows, frg_shape.cols)){
      //if is within the foreground shape mask
      if(frg_shape.at<uchar>(y,x) == 255){
	depth = depth_row[i];

	if(depth < frg_min_depth){
	  bkg_ind.push_back(true);
	} else{
	  bkg_ind.push_back(false);
	}

      }
      else{
	bkg_ind.push_back(true);
      }
    
    } else{
      bkg_ind.push_back(false);
    } //end if in image plane

  }


}

void MaskCreater::refine_pc_within_mask(Mat& out, const Mat& in, const Mat& mask)
{
  //refine the input point clouds
  //select only those that are within the mask

  CV_Assert(in.type() == CV_32FC1);
  CV_Assert(mask.type() == CV_8UC1);
  CV_Assert(in.rows == 2);

  //assume in has size 2 x num_elements

  Mat tmp;

  vector<bool> pc_ind;

  int num_elements = in.cols;
  int x, y;

  const float* x_row = in.ptr<float>(0);
  const float* y_row = in.ptr<float>(1);

  for(int i = 0; i < num_elements; ++i){
    x = static_cast<int>(x_row[i] + 0.5);
    y = static_cast<int>(y_row[i] + 0.5);
   
    //if is within the mask
    if(mask.at<uchar>(y,x) == 255){
      pc_ind.push_back(true);
    }
    else{
      pc_ind.push_back(false);
    }
  }

  select_by_indicator(in, pc_ind, tmp);

  out = tmp;
}

void MaskCreater::set_mask_by_soft_mask(Mat& mask, const Mat& velo_pc, const Mat& pc_2D, vector<bool>& frg_ind, vector<bool>& bkg_ind, const Mat& dist, const float thre1, const float thre2)
{
  CV_Assert(mask.type() == CV_8UC1);
  CV_Assert(soft_mask.size() == mask.size());

  CV_Assert(pc_2D.type() == CV_32FC1);
  CV_Assert(dist.type() == CV_32FC1);  
  CV_Assert(pc_2D.cols == dist.cols);
  CV_Assert(dist.rows == 1);  //assume dist is a row vector

  CV_Assert(dist.cols == (int)frg_ind.size());
  CV_Assert(dist.cols == (int)bkg_ind.size());

  CV_Assert(velo_pc.cols == dist.cols);

  //
  int x, y;
  const float* pc_row[2];
  const float* dist_row;
  const float* height_row;

  pc_row[0] = pc_2D.ptr<float>(0);
  pc_row[1] = pc_2D.ptr<float>(1);

  dist_row = dist.ptr<float>(0);

  //Assume velo_pc is in camera coordinate system
  //dim 0: x (right positive), 
  //dim 1: y (down positive), 
  //dim 2: z (depth, point out)
  height_row = velo_pc.ptr<float>(1);

  float middle = (thre1+thre2)/2.0;
  float val;
  float min_bkg = -2;

  //set foreground/background point cloud depth within whole image
  for(int i = 0; i < pc_2D.cols; ++i){
    if(frg_ind[i] || bkg_ind[i]){
      x = static_cast<int>(pc_row[0][i] + 0.5);
      y = static_cast<int>(pc_row[1][i] + 0.5);

      if(is_within_image_plane(x, y, soft_mask.rows, soft_mask.cols)){
	if(frg_ind[i]){
	  //signed distance transform
	  val = -dist_row[i];

	  //	  if( val < thre1 ){
	  //	    soft_mask.at<float>(y,x) = 1;
	  //	  } else {
	  //	    soft_mask.at<float>(y,x) = -2*val + 0.5;
	  soft_mask.at<float>(y,x) = -val + 0.25;
	    //	  }
	  
	  // /* remove car point clouds close to ground
	  if(height_row[i] > 1.48){
	    //there are car point clouds leaking to the ground
	    //set mask value to be unknown
	    mask.at<uchar>(y,x) = UNDEFINED_LABEL;
	    soft_mask.at<float>(y,x) = -2*middle + 0.5;  //any value, not used anyway
	  }
	  //*/
	}

	if(bkg_ind[i]){
	  val = dist_row[i];

	  //	  if(val > thre2){
	  //	      soft_mask.at<float>(y,x) = -1;
	  //	  } else{
	  //	  soft_mask.at<float>(y,x) = -2*val + 0.5;
	  soft_mask.at<float>(y,x) = -val + 0.25;
	    //	  }

	  ///*one-sided threshold	  
	  if(soft_mask.at<float>(y,x) < min_bkg){
	    soft_mask.at<float>(y,x) = min_bkg;
	  }
	  //*/

	  //soft value > 0, change mask value to be frg
	  if(soft_mask.at<float>(y,x) > 0){
	    mask.at<uchar>(y,x) = FOREGROUND_LABEL;
	  }

	  if(height_row[i] > 1.48){
	    //want to keep ground point cloud to be strong bkg
	    mask.at<uchar>(y,x) = STRICT_BACKGROUND_LABEL;

	    //threshold the ground bkg
	    //soft_mask.at<float>(y,x) = -1 * abs(val); //min_bkg/2;
	    //soft_mask.at<float>(y,x) = min_bkg;
	  }


	}

      }
    }
  }
  

}

/*
void MaskCreater::set_mask_by_threshold_soft_mask(Mat& mask, const float thre1, const float thre2)
{
  CV_Assert(mask.type() == CV_8UC1);
  CV_Assert(soft_mask.size() == mask.size());

  float* soft_row;
  uchar* mask_row;
  float dist;
  
  for(int row = 0; row < soft_mask.rows; ++row){
    soft_row = soft_mask.ptr<float>(row);
    mask_row = mask.ptr<uchar>(row);

    for(int col = 0; col < soft_mask.cols; ++col){
      if(soft_row[col] > thre2){
	dist = -1;  //-1 for bkg
      } else if(soft_row[col] < thre1){
	dist = 1;   //1 for frg
      }else{
	dist = -2*soft_row[col] + 0.5;
	//dist = 0.5 * (1 - soft_row[col]/threshold);
	//dist = -soft_row[col] + 2.0 / 3.0;
      }

      if(dist > 0){
	mask_row[col] = FOREGROUND_LABEL;
      } else if(dist < 0){
	mask_row[col] = STRICT_BACKGROUND_LABEL;
      } //for dist = 0, it is ambiguous, so no label assigned

    }


  }
  


}

void MaskCreater::set_soft_mask_dist(const Mat& velo_pc, const Mat& pc_2D, vector<bool>& frg_ind, vector<bool>& bkg_ind, const Mat& dist, const float thre1, const float thre2)
{
  CV_Assert(pc_2D.type() == CV_32FC1);
  CV_Assert(dist.type() == CV_32FC1);  
  CV_Assert(pc_2D.cols == dist.cols);
  CV_Assert(dist.rows == 1);  //assume dist is a row vector

  CV_Assert(dist.cols == (int)frg_ind.size());
  CV_Assert(dist.cols == (int)bkg_ind.size());

  CV_Assert(velo_pc.cols == dist.cols);

  //
  int x, y;
  const float* pc_row[2];
  const float* dist_row;
  const float* height_row;

  pc_row[0] = pc_2D.ptr<float>(0);
  pc_row[1] = pc_2D.ptr<float>(1);

  dist_row = dist.ptr<float>(0);

  //Assume velo_pc is in camera coordinate system
  //dim 0: x (right positive), 
  //dim 1: y (down positive), 
  //dim 2: z (depth, point out)
  //height_row = velo_pc.ptr<float>(1);

  float middle = (thre1+thre2)/2;

  //set foreground/background point cloud depth within whole image
  for(int i = 0; i < pc_2D.cols; ++i){
    if(frg_ind[i] || bkg_ind[i]){
      x = static_cast<int>(pc_row[0][i] + 0.5);
      y = static_cast<int>(pc_row[1][i] + 0.5);

      if(is_within_image_plane(x, y, soft_mask.rows, soft_mask.cols)){
	//signed distance transform
	if(frg_ind[i]){
	  //if(height_row[i] > 1.48){
	    //there are car point clouds leaking to the ground
	  //soft_mask.at<float>(y,x) = middle;  //after threshold, the value will pay no penalty for labeling as frg and as bkg
	  //}
	  //else{
	    soft_mask.at<float>(y,x) = -dist_row[i];
	    //}
	}
	else{
	  //if(height_row[i] > 1.48){
	    //want to keep ground point cloud to be strong bkg
	  //  soft_mask.at<float>(y,x) = 1;
	  //}
	  //else{
	    soft_mask.at<float>(y,x) = dist_row[i];
	    //}
	}
      }
    }
  }
  

}
*/

void MaskCreater::read_depth_values(Mat& depth_img, const Mat& pc_2D, const Mat& velo_pc, vector<bool>& frg_ind, vector<bool>& bkg_ind)
{

  CV_Assert(pc_2D.type() == CV_32FC1);
  CV_Assert(velo_pc.type() == CV_32FC1);
  CV_Assert(pc_2D.rows == 2 && velo_pc.rows == 3);
  CV_Assert(pc_2D.cols == velo_pc.cols);

  CV_Assert(velo_pc.cols == (int)frg_ind.size());
  CV_Assert(velo_pc.cols == (int)bkg_ind.size());


  //copy depth value to depth_img
  int x, y;
  const float* pc_row[2];
  const float* depth_row;

  pc_row[0] = pc_2D.ptr<float>(0);
  pc_row[1] = pc_2D.ptr<float>(1);

  //Assume depth is the third row of velo_pc
  depth_row = velo_pc.ptr<float>(2);
  
  //set foreground/background point cloud depth within whole image
  for(int i = 0; i < pc_2D.cols; ++i){
    if(frg_ind[i] || bkg_ind[i]){
      x = static_cast<int>(pc_row[0][i] + 0.5);
      y = static_cast<int>(pc_row[1][i] + 0.5);

      if(is_within_image_plane(x, y,depth_img.rows,depth_img.cols)){
	depth_img.at<float>(y,x) = depth_row[i];
      }
    }
  }

}


void MaskCreater::read_depth_values(Mat& depth_img, const Mat& pc_2D, const Mat& velo_pc)
{

  CV_Assert(pc_2D.type() == CV_32FC1);
  CV_Assert(velo_pc.type() == CV_32FC1);
  CV_Assert(pc_2D.rows == 2 && velo_pc.rows == 3);
  CV_Assert(pc_2D.cols == velo_pc.cols);

  //copy depth value to depth_img
  int x, y;
  const float* pc_row[2];
  const float* depth_row;

  pc_row[0] = pc_2D.ptr<float>(0);
  pc_row[1] = pc_2D.ptr<float>(1);

  //Assume depth is the third row of velo_pc
  depth_row = velo_pc.ptr<float>(2);
  
  //set foreground/background point cloud depth within whole image
  for(int i = 0; i < pc_2D.cols; ++i){
    x = static_cast<int>(pc_row[0][i] + 0.5);
    y = static_cast<int>(pc_row[1][i] + 0.5);

    if(is_within_image_plane(x, y,depth_img.rows,depth_img.cols)){
      depth_img.at<float>(y,x) = depth_row[i];
    }

  }

}

bool MaskCreater::bx2D_outside_image_plane(const Rect& box, const int rows, const int cols)
{
  bool result;

  if( is_within_image_plane(box.x          , box.y           , rows, cols) ||
      is_within_image_plane(box.x+box.width, box.y           , rows, cols) ||
      is_within_image_plane(box.x          , box.y+box.height, rows, cols) ||
      is_within_image_plane(box.x+box.width, box.y+box.height, rows, cols) ) {
    result = false;
  } else{
    result = true;
  }

  return result;
}
