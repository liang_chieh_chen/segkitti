#include "GraphCutWrapper.h"
#include "graph.h"
#include <opencv2/core/core.hpp>
#include <iostream>
   
using namespace std;
using namespace cv;

GraphCutWrapper::GraphCutWrapper(const int _num_nodes, const Mat& PI, const int _mul)
  :num_nodes(_num_nodes),
   num_edges(PI.cols),
   mul(_mul)
{

  //Assume PI has size 2 x number_edges
  CV_Assert(PI.rows == 2);
  CV_Assert(PI.type() == CV_32SC1);

  initialize_graph(PI);
}

GraphCutWrapper::~GraphCutWrapper()
{
  delete g;
} 
 
void GraphCutWrapper::initialize_graph(const Mat& PI)
{
  g = new GraphType(num_nodes, num_edges);

  const int* pi_row[2];
  for(int i = 0; i<2; i++)
    pi_row[i] = PI.ptr<int>(i);

  //initialize nodes
  g->add_node(num_nodes);

  //initialize t weights 
  for(int i = 0; i < num_nodes; ++i)
    g->add_tweights(i, 0, 0);

  //initialize neighbor weights
  for(int j = 0; j < num_edges; ++j){
    int start_ind = pi_row[0][j];
    int end_ind   = pi_row[1][j];

    g->add_edge(start_ind, end_ind, 0, 0);

  }
  
  //Might want to call g->maxflow(), if the dgc code does not initialize the search trees properly???
  g->maxflow();

}

void GraphCutWrapper::compute_map(const Mat& UE, const Mat& PE, const Mat& PI, Mat& x_opt, double& energy)
{
  //assume ue and pe have float type. will transform them to int for graphcut
  CV_Assert(UE.type() == PE.type() && PE.type() == CV_32FC1);
  CV_Assert(UE.rows == 2 && UE.cols == num_nodes);
  CV_Assert(PI.rows == 2 && PI.cols == num_edges);
  CV_Assert(PE.rows == 2 || PE.rows == 1);
  CV_Assert(PE.cols == num_edges);

  //setup parameters
  energy = 0;
  bool is_undirectional_graph;

  if(PE.rows == 1)
    is_undirectional_graph = true;
  else
    is_undirectional_graph = false;

  const float* ue_row[2];
  const int* pi_row[2];
  const float* pe_row[2];

  for(int i = 0; i < 2; i++){
    ue_row[i] = UE.ptr<float>(i);
    pi_row[i] = PI.ptr<int>(i);
  }
  pe_row[0] = PE.ptr<float>(0);

  if(!is_undirectional_graph)
    pe_row[1] = PE.ptr<float>(1);


  //update the unary energies
  int source_to_node;
  int node_to_sink;
  for(int i = 0; i < num_nodes; ++i){
    source_to_node = static_cast<int>(ue_row[0][i] * mul + 0.5);
    node_to_sink   = static_cast<int>(ue_row[1][i] * mul + 0.5);

    g->edit_tweights(i, source_to_node, node_to_sink);
    
    //For dynamic graph cut
    //g->mark_node(i);
  }

  //update binary energies
  //weight for forward capacity and reverse capacity, resepectively
  int cap, rev_cap;
  int start_ind, end_ind;
  if(is_undirectional_graph){
    for(int j = 0; j < num_edges; ++j){
      cap = rev_cap = static_cast<int>(pe_row[0][j] * mul + 0.5);

      start_ind = pi_row[0][j];
      end_ind   = pi_row[1][j];

      g->edit_edge(start_ind, end_ind, cap, rev_cap);
 
      //For dynamic graph cut
      //g->mark_node(start_ind);
      //g->mark_node(end_ind);
    }
  } 
  else{
    for(int j = 0; j < num_edges; ++j){
      cap     = static_cast<int>(pe_row[0][j] * mul + 0.5);
      rev_cap = static_cast<int>(pe_row[1][j] * mul + 0.5);
      
      //my direction of PI is different from Graphcut's
      //cap     = static_cast<int>(pe_row[1][j] * mul);
      //rev_cap = static_cast<int>(pe_row[0][j] * mul);

      start_ind = pi_row[0][j];
      end_ind   = pi_row[1][j];

      g->edit_edge(start_ind, end_ind, cap, rev_cap);

      //For dynamic graph cut
      //g->mark_node(start_ind);
      //g->mark_node(end_ind);

    }
  }

  //perform graph cut
  energy = g->maxflow();
 
  //format the ouptut to x_opt
  x_opt.create(1, num_nodes, CV_32SC1);

  int* x_row = x_opt.ptr<int>(0);

  for(int i = 0; i < num_nodes; ++i) {
    x_row[i] = static_cast<int>(g->what_segment(i)==GraphType::SOURCE?1:0);
  }
 

}
