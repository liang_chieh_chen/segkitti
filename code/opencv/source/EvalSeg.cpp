/*
 * Evaluate the segmentation results
 *
 *
 *
 *
 * Liang-Chieh Chen@TTIC, August 2013
 *
 */

#include "Constants.h"
#include "ConfusionMatrix.h"
#include "ExpConfiguration.h"
#include "Utility.h"

#include <vector>
#include <fstream>

using namespace std;

bool save_detailed_results = false;  //save detailed results
int num_input_argument = 5;  //including the exe name

static void help(char *argv[])
{
  cout << endl
       << "argv[1]: experiment setting (.xml)" << endl
       << "argv[2]: test image list (.txt)"    << endl
       << "argv[3]: target folder (where the map results stored)" << endl
       << "argv[4]: output file name (.txt)"   << endl;
  
}

int main(int argc, char *argv[])
{
  if (argc < num_input_argument){
    help(argv);
    return -1;
  }
 
  int result;

  //read the experiment configuration
  string filename = argv[1];
  
  ExpConfiguration config;
    
  result = readExpConfigurationFromFile(filename, config);

  if(result == -1){
    cerr << "Failed to open " << filename << "in SingleImgGrabcutDemo" << endl;
    exit(-1);
  }

  //get test image list
  string test_filename = argv[2];
  vector<string> file_names;

  string map_path = argv[3];

  read_filenames(test_filename, file_names);

  ////find the train SVM C from the file name
  string exp_id, train_c;
  //folder encoded as map_expIDx_Cx_
  extract_expID_and_C(map_path, exp_id, train_c);
  //


  /*debug
    for(size_t i = 0; i < file_names.size(); ++i){
    cout << file_names[i] << endl;
    }
  //*/

  string gt_path;

  gt_path = config.detectionDataBaseDir + config.trackletsDir + GT_FOLDER + "/";

  //will load the pc image to compute the number of car point clouds
  string pc_path;
  
  pc_path = config.detectionDataBaseDir + config.trackletsDir + PC_FOLDER + "/";


  //accumulate the confusion matrix
  int num_class = 2;

  ConfusionMatrix confusion(num_class);
  vector<double> scores(file_names.size(), 0.0);

  vector<ConfusionMatrix*> scale_confusions;

  for(int i = 0; i < TRACKLET_BD_REGION_SIZE; ++i){
    scale_confusions.push_back(new ConfusionMatrix(num_class));
  }

  //accumulate the number of car point clouds for each image
  vector<double> frg_pc_count(TRACKLET_BD_REGION_SIZE, 0);
  vector<double> frg_img_count(TRACKLET_BD_REGION_SIZE, 0);


  //read the groundtruth and map results
  for(size_t i = 0; i < file_names.size(); ++i){
    cout << "Evaluating image " << file_names[i] << endl;
    Mat gt;
    Mat map;

    filename = gt_path+file_names[i]+config.imgExt;

    gt  = imread(filename, CV_LOAD_IMAGE_GRAYSCALE);
    
    filename = map_path+file_names[i]+config.imgExt;

    map = imread(filename, CV_LOAD_IMAGE_GRAYSCALE);

    ////load the pc image and compute the number of car point clouds
    Mat pc;

    filename = pc_path+file_names[i]+config.imgExt;
    pc = imread(filename, CV_LOAD_IMAGE_GRAYSCALE);

    int frg_count = 0;

    uchar* pc_row;
    for(int row = 0; row < pc.rows; ++row){
      pc_row = pc.ptr<uchar>(row);
      for(int col = 0; col < pc.cols; ++col){
	if(pc_row[col] == 255)
	  frg_count++;
      }
    }

    /*for debug
      double mmin, mmax;
      minMaxLoc(gt, &mmin, &mmax);
      cout << "gt m:" << mmin << "," << mmax << endl;
      minMaxLoc(map, &mmin, &mmax);
      cout << "map m:" << mmin << "," << mmax << endl;

      cout << gt.size() << "," << map.size() << endl;
      imshow("gt", gt);
      imshow("map", map);
    //*/

    CV_Assert(gt.size() == map.size());

    double gt_size = sqrt(gt.rows * gt.cols);
    int region_id = -1;
    double region_val = -1;

    find_closest_element(gt_size, TRACKLET_BD_REGION_RANGE, TRACKLET_BD_REGION_SIZE, &region_val, &region_id);

    uchar* gt_row;
    uchar* map_row;

    ConfusionMatrix imgConfusion(num_class);

    for(int row = 0; row < gt.rows; ++row){
      gt_row = gt.ptr<uchar>(row);
      map_row = map.ptr<uchar>(row);

      for(int col = 0; col < gt.cols; ++col){
	//divide value by 255, since foreground = 255
	imgConfusion.accumulate( gt_row[col]/255, map_row[col]/255 );
      }
    }

    /*for debug
      imgConfusion.printCounts();
      waitKey();
    //*/

    scores[i] = imgConfusion.accuracy();

    confusion.accumulate(imgConfusion);

    scale_confusions[region_id]->accumulate(imgConfusion);

    //accumulate frg point clouds
    frg_pc_count[region_id] += frg_count;
    frg_img_count[region_id]++;

    if(region_id >= TRACKLET_BD_REGION_SIZE-2){
      cout << "id,name:" << region_id << "," << file_names[i] << endl;
      cout << "size:" << frg_count << "," << region_val << end;
    }

  }
  
  /* for debug
  for(size_t kk = 0; kk < frg_pc_count.size(); ++kk){
    cout << frg_pc_count[kk] << ' ';
  }
  cout << endl;
  for(size_t kk = 0; kk < frg_img_count.size(); ++kk){
    cout << frg_img_count[kk] << ' ';
  }
  cout << endl;
  //*/

  //display results
  confusion.printCounts(cout, "--- Raw Confusion Matrix ---");
  confusion.printRowNormalized(cout, "--- Class Confusion Matrix ---");
  //confusion.printPrecisionRecall(cout, "--- Recall/Precision (by Class) ---");
  //confusion.printF1Score(cout, "--- F1-Score (by Class) ---");
  confusion.printJaccard(cout, "--- Intersection/Union Metric (by Class) ---");
   
  cout << "Overall class accuracy: " << confusion.accuracy()  << endl;
  cout << "Average class accuracy: " << confusion.avgRecall() << endl;
  cout << "Average jaccard score:  " << confusion.avgJaccard()<< endl;

  cout << "Average jaccard score for each scale: ";
  for(size_t i = 0; i < scale_confusions.size(); ++i){
    cout << scale_confusions[i]->avgJaccard() << ";";
  }
  cout << endl;

  cout << "Average number of foreground point clouds for each scale: ";
  for(size_t i = 0; i < frg_pc_count.size(); ++i){
    if(frg_img_count[i] != 0)
      cout << frg_pc_count[i] / frg_img_count[i] << ";";
    else
      cout << 0 << ";";
  }
  cout << endl;

  ////save the results
  filename = argv[4];

  fstream fs(filename.c_str(), fstream::out | fstream::app);

  if( !fs.is_open() ){
    cerr << "Fail to open " << filename << " for saving results..." << endl;
    return -1;
  }

  if(save_detailed_results){
    ///* detailed results
    confusion.printRowNormalized(fs, "--- Class Confusion Matrix ---");
    //confusion.printPrecisionRecall(fs, "--- Recall/Precision (by Class) ---");
    //confusion.printF1Score(fs, "--- F1-Score (by Class) ---");
    confusion.printJaccard(fs, "--- Intersection/Union Metric (by Class) ---");
   
    fs << "Overall class accuracy: " << confusion.accuracy()  << endl;
    fs << "Average class accuracy: " << confusion.avgRecall() << endl;
    fs << "Average jaccard score:  " << confusion.avgJaccard()<< endl;

    for(size_t i = 0; i < scale_confusions.size(); ++i){
      fs << scale_confusions[i]->avgJaccard() << ",";
    }
    fs << ";";
    
    for(size_t i = 0; i < frg_pc_count.size(); ++i){
      if(frg_img_count[i] != 0)
	fs << frg_pc_count[i] / frg_img_count[i] << ",";
      else
	fs << 0 << ";";
    }
    
    fs << endl;


    //*/
  } else {
    ///*short results
    fs << exp_id << " " << train_c << " " << confusion.accuracy() << " "  << confusion.avgRecall() << " " << confusion.avgJaccard() << " ";

    for(size_t i = 0; i < scale_confusions.size(); ++i){
      fs << scale_confusions[i]->avgJaccard() << " ";
    }
    
    //fs << ";";
    
    for(size_t i = 0; i < frg_pc_count.size(); ++i){
      if(frg_img_count[i] != 0)
	fs << frg_pc_count[i] / frg_img_count[i] << " ";
      else
	fs << 0 << " ";
    }

    fs << endl;

    //*/
  }

  

  fs.close();

  for(size_t i = 0; i < scale_confusions.size(); ++i){
    delete scale_confusions[i];
  }


  return 0;
}

