/*  This file is part of Structured Prediction (SP) - http://www.alexander-schwing.de/
 *
 *  Structured Prediction (SP) is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation, either
 *  version 3 of the License, or (at your option) any later version.
 *
 *  Structured Prediction (SP) is distributed in the hope
 *  that it will be useful, but WITHOUT ANY WARRANTY; without even the
 *  implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Structured Prediction (SP).
 *  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright (C) 2010-2013  Alexander G. Schwing  [http://www.alexander-schwing.de/]
 */

//Author: Alexander G. Schwing

/*  ON A PERSONAL NOTE: I spent a significant amount of time to go through
 *  both, theoretical justifications and coding of this framework.
 *  I hope the package is useful for your task. Any citations, requests, 
 *  feedback, donations and support would be greatly appreciated.
 *  Thank you for contacting me!
 */   
#include <iostream>
#include <vector>
#include <cmath>
#include <cstdlib>
#include <stdio.h>
#include <string.h>
#include <mpi.h>
#include <omp.h>
#include <fstream>

#include "gurobi_c++.h"
#include "Timer.h"
#include "TrainingSample.h"
#include "Constants.h"

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace cv;



#ifdef USE_ON_WINDOWS
#include <io.h>
void TraverseDirectory(const std::string& path, std::string& pattern, bool subdirectories, std::vector<std::string>& fileNames) {
  struct _finddatai64_t data;
  std::string fname = path + "\\" + pattern;
  // start the finder -- on error _findfirsti64() will return -1, otherwise if no
  // error it returns a handle greater than -1.
  intptr_t h = _findfirsti64(fname.c_str(),&data);
  if(h >= 0) {
    do {
      if( (data.attrib & _A_SUBDIR) ) {
	if( subdirectories && strcmp(data.name,".") != 0 && strcmp(data.name,"..") != 0) {
	  fname = path + "\\" + data.name;
	  TraverseDirectory(fname,pattern,true, fileNames);
	}
      } else {
	fileNames.push_back(path + "\\" + data.name);
      }
    } while( _findnexti64(h,&data) == 0);

    _findclose(h);
  }
}
#else
#include <dirent.h>
#include <fnmatch.h>
void TraverseDirectory(const std::string& path, std::string& pattern, bool subdirectories, std::vector<std::string>& fileNames) {
  DIR *dir, *tstdp;
  struct dirent *dp;

  //open the directory
  if((dir  = opendir(path.c_str())) == NULL)
    {
      std::cout << "Error opening " << path << std::endl;
      return;
    }

  while ((dp = readdir(dir)) != NULL)
    {
      tstdp=opendir(dp->d_name);
		
      if(tstdp) {
	closedir(tstdp);
	if(subdirectories) {
	  //TraverseDirectory(
	}
      } else {
	if(fnmatch(pattern.c_str(), dp->d_name, 0)==0) {
	  std::string tmp(path);

	  tmp.append("/").append(dp->d_name);
	  fileNames.push_back(tmp);

	  //fileNames.push_back(dp->d_name);

	  //std::cout << fileNames.back() << std::endl;
	}
      }
    }

  closedir(dir);
  return;

}
#endif

struct InputData {
  const char* dir;
  char* InputModel;
  char* OutputModel;
  char* DataFile;
  int MaxIterations;
  int ItCounter;
  double C;
  double time;

  int ExpID;
  /*
   * ExpID: Experiment ID
   * 0 : GMM + Potts
   * 1 : GMM + Potts + discrete depth
   * 2 : GMM + Potts + continuous depth
   * 3 : GMM + Potts + star shape
   * 4 : GMM + Potts + discrete and continuous depth
   * 5 : GMM + Potts + discrete/continuous depth + star shape
   * 6 : GMM + Potts + cad contour
   * 7 : GMM + Potts + discrete/continuous + star + contour = Full Model
   * 8 : Full Model - discrete depth
   * 9 : Full Model - continuous depth
   *10 : Full Model - star
   *11 : GMM + Potts + discrete + star + contour + stereo (no cont. depth)
   *12 : GMM + Potts + discrete/cont + star + contour + stereo
   *13 : Replace CAD shape in 7 by Turker 1, (9)
   *14 : Replace CAD shape in 7 by Turker 2, (10)
   *15 : Replace CAD shape in 7 by Turker 3, (11)
   *16 : GMM + Potts + star shape + cad contour + stereo (no discrete and cont. depth), REMEMBER to set USE_ORIG_MASK=false in Constants.h!!!!
   *17 : GMM(0) + Potts(2) (Check effect of Ising prior)
   *18 : Replace CAD shape in 7 by Turker 1, 2, 3 (use three turkers)
   *19 : Full Model with CAD and plus Turker 1
   *20 : Full Model with CAD and plus Turker 2
   *21 : Full Model with CAD and plus Turker 3
   *22 : Full Model with CAD and plus Turker 1,2,3
   *23 : GMM + Potts + stereo                   REMEMBER to set USE_ORIG_MASK=false
   *24 : GMM + Potts + star shape + cad contour REMEMBER to set USE_ORIG_MASK=false
   *25 : GMM + Potts + star shape + stereo      REMEMBER to set USE_ORIG_MASK=false
   *26 : GMM + Potts + cad + stereo             REMEMBER to set USE_ORIG_MASK=false
   */

};


struct MarginRescalingData {
  int GlobalSampleID;
  int Successful;
  std::vector<double> featureDiffAndLoss;
  MarginRescalingData(size_t sz) : GlobalSampleID(-1), Successful(0) {
    featureDiffAndLoss.assign(sz, 0.0);
  }
};

class TrainingData {
  struct TrainingDataParams* params;
  std::vector<TrainingSample*> Data;

public:
  TrainingData(){}
  ~TrainingData() {
    for(std::vector<TrainingSample*>::iterator it=Data.begin(),it_e=Data.end();it!=it_e;++it) {
      delete *it;
    }
  };

  //int LoadData(int myID, int ClusterSize, struct TrainingDataParams* params) {
  //jay modify function prototype
  int LoadData(int myID, int ClusterSize, struct TrainingDataParams* params, const vector<double>& wl){
    int numFeatures = 0;
    this->params = params;
    for(int k=myID, k_e=int(params->SampleData.size());k<k_e;k+=ClusterSize) {
      TrainingSample* tmp = new TrainingSample();
      numFeatures = tmp->Load(params->SampleData[k]);

      ///* jay add
      tmp->setup_weighted_loss(wl);
      //*/

      Data.push_back(tmp);
    }
    return numFeatures;
  }
  int MaxMarginRescaling(int myID, int ClusterSize, std::vector<double>& w, std::vector<struct MarginRescalingData*>& MRData, double time) {
    int numSamplesSuccessful = 0;
    int k_e = int(Data.size());
    MRData.assign(k_e, NULL);
#pragma omp parallel
    {
      int numSamples_local = 0;
#pragma omp for nowait schedule(dynamic,1)
      for(int k=0;k<k_e;++k) {
	int res = Data[k]->MarginRescaling(w, time);
	MRData[k] = new struct MarginRescalingData(w.size()+1);
	MRData[k]->GlobalSampleID = myID + k*ClusterSize;
	if(res==0) {
	  ++numSamples_local;
	  Data[k]->AddFeatureDiffAndLoss(MRData[k]->featureDiffAndLoss, w);
	  MRData[k]->Successful = 1;
	}
      }
#pragma omp critical
      {
	numSamplesSuccessful += numSamples_local;
      }
    }
    return numSamplesSuccessful;
  }
};

class QPSolver {
  GRBEnv *env;
  GRBModel *model;
  GRBVar* vars;
public:
  QPSolver() : env(NULL), model(NULL) {};
  ~QPSolver() {
    if(model!=NULL)
      delete model;
    model = NULL;
    if(env!=NULL)
      delete env;
    env = NULL;
  };

  //jay modify prototype
  //int InitializeSolver(int numFeatures, double C, size_t numSamples) 
  int InitializeSolver(int numFeatures, double C, int ExpID, size_t numSamples) {
    std::cout << "Initializing Solver" << std::endl;
    try {
      env = new GRBEnv();

      model = new GRBModel(*env);
      model->getEnv().set(GRB_IntParam_OutputFlag, 0);
      model->getEnv().set(GRB_DoubleParam_BarConvTol, 1e-2);

      vars = model->addVars(numFeatures+int(numSamples));
      model->update();

      for(int k=0;k<numFeatures;++k) {
	///*jay modify
	///* wi >= 0 to maintain submodularity
	if(k == 0 || k == 3 || k == 4 || k == 5 || k == 7 || k == 8){
	  vars[k].set(GRB_DoubleAttr_LB, -GRB_INFINITY);
	} else{
	  vars[k].set(GRB_DoubleAttr_LB, 0.0);
        }

	/*
	 * ExpID: Experiment ID
	 *  0 : GMM(0) + Potts(1,2)
	 *  1 : GMM + Potts + discrete depth(3,4)
	 *  2 : GMM + Potts + continuous depth(5)
	 *  3 : GMM + Potts + star shape(6)
	 *  4 : GMM + Potts + discrete (3,4) and continuous depth (5)
	 *  5 : GMM + Potts + discrete/continuous depth + star shape
	 *  6 : GMM + Potts + cad contour(7)
	 *  7 : GMM + Potts + discrete/continuous + star + contour
	 *  8 : Full Model - discrete depth
	 *  9 : Full Model - continuous depth
	 * 10 : Full Model - star
	 * 11 : GMM + Potts + discrete (3,4) + star (6) + contour (7) + stereo (8)
	 * 12 : GMM + Potts + discrete/continous + star + contour + stereo
	 * 13 : Replace CAD shape in 7 by Turker 1, (9)
	 * 14 : Replace CAD shape in 7 by Turker 2, (10)
  	 * 15 : Replace CAD shape in 7 by Turker 3, (11)
	 * 16 : GMM + Potts + star shape + cad contour + stereo (no discrete and cont. depth)
	 * 17 : GMM(0) + Potts(2) (Check effect of Ising prior)
	 * 18 : Replace CAD shape in 7 by Turker 1, 2, and 3 (use three turkers).
	 * 19 : Full Model with CAD and plus Turker 1
	 * 20 : Full Model with CAD and plus Turker 2
	 * 21 : Full Model with CAD and plus Turker 3
	 * 22 : Full Model with CAD and plus Turker 1,2,3
	 * 23 : GMM + Potts + stereo
	 * 24 : GMM + Potts + star shape + cad contour 
	 * 25 : GMM + Potts + star shape + stereo
         * 26 : GMM + Potts + cad + stereo        
	 */

	//only expid = 11,12 will use stereo depth
	//if(ExpID != 11 && ExpID != 12){
	//  vars[8].set(GRB_DoubleAttr_LB, 0.0);
	//  vars[8].set(GRB_DoubleAttr_UB, 0.0);
	//}

	if(ExpID == 0){
	  //set all the other variables to be zero
	  if(k!=0 && k!=1 && k!=2){
	    vars[k].set(GRB_DoubleAttr_LB, 0.0);
	    vars[k].set(GRB_DoubleAttr_UB, 0.0);
	  }
	} else if(ExpID == 1){
	  if(k!=0 && k!=1 && k!=2 && k!=3 && k!= 4){
	    vars[k].set(GRB_DoubleAttr_LB, 0.0);
	    vars[k].set(GRB_DoubleAttr_UB, 0.0);
	  }
	}else if(ExpID == 2){
	  if(k!=0 && k!=1 && k!=2 && k!=5){
	    vars[k].set(GRB_DoubleAttr_LB, 0.0);
	    vars[k].set(GRB_DoubleAttr_UB, 0.0);
	  }
	}else if(ExpID == 3){
	  if(k!=0 && k!=1 && k!=2 && k!=6){
	    vars[k].set(GRB_DoubleAttr_LB, 0.0);
	    vars[k].set(GRB_DoubleAttr_UB, 0.0);
	  }
	}else if(ExpID == 4){
	  if(k!=0 && k!=1 && k!=2 && k!=3 && k!=4 && k!=5){
	    vars[k].set(GRB_DoubleAttr_LB, 0.0);
	    vars[k].set(GRB_DoubleAttr_UB, 0.0);
	  }
	}else if(ExpID == 5){
	  if(k!=0 && k!=1 && k!=2 && k!=3 && k!=4 && k!=5 && k!=6){
	    vars[k].set(GRB_DoubleAttr_LB, 0.0);
	    vars[k].set(GRB_DoubleAttr_UB, 0.0);
	  }
	}else if(ExpID == 6){
	  if(k!=0 && k!=1 && k!=2 && k!=7){
	    vars[k].set(GRB_DoubleAttr_LB, 0.0);
	    vars[k].set(GRB_DoubleAttr_UB, 0.0);
	  }
	}else if(ExpID == 7) {
	  if(k!=0 && k!=1 && k!=2 && k!=3 && k!=4 && k!=5 && k!=6 && k!=7){
	    vars[k].set(GRB_DoubleAttr_LB, 0.0);
	    vars[k].set(GRB_DoubleAttr_UB, 0.0);
	  }
	}else if(ExpID == 8){
	  if(k!=0 && k!=1 && k!=2 && k!=5 && k!=6 && k!=7){
	    vars[k].set(GRB_DoubleAttr_LB, 0.0);
	    vars[k].set(GRB_DoubleAttr_UB, 0.0);
	  }
	}else if(ExpID == 9){
	  if(k!=0 && k!=1 && k!=2 && k!=3 && k!=4 && k!=6 && k!=7){
	    vars[k].set(GRB_DoubleAttr_LB, 0.0);
	    vars[k].set(GRB_DoubleAttr_UB, 0.0);
	  }
	}else if(ExpID == 10){
	  if(k!=0 && k!=1 && k!=2 && k!=3 && k!=4 && k!=5 && k!=7){
	    vars[k].set(GRB_DoubleAttr_LB, 0.0);
	    vars[k].set(GRB_DoubleAttr_UB, 0.0);
	  }
	}else if(ExpID == 11){
	  if(k!=0 && k!=1 && k!=2 && k!=3 && k!=4 && k!=6 && k!=7 && k!=8){
	    vars[k].set(GRB_DoubleAttr_LB, 0.0);
	    vars[k].set(GRB_DoubleAttr_UB, 0.0);
	  }
	}else if(ExpID == 12){
	  if(k!=0 && k!=1 && k!=2 && k!=3 && k!=4 && k!=5 && k!=6 && k!=7 && k!=8){
	    vars[k].set(GRB_DoubleAttr_LB, 0.0);
	    vars[k].set(GRB_DoubleAttr_UB, 0.0);
	  }

	}else if(ExpID == 13){
	  if(k!=0 && k!=1 && k!=2 && k!=3 && k!=4 && k!=5 && k!=6 && k!=9){
	    vars[k].set(GRB_DoubleAttr_LB, 0.0);
	    vars[k].set(GRB_DoubleAttr_UB, 0.0);
	  }
	}else if(ExpID == 14){
	  if(k!=0 && k!=1 && k!=2 && k!=3 && k!=4 && k!=5 && k!=6 && k!=10){
	    vars[k].set(GRB_DoubleAttr_LB, 0.0);
	    vars[k].set(GRB_DoubleAttr_UB, 0.0);
	  }
	}else if(ExpID == 15){
	  if(k!=0 && k!=1 && k!=2 && k!=3 && k!=4 && k!=5 && k!=6 && k!=11){
	    vars[k].set(GRB_DoubleAttr_LB, 0.0);
	    vars[k].set(GRB_DoubleAttr_UB, 0.0);
	  }
	}else if(ExpID == 16){
	  if(k!=0 && k!=1 && k!=2 && k!=6 && k!=7 && k!=8){
	    vars[k].set(GRB_DoubleAttr_LB, 0.0);
	    vars[k].set(GRB_DoubleAttr_UB, 0.0);
	  }
	}else if(ExpID == 17){
	  if(k!=0 && k!=2){
	    vars[k].set(GRB_DoubleAttr_LB, 0.0);
	    vars[k].set(GRB_DoubleAttr_UB, 0.0);
	  }
	}else if(ExpID == 18){
	  if(k!=0 && k!=1 && k!=2 && k!=3 && k!=4 && k!=5 && k!=6 && k!=9 && k!=10 && k!=11){
	    vars[k].set(GRB_DoubleAttr_LB, 0.0);
	    vars[k].set(GRB_DoubleAttr_UB, 0.0);
	  }
	}else if(ExpID == 19){
	  if(k!=0 && k!=1 && k!=2 && k!=3 && k!=4 && k!=5 && k!=6 && k!=7 && k!=9){
	    vars[k].set(GRB_DoubleAttr_LB, 0.0);
	    vars[k].set(GRB_DoubleAttr_UB, 0.0);
	  }
	}else if(ExpID == 20){
	  if(k!=0 && k!=1 && k!=2 && k!=3 && k!=4 && k!=5 && k!=6 && k!=7 && k!=10){
	    vars[k].set(GRB_DoubleAttr_LB, 0.0);
	    vars[k].set(GRB_DoubleAttr_UB, 0.0);
	  }
	}else if(ExpID == 21){
	  if(k!=0 && k!=1 && k!=2 && k!=3 && k!=4 && k!=5 && k!=6 && k!=7 && k!=11){
	    vars[k].set(GRB_DoubleAttr_LB, 0.0);
	    vars[k].set(GRB_DoubleAttr_UB, 0.0);
	  }
	}else if(ExpID == 22){
	  if(k!=0 && k!=1 && k!=2 && k!=3 && k!=4 && k!=5 && k!=6 && k!=7 && k!=9 && k!=10 && k!=11){
	    vars[k].set(GRB_DoubleAttr_LB, 0.0);
	    vars[k].set(GRB_DoubleAttr_UB, 0.0);
	  }
	}else if(ExpID == 23){
	  if(k!=0 && k!=1 && k!=2 && k!=8){
	    vars[k].set(GRB_DoubleAttr_LB, 0.0);
	    vars[k].set(GRB_DoubleAttr_UB, 0.0);
	  }
	}else if(ExpID == 24){
	  if(k!=0 && k!=1 && k!=2 && k!=6 && k!=7){
	    vars[k].set(GRB_DoubleAttr_LB, 0.0);
	    vars[k].set(GRB_DoubleAttr_UB, 0.0);
	  }
	}else if(ExpID == 25){
	  if(k!=0 && k!=1 && k!=2 && k!=6 && k!=8){
	    vars[k].set(GRB_DoubleAttr_LB, 0.0);
	    vars[k].set(GRB_DoubleAttr_UB, 0.0);
	  }
	}else if(ExpID == 26){
	  if(k!=0 && k!=1 && k!=2 && k!=7 && k!=8){
	    vars[k].set(GRB_DoubleAttr_LB, 0.0);
	    vars[k].set(GRB_DoubleAttr_UB, 0.0);
	  }
	}else {
	  std::cerr << "UNDEFINED type id" << std::endl;
	  exit(-1);
	}

	// end jay */ 

	//vars[k].set(GRB_DoubleAttr_LB, -GRB_INFINITY);

	vars[k].set(GRB_DoubleAttr_Obj, 0.0);
	vars[k].set(GRB_CharAttr_VType, 'C');
      }
      for(size_t k=numFeatures;k<numFeatures+numSamples;++k) {
	vars[k].set(GRB_DoubleAttr_LB, 0.0);
	vars[k].set(GRB_CharAttr_VType, 'C');
      }

      GRBQuadExpr obj = 0;
      for(int k=0;k<numFeatures;++k) {
	obj += 0.5*vars[k]*vars[k];
      }
      for(size_t k=numFeatures;k<numFeatures+numSamples;++k) {
	obj += (C/double(numSamples))*vars[k];
      }
      model->setObjective(obj, GRB_MINIMIZE);
      model->update();
    } catch (GRBException e) {
      std::cout << "Error number: " << e.getErrorCode() << std::endl;
      std::cout << e.getMessage() << std::endl;
      return -1;
    } catch (...) {
      std::cout << "Error during initialization." << std::endl;
      return -1;
    }
    return 0;
  }
  int AddConstraintNoCheck(struct MarginRescalingData* tmp) {
    GRBLinExpr expr = 0;
    int numFeatures = int(tmp->featureDiffAndLoss.size()) - 1;
    for(int k=0;k<numFeatures;++k) {
      expr += tmp->featureDiffAndLoss[k]*vars[k];
    }
    expr += vars[numFeatures+tmp->GlobalSampleID];
    model->addConstr(expr >= tmp->featureDiffAndLoss[numFeatures]);
    return 0;
  }
  int AddConstraint(std::vector<struct MarginRescalingData*>& MRData, std::vector<double>& w, std::vector<double>& xi) {
    int numFeatures = int(w.size());
    int added = 0;
    for(std::vector<struct MarginRescalingData*>::iterator iter=MRData.begin(),iter_e=MRData.end();iter!=iter_e;++iter) {
      int Sample = (*iter)->GlobalSampleID;
      GRBLinExpr expr = 0;
      double termination = 0.0;
      for(int k=0;k<numFeatures;++k) {
	expr += (*iter)->featureDiffAndLoss[k]*vars[k];
	termination += w[k]*(*iter)->featureDiffAndLoss[k];
      }
      if((*iter)->featureDiffAndLoss[numFeatures] - termination <= xi[Sample] + 1e-5) {
	std::cout << "Sample " << Sample << ": \\delta(\\hat y, y_i) - w^T(\\phi(x,y_i) - \\phi(x,\\hat y)) <= x_i + 1e-5: " << std::endl;
	std::cout << (*iter)->featureDiffAndLoss[numFeatures] << " - " << termination << " <= " << xi[Sample] << " + 1e-5" << std::endl;
	//continue;
      }
      expr += vars[numFeatures+Sample];
      model->addConstr(expr >= (*iter)->featureDiffAndLoss[numFeatures]);
      ++added;
    }
    return added;
  }
  double Solve(std::vector<double>& w, std::vector<double>& xi) {
    model->optimize();

    for(size_t k=0;k<w.size();++k) {
      w[k] = vars[k].get(GRB_DoubleAttr_X);
    }
    for(size_t k=0;k<xi.size();++k) {
      xi[k] = vars[k+w.size()].get(GRB_DoubleAttr_X);
    }


    //Potentially remove constraints if they haven't been active for a while
    //see internal sources for those improvements

    return model->get(GRB_DoubleAttr_ObjVal);
  }
};

int ParseInput(int argc, char** argv, struct InputData& OD) {
  for(int k=1;k<argc;++k) {
    if(::strcmp(argv[k], "-d")==0 && k+1!=argc) {
      OD.dir = argv[++k];
    } else if(::strcmp(argv[k], "-m")==0 && k+1!=argc) {
      OD.InputModel = argv[++k];
    } else if(::strcmp(argv[k], "-o")==0 && k+1!=argc) {
      OD.OutputModel = argv[++k];
    } else if(::strcmp(argv[k], "-f")==0 && k+1!=argc) {
      OD.DataFile = argv[++k];
    } else if(::strcmp(argv[k], "-i")==0 && k+1!=argc) {
      OD.MaxIterations = atoi(argv[++k]);
    } else if(::strcmp(argv[k], "-c")==0 && k+1!=argc) {
      OD.C = double(atof(argv[++k]));
    } else if(::strcmp(argv[k], "-t")==0 && k+1!=argc) {
      OD.time = double(atof(argv[++k]));
    } else if(::strcmp(argv[k], "-n")==0 && k+1!=argc) {
      OD.ItCounter = atoi(argv[++k]);
    } else if(::strcmp(argv[k], "-e")==0 && k+1!=argc) { 
      OD.ExpID = atoi(argv[++k]);
    }
  }
  return 0;
}

int InitializeModel(std::vector<double>& theta, const char* fn, int myID, QPSolver* Solver, std::vector<struct MarginRescalingData*>& MRDataAll) {
  int vecSize = 0;
  std::ifstream ifs(fn, std::ios_base::in | std::ios_base::binary);
  if(!ifs.is_open()) {
    std::cout << "Model not found." << std::endl;
    return -1;
  }
  ifs.read((char*)&vecSize, sizeof(int));
  if(vecSize!=int(theta.size())) {
    std::cout << "Model does not match feature size." << std::endl;
    ifs.close();
    return -1;
  } else {
    ifs.read((char*)&theta[0], vecSize*sizeof(double));
    if(myID==0) {
      struct MarginRescalingData* tmp = new struct MarginRescalingData(vecSize+1);
      ifs.read((char*)&tmp->GlobalSampleID, sizeof(int));
      ifs.read((char*)&tmp->featureDiffAndLoss[0], (vecSize+1)*sizeof(double));
      size_t numConAdded = 0;
      while(!ifs.eof()) {
	MRDataAll.push_back(tmp);
	Solver->AddConstraintNoCheck(tmp);
	++numConAdded;
	tmp = new struct MarginRescalingData(vecSize+1);
	ifs.read((char*)&tmp->GlobalSampleID, sizeof(int));
	ifs.read((char*)&tmp->featureDiffAndLoss[0], (vecSize+1)*sizeof(double));
      }
      delete tmp;
      std::cout << "  " << numConAdded << " constraint(s) found." << std::endl;
    }
    ifs.close();
    return 0;
  }
  return 0;
}

int RemoveFiles(std::vector<std::string>& fileNames, const char* fn) {
  std::ifstream ifs(fn, std::ios_base::in);
  std::string imageName;
  ifs >> imageName;
  std::vector<size_t> FoundIndices;
  while(imageName.size()>0) {
    std::string pattern = imageName.substr(0, imageName.find_last_of("."));
    for(size_t ix=0;ix<fileNames.size();++ix) {
#ifdef USE_ON_WINDOWS
      std::string tmp = fileNames[ix].substr(fileNames[ix].find_last_of("\\")+1, fileNames[ix].find_last_of(".") - fileNames[ix].find_last_of("\\") - 1);
#else
      std::string tmp = fileNames[ix].substr(fileNames[ix].find_last_of("/")+1, fileNames[ix].find_last_of(".") - fileNames[ix].find_last_of("/") - 1);
#endif
      if(tmp==pattern) {
	FoundIndices.push_back(ix);
	break;
      }
    }
    if(ifs.eof()) {
      break;
    }
    imageName = "";
    ifs >> imageName;
  }
  ifs.close();
  std::vector<std::string> tmp;
  for(size_t ix=0;ix<FoundIndices.size();++ix) {
    tmp.push_back(fileNames[FoundIndices[ix]]);
  }
  fileNames = tmp;
  return 0;
}

int MergeMaxMarginData(int myID, int ClusterSize, size_t numSamples, int numFeatures, std::vector<struct MarginRescalingData*>& MRData, std::vector<struct MarginRescalingData*>& MRDataMerged) {
  std::vector<double> data(numSamples*(numFeatures+2), 0.0);
  for(std::vector<struct MarginRescalingData*>::iterator iter=MRData.begin(),iter_e=MRData.end();iter!=iter_e;++iter) {
    if((*iter)->Successful>0) {
      data[(*iter)->GlobalSampleID*(numFeatures+2)] = double((*iter)->GlobalSampleID);
      memcpy((char*)&data[(*iter)->GlobalSampleID*(numFeatures+2)+1], (char*)&(*iter)->featureDiffAndLoss[0], sizeof(double)*(numFeatures+1));
    } else {
      data[(*iter)->GlobalSampleID*(numFeatures+2)] = -1.0;
    }
  }
  std::vector<double> dataMerged(int(numSamples)*(numFeatures+2), 0.0);
  int numSuccessful = 0;
  MPI::COMM_WORLD.Reduce(&data[0], &dataMerged[0], int(numSamples)*(numFeatures+2), MPI::DOUBLE, MPI::SUM, 0);
  if(myID==0) {
    for(size_t k=0;k<numSamples;++k) {
      if(dataMerged[k*(numFeatures+2)]>=0.0) {
	++numSuccessful;
	struct MarginRescalingData* tmp = new struct MarginRescalingData(numFeatures+1);
	tmp->Successful = 1;
	tmp->GlobalSampleID = int(dataMerged[k*(numFeatures+2)]);
	std::copy(&dataMerged[k*(numFeatures+2)]+1, &dataMerged[k*(numFeatures+2)]+numFeatures+2, &tmp->featureDiffAndLoss[0]);
	MRDataMerged.push_back(tmp);
      }
    }
  }
  return numSuccessful;
}

int main(int argc, char** argv) {
#ifdef USE_ON_WINDOWS
  std::string GroundTruthFolder = "";
#else
  std::string GroundTruthFolder = "";
#endif

  omp_set_num_threads(4);
  //omp_set_num_threads(1);

  std::cout << "NUM_STAR:" << NUM_STAR << std::endl;
  std::cout << "RECOVER_DEPTH_WEIGHT:" << RECOVER_DEPTH_WEIGHT << std::endl;



  InputData inp;
  inp.dir = GroundTruthFolder.c_str();
  inp.OutputModel = NULL;
  inp.InputModel = NULL;
  inp.DataFile = NULL;
  inp.MaxIterations = 10;
  inp.ItCounter = 0;
  inp.C = 1;
  inp.time = -1;

  //jay
  inp.ExpID = -1;
  //

  ParseInput(argc, argv, inp);

  std::string pattern = "*.png";
  std::vector<std::string> fileNames;
  std::string folder(inp.dir);
 
  TraverseDirectory(folder, pattern, false, fileNames);
  if(inp.DataFile!=NULL) {
    RemoveFiles(fileNames, inp.DataFile);
  }
 
  ///* jay add
  bool use_weighted_hamming_loss = false;
  int num_class = 2;
  std::vector<double> wl;

  if(use_weighted_hamming_loss){
    wl.assign(num_class, 0.0);

    for(size_t i = 0; i < fileNames.size(); ++i){
      Mat gt_img;

      gt_img = imread(fileNames[i], CV_LOAD_IMAGE_GRAYSCALE);

      CV_Assert(gt_img.type() == CV_8UC1);

      uchar* gt_row;
      for(int row = 0; row < gt_img.rows; ++row){
	gt_row = gt_img.ptr<uchar>(row);
	for(int col = 0; col < gt_img.cols; ++col){
	  if(gt_row[col] == 0){
	    wl[0]++;
	  } else if(gt_row[col] == 255) {
	    wl[1]++;
	  } else {
	    std::cerr << "Wrong gt label." << std::endl;
	  }
	}
      }

      //for debug
      //std::cout << "w:" << wl[0] << ',' << wl[1] << std::endl;
      //imshow("gt", gt_img);
      //waitKey();

    }

    double total = 0.0;
    for(size_t i = 0; i < wl.size(); ++i){
      total += wl[i];
    }
    for(size_t i = 0; i < wl.size(); ++i){
      wl[i] = total / wl[i];

      //wl[i] = 1 / wl[i];
    }

  } else {
    //equal weight
    wl.assign(num_class, 1.0);
  }

  /*for debug
  std::cout << "weighted loss:";
  for(size_t i = 0; i < wl.size(); ++i)
    std::cout << wl[i] << " ";
  std::cout << std::endl;
  exit(-1);
  //*/

  /*debug
  for(size_t i = 0; i < fileNames.size(); ++i){
    std::cout << fileNames[i] << std::endl;
  }
  
  exit(-1);
  //*/

  if(fileNames.size()==0) {
    std::cout << "No files to process." << std::endl;
    return 0;
  }

  struct TrainingDataParams params;
  for(size_t k=0;k<fileNames.size();++k) {
    struct SampleParams* s = new struct SampleParams;
    s->fn = fileNames[k];
    params.SampleData.push_back(s);
  }

  MPI::Init(argc, argv);
  int ClusterSize = MPI::COMM_WORLD.Get_size();
  int myID = MPI::COMM_WORLD.Get_rank();

  TrainingData Data;
  int numFeatures = Data.LoadData(myID, ClusterSize, &params, wl);
  MPI::COMM_WORLD.Bcast(&numFeatures, 1, MPI::INT, 0);

  QPSolver Solver;
  int retInit;
  if(myID==0) {
    std::cout << "Training Data: (" << fileNames.size() << ")" << std::endl;
    for(size_t k=0;k<fileNames.size();++k) {
      std::cout << "  " << fileNames[k] << std::endl;
    }
    //jay modify
    retInit = Solver.InitializeSolver(numFeatures, inp.C, inp.ExpID, params.SampleData.size());
    //
    //retInit = Solver.InitializeSolver(numFeatures, inp.C, params.SampleData.size());
    MPI::COMM_WORLD.Bcast(&retInit, 1, MPI::INT, 0);
  } else {
    MPI::COMM_WORLD.Bcast(&retInit, 1, MPI::INT, 0);
  }
  if(retInit!=0) {
    MPI::Finalize();
    return 0;
  }

  //jay change initial value to be all one
  std::vector<double> w(numFeatures, 1.0);
  //std::vector<double> w(numFeatures, -1.0);

  std::vector<double> xi(params.SampleData.size(), 0.0);

  std::vector<struct MarginRescalingData*> MRData;
  std::vector<struct MarginRescalingData*> MRDataMerged;
  std::vector<struct MarginRescalingData*> MRDataAll;

  if(inp.InputModel!=NULL) {
    if(InitializeModel(w, inp.InputModel, myID, &Solver, MRDataAll)!=0) {
      MPI::Finalize();
      return 0;
    }
    if(myID==0) {
      std::vector<double> w_tmp(numFeatures, 0.0);
      std::vector<double> xi_tmp(params.SampleData.size(), 0.0);
      Solver.Solve(w_tmp, xi_tmp);
      double diff = 0.0;
      for(int k=0;k<numFeatures;++k) {
	diff += fabs(w[k]-w_tmp[k]);
      }
      std::cout << "  Difference: " << diff << std::endl;
    }
  }


  //jay add for timing
  std::fstream fs("../result/training_time.txt", std::fstream::out);
  //

  CPrecisionTimer CTmr;
  int added = 0;
  double obj = 0.0;
  int numSamplesSuccessful;
  for(int iter=inp.ItCounter;iter<inp.MaxIterations;++iter) {
    //jay add for timing
    double t1, t2;
    t1 = (double)getTickCount();


    if(myID==0) {
      std::cout << "Iteration: " << iter << std::endl;
      CTmr.Start();
      MRDataMerged.clear();
    }
    for(size_t k=0;k<MRData.size();++k) {
      delete MRData[k];
    }
    MRData.clear();
    numSamplesSuccessful = 0;
    int numSamplesSuccessful_local = Data.MaxMarginRescaling(myID, ClusterSize, w, MRData, inp.time);
    MPI::COMM_WORLD.Allreduce(&numSamplesSuccessful_local, &numSamplesSuccessful, 1, MPI::INT, MPI::SUM);
    if(numSamplesSuccessful==0) {
      if(myID==0) {
	std::cout << std::endl << "No successful samples." << std::endl;
      }
      break;
    } else {
      int sanityCheck = MergeMaxMarginData(myID, ClusterSize, params.SampleData.size(), numFeatures, MRData, MRDataMerged);
      if(myID==0) {
	assert(sanityCheck==numSamplesSuccessful);
	std::cout << numSamplesSuccessful << " successful samples." << std::endl;
	std::cout << "Time for MarginRescaling: " << CTmr.Stop() << std::endl;
      }
    }
		
    if(myID==0) {
      added = Solver.AddConstraint(MRDataMerged, w, xi);
      MRDataAll.insert(MRDataAll.end(), MRDataMerged.begin(), MRDataMerged.end());
      MPI::COMM_WORLD.Bcast(&added, 1, MPI::INT, 0);
      if(added==0) {
	break;
      }
      CTmr.Start();
      obj = Solver.Solve(w, xi);
      std::cout << "Time for solver: " << CTmr.Stop() << std::endl;
      MPI::COMM_WORLD.Bcast(&w[0], int(w.size()), MPI::DOUBLE, 0);
      std::cout << "Objective: " << obj << std::endl;
      std::cout << "w = [";
      for(size_t k=0;k<w.size();++k) {
	std::cout << w[k] << " ";
      }
      std::cout << "]" << std::endl;
      if(inp.OutputModel!=NULL) {
	char fn[257];
	sprintf(fn, "%s.%d", inp.OutputModel, iter);
	std::ofstream ofs(fn, std::ios_base::out | std::ios_base::binary);
	ofs.write((char*)&numFeatures, sizeof(int));
	ofs.write((char*)&w[0], numFeatures*sizeof(double));
	for(size_t k=0;k<MRDataAll.size();++k) {
	  ofs.write((char*)&MRDataAll[k]->GlobalSampleID, sizeof(int));
	  ofs.write((char*)&MRDataAll[k]->featureDiffAndLoss[0], sizeof(double)*(numFeatures+1));
	}
	ofs.close();
      }
    } else {
      MPI::COMM_WORLD.Bcast(&added, 1, MPI::INT, 0);
      if(added==0) {
	break;
      }
      MPI::COMM_WORLD.Bcast(&w[0], int(w.size()), MPI::DOUBLE, 0);
    }


    //jay add for timing
    t2 = (double)getTickCount() - t1;
    fs << iter << " " <<  t2 / getTickFrequency() << std::endl;
    //
  }

  if(myID==0) {
    for(size_t k=0;k<MRDataAll.size();++k) {
      delete MRDataAll[k];
    }
    MRDataAll.clear();
    MRDataMerged.clear();
  }
  for(size_t k=0;k<MRData.size();++k) {
    delete MRData[k];
  }
  MRData.clear();
  for(size_t k=0;k<params.SampleData.size();++k) {
    delete params.SampleData[k];
  }

  MPI::Finalize();


  //jay add for timing
  fs.close();
  //


  return 0;
}
