#include "GmmModel.h"
#include "Constants.h"
#include <cfloat>
#include <cmath>
#include <iostream>

using namespace std;

GmmModel::GmmModel(int _num_comp, int _feat_size)
  :num_components(_num_comp),
   feat_size(_feat_size)
{
  int num_para = num_components * (1 + feat_size + feat_size*feat_size);

  parameters.create(1, num_para, CV_32FC1);
  parameters.setTo(Scalar(0));

  mix_weight = parameters.ptr<float>(0);
  mix_mean   = mix_weight + num_components;
  mix_cov    = mix_mean + num_components * feat_size;

  //allocate memory
  inverse_covs.resize(num_components);
  cov_determs.resize(num_components);
  ss_sums.resize(num_components);
  ss_prods.resize(num_components);
  for(int k = 0; k < num_components; ++k){
    inverse_covs[k] = new float[feat_size*feat_size];
    ss_sums[k]      = new float[feat_size];
    ss_prods[k]     = new float[feat_size*feat_size];

    for(int i = 0; i < feat_size*feat_size; ++i)
      inverse_covs[k][i] = 0;
  }

  sample_counts.resize(num_components);

  reset_ss();

}

GmmModel::~GmmModel()
{
  //release memory
  for(int k = 0; k < num_components; ++k){
    delete[] inverse_covs[k];
    delete[] ss_sums[k];
    delete[] ss_prods[k];
  }
}

void GmmModel::reset_ss()
{
  for(int k = 0; k < num_components; ++k){
    for(int i = 0; i < feat_size; ++i)
      ss_sums[k][i] = 0;
    for(int i = 0; i < feat_size*feat_size; ++i)
      ss_prods[k][i] = 0;

    sample_counts[k] = 0;
  }

  total_sample_count = 0;

}


float GmmModel::get_pxl_likelihood(const float* pxl_feature) const
{
  float lik = 0;

  for(int k = 0; k < num_components; ++k){
    lik += get_pxl_likelihood(k, pxl_feature);
  }


  return lik;
}

float GmmModel::get_pxl_likelihood(int k, const float* pxl_feature) const
{
  float lik = 0;

  if(mix_weight[k] > 0){
    //CV_Assert(cov_determs[k] > numeric_limits<float>::epsilon());

    float* mix_m = mix_mean + k*feat_size;

    vector<float> diff(feat_size);
    for(size_t i = 0; i < diff.size(); ++i){
      diff[i] = pxl_feature[i] - mix_m[i];
    }

    //compute diff * inv(cov) * diff'

    for(int j = 0; j < feat_size; ++j) {
      for(int i = 0; i < feat_size; ++i) {
        lik += diff[j] * inverse_covs[k][j*feat_size+i] * diff[i];
      }
    }

    lik = 1.0 / sqrt(cov_determs[k]) * exp(-0.5 * lik);

    /* for debug
    if(isnan(tmp)){
      cout << "lik:" << lik << "=" << cov_determs[k] << endl;
      exit(-1);
    }
    if(isinf(tmp)){
      cout << "lik:" << lik << "=" << cov_determs[k] << endl;
      exit(-1);
    }
    //*/

  }

  if(lik < ZERO_PROB){
    lik = ZERO_PROB;
  }

  if(lik > FLOAT_INF){
    lik = FLOAT_INF;
  }

  return mix_weight[k] * lik;

}

int GmmModel::find_most_likely_component(const float* pxl_feature) const
{
  int max_k = 0;

  float max_lik = 0;

  for(int k = 0; k < num_components; ++k) {
    float lik = get_pxl_likelihood(k, pxl_feature);

    if(max_lik < lik){
      max_lik = lik;
      max_k = k;
    }
  }

  return max_k;
}

/*
void GmmModel::extract_pxl_feature(Mat& img, int y, int x, vector<float>& feat)
{
  feat.resize(feat_size);

  //use color
  int nChannels = img.channels();

  const float* img_row = img.ptr<float>(y);

  for(int c = 0; c < nChanels; ++c){
    feat[c] = img_row[x+c];
  }

  if(feat_type != 0)
  {  //use color + pos

    float y_pos = (float)y/img.rows;
    float x_pos = (float)x/img.cols;

    feat[nChannels] = y_pos;
    feat[nChannels+1] = x_pos;
  }

}
*/

void GmmModel::compute_inverseCov_and_determ(int k)
{
  if(mix_weight[k] > 0) {
    Mat cov(feat_size, feat_size, CV_32FC1, mix_cov + k*feat_size*feat_size);

    cov_determs[k] = determinant(cov);

    ///*
    if(cov_determs[k] < 1e-20){
      //add small values along the diagonal matrix (regularization term)
      //to avoid singular covariance matrix
      cov = 0.01 * Mat::eye(cov.rows, cov.cols, CV_32FC1) + cov;
      cov_determs[k] = determinant(cov);
    }
    //*/

    //CV_Assert( cov_determs[k] > numeric_limits<float>::epsilon() );

    Mat cov_inv = cov.inv();

    for(int j = 0; j < cov_inv.rows; ++j){
      const float* cov_inv_row = cov_inv.ptr<float>(j);
      for(int i = 0; i < cov_inv.cols; ++i)
        inverse_covs[k][j*feat_size+i] = cov_inv_row[i];
    }

  }
}

void GmmModel::accumulate_ss(int k, const float* pxl_feature)
{
  for(int i = 0; i < feat_size; ++i){
    ss_sums[k][i] += pxl_feature[i];
  }

  for(int j = 0; j < feat_size; ++j)
    for(int i = 0; i < feat_size; ++i)
      ss_prods[k][j*feat_size+i] += pxl_feature[j]*pxl_feature[i];

  sample_counts[k]++;
  total_sample_count++;
}

void GmmModel::train()
{
  //const float small_value = 0;//0.000001;

  for(int k = 0; k < num_components; ++k){
    mix_weight[k] = (float)sample_counts[k] / total_sample_count;

    for(int c = 0; c < feat_size; ++c){
      mix_mean[k*feat_size + c] = ss_sums[k][c] / sample_counts[k];
    }

    int k_offset  = k*feat_size;
    int k_offset2 = k*feat_size*feat_size;

    for(int j = 0; j < feat_size; ++j){
      int j_offset  = j*feat_size;

      for(int i = 0; i < feat_size; ++i){
        mix_cov[k_offset2 + j_offset + i] = ss_prods[k][j_offset+i]/sample_counts[k] - mix_mean[k_offset + j]*mix_mean[k_offset + i];

        //add small values along the diagonal matrix (regularization term)
        //to avoid singular covariance matrix
        //if(i == j){
        //  mix_cov[k_offset2 + j_offset + i] += small_value;
        //}
      }
    }

    //update the inverse covariance matrix and determinant for the k-th mixture
    compute_inverseCov_and_determ(k);

  }  //end k

}

void GmmModel::print_out_parameters() const
{
  const float* para_row = parameters.ptr<float>(0);

  cout << "printing parameters:" << endl;
  for(int i = 0; i < parameters.cols; ++i){
    cout << para_row[i] << ",";
  }
  cout << endl;
}

void GmmModel::print_out_ss() const
{
  cout << "printing 1st order ss:" << endl;
  for(int k = 0; k < num_components; ++k) {
    for(int c = 0; c < feat_size; ++c)
      cout << ss_sums[k][c] << ",";
    cout << endl;
  }

  cout << "printing 2nd order ss:" << endl;
  for(int k = 0; k < num_components; ++k){
    for(int j = 0; j < feat_size; ++j){
      for(int i = 0; i < feat_size; ++i) {
        cout << ss_prods[k][j*feat_size+i] << ",";
      }
      cout << endl;
    }
    cout << endl;
  }
}
