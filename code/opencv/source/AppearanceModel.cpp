#include <opencv2/core/core.hpp>
#include <iostream>

#include "AppearanceModel.h"
#include "Constants.h"

using namespace cv;
using namespace std;

AppearanceModel::AppearanceModel()
{}

AppearanceModel::~AppearanceModel()
{}

AppearanceModel::AppearanceModel(const Mat& mask, const Rect& _roi, const Rect& _wb_roi)
  :map_result(mask.size(), mask.type(), Scalar(0)),
   roi(_roi),
   roi_wb(_wb_roi)
{
  //NOTE
  //mask provides the seeds for first iteration
  //map result provides the seeds for the following iterations

  CV_Assert(mask.depth() == CV_8UC1);    //accept only uchar gray mask

  map_result(roi) = 1;

}




