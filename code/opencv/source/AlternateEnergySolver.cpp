#include "AlternateEnergySolver.h"
#include "AppearanceModel.h"
#include "Constants.h"
#include "ContourCreater.h"
#include "ContrastSensitivePM.h"
#include "MaskCreater.h"
#include "GraphCutWrapper.h"
#include "StarShapeModel.h"
#include "Utility.h"

#include <opencv2/core/core.hpp>
#include <string>
#include <vector>
#include <sstream>
#include <iostream>

using namespace cv;
using namespace std;

AlternateEnergySolver::AlternateEnergySolver(const int _num_itr, const string& img_name)
  :EnergySolver(_num_itr, img_name)
{}
 
AlternateEnergySolver::~AlternateEnergySolver()
{}

void AlternateEnergySolver::run(AppearanceModel* app_model, ContrastSensitivePM* binary_model, MaskCreater* mask_creater, ContourCreater* contour_creater, Mat& img, Mat& mask, vector<double>& lambda, int gc_max_itr)
{
  const Rect roi = app_model->get_roi();

  Mat prv_ue, prv_pe;
  Mat ue, pe, pi;
  Mat pe2;

  Mat p_pe, p_pi;  //for Potts model
  Mat s_pe, s_pi;  //for Star model
  
  Mat ue_diff, pe_diff;
  Mat x_opt;
  double energy;
  GraphCutWrapper* gc;

  string file_name;
  stringstream ss;

  int use_dynamic = 0;  //TODO:use dynamic graph cut not implemented!!

  double t1,t2;
  
  //find initial value of depth
  Mat recon_depth;

  //t1 = (double)getTickCount();
  mask_creater->find_init_depth(recon_depth, img);
  //t2 = ((double)getTickCount() - t1) / getTickFrequency();
  //cout << "computing inital recoverd depth time:" << t2 << " sec" << endl;

  //unary term related to depth
  Mat d_ue;   
  mask_creater->build_depth_ue(d_ue, recon_depth, lambda[2]);

#if DEBUG == 1
  file_name = get_save_path() + "depth_ue_initial" + SAVE_IMG_EXT;
  visualize_ue(d_ue, roi, file_name);
#endif

  //unary term related to contour
  Mat s_ue;
  contour_creater->build_contour_ue(s_ue, img(roi), mask(roi), lambda[3]);


  for(int itr = 0; itr < get_num_itr(); ++itr) {
    //app_model->update_model_parameters(img, mask);
    app_model->compute_unary_term(img, mask, ue);

#if DEBUG ==1
    //save model parameter
    file_name = "model_parameters.dat";
    app_model->save_parameters(itr, file_name);

    //show the ue for foreground object and save it
    ss.clear();
    ss << get_save_path() << get_img_name() << "_itr" << itr << "_ue" + SAVE_IMG_EXT;;
    ss >> file_name;

    visualize_ue(ue, roi, file_name);

#endif
  
    //only need to compute the binary energy at 1st iteration
    if(itr == 0){
     
      binary_model->compute_pe(img, mask, lambda, pe, pe2, pi);          

      gc = new GraphCutWrapper(ue.cols, pi);

    } 
   
    gc->compute_map(ue + d_ue + s_ue, pe + pe2, pi, x_opt, energy);
 

    model_energy.push_back(energy);

    x_opt = x_opt.reshape(0, roi.height);
    //update map result

    app_model->update_map_result_within_roi(x_opt);
 
    //show the map result and save it
    file_name = get_save_path() + get_img_name() + "_map" + SAVE_IMG_EXT;;
    visualize_binary_img(app_model->get_map_result(), roi, file_name);

    //show the masked image
    file_name = get_save_path() + get_img_name() + "_masked" + SAVE_IMG_EXT;;
    visualize_map_result(img, app_model->get_map_result(), roi, file_name);


    //stopping criterion
    if( satisfy_stopping_criterion(itr) )
      break;

    ue.copyTo(prv_ue);
    pe.copyTo(prv_pe);

    app_model->update_model_parameters(img, mask);


  } //end itr


  //  delete pd;
  delete gc;

  //#if DEBUG == 1
  t2 = (double)getTickCount() - t1;
  cout << "time: " << t2 / getTickFrequency() << " sec." << endl;
  //#endif

  //show the model energy for each iteration
  if(!model_energy.empty()){
    cout << "model energy: ";
    for(size_t i = 0; i < model_energy.size(); ++i)
      cout << model_energy[i] << ", ";
    cout << endl;

    string file_name = "model_energy.dat";
    save_model_energy(file_name);
  }
  else{    
    cout << "model_energy is empty." << endl;
  }

  
}

bool AlternateEnergySolver::satisfy_stopping_criterion(const int itr)
{
  if(itr == 0)
    return false;

  if(abs((model_energy[itr]-model_energy[itr-1])/model_energy[itr-1]) < 1e-4)
    return true;
  else
    return false;
}

