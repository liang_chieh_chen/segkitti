/*******************************************
 * Shortest path computation
 * Modified from codes by Gabriel Peyr�
 ******************************************/
/*=================================================================
% Do shortest path computation, given existing shortest paths
%
%   [D,S,Q,stPoints] = shortestPaths_constrained(W,start_points,end_points,nb_iter_max,geoGamma,nbrHoodSize,rescale_geo);
%
%   'D' is a 2D array containing the value of the distance function to seed.
%	'S' is a 2D array containing the state of each point : 
%		-1 : dead, distance have been computed.
%		 0 : open, distance is being computed but not set.
%		 1 : far, distance not already computed.
% 'Q' contains the backlink to the node it came from (0 indexed)
%  stPoints -> index of the root node to which the point is nearest to
%	'W' is the weight matrix (inverse of the speed).
%	'start_points' is a 2 x num_start_points matrix where k is the number of starting points.

%  Distance is computed as \sum (sqrt((1-geoGamma)eucledian_ij+geoGamma*|grad(I)_ij|^2) )
%  grad(I) is divided by the number of channels in the image
%   
%   Copyright (c) 2004 Gabriel Peyr�
*=================================================================*/

#include <iostream>
#include "perform_front_propagation_2d_color.h"

#define kDead -1
#define kOpen 0
#define kFar 1

/* Global variables */
int n;			// height
int p;			// width
int nCh; // The number of channels in the image
int nPixels; // The number of pixels = n*p
int nbrHoodSize;
float Gamma=0;
float rescale_geo=1;
float* D = NULL;
float* S = NULL;
float* W = NULL;
float* Q = NULL;
float* start_points = NULL;
float* stPoints=NULL;
float* H = NULL;
float* L = NULL;
int nb_iter_max = 100000;
int nb_start_points = 0;


//#define ACCESS_ARRAY(a,i,j) a[(i)+n*(j)]  //column scan order
#define ACCESS_ARRAY(a,i,j) a[(j)+p*(i)]    //row scan order

#define D_(i,j) ACCESS_ARRAY(D,i,j)
#define S_(i,j) ACCESS_ARRAY(S,i,j)
#define H_(i,j) ACCESS_ARRAY(H,i,j)
#define Q_(i,j) ACCESS_ARRAY(Q,i,j)
#define L_(i,j) ACCESS_ARRAY(L,i,j)
#define stPoints_(i,j) ACCESS_ARRAY(stPoints,i,j)
#define heap_pool_(i,j) ACCESS_ARRAY(heap_pool,i,j)
#define start_points_(i,k) start_points[(i)+2*(k)]


class point
{
public:
  int i,j;
  point( int ii, int jj )
  { i = ii; j = jj; }

  friend int operator<(const point& rhs,const point& lhs){
    if( H==NULL )
      return D_(lhs.i,lhs.j)< D_(rhs.i,rhs.j);
    else
      return D_(lhs.i,lhs.j)+H_(lhs.i,lhs.j)<D_(rhs.i,rhs.j)+H_(rhs.i,rhs.j);
  }
  // Note: The meaning of the < operator is reversed, because using a max heap here

};

fibonacci_heap<point>::pointer* heap_pool = NULL;

typedef vector<point*> point_list;


// test the heap validity
void check_heap( int i, int j )
{
  for( int x=0; x<n; ++x )
    for( int y=0; y<p; ++y )
    {
      if( heap_pool_(x,y)!=NULL )
      {
	const point& pt = heap_pool_(x,y)->data();
	
	if( H==NULL )
        {
	  if( D_(i,j)>D_(pt.i,pt.j) ){
	    cerr << "Problem with heap." << endl;
	  }
        }
	else
	{
	  if( D_(i,j)+H_(i,j)>D_(pt.i,pt.j)+H_(pt.i,pt.j) ){
	    cerr << "Problem with heap(H)." << endl;
	  }
        }
      }
    }

  return;
}

// select to test or not to test (debug purpose)
//#define CHECK_HEAP check_heap(i,j);
#define CHECK_HEAP


//void perform_front_propagation_2d_wrapper(const Mat& fimg, const Mat& st_pts, const Rect& roi, const int ngh_conn, Mat& distance, Mat& root_points, Mat& backlinks)
void perform_front_propagation_2d_wrapper(const Mat& fimg, const Mat& st_pts, const int ngh_conn, Mat& distance, Mat& root_points, Mat& backlinks)
{

  //assume start_points has size num_elements x 2
  CV_Assert(fimg.type() == CV_32FC3);
  CV_Assert(st_pts.type() == CV_32SC1);
  CV_Assert(st_pts.cols == 2);

  //****************
  //setting up
  //****************
  //n = roi.height;
  //p = roi.width;
  n = fimg.rows;
  p = fimg.cols;
  nCh = fimg.channels();

  nPixels = n*p;
  
  nb_start_points = st_pts.rows;
  Gamma = 0;   //we only consider Euclidean distance (no Geodesic distance)
  nbrHoodSize = ngh_conn;
  rescale_geo = 0;  //only used when Gamma != 0
  
  //output variables
  distance.create(1, nPixels, CV_32FC1);
  root_points.create(1, nPixels, CV_32FC1);
  backlinks.create(1, nPixels, CV_32FC1);  

  Mat node_state(1, nPixels, CV_32FC1);

  D = reinterpret_cast<float*>(distance.data);
  S = reinterpret_cast<float*>(node_state.data);
  Q = reinterpret_cast<float*>(backlinks.data);
  stPoints = reinterpret_cast<float*>(root_points.data);

  //********************
  //main job
  //********************
  //perform_front_propagation_2d(fimg(roi), st_pts, roi);
  perform_front_propagation_2d(fimg, st_pts);


  //********************
  //Reformat Output
  //********************
  distance    = distance.reshape(0, n);
  backlinks   = backlinks.reshape(0, n);
  root_points = root_points.reshape(0, n);

}
  
//void perform_front_propagation_2d(const Mat& img_roi, const Mat& st_pts, const Rect& roi)
void perform_front_propagation_2d(const Mat& img_roi, const Mat& st_pts)
{
  // create the Fibonacci heap
  fibonacci_heap<point> open_heap;

  // initialize points
  for( int i=0; i<n; ++i )
    for( int j=0; j<p; ++j )
    {
      D_(i,j) = GW_INFINITE;
      //      D_(i,j) = D_in_(i,j);
      S_(i,j) = kFar;
      Q_(i,j) = -1;
      //      Q_(i,j) = Q_in_(i,j);
      stPoints_(i,j) = -1;
    }

  // record all the points
  heap_pool = new fibonacci_heap<point>::pointer[n*p]; 
  memset( heap_pool, 0, n*p*sizeof(fibonacci_heap<point>::pointer) );

  // initialize open list
  point_list existing_points;
  for( int k=0; k<nb_start_points; ++k )
  {
    const int* st_pts_row = st_pts.ptr<int>(k);
    //int i = st_pts_row[0] - roi.y;  //offset by roi top-left position
    //int j = st_pts_row[1] - roi.x;

    int i = st_pts_row[0];  //offset by roi top-left position
    int j = st_pts_row[1];
  
    point* pt = new point( i,j );
    existing_points.push_back( pt );			// for deleting at the end
    heap_pool_(i,j) = open_heap.push(*pt);			// add to heap
    D_( i,j ) = 0;
    S_( i,j ) = kOpen;
    stPoints_(i,j) = k;
    //Q_(i,j) = i+n*j;   //column-scan order
    Q_(i,j) = j+p*i;         //row-scan order


  }

  if(nbrHoodSize==2){
    // perform the front propagation
    int num_iter = 0;
    bool stop_iteration = GW_False;
    //since we only consider Eucledian distance, these two variables are not used.
    //float eucledianConstant=(1-Gamma);
    //float geoScale=rescale_geo*Gamma;

    while( !open_heap.empty() && num_iter<nb_iter_max && !stop_iteration )
    {
      num_iter++;

      // current point
      const point& cur_point = open_heap.top();
      open_heap.pop();
      int i = cur_point.i;
      int j = cur_point.j;
      heap_pool_(i,j) = NULL;
      S_(i,j) = kDead;
      stop_iteration = false;

      CHECK_HEAP;

      // recurse on each neighbor
      int nei_i[4] = {i+1,i,i-1,i};
      int nei_j[4] = {j,j+1,j,j-1};
      for( int k=0; k<4; ++k )
      {
	int ii = nei_i[k];
	int jj = nei_j[k];
	// check that it is not going into the forbidden region
	bool entryOk=true;

	if( ii>=0 && jj>=0 && ii<n && jj<p)
	{
	  if(entryOk)
	  {

	    /*Since we only use Eucleadian distance, we igore image gradient to make it faster
	    float l2_gradient=0;

	    Vec3f cur_color = img_roi.at<Vec3f>(i,j);
	    Vec3f ngh_color = img_roi.at<Vec3f>(ii,jj);
	    Vec3f color_diff = cur_color - ngh_color;

	    l2_gradient = color_diff.dot(color_diff);
	    
	    float A1 = D_(i,j) + sqrt(eucledianConstant+geoScale*l2_gradient);
	    */
	    float A1 = D_(i,j) + 1; 

	    if( ((int) S_(ii,jj)) == kDead )
	    {}
	    else if( ((int) S_(ii,jj)) == kOpen )
	    {
	      // check if action has change.
	      if( A1<D_(ii,jj) )
	      {

	        D_(ii,jj) = A1;
		// update the value of the closest starting point
	        //////Q_(ii,jj) = Q_(i,j);
		stPoints_(ii,jj) = stPoints_(i,j);
		//Q_(ii,jj) = i+n*j;  //column-scan
		Q_(ii,jj) = j+p*i;    //row-scan


		// Modify the value in the heap
		fibonacci_heap<point>::pointer cur_el = heap_pool_(ii,jj);
		if( cur_el!=NULL ){
		  open_heap.increase(cur_el, cur_el->data() );	// use same data for update
		  // Its increase instead of decrease because using a max_heap now!!
		}
		else{
		  cerr << "Error in heap pool allocation." << endl; 
		}
	      }
	    }
	    else if( ((int) S_(ii,jj)) == kFar )
	    {
	      //if( D_(ii,jj)!=GW_INFINITE )
	      //mexErrMsgTxt("Distance must be initialized to Inf");

	      if(A1<D_(ii,jj)){

		S_(ii,jj) = kOpen;
		// distance must have change.
		D_(ii,jj) = A1;
		// update the value of the closest starting point
		///////Q_(ii,jj) = Q_(i,j);
		stPoints_(ii,jj) = stPoints_(i,j);
		//Q_(ii,jj) = i+n*j;  //column-scan
		Q_(ii,jj) = j+p*i;    //row-scan


		// add to open list
		point* pt = new point(ii,jj);
		existing_points.push_back( pt );
		heap_pool_(ii,jj) = open_heap.push(*pt );			// add to heap	
	      }
	    }
	    else{
	      cerr << "Unkwnown state." << endl; 
	    }

	  }	// end switch
	}
      }		// end for
    }			// end while
  }
  else if(nbrHoodSize==4){
    int num_iter = 0;
    bool stop_iteration = GW_False;
    int iOffsets[8] = {1,1,0,-1,-1,-1,0,1};
    int jOffsets[8] = {0,1,1,1,0,-1,-1,-1};
    
    //since we only use Eucledian distance, geoScale not used
    //float geoScale=rescale_geo*Gamma;
    float eucledianConstants[8];

    for(int k=0;k<8;k++){
      eucledianConstants[k]=(1-Gamma)*(SQR(iOffsets[k])+SQR(jOffsets[k]));
    }

    while( !open_heap.empty() && num_iter<nb_iter_max && !stop_iteration )
    {
      num_iter++;

      // current point
      const point& cur_point = open_heap.top();
      open_heap.pop();
      int i = cur_point.i;
      int j = cur_point.j;
      heap_pool_(i,j) = NULL;
      S_(i,j) = kDead;
      stop_iteration = false;//end_points_reached(i,j);
      //bool inForbidden=false;//prevSeg_(i,j);

      CHECK_HEAP;

      for( int k=0; k<8; ++k )
      {
	int ii = i+iOffsets[k];
	int jj = j+jOffsets[k];

	bool entryOk=true;

	if( ii>=0 && jj>=0 && ii<n && jj<p)
	{
	  //	  if(!inForbidden && prevSeg_(ii,jj))
	  //	    entryOk=false;
          
	  if(entryOk)
	  {
	    /*Since we only consider Eucledian distance, we ignore image gradient for speed
	    float l2_gradient = 0;

	    Vec3f cur_color = img_roi.at<Vec3f>(i,j);
	    Vec3f ngh_color = img_roi.at<Vec3f>(ii,jj);
	    Vec3f color_diff = cur_color - ngh_color;

	    l2_gradient = color_diff.dot(color_diff);
	    
	    float A1 = D_(i,j) + sqrt(eucledianConstants[k]+geoScale*l2_gradient);
	    */
	    
	    float A1 = D_(i,j) + sqrt(eucledianConstants[k]);

	    if( ((int) S_(ii,jj)) == kDead )
	    {}
	    else if( ((int) S_(ii,jj)) == kOpen )
	    {
	      // check if action has change.
	      if( A1<D_(ii,jj) )
	      {
		D_(ii,jj) = A1;
		// update the value of the closest starting point
		//////Q_(ii,jj) = Q_(i,j);
		stPoints_(ii,jj) = stPoints_(i,j);
		//Q_(ii,jj) = i+n*j;  //column-scan
		Q_(ii,jj) = j+p*i;    //row-scan


		// Modify the value in the heap
		fibonacci_heap<point>::pointer cur_el = heap_pool_(ii,jj);
		if( cur_el!=NULL ){
		  open_heap.increase(cur_el, cur_el->data() );	// use same data for update
		  // Its increase instead of decrease because using a max_heap now!!
		}
		else{
		  cerr << "Error in heap pool allocation." << endl; 
		}
	      }
	    }
	    else if( ((int) S_(ii,jj)) == kFar )
	    {
	      //if( D_(ii,jj)!=GW_INFINITE )
	      //mexErrMsgTxt("Distance must be initialized to Inf");

	      if(A1<D_(ii,jj)){

		S_(ii,jj) = kOpen;
		// distance must have change.
		D_(ii,jj) = A1;
		// update the value of the closest starting point
		////////Q_(ii,jj) = Q_(i,j);
		stPoints_(ii,jj) = stPoints_(i,j);
		//Q_(ii,jj) = i+n*j;   //column-scan
		Q_(ii,jj) = j+p*i;     //row-scan


		// add to open list
		point* pt = new point(ii,jj);
		existing_points.push_back( pt );
		heap_pool_(ii,jj) = open_heap.push(*pt );			// add to heap	
	      }
	    }
	    else{
	      cerr << "Unkwnown state." << endl; 
	    }

	  }	// end switch
	}
      }		// end for
    }			// end while
  }
  else{
    cerr << "Invalid neighbourhood size." << endl;
  }

  // free point pool
  for( point_list::iterator it = existing_points.begin(); it!=existing_points.end(); ++it )
    GW_DELETE( *it );
  // free fibheap pool
  GW_DELETEARRAY(heap_pool);
  return;
}

