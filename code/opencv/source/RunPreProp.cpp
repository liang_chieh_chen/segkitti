#include "DataProcessor.h"

#include <iostream>
#include <string>

using namespace std;

int num_input_argument = 3; 

static void help(char **argv)
{
  cout << endl
       << "argv[1]: experiment setting (.xml)" << endl
       << "argv[2]: target name (DEFAULT:-dataset or -cad, or -mt)" << endl;
}

int main(int argc, char** argv)
{
  if (argc != num_input_argument){
    help(argv);
    return -1;
  }

  string file_name = argv[1];

  DataProcessor dp;

  string target_name = argv[2];

  if(target_name == "-cad"){
    dp.preprop_cad(file_name, 0);
  } else if(target_name == "-mt"){
    dp.preprop_cad(file_name, 1);
  } else{
    dp.preprop_dataset(file_name);
  } 

  return 0;
}
