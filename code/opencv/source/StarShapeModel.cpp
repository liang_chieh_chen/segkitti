#include "StarShapeModel.h"
#include "Constants.h"
#include "perform_front_propagation_2d_color.h"
#include "Utility.h"

#include <vector>
#include <opencv2/core/core.hpp>
#include <cmath>
#include <iostream>
#include <cstdlib>
#include <limits>

using namespace std;
using namespace cv;


StarShapeModel::StarShapeModel(const int conn, const int num_sub, const string& _file_path)
  :ngh_conn(conn/2),
   num_subsample(num_sub),
   file_path(_file_path)
{
  if(ngh_conn != 2 && ngh_conn != 4){
    cerr << "nc must be 4 or 8!" << endl;
    exit(1);
  }
}

StarShapeModel::~StarShapeModel()
{}

void StarShapeModel::compute_pe(const Mat& fimg, const Mat& mask, Mat& PE, Mat& PI, const bool load_file, const string& target_name)
{
  CV_Assert(fimg.type() == CV_32FC3);
  CV_Assert(mask.type() == CV_8UC1);

  string filename;
    
  if(load_file){
    filename = file_path + target_name + "_pe_" + NumberToString(num_subsample) + ".bin";

    load_sparse_float_mat(filename, PE);

    filename = file_path + target_name + "_pi_" + NumberToString(num_subsample) + ".bin";

    load_int_mat_bin(filename, PI);


  } else {
    //extract foreground seeds from MASK
    Mat start_points;
    extract_frg_seeds(mask, start_points);

#if DEBUG == 1
    visualize_position("star centers", start_points, mask.rows, mask.cols);
#endif

    //compute shortest paths
    Mat distance, root_points, backlinks;
    compute_shortest_paths(fimg, start_points, distance, root_points, backlinks);

#if DEBUG == 1
    /*
      imagesc_bw("distance to stars", distance);
      imagesc_bw("star regions", root_points);
    */

#endif

    ///*
    //save all necessary information,
    //so that we can draw results by MATLAB    
    filename = file_path + "_star" + NumberToString(NUM_STAR) + "_backlinks.txt";
    save_float_mat(filename, backlinks);

    filename = file_path + "_star" + NumberToString(NUM_STAR) + "_distance.txt";
    save_float_mat(filename, distance);

    filename = file_path + "_star" + NumberToString(NUM_STAR) + "_img_roi.txt";
    save_float_mat(filename, fimg);

    //save_float_mat("../result/star_backlinks.txt", backlinks);
    //save_float_mat("../result/star_distance.txt", distance);
    //save_float_mat("../result/star_img_roi.txt", fimg);

    //*/


    //compute PE, PI
    int num_edge = fimg.cols*(fimg.rows-1) + (fimg.cols-1)*fimg.rows;

    if (ngh_conn == 4)
      num_edge += 2*(fimg.cols-1)*(fimg.rows-1);

    //PE has two rows where second row specifies the reverse edge weight
    PE.create(2, num_edge, CV_32FC1);  
    PI.create(2, num_edge, CV_32SC1);

    float* pe_row[2];
  
    for(int i = 0; i < 2; ++i)
      pe_row[i] = PE.ptr<float>(i);

    int* pi_row[2];
  
    for(int i = 0; i < 2; ++i)
      pi_row[i] = PI.ptr<int>(i);

    //backlinks is of size M x N (within the roi)
    int node_ind = 0;
    int pi_count = 0;
    float* bkl_row;

    //float max_f_value = 100;//numeric_limits<int>::max() / 10;

    //will learn the weight for max_f_value
    float max_f_value = 1;
  
    for(int row = 0; row < backlinks.rows; ++row){
      bkl_row = backlinks.ptr<float>(row);

      for(int col = 0; col < backlinks.cols; ++col){
	if(node_ind != bkl_row[col]){
	  //node i (pi_row[0]) to node j (pi_row[1])
	  pi_row[0][pi_count] = static_cast<int>(bkl_row[col]);
	  pi_row[1][pi_count] = node_ind;

	  //to fit for Graphcut's convention.
	  //we assume 1 for foreground, however, Graphcut assumes 0!
	  pe_row[0][pi_count] = 0;
	  pe_row[1][pi_count] = max_f_value;

	  pi_count++;


	}

	node_ind++;      
      }
    }
  
    //shrink pe, pi
    PE = PE(Range::all(), Range(0, pi_count));
    PI = PI(Range::all(), Range(0, pi_count));
  }
}

void StarShapeModel::compute_shortest_paths(const Mat& fimg, const Mat& start_points, Mat& distance, Mat& root_points, Mat& backlinks)
{
  perform_front_propagation_2d_wrapper(fimg, start_points, ngh_conn, distance, root_points, backlinks);

}

void StarShapeModel::extract_frg_seeds(const Mat& mask, Mat& start_points)
{
  //output start_points has size = num_elements x 2

  //assume at most 1/5 of mask are foreground seeds
  int max_num_elements = mask.cols * mask.rows;

  start_points.create(max_num_elements, 2, CV_32SC1);
  

  const uchar* mask_row;
  int num_elements = 0;

  for(int row = 0; row < mask.rows; ++row){
    mask_row = mask.ptr<uchar>(row);

    for(int col = 0; col < mask.cols; ++col){
      if(mask_row[col] == FOREGROUND_LABEL){
	int* sp_row = start_points.ptr<int>(num_elements);

	sp_row[0] = row;
	sp_row[1] = col;

	num_elements++;	  
      }

      if(num_elements > max_num_elements){
	cerr << "number of elements larger than expected in StarShapeModel::extract_frg_seeds!" << endl;
      }

    }
  }

  start_points = start_points(Range(0, num_elements), Range::all());

  //do KMeans on start_points; take only a few as foreground seeds
  //(maybe a simple subsampling method can be used, instead)
  int num_sub;

  if(num_subsample > num_elements)
    num_sub = num_elements;
  else
    num_sub = num_subsample;

  Mat labels;
  Mat samples;
  Mat centers;
  double eps = 0.000001;
  int max_itr = 100;
  int attempts = 0;


  start_points.convertTo(samples, CV_32F);

  kmeans(samples, num_sub, labels, TermCriteria(CV_TERMCRIT_ITER|CV_TERMCRIT_EPS, eps, max_itr), attempts, KMEANS_PP_CENTERS, centers );
  
  centers.convertTo(start_points, CV_32S);


}

/*
StarShapeModel::StarShapeModel(const Rect& _roi, int conn, int num_sub)
  :roi(_roi),
   ngh_conn(conn/2),
   num_subsample(num_sub)
{
  if(ngh_conn != 2 && ngh_conn != 4){
    cerr << "nc must be 4 or 8!" << endl;
    exit(1);
  }
}

StarShapeModel::~StarShapeModel()
{}

void StarShapeModel::compute_pe(const Mat& fimg, const Mat& mask, Mat& PE, Mat& PI)
{
  CV_Assert(fimg.type() == CV_32FC3);
  CV_Assert(mask.type() == CV_8UC1);

  //extract foreground seeds from MASK
  Mat start_points;
  extract_frg_seeds(mask, start_points);

#if DEBUG == 1
  visualize_position("star centers", start_points, mask.rows, mask.cols);
#endif

  //compute shortest paths
  Mat distance, root_points, backlinks;
  compute_shortest_paths(fimg, start_points, distance, root_points, backlinks);

#if DEBUG == 1
  imagesc_bw("distance to stars", distance);
  imagesc_bw("star regions", root_points);

  //save all necessary information,
  //so that we can draw results by MATLAB
  save_float_mat("../result/star_backlinks.txt", backlinks);
  save_float_mat("../result/star_distance.txt", distance);
  save_float_mat("../result/star_img_roi.txt", fimg(roi));
#endif

  //compute PE, PI
  int num_edge = roi.width*(roi.height-1) + (roi.width-1)*roi.height;

  if (ngh_conn == 4)
    num_edge += 2*(roi.width-1)*(roi.height-1);

  //PE has two rows where second row specifies the reverse edge weight
  PE.create(2, num_edge, CV_32FC1);  
  PI.create(2, num_edge, CV_32SC1);

  float* pe_row[2];
  
  for(int i = 0; i < 2; ++i)
    pe_row[i] = PE.ptr<float>(i);

  int* pi_row[2];
  
  for(int i = 0; i < 2; ++i)
    pi_row[i] = PI.ptr<int>(i);

  //backlinks is of size M x N (within the roi)
  int node_ind = 0;
  int pi_count = 0;
  float* bkl_row;

  float max_f_value = 10000;//numeric_limits<int>::max() / 10;

  for(int row = 0; row < backlinks.rows; ++row){
    bkl_row = backlinks.ptr<float>(row);

    for(int col = 0; col < backlinks.cols; ++col){
      if(node_ind != bkl_row[col]){
	pi_row[0][pi_count] = static_cast<int>(bkl_row[col]);
	pi_row[1][pi_count] = node_ind;

	//to fit for Graphcut's convention.
	//we assume 1 for foreground, however, Graphcut assumes 0!
	pe_row[0][pi_count] = 0;
	pe_row[1][pi_count] = max_f_value;

	pi_count++;


      }

      node_ind++;      
    }
  }
  
  //shrink pe, pi
  PE = PE(Range::all(), Range(0, pi_count));
  PI = PI(Range::all(), Range(0, pi_count));

}

void StarShapeModel::compute_shortest_paths(const Mat& fimg, const Mat& start_points, Mat& distance, Mat& root_points, Mat& backlinks)
{
  perform_front_propagation_2d_wrapper(fimg, start_points, roi, ngh_conn, distance, root_points, backlinks);
}

void StarShapeModel::extract_frg_seeds(const Mat& mask, Mat& start_points)
{
  //output start_points has size = num_elements x 2

  //assume at most 1/5 of mask are foreground seeds
  int max_num_elements = roi.width * roi.height / 5;

  start_points.create(max_num_elements, 2, CV_32SC1);
  

  const uchar* mask_row;
  int num_elements = 0;

  for(int row = roi.y; row < roi.y + roi.height; ++row){
    mask_row = mask.ptr<uchar>(row);

    for(int col = roi.x; col < roi.x + roi.width; ++col){
      if(mask_row[col] == FOREGROUND_LABEL){
	int* sp_row = start_points.ptr<int>(num_elements);

	sp_row[0] = row;
	sp_row[1] = col;

	num_elements++;	  
      }

      if(num_elements > max_num_elements){
	cerr << "number of elements larger than expected in StarShapeModel::extract_frg_seeds!" << endl;
      }

    }
  }

  start_points = start_points(Range(0, num_elements), Range::all());

  //do KMeans on start_points; take only a few as foreground seeds
  //(maybe a simple subsampling method can be used, instead)
  int num_sub;

  if(num_subsample > num_elements)
    num_sub = num_elements;
  else
    num_sub = num_subsample;

  Mat labels;
  Mat samples;
  Mat centers;
  double eps = 0.000001;
  int max_itr = 100;
  int attempts = 0;


  start_points.convertTo(samples, CV_32F);

  kmeans(samples, num_sub, labels, TermCriteria(CV_TERMCRIT_ITER|CV_TERMCRIT_EPS, eps, max_itr), attempts, KMEANS_PP_CENTERS, centers );
  
  centers.convertTo(start_points, CV_32S);


}
*/
