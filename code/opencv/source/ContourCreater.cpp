#include "Constants.h"
#include "ContourCreater.h"
#include "Utility.h"

#include <opencv2/imgproc/imgproc.hpp>
#include <vector>
#include <cmath>

using namespace std;

ContourCreater::ContourCreater(string filename, bool use_dt)
  :shape_filename(filename),
   m_use_dt(use_dt)
{}

ContourCreater::~ContourCreater()
{}

void ContourCreater::compute_unary_term(Mat& s_ue)
{
  string filename;

  //load the shape image
  Mat shape_img;
  shape_img = imread(shape_filename, CV_LOAD_IMAGE_GRAYSCALE);

  if(!shape_img.data){     
    cerr << "Failed to load img " << shape_filename << endl;
    return;
  }

  //for debug
  //imwrite("../result/tmp/shape.png", shape_img);
  //*/

  //find the shape contour
  Mat dt_contour; //(shape_img.size(), CV_8UC1, Scalar(0));


  if(m_use_dt){ 
    ////will compute the contour and perform distance transform

    build_region_contour(dt_contour, shape_img);

    //perform distance transform for the contour
    //1.change the edge value to be zero
    //2.perform distance transform
    dt_contour = 255 - dt_contour;

    //imshow("edge", dt_contour);
    //imwrite("../result/tmp/edge.png", dt_contour);
    //waitKey();

    //note the type of dt_contour is changed to 32FC1
    distanceTransform(dt_contour, dt_contour, CV_DIST_L2, CV_DIST_MASK_PRECISE);

    //normalize the distanceTransform
    double e_min, e_max;
    minMaxLoc(dt_contour, &e_min, &e_max);

    if(e_max != 0)
      dt_contour = dt_contour / e_max;

#if DEBUG == 1
    //filename = "../result/tmp/frg_dt_contour.png";
    //imagesc_bw("dt_contour", dt_contour, filename);
    //imagesc_bw("dt_contour", dt_contour);
    //waitKey();
#endif

    add_sign_distance_transform(dt_contour, shape_img);

    /* for debug
       double mmin, mmax;
       minMaxLoc(dt_contour, &mmin, &mmax);
       cout << "dt m:" << mmin << "," << mmax << endl;
    //*/

#if DEBUG == 1
    //filename = "../result/tmp/frg_sign_dt_contour.png"; // + SAVE_IMG_EXT;
    //imagesc_bw("sign_dt_contour", dt_contour, filename);
    //imagesc_bw("sign dt", dt_contour);
    //waitKey();
#endif

  } else {
    //no need to perform distance transform
    //used for e.g., average shape
    //all we need to do is normalize the value and change sign

    /*
    double mmin, mmax;

    minMaxLoc(dt_contour, &mmin, &mmax);

    if(mmax < 0){
      cerr << "something wrong in ContourCreater: max value of dt_contour < 0" << endl;
    }

    if(mmax == 0)
      mmax = 1;

    shape_img.convertTo(dt_contour, CV_32F, -1.0/mmax);
    //*/

    shape_img.convertTo(dt_contour, CV_32F, -1.0/255);

  }
  
  //build the unary term
  //assume the contour has size the same as roi
  CV_Assert(dt_contour.type() == CV_32FC1);

  s_ue.create(2, dt_contour.rows * dt_contour.cols, CV_32FC1);
  s_ue.setTo(Scalar(0));

  float* ue_bkg_row = s_ue.ptr<float>(0);
  float* ue_frg_row = s_ue.ptr<float>(1);

  const float* dt_row;
  int node_id = 0;

  float energy;
  //float max_energy = 0.1;  //threshold the output to avoid large value of distance transform

  for(int row = 0; row < dt_contour.rows; ++row){
    dt_row = dt_contour.ptr<float>(row);

    for(int col = 0; col < dt_contour.cols; ++col){
      energy = dt_row[col];

      /* //threshold the output to avoid overflow
	 if(energy > max_energy){
	 energy = max_energy;
	 }
	 if(energy < -max_energy){
	 energy = -max_energy;
	 }	  
      //*/

      //normalize the unary to avoid overflow
      energy /= 2;

      ue_bkg_row[node_id] = -energy;
      ue_frg_row[node_id] = energy; //dt_row[col];

      node_id++;
    }
  } 
   
}

void ContourCreater::build_region_contour(Mat& dest, const Mat& source)
{
  //use canny edge detector to find the region contour
  //

  Mat tmp;

  double threshold1 = 100;
  double threshold2 = 200;

  Canny( source, tmp, threshold1, threshold2);

  tmp.copyTo(dest);

}

void ContourCreater::add_sign_distance_transform(Mat& dt, const Mat& region)
{
  CV_Assert(dt.type() == CV_32FC1);
  CV_Assert(region.type() == CV_8UC1);
  CV_Assert(dt.size() == region.size());

  float* dt_row;
  const uchar* region_row;

  for(int row = 0; row < dt.rows; ++row){
    dt_row = dt.ptr<float>(row);
    region_row = region.ptr<uchar>(row);

    for(int col = 0; col < dt.cols; ++col){
      if(region_row[col]){
	dt_row[col] = -1 * dt_row[col];
      }
    }
  }
  
}











/* svm-related
ContourCreater::ContourCreater(int _feat_dim)
  : feat_dim(_feat_dim),
    pos_weight(1.0),
    normalize_pos(false),
    change_label_sign(false)
{
}

ContourCreater::~ContourCreater()
{}

void ContourCreater::create_dt_contour(Mat& dt_contour, const Mat& fimg, const Mat& mask)
{
  //
  //NOTE values of contour are 0 or 1
  //
  CV_Assert(mask.type() == CV_8UC1);
  CV_Assert(fimg.type() == CV_32FC3);
  
  dt_contour.create(mask.size(), mask.type());
  dt_contour.setTo(Scalar(0));

  create_dt_contour_by_svm(dt_contour, fimg, mask);

}

void ContourCreater::create_dt_contour_by_svm(Mat& dt_contour, const Mat& fimg, const Mat& mask)
{
  //setup svm parameters
  CvSVMParams params;

  params.svm_type    = CvSVM::C_SVC;
  params.kernel_type = CvSVM::RBF;
  params.gamma       = 0.0001;  //weight: exp(-gamma \|x_i - x_j\|^2 )
  params.C           = 100;  //Parameter C of a SVM optimization problem
  params.term_crit   = cvTermCriteria(CV_TERMCRIT_ITER, 1000, 1e-6);


  //collect training data from mask
  //+1 : frg point clouds 
  //-1 : bkg point clouds
  Mat train_labels;
  Mat train_data;
  collect_training_data(train_labels, train_data, fimg, mask);

  //train the svm
  CvSVM svm;
  svm.train(train_data, train_labels, Mat(), Mat(), params);

  //find the decision regions
  Mat region(dt_contour.size(), dt_contour.type(), Scalar(0));
  build_region_by_svm(region, fimg, svm);

  string filename;
#if DEBUG == 1
  filename = "../result/frg_region" + SAVE_IMG_EXT;
  imshow("frg_region", 255*region);
  imwrite(filename, 255*region);
#endif

  //find the contour of the decision regions
  build_region_contour(dt_contour, region);

#if DEBUG == 1
  filename = "../result/frg_contour" + SAVE_IMG_EXT;
  imshow("frg_contour", 255*dt_contour);
  imwrite(filename, 255*dt_contour);
#endif 

  //find the distance transform of the contour
  //1.change the edge value to be zero
  //2.perform distance transform
  dt_contour = 1 - dt_contour;

  distanceTransform(dt_contour, dt_contour, CV_DIST_L2, CV_DIST_MASK_PRECISE);

#if DEBUG == 1
  filename = "../result/frg_dt_contour" + SAVE_IMG_EXT;
  imagesc_bw("dt_contour", dt_contour, filename);
#endif
 
  add_sign_distance_transform(dt_contour, region);

#if DEBUG == 1
  filename = "../result/frg_sign_dt_contour" + SAVE_IMG_EXT;
  imagesc_bw("sign_dt_contour", dt_contour, filename);
#endif
  
  
}

void ContourCreater::build_contour_ue(Mat& s_ue, const Mat& fimg, const Mat& mask, const double lambda_s_u)
{
  Mat dt_contour;
  create_dt_contour(dt_contour, fimg, mask);

  //build the unary term
  //assume the contour has size the same as roi
  s_ue.create(2, dt_contour.rows * dt_contour.cols, CV_32FC1);
  
  float* ue_bkg_row = s_ue.ptr<float>(0);
  float* ue_frg_row = s_ue.ptr<float>(1);

  const float* dt_row;
  int node_id = 0;

  for(int row = 0; row < dt_contour.rows; ++row){
    dt_row = dt_contour.ptr<float>(row);

    for(int col = 0; col < dt_contour.cols; ++col){
      ue_bkg_row[node_id] = 0;
      ue_frg_row[node_id] = lambda_s_u * dt_row[col];

      node_id++;
    }
  } 
 
}

void ContourCreater::add_sign_distance_transform(Mat& dt, const Mat& region)
{
  CV_Assert(dt.type() == CV_32FC1);
  CV_Assert(region.type() == CV_8UC1);
  CV_Assert(dt.size() == region.size());

  float* dt_row;
  const uchar* region_row;

  for(int row = 0; row < dt.rows; ++row){
    dt_row = dt.ptr<float>(row);
    region_row = region.ptr<uchar>(row);

    for(int col = 0; col < dt.cols; ++col){
      if(region_row[col] == 1){
	dt_row[col] = -1 * dt_row[col];
      }
    }
  }
  

}

void ContourCreater::build_region_contour(Mat& dest, const Mat& source)
{
  //use canny edge detector to find the region contour
  //

  Mat tmp;

  double threshold1 = 100;
  double threshold2 = 200;

  Canny( source*255, tmp, threshold1, threshold2);

  tmp.copyTo(dest);

}

void ContourCreater::collect_training_data(Mat& train_labels, Mat& train_data, const Mat& fimg, const Mat& mask)
{
  CV_Assert(fimg.size() == mask.size());

  vector<float> labels;

  train_data.create(fimg.rows*fimg.cols, feat_dim, CV_32FC1);

  const uchar* mask_row;
  int count = 0;
  int img_row = fimg.rows;
  int img_col = fimg.cols;

  for(int row = 0; row < fimg.rows; ++row){
    mask_row = mask.ptr<uchar>(row);

    for(int col = 0; col < fimg.cols; ++col){
      if(mask_row[col] == FOREGROUND_LABEL){
	labels.push_back(1.0);

	float r, c;

	if(normalize_pos){
	  c = (float)col / img_col;
	  r = (float)row / img_row;
	} else{
	  c = col;
	  r = row;
	}

	train_data.at<float>(count, 0) = pos_weight * c;
	train_data.at<float>(count, 1) = pos_weight * r;

	if(feat_dim == 5){
	  Vec3f color = fimg.at<Vec3f>(row,col);
	  for(int c = 2; c < 5; c++)
	    train_data.at<float>(count, c) = color[c-2];
	}
	
	count++;
      }
      else if(mask_row[col] == STRICT_BACKGROUND_LABEL){
	labels.push_back(-1.0);

	float r, c;

	if(normalize_pos){
	  c = (float)col / img_col;
	  r = (float)row / img_row;
	} else{
	  c = col;
	  r = row;
	}


	train_data.at<float>(count, 0) = pos_weight * c;
	train_data.at<float>(count, 1) = pos_weight * r;

	if(feat_dim == 5){
	  Vec3f color = fimg.at<Vec3f>(row,col);
	  for(int c = 2; c < 5; c++)
	    train_data.at<float>(count, c) = color[c-2];
	}

	count++;
      }
    }
  }

  if(labels[0] == -1.0){
    //    change_label_sign = true;  //changing the sign or not makes no difference
    //cout << "change sign" << endl;
  }

  train_labels = Mat(labels).clone();
  train_data = train_data.rowRange(0, count);

  if(change_label_sign)
    train_labels = -1.0 * train_labels;

}

void ContourCreater::build_region_by_svm(Mat& region, const Mat& fimg, const CvSVM& svm)
{
  CV_Assert(region.type() == CV_8UC1);

  uchar* region_row;

  int img_row = fimg.rows;
  int img_col = fimg.cols;

  for(int row = 0; row < fimg.rows; ++row){
    region_row = region.ptr<uchar>(row);

    for(int col = 0; col < fimg.cols; ++col){
      Mat sample;

      float r, c;

      if(normalize_pos){
	c = (float)col / img_col;
	r = (float)row / img_row;
      } else{
	c = col;
	r = row;
      }


      if(feat_dim == 5){
	Vec3f color = fimg.at<Vec3f>(row, col);
	sample = ( Mat_<float>(1,feat_dim) << pos_weight*c, pos_weight*r, color[0],color[1],color[2] );
      } else{
	sample = (Mat_<float>(1, 2) << pos_weight*c, pos_weight*r);
      }

      float response = svm.predict(sample);

      if(change_label_sign)
      	response = response * -1;

      if(response == 1)
	region_row[col] = 1;
      
    }
  }

}
//*/
