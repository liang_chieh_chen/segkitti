#include "EnergySolver.h"
#include <string>
#include <cmath>
#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>
#include <map>
#include <utility>

using namespace std;

EnergySolver::EnergySolver(const int _num_itr, const string& _img_name)
  : num_itr(_num_itr),
    img_name(_img_name)
{}

EnergySolver::~EnergySolver()
{}

void EnergySolver::save_model_energy(string& file_name)
{
  string f_name = save_path + file_name;

  fstream fs(f_name.c_str(), fstream::out);

  if(!fs.is_open())
  {
    cerr << "fail to open " << f_name << endl;
    return;
  }

  for(size_t i = 0; i < model_energy.size(); i++){
    fs << model_energy[i] << endl;
  }

  fs.close();

}


