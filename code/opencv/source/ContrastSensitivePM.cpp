#include "ContrastSensitivePM.h"
#include <vector>
#include <opencv2/core/core.hpp>
#include <cmath>
#include <iostream>
#include <cstdlib>

using namespace std;
using namespace cv;

ContrastSensitivePM::ContrastSensitivePM(const Rect &_roi, int conn)
  :roi(_roi),
  ngh_conn(conn/2)
{
  if(ngh_conn != 2 && ngh_conn != 4){
    cerr << "nc must be 4 or 8!" << endl;
    exit(1);
  }
}

ContrastSensitivePM::~ContrastSensitivePM()
{}

//void ContrastSensitivePM::compute_img_diff(const Mat& fimg, const vector<double>& lambda, vector<Mat>& img_diff, double& total_diff)
void ContrastSensitivePM::compute_img_diff(const Mat& fimg, vector<Mat>& img_diff, double& total_diff)
{
  //CV_Assert(lambda.size() == 2);

  img_diff.clear();

  for(int n = 0; n < ngh_conn; ++n){
    img_diff.push_back(Mat::zeros(fimg.size(), CV_32FC1));
  }

  total_diff = 0;

  int count = 0;

  int nChannels = fimg.channels();

  //////////
  for(int row = 0; row < fimg.rows; ++row){
    //get the pointers to previous row, current row and next row
    const float* prv_row;

    if(row >= 1)
      prv_row = fimg.ptr<float>(row-1);
    else
      prv_row = NULL;

    const float* cur_row = fimg.ptr<float>(row);

    const float* nxt_row;

    if(row < fimg.rows-1)  //not last row
      nxt_row = fimg.ptr<float>(row+1);
    else
      nxt_row = NULL;

    //get pointers to the row where we will store results
    float* diff_rows[ngh_conn];
    for(int n = 0; n < ngh_conn; ++n)
      diff_rows[n] = img_diff[n].ptr<float>(row);

    for(int i = 0; i < fimg.cols; ++i){
      //get positions of current column and next column
      int cur_col = i*nChannels;
      int nxt_col = (i+1)*nChannels;

      //for center-south
      if(row<fimg.rows-1){
        double value = 0;

        for(int c = 0; c < nChannels; ++c){
          value += (cur_row[cur_col+c] - nxt_row[cur_col+c]) * (cur_row[cur_col+c] - nxt_row[cur_col+c]);
        }

        diff_rows[0][i] = value;
        total_diff += value;
        count++;
      }

      //for center-east
      if(i<fimg.cols-1){
        double value = 0;

        for(int c = 0; c < nChannels; ++c){
          value += (cur_row[cur_col+c] - cur_row[nxt_col+c]) * (cur_row[cur_col+c] - cur_row[nxt_col+c]);
        }

        diff_rows[1][i] = value;
        total_diff += value;
        count++;
      }

      //for center-northeast
      if(ngh_conn == 4){
        if(row>=1 && i<fimg.cols-1){
          double value = 0;

          for(int c = 0; c < nChannels; ++c){
            value += (cur_row[cur_col+c] - prv_row[nxt_col+c]) * (cur_row[cur_col+c] - prv_row[nxt_col+c]);
          }

          diff_rows[2][i] = value;
          total_diff += value;
          count++;
        }

        //for center-southeast
        if(row<fimg.rows-1 && i<fimg.cols-1){
          double value = 0;

          for(int c = 0; c < nChannels; ++c){
            value += (cur_row[cur_col+c] - nxt_row[nxt_col+c]) * (cur_row[cur_col+c] - nxt_row[nxt_col+c]);
          }

          diff_rows[3][i] = value;
          total_diff += value;
          count++;
        }

      }

    } //end col

  }  //end row


  total_diff /= count;
  
  //contrast = exp(- ||zi - zj||^2 / (2*total_diff) )
  //double dist = 1;

  for(size_t i = 0; i < img_diff.size(); ++i){
    img_diff[i] = -img_diff[i] / (2*total_diff);

    exp(img_diff[i], img_diff[i]);

    /*
    if(i>=2){
      dist = sqrt(2);
    } else {
      dist = 1;
    }

    //img_diff[i] = (lambda[0] + lambda[1] * img_diff[i]) / dist;
    img_diff[i] = img_diff[i] / dist;
    */
  }


  /* if use all image as input argument
  const Rect roi = get_roi();

  double dist = 1;
  for(size_t i = 0; i < img_diff.size(); ++i){
    img_diff[i] = img_diff[i](roi);

    img_diff[i] = -img_diff[i]/(2*total_diff);

    exp(img_diff[i], img_diff[i]);

    if (i >= 2)
      dist = sqrt(2);

    //img_diff[i] = (lambda[0] + lambda[1] * img_diff[i]) / dist;
    img_diff[i] = img_diff[i] / dist;
  }
  */

}


void ContrastSensitivePM::compute_pe(const Mat& img, const Mat& mask, const vector<double>& lambda, Mat& PE1, Mat& PE2, Mat& PI)
{
  //PE1 is the pairwise term for Ising Prior
  //PE2 is the pairwise term for constrast sensitive Potts model

  Mat fimg;

  //convert to the image to doulbe
  if( img.type() == CV_8UC3 || img.type() == CV_8UC1 ){
    img.convertTo(fimg, CV_32F, 1.0/255);
  }
  else{
    fimg = img;
  }

  //n=0: center-south, n=1: center-east, n=2:center-noutheast, n=3:center-southeast
  vector<Mat> img_diff;
  double total_diff;

  //only take use of roi or all image?
  //compute_img_diff(fimg(roi), lambda, img_diff, total_diff);
  compute_img_diff(fimg(roi), img_diff, total_diff);

  //compute_img_diff(fimg, lambda, img_diff, total_diff);


  int count = 0;

  //start to compute PE, PI
  //initialization
  int num_edge = roi.width*(roi.height-1) + (roi.width-1)*roi.height;

  if (ngh_conn == 4)
    num_edge += 2*(roi.width-1)*(roi.height-1);

  PE1.create(1, num_edge, CV_32FC1);
  //PE1.setTo(Scalar(lambda[0]));
  PE2.create(1, num_edge, CV_32FC1);
  PI.create(2, num_edge, CV_32SC1);

  float* PE1_row = PE1.ptr<float>(0);
  float* PE2_row = PE2.ptr<float>(0);
  int *PI_row[2];

  for(int i = 0; i < 2; ++i)
    PI_row[i] = PI.ptr<int>(i);

  //compute PE, PI
  count = 0;
  //for c-s
  for(int row = 0; row < roi.height-1; ++row) {
    float* diff_row = img_diff[0].ptr<float>(row);

    for(int col = 0; col < roi.width; ++col) {
      PE1_row[count]   = lambda[0];
      PE2_row[count]   = lambda[1] * diff_row[col];
      PI_row[0][count] = row*roi.width + col;
      PI_row[1][count] = (row+1)*roi.width + col;

      count++;
    } //end col
  }   //end row

  //for c-e
  for(int row = 0; row < roi.height; ++row) {
    float* diff_row = img_diff[1].ptr<float>(row);

    for(int col = 0; col < roi.width-1; ++col) {
      PE1_row[count]   = lambda[0];
      PE2_row[count]   = lambda[1] * diff_row[col];
      PI_row[0][count] = row*roi.width + col;
      PI_row[1][count] = row*roi.width + col+1;

      count++;
    }
  }

  //get the other img_diff, if necessary
  double dist = sqrt(2);

  if (ngh_conn == 4) {
    //for c-ne
    for(int row = 1; row < roi.height; ++row) {
      float* diff_row = img_diff[2].ptr<float>(row);

      for(int col = 0; col < roi.width-1; ++col) {
	PE1_row[count]   = lambda[0] / dist;
        PE2_row[count]   = lambda[1] * diff_row[col] / dist;
        PI_row[0][count] = row*roi.width + col;
        PI_row[1][count] = (row-1)*roi.width + col+1;

        count++;
      }
    }

    //for c-se
    for(int row = 0; row < roi.height-1; ++row) {
      float* diff_row = img_diff[3].ptr<float>(row);

      for(int col = 0; col < roi.width-1; ++col) {
	PE1_row[count]   = lambda[0] / dist;
        PE2_row[count]   = lambda[1] * diff_row[col] / dist;
        PI_row[0][count] = row*roi.width + col;
        PI_row[1][count] = (row+1)*roi.width + col+1;

        count++;
      }
    }

  } //end if

}


