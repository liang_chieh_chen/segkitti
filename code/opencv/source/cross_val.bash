#!/bin/bash
#do cross validation to find best C
#
trainItr=30
testItr=$((trainItr-1))
expID=7

TRACKLET_PATH=~/dataset/segkitti/tracklets/
trainList=${TRACKLET_PATH}train_list.txt
testList=${TRACKLET_PATH}val_list.txt

resultPath=~/workspace/SegKITTI/codes/opencv/result/

rangeC=(0.01 0.1 1 2 4 8 16)

echo "train on ${trainList}"
echo "test on ${testList}"
echo "expID ${expID}"


for trainC in ${rangeC[@]}
do
    #train
    TRAIN_OUT_FOLDER=${resultPath}model_expID${expID}_svmC${trainC}_/
    #TRAIN_OUT_FOLDER=${resultPath}model_expID${expID}_svmC${trainC}_/
    mkdir ${TRAIN_OUT_FOLDER}
    
    ./ProgNSlack -d ${TRACKLET_PATH}tracklet_gt -f ${trainList} -o ${TRAIN_OUT_FOLDER}svm_model.dat -c ${trainC} -i ${trainItr} -e ${expID} 

    #test on validation
    TEST_OUT_FOLDER=${resultPath}map_expID${expID}_svmC${trainC}_/
    mkdir ${TEST_OUT_FOLDER}

    ./SingleGC kittiConfig.xml ${TRAIN_OUT_FOLDER}svm_model.dat.${testItr} ${testList} ${TEST_OUT_FOLDER}

    #get results
    SCORE_OUT_FILE=${resultPath}scores_expID${expID}_.txt
    
    ./EvalScore kittiConfig.xml ${testList} ${TEST_OUT_FOLDER} ${SCORE_OUT_FILE}

done

##find max score with the corresponding C
cat ${SCORE_OUT_FILE}

read -a var <<< $(sort -nr -k5 ${SCORE_OUT_FILE} | head -1)

#train on trainval based on the found C
#eID=${var[0]}
trainC=${var[1]}

trainList=${TRACKLET_PATH}trainval_list.txt
testList=${TRACKLET_PATH}test_list.txt


TRAIN_OUT_FOLDER=${resultPath}model_expID${expID}_finalsvmC${trainC}_/
mkdir ${TRAIN_OUT_FOLDER}

echo $TRAIN_OUT_FOLDER    
./ProgNSlack -d ${TRACKLET_PATH}tracklet_gt -f ${trainList} -o ${TRAIN_OUT_FOLDER}svm_model.dat -c ${trainC} -i ${trainItr} -e ${expID}

#test on test set
TEST_OUT_FOLDER=${resultPath}map_expID${expID}_finalsvmC${trainC}_/
mkdir ${TEST_OUT_FOLDER}

echo $TEST_OUT_FOLDER
./SingleGC kittiConfig.xml ${TRAIN_OUT_FOLDER}svm_model.dat.${testItr} ${testList} ${TEST_OUT_FOLDER}

#get results
SCORE_OUT_FILE=${resultPath}/scores_final_expID${expID}.txt
    
./EvalScore kittiConfig.xml ${testList} ${TEST_OUT_FOLDER} ${SCORE_OUT_FILE}

