#include <opencv2/core/core.hpp>
#include <iostream>
#include <string>

#include "ExpConfiguration.h"

using namespace cv;
using namespace std;


void ExpConfiguration::write(FileStorage& fs) const
{
  fs << "{" 
     << "detectionDataBaseDir" << detectionDataBaseDir
     << "detectionImgDir"      << detectionImgDir
     << "annotationMaskDir"    << annotationMaskDir
     << "annotationMaskBDDir"  << annotationMaskBDDir
    //<< "rawDataBaseDir"       << rawDataBaseDir
    //<< "rawDataImgDir"        << rawDataImgDir
     << "imgExt"               << imgExt
     << "rawDataImgDir"        << rawDataImgDir
     << "veloDir"              << veloDir
     << "veloExt"              << veloExt
     << "mappingList"          << mappingList
     << "resultDir"            << resultDir
     << "trackletsDir"         << trackletsDir
     << "cadSegDir"            << cadSegDir
     << "cadSegBdDir"          << cadSegBdDir
     << "mtSegDir"             << mtSegDir
     << "mtSegBdDir"           << mtSegBdDir
     << "}";

}

void ExpConfiguration::read(const FileNode& node)
{
  detectionDataBaseDir = (string)node["detectionDataBaseDir"];
  detectionImgDir      = (string)node["detectionImgDir"];
  annotationMaskDir    = (string)node["annotationMaskDir"];
  annotationMaskBDDir  = (string)node["annotationMaskBDDir"];
  //  rawDataBaseDir       = (string)node["rawDataBaseDir"];
  //  rawDataImgDir        = (string)node["rawDataImgDir"];
  imgExt               = (string)node["imgExt"];
  rawDataImgDir        = (string)node["rawDataImgDir"];
  veloDir              = (string)node["veloDir"];
  veloExt              = (string)node["veloExt"];
  mappingList          = (string)node["mappingList"];
  resultDir            = (string)node["resultDir"];
  trackletsDir         = (string)node["trackletsDir"];
  cadSegDir            = (string)node["cadSegDir"];
  cadSegBdDir          = (string)node["cadSegBdDir"];
  mtSegDir             = (string)node["mtSegDir"];
  mtSegBdDir           = (string)node["mtSegBdDir"];
  // gtDir                = (string)node["gtDir"];
  //  depthDir             = (string)node["depthDir"];
  //  chDir                = (string)node["chDir"];
  //  pcDir                = (string)node["pcDir"];


}

int readExpConfigurationFromFile(const string& filename, ExpConfiguration& config)
{
  FileStorage fs(filename, FileStorage::READ);
  if (!fs.isOpened())
  {
    cerr << "Failed to open " << filename << endl;
    return -1;
  }
  fs["ExpConfiguration"] >> config;

  return 0;
}

