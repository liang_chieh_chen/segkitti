#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "HistAppearance.h"
#include "AppearanceModel.h"
#include <cmath>
#include <cfloat>
#include <iostream>
#include <sstream>
#include <fstream>
//#include <vector>
//#include <string>

using namespace std;

HistAppearance::~HistAppearance()
{}

HistAppearance::HistAppearance(Mat& img, Mat& mask, Rect& roi, Rect& wb_roi, int _num_bins, float bkg_ene, float frg_ene)
  :AppearanceModel(mask, roi, wb_roi, bkg_ene, frg_ene),
   num_bins(_num_bins),
   bin_width(1.0/_num_bins),
   parameters(2, pow(_num_bins,3), CV_32FC1, Scalar(0)),
   quan_img(img.size(), CV_8UC3)
{
  total_pxl.assign(2,0);

  const int nChannels = quan_img.channels();

  Mat fimg;
  //convert to the image to float
  if( img.type() == CV_8UC3 || img.type() == CV_8UC1 ){
    img.convertTo(fimg, CV_32F, 1.0/255);
  }
  else{
    fimg = img;
  }

  //quantize the image
  for(int j = 0; j < quan_img.rows; ++j){
    const float* fimg_row = fimg.ptr<float>(j);

    uchar* quan_img_row = quan_img.ptr<uchar>(j);

    for(int i = 0; i < quan_img.cols*nChannels; ++i){
      uchar idx = (uchar)(fimg_row[i] / bin_width);

      if (idx == num_bins)
        quan_img_row[i] = num_bins - 1;
      else
        quan_img_row[i] = idx;
    }

  }

  //find the quantized value (bin value) for the roi
  Mat quan_roi(quan_img, get_roi());
  roi_bin_id.create(get_roi().height, get_roi().width, CV_32SC1);

  int bin_id;

  for(int j = 0; j < roi_bin_id.rows; ++j){
    int* roi_bin_id_row = roi_bin_id.ptr<int>(j);
    const uchar* quan_roi_row = quan_roi.ptr<uchar>(j);

    for(int i = 0; i < roi_bin_id.cols; ++i){
      bin_id = 0;

      for(int c = 0; c < nChannels; ++c) {
        bin_id += quan_roi_row[i*nChannels+c] * pow(num_bins,c);
      }
      roi_bin_id_row[i] = bin_id;


    }
  }

  //get the initial model parameters
  update_model_parameters(img, mask);

}

void HistAppearance::update_model_parameters(Mat& img, Mat& mask)
{
  //initialization (set the initial counts to be a very small value, not zero)
  parameters = Mat::ones(parameters.size(), parameters.type()) * FLT_MIN;

  int nChannels = img.channels();

  //find the histograms for background and foreground
  int bin_id;

  float* bkg_hist = parameters.ptr<float>(0);
  float* frg_hist = parameters.ptr<float>(1);

  for(int j = 0; j < mask.rows; ++j){
    const uchar* mask_row = mask.ptr<uchar>(j);
    const uchar* map_row  = get_map_result().ptr<uchar>(j);
    const uchar* quan_img_row = quan_img.ptr<uchar>(j);

    for(int i = 0; i < mask.cols; ++i){
      if((mask_row[i]==WEAK_BACKGROUND_LABEL || mask_row[i]==UNDEFINED_LABEL) && map_row[i]==0 )
      {
        bin_id = 0;
        for(int c = 0; c < nChannels; ++c) {
          bin_id += quan_img_row[i*nChannels+c] * pow(num_bins,c);
        }
        bkg_hist[bin_id]++;

      }
      if((mask_row[i]==FOREGROUND_LABEL || mask_row[i]==UNDEFINED_LABEL) && map_row[i]==1 )
      {
        bin_id = 0;
        for(int c = 0; c < nChannels; ++c) {
          bin_id += quan_img_row[i*nChannels+c] * pow(num_bins,c);
        }
        frg_hist[bin_id]++;

      }

    }

  }


  //L1-normalize the histograms
  for(int i = 0; i < 2; ++i){
    Scalar hist_total;
    hist_total = sum(parameters.row(i));
    total_pxl[i] = hist_total.val[0];

    parameters.row(i) = parameters.row(i) / total_pxl[i];
  }

}

void HistAppearance::compute_unary_term(const Mat& img, const Mat& mask, Mat& ue)
{
  const Rect roi = get_roi();

  ue.create(2, roi.width*roi.height, CV_32FC1);


  int bin_id;
  int node_id = 0;

  float* ue_bkg_row = ue.ptr<float>(0);
  float* ue_frg_row = ue.ptr<float>(1);

  const float* model_bkg = parameters.ptr<float>(0);
  const float* model_frg = parameters.ptr<float>(1);

  //note the node index for ue is in row-scan-order within the image
  for(int j = 0; j < roi.height; ++j) {
    const int* roi_row = roi_bin_id.ptr<int>(j);

    for(int i = 0; i < roi.width; ++i) {
      bin_id = roi_row[i];
      ue_bkg_row[node_id] = model_bkg[bin_id];
      ue_frg_row[node_id] = model_frg[bin_id];
      node_id++;

    }
  }

  //return -log(ue)
  log(ue, ue);

  ue = -ue;

  //return -ue;
}

void HistAppearance::save_parameters(const int num_itr, const string& filename) const
{
  //output file name
  stringstream ss;
  string f_name;

  ss << "itr" << num_itr << "_" << filename;
  ss >> f_name;

  f_name = get_save_path() + f_name;

  //open file
  fstream fs(f_name.c_str(), fstream::out);

  if(!fs.is_open())
  {
    cout <<f_name<<endl;
    cerr << "Fail to open " << filename << endl;
    return;
  }

  //save parameters in this format
  //bin_id bkg frg (each row for each bin)
  for(int i = 0; i < parameters.cols; ++i){
    fs << i;
    for(int j = 0; j < parameters.rows; ++j){
      fs << " " << parameters.at<float>(j, i);
    }
    fs << endl;
  }

  fs.close();
}

