/*
 * set the configuration for experiments
 *
 * modified from OPENCV tutorial
 * http://docs.opencv.org/doc/tutorials/core/file_input_output_with_xml_yml/file_input_output_with_xml_yml.html#fileinputoutputxmlyaml
 *
 * Liang-Chieh@UCLA, April 2013
 *
 */

#include <opencv2/core/core.hpp>
#include <iostream>
#include <string>

#include "ExpConfiguration.h"

using namespace cv;
using namespace std;

static void help(char** av)
{
    cout << endl
        << av[0] << " shows the usage of the OpenCV serialization functionality."         << endl
        << "usage: "                                                                      << endl
        <<  av[0] << " outputfile.yml.gz"                                                 << endl
        << "The output file may be either XML (xml) or YAML (yml/yaml). You can even compress it by "
        << "specifying this in its extension like xml.gz yaml.gz etc... "                  << endl
        << "With FileStorage you can serialize objects in OpenCV by using the << and >> operators" << endl
        << "For example: - create a class and have it serialized"                         << endl
        << "             - use it to read and write matrices."                            << endl;
}

int main(int ac, char** av)
{
    if (ac != 2)
    {
        help(av);
        return 1;
    }

    string filename = av[1];

    ExpConfiguration config;

    config.detectionDataBaseDir = "/home/lcchen/dataset/segkitti/";
    config.detectionImgDir      = "Images_left/";
    config.annotationMaskDir    = "AnnotMasks_left/";
    config.annotationMaskBDDir  = "AnnotMasksBD_left/";
    config.imgExt               = ".png";
    config.rawDataImgDir        = "image_02/data/";
    config.veloDir              = "velodyne_points/data/";
    config.veloExt              = ".bin";
    config.mappingList           = "/home/lcchen/dataset/segkitti/code/labelingtool_segkitti/mapping/all_mapping.txt";
    config.resultDir            = "/home/lcchen/workspace/SegKITTI/codes/opencv/result/";
    config.trackletsDir         = "tracklets/";
    config.cadSegDir            = "Segmentation_CAD/v2/SegmentationMasks_left/";
    config.cadSegBdDir          = "Segmentation_CAD/v2/SegmentationMasksBD_left/";
    config.mtSegDir             = "mturk/segmentation/results_batch_01/car/SegmentationMasks_left/";
    config.mtSegBdDir           = "mturk/segmentation/results_batch_01/car/SegmentationMasksBD_left/";
    //    config.gtDir                = "gt/";
    //    config.depthDir             = "depth/";
    //    config.chDir                = "ch/";
    //    config.pcDir                = "pc/";

    { //write
      FileStorage fs(filename, FileStorage::WRITE);

      fs << "ExpConfiguration" << config;

      fs.release();

      cout << "Write Done." << endl;
    }

    {//read
        cout << endl << "Reading: " << endl;
        FileStorage fs(filename, FileStorage::READ);

        if (!fs.isOpened())
        {
          cerr << "Failed to open " << filename << endl;
          help(av);

          return 1;
        }

        ExpConfiguration config2;

        fs["ExpConfiguration"] >> config2;

        cout << "ExpConfiguration = " << endl << config2 << endl;

    }


    return 0;
}
