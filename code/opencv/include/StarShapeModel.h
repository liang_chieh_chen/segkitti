#ifndef _STAR_SHAPE_MODEL_H
#define _STAR_SHAPE_MODEL_H

/*
 * Star shape model
 * Modified from Varun Gulshan's codes
 *
 * Directly work on region of interest!
 *
 *
 *
 * Reference:
 * 1. Star Shape Prior for Graph-Cut Image Segmentation, O. Veksler
 * 2. Geodesic Star Convexity for Interactive Image Segmentation, V. Gulshan et al.
 *
 *
 *
 * Liang-Chieh@TTIC, July 2013
 *
 */

#include <opencv2/core/core.hpp>
#include <vector>

using namespace cv;
using namespace std;

class StarShapeModel
{
 public:
  StarShapeModel(const int conn = 4, const int num_subsample = 20, const string& file_path="");
  ~StarShapeModel();

  void compute_pe(const Mat& fimg, const Mat& mask, Mat& PE, Mat& PI, const bool load_file=false, const string& target_name="");

  int get_num_stars() const {return num_subsample;}

 private:
  //neighbor connection. 
  //2 for 4-connected neighbors, and 4 for 8-connected ones
  //this value is computed by dividing the input argument by 2
  int ngh_conn;     

  //subsample the foreground seeds to avoid having the wrong seeds
  //since the point clouds are not accurate along object boundaries
  //we hope that by subsampling, we obtain some "trustworthy" seeds
  int num_subsample;

  //where to load the precomputed star shape energies
  string file_path;

 private:
  void extract_frg_seeds(const Mat& mask, Mat& start_points);

  void compute_shortest_paths(const Mat& fimg, const Mat& start_points, Mat& distance, Mat& root_points, Mat& backlinks); 



};


/*
class StarShapeModel
{
 public:
  StarShapeModel(const Rect& _roi, int conn = 4, int num_subsample = 20);
  ~StarShapeModel();

  void compute_pe(const Mat& fimg, const Mat& mask, Mat& PE, Mat& PI);

  const Rect& get_roi() const {return roi;}

 private:
  Rect roi;        //region of interest

  //neighbor connection. 
  //2 for 4-connected neighbors, and 4 for 8-connected ones
  //this value is computed by dividing the input argument by 2
  int ngh_conn;     

  //subsample the foreground seeds to avoid having the wrong seeds
  //since the point clouds are not accurate along object boundaries
  //we hope that by subsampling, we obtain some "trustworthy" seeds
  int num_subsample;

 private:
  void extract_frg_seeds(const Mat& mask, Mat& start_points);

  void compute_shortest_paths(const Mat& fimg, const Mat& start_points, Mat& distance, Mat& root_points, Mat& backlinks); 



};
*/


#endif
