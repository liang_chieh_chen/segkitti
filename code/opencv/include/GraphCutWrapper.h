#ifndef _GRAPH_CUT_WRAPPER_H
#define _GRAPH_CUT_WRAPPER_H

/***************************************
 * Wrapper for Graph cut
 *
 *
 * Liang-Chieh Chen@TTIC, July 2013
 *
 ***************************************/

//#include "MapMethod.h"
#include <opencv2/core/core.hpp>
#include "graph.h"

using namespace cv;

class GraphCutWrapper 
{
 public:
  GraphCutWrapper(const int _num_nodes, const Mat& PI, const int _mul = 100);
  ~GraphCutWrapper();

  void compute_map(const Mat& UE, const Mat& PE, const Mat& PI, Mat& x_opt, double& energy);

 private:
  typedef Graph<int,int,int> GraphType;
  
  GraphType *g;   //graph

  int num_nodes;
  int num_edges;
  int mul;         //will multiply float energy by this

 private:
  void initialize_graph(const Mat& PI);

};



#endif
