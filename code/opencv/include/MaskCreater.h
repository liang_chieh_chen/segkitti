#ifndef _MASK_CREATER_H
#define _MASK_CREATER_H

/*
 * Create object mask
 *
 *
 * Liang-Chieh@TTIC, July 2013
 *
 */


#include <opencv2/core/core.hpp>
#include <string>
#include "tracklets.h"

using namespace cv;
using namespace std;

//class Tracklets::tTracklet;

class MaskCreater
{
 public:
  MaskCreater(const string& v_c_file, const string& c_c_file, const string& v_p_file, const int c_id=2, const int e_width=20);
  ~MaskCreater();

  int create_mask(const Mat& fimg, const int img_id, const Tracklets::tTracklet* target, Mat& mask, Rect& roi, Rect& wb_roi);

  void create_inverse_mask(const string& tracklet_name, const Mat& depth_img, const int img_id, const Rect& roi, const Tracklets::tTracklet* car, Mat& inverse_mask);


  //rotate/translate ith mask/depth_img to the jth image frustrum
  void rotate_translate_maski_to_maskj(const string& tracklet_name, const Tracklets::tTracklet* car, const Mat& mask_i, const Mat& depth_img_i, const Rect& roi_i, const Rect& roi_j, const int img_id_i, const int img_id_j, const Mat& img_i, const Mat& img_j, Mat& rectified_mask, Mat& rectified_depth_img);

  //rotate/translate ith mask/depth_img to the jth image frustrum by iterative closest point fitting
  void rotate_translate_maski_to_maskj_icp(const string& tracklet_name, const Tracklets::tTracklet* car, const Mat& mask_i, const Mat& mask_j, const Mat& depth_img_i, const Mat& depth_img_j, const Rect& roi_i, const Rect& roi_j, const int img_id_i, const int img_id_j, const Mat& img_i, const Mat& img_j, Mat& rectified_mask, Mat& rectified_depth_img);


  //load the depth from the velodyne file
  void load_depth_from_velodyne(Mat& depth, const Mat& fimg);

  void setup_depth_var(double _lambda_dep, int recover_depth_method)
  {
    lambda_dep = _lambda_dep;
    recover_method = recover_depth_method;
  }

  //  void find_init_depth(Mat& recon_depth_img, const Mat& fimg);

  //  void build_depth_ue(Mat& d_ue, const Mat& recon_depth, const double lambda_d_u);

  const float get_frg_median_depth() const {return frg_median_depth;}
  const Mat& get_depth_img() const {return depth_img;}
  //const Mat& get_depth_ind() const {return depth_ind;}
  const Mat& get_frg_shape() const {return frg_shape;}
  const Mat& get_soft_mask() const {return soft_mask;}
  const Rect& get_roi() const {return box;}
  const Rect& get_wb_roi() const {return wb_box;}

 private:
  string velo_to_cam_filename;
  string cam_to_cam_filename;
  string tracklet_filename;
  string velo_pc_filename;

  int camera_id;
  int expand_width;  //exapnd the 2D bounding box. outside regions = background seeds

  //matrices are stored in row-order
  struct Calib
  {
    float corner_dist;
    float S[2];
    float K[9];
    float D[5];
    float R[9];
    float T[3];
    float S_rect[2];
    float R_rect[9];
    float P_rect[12];
    
  };

  Rect box;       //it is roi, renamed as box here
  Rect wb_box;    //it is wb_roi, renamed as wb_box here
  float frg_median_depth;
  Mat frg_shape;
  Mat depth_img;
  Mat soft_mask;   //mask with the value = point cloud to 3D bounding box
  //Mat depth_ind;

  double lambda_dep;
  int    recover_method;

 private:
  //load files
  int load_calib_velo_to_cam(string rigid_file_name, string cam_file_name, Mat* velo_to_cam, Mat& K);
  int load_velo_points(string file_name, vector<Mat>& velo);
  int load_calib_rigid(string file_name, float* mat);
  int load_calib_cam_to_cam(string file_name, Calib* calib, int num_cam );

  int load_velo_points(string file_name, Mat& velo);

  //when doing translate and rotate img i to img j, check if the matching point is an outlier or not
  bool is_matching_outlier(const Mat& img_i, const Mat& img_j, const Point2i& p_i, const Point2i& p_j);

  //create the 3D bounding box coordinate by height, width, length
  void get_3D_boundingbox_coordinate(const float h, const float w, const float l, Mat& bx3D);
  
  //get the 2D bounding box from the 2D points
  Rect get_2Drectangle_from_colVectors(const Mat& points2D);
  //Rect get_2Drectangle_from_colVectors(vector<Mat>& points2D);

  //check if the point is in the range specified by min_val and max_val
  bool is_inRange(const Mat& point, const Mat& min_val, const Mat& max_val, vector<int>& in_segment);

  //select the point clouds that are within the 3D bounding box
  void find_pc_within_3Dbx(vector<bool>& indicator, Mat& dist, const Mat& all_pc, const Mat& bx3D, const bool remove_car_ground = false);

  void find_pc_within_3Dbx(vector<bool>& target_indicator, Mat& dist, const Mat& all_pc, const Mat& bx3D, const bool remove_car_ground, const Mat& tracklet_R, const Mat& tracklet_T);

  //void find_pc_within_3Dbx(vector<bool>& indicator, const Mat& all_pc, const Mat& bx3D, const bool remove_car_ground = false);

  //void find_pc_within_3Dbx(vector<bool>& target_indicator, const Mat& all_pc, const Mat& bx3D, const bool remove_car_ground, const Mat& tracklet_R, const Mat& tracklet_T);

  //compute the distance from a point to the lower or upper coordinate of a 3D bbox
  void find_dist_to_vertex(Mat& dist, const Mat& point, const Mat& lower, const Mat& upper);

  //compute the distance from a point to a 3D bbox. in_segment specifies the locations of the point (there are 27 cases)
  float find_dist(const Mat& dist, vector<int>& in_segment);

  //select the point clouds that are within the 2D bounding box
  void find_pc_within_2Dbx(vector<bool>& indicator, const Mat& all_pc, const Rect& bx2D);
  //select the point clouds that are within the mask
  void refine_pc_within_mask(Mat& out, const Mat& in, const Mat& mask);

  //find the range of the points (specified by min_val and max_val)
  void find_vectorPoints_range(const vector<Mat>& points, Mat& min_val, Mat& max_val);

  //create the mask by 2D bounding box
  void create_mask_by_bx(Mat& mask, Rect& roi, Rect& wb_roi, Rect& bx2D, const Mat& frg_pc, const Mat& bkg_pc, const int expand_width=10);

  //create the mask by 2D bounding box and recovered depth images
  void create_mask_by_recovered_depth(Mat& mask, Rect& roi, Rect& wb_roi, const Rect& bx2D, const Mat& recon_depth, const double frg_depth, const double threshold, const int expand_width=10);

  //check the point (x,y) is within the image plane or not
  bool is_within_image_plane(const int x, const int y, const int img_row, const int img_col);

  //check if the 2D bbox is totally outside the image plane
  bool bx2D_outside_image_plane(const Rect& box, const int rows, const int cols);


  //project
  void project_points(const Mat& P, const Mat& points3D, Mat& points2D);

  //change range to -pi to pi
  double wrap_to_pi(double alpha);

  //rotate and translate matrix m
  void rotate_translate_matrix(Mat& out, const Mat& R, const Mat& T, const Mat& m);

  //inverse the effect of rotate and translate
  void inverse_rotate_translate_matrix(Mat& out, const Mat& R, const Mat& T, const Mat& m);


  //remove point clouds that are likely to be ground
  void remove_ground_point_clouds(Mat& pc);
  //remove point clouds within the bx3D that are close to ground
  void remove_ground_point_clouds(Mat& pc, const Mat& bx3D);

  //remove point clouds that are behind the image plane
  void remove_behind_point_clouds(Mat& pc);

  //compute the min depth of velo point clouds specified by the ind
  double compute_pc_depth(const Mat& velo_pc, const vector<bool>& ind, const int type);
  //find the foreground shape
  void find_frg_shape(vector<Point>& contour, const Mat& pc);

  //find the background point clouds
  void find_bkg_pc(vector<bool>& bkg_ind, const vector<bool>& frg_ind, const Mat& pc_3D, const Mat& pc_2D, const Mat frg_shape, const double frg_min_depth);
  
  //read the depth value from velo_pc, and (x,y) from pc_2D
  void read_depth_values(Mat& depth_img, const Mat& pc_2D, const Mat& velo_pc);

  //read depth values only for frg/bkg point clouds within the roi
  void read_depth_values(Mat& depth_img, const Mat& pc_2D, const Mat& velo_pc, vector<bool>& frg_ind, vector<bool>& bkg_ind);

  //read the distance of point cloud to 3D bbox for frg/bkg point clouds
  //  void set_soft_mask_dist(const Mat& velo_pc, const Mat& pc_2D, vector<bool>& frg_ind, vector<bool>& bkg_ind, const Mat& dist, const float thre1, const float thre2);

  //set threshold for the distance to the 3d bbox, and change mask
  //  void set_mask_by_threshold_soft_mask(Mat& mask, const float thre1, const float thre2);
  
  //find value for soft mask, threshold it and change mask
  void set_mask_by_soft_mask(Mat& mask, const Mat& velo_pc, const Mat& pc_2D, vector<bool>& frg_ind, vector<bool>& bkg_ind, const Mat& dist, const float thre1, const float thre2);

  //convert from points to images
  void convert_img_to_points(const Mat& depth_img, Mat& points2D, Mat& depth_val, const Mat& mask = Mat());
  void convert_points_to_img_binary(const Mat& depth_img, const Mat& points2D, const vector<bool>& frg_ind, Mat& inverse_mask);



  //read "var_name" from the file
  template<typename T>
    int readVariable(string file_name, string var_name, int M, int N, T* value)
    {
      fstream fs;

      fs.open(file_name.c_str(), fstream::in);

      if(!fs.is_open()){
	cerr << "Fail to open " << file_name << endl;
	return -1;
      }

      //search for variable
      bool success = false;
      string read_var;
      string var = var_name + ":";

      while(!success){
	fs >> read_var;

	if(var == read_var){
	  success = true;
	}    
      }
  
      if (!success)
	return -1;

      for(int row = 0; row < M; row++){
	for(int col = 0; col < N; col++){
	  fs >> value[row*N + col];
	}
      }  


      fs.close();

      return 1;
    }


};



#endif
