#ifndef _CONTOUR_CREATER_H
#define _CONTOUR_CREATER_H

/*
 * Create object contour
 *
 * This class directly work on region of interest.
 * 
 * Liang-Chieh@TTIC, July 2013
 *
 */

#include <string>
#include <opencv2/core/core.hpp>
//#include <opencv2/ml/ml.hpp>
#include <opencv2/highgui/highgui.hpp>


using namespace std;
using namespace cv;

class ContourCreater
{
 public:
  ContourCreater(string filename="", bool use_dt = true);
  ~ContourCreater();

  void compute_unary_term(Mat& s_ue);

 private:
  string shape_filename; //compute the potention for the specified file

 private:
  //find the region contour
  void build_region_contour(Mat& dest, const Mat& source);

  //change the sign of distance transform if the region is in "foreground"
  void add_sign_distance_transform(Mat& dt, const Mat& region);

  bool m_use_dt;

};



/* svm related
class ContourCreater
{
 public:
  ContourCreater(int _feat_dim=2);
  ~ContourCreater();

  //create the distance transform of the contour
  //void create_contour(Mat& contour, const Mat& fimg, const Mat& mask, const Rect& roi);
  void create_dt_contour(Mat& dt_contour, const Mat& fimg, const Mat& mask);

  //find the contour unary term (i.e., the distance transform of the contour)
  void build_contour_ue(Mat& s_ue, const Mat& fimg, const Mat& mask, const double lambda_s_u);

 private:
  //create the distance transformed contour by using Support Vector Machine (SVM)
  //void create_contour_by_svm(Mat& contour, const Mat& fimg, const Mat& mask, const Rect& roi);
  void create_dt_contour_by_svm(Mat& contour, const Mat& fimg, const Mat& mask);

  //collect training samples for SVM
  //void collect_training_data(Mat& train_labels, Mat& train_data, const Mat& fimg, const Mat& mask, const Rect& roi);
  void collect_training_data(Mat& train_labels, Mat& train_data, const Mat& fimg, const Mat& mask);

  //build the region by svm prediction of each position
  //void build_region_by_svm(Mat& contour, const Mat& fimg, const Rect& roi, const CvSVM& svm);
  void build_region_by_svm(Mat& region, const Mat& fimg, const CvSVM& svm);
  
  //find the region contour
  //void build_region_contour(Mat& dest, const Mat& source, const Rect& roi);
  void build_region_contour(Mat& dest, const Mat& source);

  //change the sign of distance transform if the region is in "foreground"
  void add_sign_distance_transform(Mat& dt, const Mat& region);
  
 private:
  int feat_dim;             //feature dimension
  float pos_weight;         //weight for position
                            //set less than 1, if want more weight on colors
  bool normalize_pos;       //normalize the position w.r.t. image size
  bool change_label_sign;   //sometimes we need to change sing for svm
  

};
//*/


#endif
