#ifndef _GMM_MODEL_H
#define _GMM_MODEL_H
/*
 *
 * The Gaussian Mixture Models are modified from the source codes of OPENCV in /imgproc/src/grabcut.cpp
 *
 *
 * Liang-Chieh@UCLA, May 2013
 *
 */

#include <opencv2/core/core.hpp>
#include <vector>

using namespace cv;
using namespace std;


class GmmModel
{
  public:
    GmmModel(int _num_comp=5, int _feat_size=3);
    ~GmmModel();

    //reset the sufficient statistics to be zero
    void reset_ss();

    //accumulate sufficient statistic for the specific pixel in the k-th mixture
    void accumulate_ss(int k, const float* pxl_feature);

    //call train(), after accumulating all the sufficient statistics
    void train();

    //get pixel likelihood
    float get_pxl_likelihood(const float* pxl_feature) const;
    float get_pxl_likelihood(int comp, const float* pxl_feautre) const;

    //find the most likely component
    //we will only assign each pixel to one of the mixture to have fast speed
    int find_most_likely_component(const float* pxl_feature) const;

    //print out the parameeters
    void print_out_parameters() const;

    //print out sufficient statistics
    void print_out_ss() const;

    const Mat& get_parameters() const {return parameters;}

  private:
    int num_components;      //number of mixture components for foreground and background models
    int feat_size;          //feature size

    Mat parameters;         //model parameters: mixture weights, means and covaraince matrix
                            //parameters=[w0,w1,..,m0,m1,...C0,C1,...]
                            //Mat of float
    float* mix_weight;      //pointer to the starting position of mixture weight within parameters
    float* mix_mean;        //pointer to the starting position of mixture means within parameters
    float* mix_cov;         //pointer to the starting position of mixture covariance within parameters

    vector<float*> inverse_covs;      //inverse of covariance matrices
    vector<float> cov_determs;        //the determinanats of covariance matrices

    vector<float*> ss_sums;           //sufficient statistics for 1st order statistics
    vector<float*> ss_prods;          //sufficient statistics for 2nd order statistics
    vector<int> sample_counts;        //number of samples for each mixture

    int total_sample_count;

  private:
    //helper function
    //extract the features for the pixel at position (y,x) in the image
//    void extract_pxl_feature(Mat& img, int y, int x, Mat& feat);

    //compute the inverse covarance matrix and determinant for k-th mixture
    void compute_inverseCov_and_determ(int k);


};


#endif
