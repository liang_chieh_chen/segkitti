#ifndef _HIST_APPEARANCE_MODEL
#define _HIST_APPEARANCE_MODEL

/*
 * Derived class from AppearanceModel
 * In this class, we model the color appearance by histograms with "number of bins" as parameter
 *
 *
 * Liang-Chieh@UCLA, April 2013
 *
 */

#include "AppearanceModel.h"
#include "Constants.h"
#include <opencv2/core/core.hpp>
#include <vector>
#include <string>

using namespace cv;
using namespace std;

class HistAppearance : public AppearanceModel
{
  public:
    HistAppearance(Mat& img, Mat& mask, Rect& roi, Rect& wb_roi, int _num_bins=16, float known_bkg_energy = 10, float known_frg_energy = 10);
    virtual ~HistAppearance();

    virtual void update_model_parameters(Mat& img, Mat& mask);

    //note the input img is not used in this function.
    //using this so that the interface is consistent
    virtual void compute_unary_term(const Mat& img, const Mat& mask, Mat& ue);
    virtual void save_parameters(const int itr, const string& filename) const;

    //
    const Mat& get_roi_bin_id() const {return roi_bin_id;}
    const Mat& get_quan_img() const {return quan_img;}

  private:
    int   num_bins;      //number of bins for each color channel
    float bin_width;     //bin width
    Mat            parameters;    //color histograms for bg/fg. Each color channel with 'num_bins'
    Mat            quan_img;      //quatize image into 'num_bins'
    vector<float>  total_pxl;     //total number of pixels for background and foreground

    Mat            roi_bin_id;      //bin id's for the roi
};


#endif
