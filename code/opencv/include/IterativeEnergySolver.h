#ifndef _ITERATIVE_ENERGY_SOLVER_H
#define _ITERATIVE_ENERGY_SOLVER_H

/*
 * Derived class from EnergySolver
 *
 * Solve the energy in an EM-like style.
 * First, fix the segmentation (i.e., x_opt) and solve the model parameters
 * Second, fixe the model parameters, and solve the segmentation
 *
 * This method was proposed in the Grabcut.
 *
 *
 * Liang-Chieh@UCLA, April 2013
 *
 */

#include "EnergySolver.h"
#include <string>
#include <opencv2/core/core.hpp>
#include <vector>

using namespace std;
using namespace cv;

class AppearanceModel;
class ContourCreater;
class ContrastSensitivePM;
class DepthInterpolater;
class DepthPotential;
class MaskCreater;
class StarShapeModel;

class IterativeEnergySolver : public EnergySolver
{
  public:
    IterativeEnergySolver(const int _num_itr, const string& img_name);
    virtual ~IterativeEnergySolver();

    virtual void run(AppearanceModel* app_model, DepthPotential* dep_model, DepthInterpolater* dep_int, DepthInterpolater* dep_int_stereo, ContrastSensitivePM* binary_model, StarShapeModel* star_model, ContourCreater *cc, vector<ContourCreater*> cc_m, Mat& img, Mat& mask, Mat& soft_mask, Mat& recon_depth, Mat& stereo_depth, vector<double>& lambda, int gc_max_itr);

    //virtual void run(AppearanceModel* app_model, ContrastSensitivePM* binary_model, MaskCreater* mc, ContourCreater *cc, Mat& img, Mat& mask, vector<double>& lambda, int gc_max_itr) {}
    

 private:
    //stop running the algorithm if return true
    bool satisfy_stopping_criterion(const int itr);


};



#endif
