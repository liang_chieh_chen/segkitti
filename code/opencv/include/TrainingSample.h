/*  This file is part of Structured Prediction (SP) - http://www.alexander-schwing.de/
 *
 *  Structured Prediction (SP) is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation, either
 *  version 3 of the License, or (at your option) any later version.
 *
 *  Structured Prediction (SP) is distributed in the hope
 *  that it will be useful, but WITHOUT ANY WARRANTY; without even the
 *  implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE. See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Structured Prediction (SP).
 *  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright (C) 2010-2013  Alexander G. Schwing  [http://www.alexander-schwing.de/]
 */

//Author: Alexander G. Schwing

/*  ON A PERSONAL NOTE: I spent a significant amount of time to go through
 *  both, theoretical justifications and coding of this framework.
 *  I hope the package is useful for your task. Any citations, requests, 
 *  feedback, donations and support would be greatly appreciated.
 *  Thank you for contacting me!
 */

#ifndef _TRAININGSAMPLE_H
#define _TRAININGSAMPLE_H

#include <string>
#include <vector>
#include <opencv2/core/core.hpp>

struct SampleParams {
  std::string fn;
};

struct TrainingDataParams {
  std::vector<struct SampleParams*> SampleData;
};

class TrainingSample {
 private:
  struct SampleParams* sample;     //filename to load x?
  std::vector<int> labeling;       //yhat: estimated labeling
  std::vector<int> gt;             //y   : ground-truth labeling
  std::vector<double> phi_gt;      //phi(x,y)

  ///* jay add
  std::vector<double> weighted_loss;  //weight for different classes
  //*/

 public:
  TrainingSample() {};
  ~TrainingSample() {};
	  
  int Load(struct SampleParams* sample);
  int MarginRescaling(std::vector<double>& w, double time);
  int FeatureVector(std::vector<int>& y, std::vector<double>& phi);     double Loss(); 
  int AddFeatureDiffAndLoss(std::vector<double>& featureDiffAndLoss, std::vector<double>& w);

  ///* jay add
  void setup_weighted_loss(const std::vector<double>& wl);
  //*/

 private:
  ///* jay add
  void get_model_energies(cv::Mat& a_ue, cv::Mat& d_ue, cv::Mat& r_ue, cv::Mat& st_ue, cv::Mat& c_ue, std::vector<cv::Mat> & m_ue, cv::Mat& p_pe, cv::Mat& p_pe2, cv::Mat& p_pi, cv::Mat& s_pe, cv::Mat& s_pi, cv::Mat& mask_roi, cv::Mat& soft_mask_roi);
  
  //*/
};

#endif
