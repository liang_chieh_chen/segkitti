#ifndef _ALTERNATE_ENERGY_SOLVER_H
#define _ALTERNATE_ENERGY_SOLVER_H

/*
 * Derived class from EnergySolver
 *
 * The energy function contains two sets of variables.
 * This class solve the energy function alternatively.
 * That is, fix one, optimize the other, and alternate the process.
 *
 *
 * Liang-Chieh@TTIC, July 2013
 */

#include "EnergySolver.h"
#include <string>
#include <opencv2/core/core.hpp>

using namespace std;
using namespace cv;

class AppearanceModel;
class ContourCreater;
class ContrastSensitivePM;
class DepthInterpolater;
class DepthPotential;
class MaskCreater;
class StarShapeModel;

class AlternateEnergySolver : public EnergySolver
{
 public:
  AlternateEnergySolver(const int _num_itr, const string& img_name);
  ~AlternateEnergySolver();

  //NOT IMPLEMENTED YET
  virtual void run(AppearanceModel* app_model, DepthPotential* dep_model, DepthInterpolater* dep_int, ContrastSensitivePM* binary_model, StarShapeModel* star_model, ContourCreater* cc, Mat& img, Mat& mask, Mat& recon_depth, vector<double>& lambda, int gc_max_itr) {}

  //Alternating minimization. recover depth by DepthInterpolater
  virtual void run(AppearanceModel* app_model, ContrastSensitivePM* binary_model, MaskCreater* mc, ContourCreater* cc, Mat& img, Mat& mask, vector<double>& lambda, int gc_max_itr);

 private:
  bool satisfy_stopping_criterion(const int itr);


};



#endif
