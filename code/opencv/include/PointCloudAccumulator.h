#ifndef _PointCloudAccumulator_H
#define _PointCloudAccumulator_H


#include <vector>
#include <string>
#include "tracklets.h"

#include <opencv2/core/core.hpp>

using namespace std;
using namespace cv;

class MaskCreater;


class PointCloudAccumulator{
 public:
  PointCloudAccumulator(const string& v_c_file, const string& c_c_file, const string& r_folder, const string& v_folder, const int c_id=2, const int e_width=2);
  ~PointCloudAccumulator();
 
  void init(const vector<int>& i_ids);
  void clear();

  void fuse_point_clouds(const Mat& fimg, const Tracklets::tTracklet* car, Mat& fuse_mask, Mat& fuse_depth_img, const string& tracklet_name, const bool use_icp = false);

 private:
  vector<int> img_ids;

  int num_frame;
  string velo_to_cam_filename;
  string cam_to_cam_filename;
  string tracklet_filename;
  string raw_data_img_folder;
  string velo_folder;

  int camera_id;
  int expand_width;  //exapnd the 2D bounding box. outside regions = background seeds


  vector<MaskCreater* > mcs;
  

};



#endif
