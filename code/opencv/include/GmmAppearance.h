#ifndef _GMM_APPEARANCE_H
#define _GMM_APPEARANCE_H

/*
 *
 * The Gaussian Mixture Models are modified from the source codes of OPENCV in /imgproc/src/grabcut.cpp
 * The GMM model is used in the algorithm Grabcut described in "GrabCut- Interactive Foreground Extraction using Iterated Graph Cuts". Carsten Rother, Vladimir Kolmorogrov, Andrew Blake.
 *
 * This class GmmAppearance contains two GmmModel objects: one for background and the other for foreground.
 *
 *
 * Liang-Chieh@UCLA, May include
 *
 */


#include <opencv2/core/core.hpp>
#include <limits>
#include <iostream>
#include <string>
#include "Constants.h"
#include "AppearanceModel.h"

using namespace std;
using namespace cv;


class GmmModel;

class GmmAppearance : public AppearanceModel
{
  public:
    GmmAppearance(Mat& img, Mat& mask, Rect& roi, Rect& wb_roi, int _num_component = 5, int _feat_size = 3);
    virtual ~GmmAppearance();

    virtual void update_model_parameters(Mat& img, Mat& mask);

    //note the node index for ue is in row-scan-order within the image
    virtual void compute_unary_term(const Mat& img, const Mat& mask, Mat& ue);

    virtual void save_parameters(const int num_itr, const string& file_name) const;

    void printBkgSamples(){ cout << bkgSamples << endl; }
    void printFrgSamples(){ cout << frgSamples << endl; }

  private:
    int num_components;      //number of mixture components for foreground and background models
    int feat_size;           //feature length

    GmmModel* gmm[2];

    Mat bkgSamples;          //samples that belong to background
    Mat frgSamples;          //samples that belong to foreground

  private:
    void collect_samples(Mat& img, Mat& mask, Mat& bkg, Mat& frg);
};


#endif
