#ifndef _DEPTH_INTERPOLATER_H
#define _DEPTH_INTERPOLATER_H


#include <opencv2/core/core.hpp>
#include <vector>
#include <Eigen/Sparse>
#include <Eigen/Dense>

using namespace cv;
using namespace std;

typedef Eigen::Triplet<double> Trip;

/*IF use Laplacian MRF
class IloNumArray;
//*/

class GmmAppearance;

class DepthInterpolater
{
 public:
  DepthInterpolater(int _num_itr=300);
  DepthInterpolater(Mat& depth_img, Mat& mask, Rect& roi, int _num_component, int _feat_size, int _num_itr=300);

  ~DepthInterpolater();

  void interpolate(Mat& recon_depth_img, const Mat& fimg, const Mat& depth, const Mat& ind, const double lambda_dep=0.01, const int method=1);

  void compute_unary_term(const Mat& recon_depth, const double lambda_r, Mat& r_ue);

  void setup_target_depth(const float dep) {target_depth = dep;}

 private:
  int m_num_itr;  //number of iteration for solvers.

  float target_depth;

  GmmAppearance* gmm_app;

  int m_img_row;  //the row and col of the depth_img
  int m_img_col;  //save it because, recon_depth is only within roi

 private:
  //interpolate: works within only roi
  void gaussian_interpolate(Mat& recon_depth, const Mat& fimg, const Mat& depth, const Mat& indicator, const double lambda_dep);
  void laplacian_interpolate(Mat& recon_depth, const Mat& fimg, const Mat& depth, const Mat& indicator, const double lambda_dep);

  //compute image pixel difference
  void find_img_diff(vector<Mat>& img_diff, double& total_diff, const Mat& fimg);

  //create a matrix with elements marked by node order (scanned in row-order)
  void create_node_matrix(Mat& nodes);

  //create a matrix with elements specified the neighbor index
  void create_ngh_node_matrix(const Mat& nodes, const int ngh_type, Mat& ngh);

  //create the triplets used for identity matrix with dimension = dim
  void create_diag_triplets(vector<Trip>& coeffs, const int dim, const double val = 1.0);
  void create_diag_triplets(vector<Trip>& coeffs, const int dim, const Mat& val);


  //create the triplets specified by the mask (set position by cur_node_idx when mask=1)
  void create_mask_triplets(vector<Trip>& coeffs, int& count, const Mat& cur_node_idx, const Mat& mask);

  //build the triples from cur_node_idx, ngh_node_idx and val
  void create_triplets(vector<Trip>& coeffs, const Mat& cur_node_idx, const Mat& ngh_node_idx, const double val = 1.0);

  //build the triples from cur_node_idx, ngh_node_idx and img_diff_value
  //  void create_triplets(vector<Trip>& coeffs, const Mat& cur_node_idx, const Mat& ngh_node_idx, const Mat& img_diff);

  //vectorize the input matrix, location specified by indicator
  void vectorize_matrix(Eigen::VectorXd& v, const Mat& mat, const Mat& ind);

  //initialize the variables before applying interpolation
  void initialize_depth(Eigen::VectorXd &y, const Mat& depth, const Mat& indicator, const int type = 1, const int ker_size = 3);

  //copy the computed value back to opencv Mat structure
  void copy_result(Mat& recon, const Eigen::VectorXd& y);

  /*IF use Laplacian MRF
  void copy_result(Mat& recon, const IloNumArray y);
  //*/
};


#endif
