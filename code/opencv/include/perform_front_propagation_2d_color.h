#ifndef _PERFORM_FRONT_PROPAGATION_2D_H_
#define _PERFORM_FRONT_PROPAGATION_2D_H_

#include <math.h>
#include "config.h"
#include "f_heap.h"
#include <stdio.h>
#include <string.h>
#include <vector>
#include <algorithm>

#include <opencv2/core/core.hpp>

using namespace cv;
using namespace std;
using namespace dk_boost;

// some global variables
extern int n;			// width
extern int p;			// height
extern int nCh; // Number of channels in the image
extern int nPixels;
extern float* D;
extern float* S;
extern float* W;
extern float* Q;
extern float* L;
extern float* start_points;
//extern float* end_points;
extern float* H;
extern int nb_iter_max;
extern int nb_start_points;
//extern int nb_end_points;


//typedef bool (*T_callback_intert_node)(int i, int j, int ii, int jj);

void perform_front_propagation_2d_wrapper(const Mat& fimg, const Mat& st_pts, const int ngh_conn, Mat& distance, Mat& root_points, Mat& backlinks);

void perform_front_propagation_2d(const Mat& img_roi, const Mat& st_pts);


/*
void perform_front_propagation_2d_wrapper(const Mat& fimg, const Mat& st_pts, const Rect& roi, const int ngh_conn, Mat& distance, Mat& root_points, Mat& backlinks);

void perform_front_propagation_2d(const Mat& img_roi, const Mat& st_pts, const Rect& roi);
*/

#endif // _PERFORM_FRONT_PROPAGATION_2D_H_
