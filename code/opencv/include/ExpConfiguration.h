#ifndef _EXPCONFIGURATION_H
#define _EXPCONFIGURATION_H

/*
 *
 * Setup the configuration for experiments
 * For example, the image folder path, and so on
 *
 *
 *
 * Liang-Chieh@UCLA, April 2013
 *
 *
 */

#include <opencv2/core/core.hpp>
#include <iostream>
#include <string>

using namespace cv;
using namespace std;

class ExpConfiguration
{
public:
    ExpConfiguration() {}

    void write(FileStorage& fs) const;                        //Write serialization for this class
    void read(const FileNode& node);                          //Read serialization for this class

public:  
    //where detection dataset is
    string detectionDataBaseDir;

    //where the detection image is
    string detectionImgDir;

    //where the annotation masks are saved
    string annotationMaskDir;

    //where the annotation bounding boxes are saved
    string annotationMaskBDDir;

    //where raw dataset is
    //string rawDataBaseDir;

    //where the raw data image is
    //string rawDataImgDir;

    //image extension
    string imgExt;

    //where the raw data image is
    string rawDataImgDir;

    //where the Velodyne data is
    string veloDir;
    
    //velodyne file extension
    string veloExt;

    //image mapping list (before preprocessing)
    //stores the link from detection image id to raw data image id
    string mappingList;

    //where to store the segmentation results
    string resultDir;

    //where to store the preprocessed results
    string trackletsDir;

    //where the cad segmentations are saved relative to detectionDataBaseDir
    string cadSegDir;

    //where the cad segmentation bounding box's coordinates are saved relative to detectionDataBaseDir;
    string cadSegBdDir;

    //where the mechanical turk segmentations are saved relative to detectionDataBaseDir
    string mtSegDir;

    //where the mechanical turk segmentation bounding box's coordinates are saved relative to detectionDataBaseDir;
    string mtSegBdDir;


    ///////
    //below variables are under the folder of preprocDir
    //HARD CODE BELOW FOLDER PATHS
    ///////
    //where to store the segmentation ground truth    
    //string gtDir;
    
    //where to save point cloud depth values
    //    string depthDir;

    //where to save car convex hull
    //    string chDir;

    //where to save point clouds
    //    string pcDir;

};

//These write and read functions must be defined for the serialization in FileStorage to work
static void write(FileStorage& fs, const string&, const ExpConfiguration& x)
{
    x.write(fs);
}
static void read(const FileNode& node, ExpConfiguration& x, const ExpConfiguration& default_value = ExpConfiguration()){
    if(node.empty())
        x = default_value;
    else
        x.read(node);
}

// This function will print our custom class to the console for debug
static ostream& operator<<(ostream& out, const ExpConfiguration& c)
{
  //  out << "imgDir: " << c.imgDir << endl;
  //  out << "maskDir: " << c.maskDir << endl;
  //out << "outputDir: " << c.outputDir << endl;

  return out;
}

//These write and read functions must be defined for the serialization in FileStorage to work
//static void write(FileStorage& fs, const std::string&, const ExpConfiguration& x);
//static void read(const FileNode& node, ExpConfiguration& x, const ExpConfiguration& default_value = ExpConfiguration());
// This function will print our custom class to the console for debug
//static ostream& operator<<(ostream& out, const ExpConfiguration& c);


//utility function
int readExpConfigurationFromFile(const string& filename, ExpConfiguration& config);


#endif
