#ifndef _MAP_METHOD_H
#define _MAP_METHOD_H

/*
 * interface for Map method
 *
 * Given the model parameters, we can find the most probable label assignment (x_opt) by graphcut or belief propagation or any other method.
 * This class is used as their interfacee class.
 *
 *
 * Liang-Chieh@UCLA, April 2013
 *
 */

#include <opencv2/core/core.hpp>

using namespace cv;

class MapMethod
{
  public:
    virtual ~MapMethod();
    virtual float compute_map(const Mat& UE, const Mat& PE, const Mat& PI, Mat& x_opt) = 0;

};


#endif
