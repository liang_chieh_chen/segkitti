#ifndef _ENERGY_SOLVER_H
#define _ENERGY_SOLVER_H

/*
 * interface for energy solver
 *
 * EnergySolver is used to solve the energy function
 * Since solving the function is NP-hard, we may use iterative method or dual decomposition to solve it.
 *
 *
 * Liang-Chieh@UCLA, April 2013
 *
 */

#include <opencv2/core/core.hpp>
#include <vector>

using namespace cv;
using namespace std;

class AppearanceModel;
class ContourCreater;
class ContrastSensitivePM;
class DepthInterpolater;
class DepthPotential;
class MaskCreater;
class StarShapeModel;
class FastPDWrapper;

class EnergySolver
{
  public:
    EnergySolver(const int _num_itr = 1, const string& _img_name = "");
    virtual ~EnergySolver();

    //solve the energy    
    virtual void run(AppearanceModel* app_model, DepthPotential* dep_model, DepthInterpolater* dep_int, DepthInterpolater* dep_int_stereo, ContrastSensitivePM* binary_model, StarShapeModel* star_model, ContourCreater* cc, vector<ContourCreater*> cc_m, Mat& img, Mat& mask, Mat& soft_mask, Mat& recon_depth, Mat& stereo_depth, vector<double>& lambda, int gc_max_itr) {}

    //    virtual void run(AppearanceModel* app_model, ContrastSensitivePM* binary_model, MaskCreater* mc, ContourCreater* cc, Mat& img, Mat& mask, vector<double>& lambda, int gc_max_itr) {}
    

    void save_model_energy(string& file_name);
    void setup_save_path(string& s){save_path = s;}

    //get functions
    const int get_num_itr() const {return num_itr;}
    const string get_img_name() const {return img_name;}
    const bool get_verbose() const {return verbose;}
    const string& get_save_path() const {return save_path;}

    vector<double> model_energy;         //keep track of the model energy

  private:
    int num_itr;                         //number of iteration for the EM-like method
    string img_name;
    bool verbose;                        //show the result at each iteration if verbose
    string save_path;                    //path where to save result
};



#endif
