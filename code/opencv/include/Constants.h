#ifndef _GC_CONSTANTS_H
#define _GC_CONSTANTS_H

/*
 * Global Constants
 *
 * 1. The labels used in the Trimaps of Grabcut dataset
 *
 *
 * Liang-Chieh@UCLA, April 2013
 *
 */

#include <string>

#define DEBUG 1

//use the soft mask or not (each point cloud has penalty related to its distance to the 3D bounding box)
const bool USE_SOFT_MASK = false;

//use original mask (projected point clouds) or use inverse project mask (inverse project LapMRF or Stereo)
const bool USE_ORIG_MASK = true;

//use average shape to replace CAD shape
const bool USE_AVG_SHAPE = false;

//compute potentials for turker shapes. will be faster if turned off.
const bool USE_TURKER = false;

//use the inverse mask to find stars
const bool USE_INVERSE_PROJ_STAR = false;


/*
 * folder names
 */
const std::string GT_FOLDER           = "tracklet_gt";

const std::string STAR_FOLDER         = "tracklet_star_pc";          //star formed by Velodyne
//const std::string STAR_FOLDER         = "tracklet_star_stereo"; //star formed by stereo inverse projection

const std::string IMG_FOLDER          = "images";
const std::string ROI_FOLDER          = "roi";
const std::string PC_FOLDER           = "tracklet_pc";
const std::string SOFT_PC_FOLDER      = "tracklet_pc_soft";
const std::string DEPTH_FOLDER        = "tracklet_depth";
const std::string RECON_DEPTH_FOLDER  = "tracklet_recon_depth_LapMRF";  // or tracklet_recon_depth_GauMRF

const std::string CAD_SHAPE_FOLDER    = "tracklet_cadShape";

//const std::string AVG_SHAPE_FOLDER    = "tracklet_avgShape"; 
const std::string AVG_SHAPE_FOLDER    = "tracklet_avgShape_v3"; 

const std::string MASKED_FOLDER       = "tracklet_roi";
const std::string STEREO_FOLDER       = "tracklet_stereo_depth";
const std::string INVERSE_MASK_FOLDER = "tracklet_inverse_mask";

const std::string MT_SHAPE_FOLDER_B1  = "tracklet_mtShape_batch1";
//const std::string MT_SHAPE_FOLDER_B1  = "tracklet_mtShape_b1b2b3";

const std::string MT_SHAPE_FOLDER_B2  = "tracklet_mtShape_batch2";
const std::string MT_SHAPE_FOLDER_B3  = "tracklet_mtShape_batch3";

//where to save the data preprocessed results
const std::string MT_SHAPE_FOLDER     = "tracklet_mtShape";


const std::string FUSE_PATH = "_fuse";


const unsigned char STRICT_BACKGROUND_LABEL = 0;
const unsigned char WEAK_BACKGROUND_LABEL = 64;
const unsigned char UNDEFINED_LABEL = 128;
const unsigned char FOREGROUND_LABEL = 255;

//zero probability. cannot really use 0, due to -log(0) = inf.
const float ZERO_PROB = 1e-6;
const float FLOAT_INF = 1e20;

const double D_PI = 3.141596;
const int MAX_NUMBER_POINT_CLOUDS = 1000000;

const std::string SAVE_IMG_EXT = ".png";

const bool DO_NORMALIZE_ENERGY = false;

//number of stars for star shape model
const int NUM_STAR = 1;   
////edge weight for the star shape model, out-of-date
////const float STAR_WEIGHT = 100;

//recovered depth model type 
//note MRF_TYPE is only used in DataProcessor
const int MRF_TYPE = 1; //1:laplacian, 2: gaussian 
//pairwise weight for recovered depth image
const double RECOVER_DEPTH_WEIGHT = 0.01;  

//will only focus on tracklet with at least so many point clouds
const int FRG_PC_THRESHOLD = 20;  

const float GROUND_VAL = -1.55;    //ALL CAMERA HEIGHTs = 1.65m 
                                   //in Velodyne coordinate system
const double JET_COLOR[192] = 
  {0,	0,	0.562500000000000,
   0,	0,	0.625000000000000,
   0,	0,	0.687500000000000,
   0,	0,	0.750000000000000,
   0,	0,	0.812500000000000,
   0,	0,	0.875000000000000,
   0,	0,	0.937500000000000,
   0,	0,	1,
   0,	0.0625000000000000,	1,
   0,	0.125000000000000,	1,
   0,	0.187500000000000,	1,
   0,	0.250000000000000,	1,
   0,	0.312500000000000,	1,
   0,	0.375000000000000,	1,
   0,	0.437500000000000,	1,
   0,	0.500000000000000,	1,
   0,	0.562500000000000,	1,
   0,	0.625000000000000,	1,
   0,	0.687500000000000,	1,
   0,	0.750000000000000,	1,
   0,	0.812500000000000,	1,
   0,	0.875000000000000,	1,
   0,	0.937500000000000,	1,
   0,	1,	1,
   0.062500000000000,	1,	0.937500000000000,
   0.125000000000000,	1,	0.875000000000000,
   0.187500000000000,	1,	0.812500000000000,
   0.250000000000000,	1,	0.750000000000000,
   0.312500000000000,	1,	0.687500000000000,
   0.375000000000000,	1,	0.625000000000000,
   0.437500000000000,	1,	0.562500000000000,
   0.500000000000000,	1,	0.500000000000000,
   0.562500000000000,	1,	0.437500000000000,
   0.625000000000000,	1,	0.375000000000000,
   0.687500000000000,	1,	0.312500000000000,
   0.750000000000000,	1,	0.250000000000000,
   0.812500000000000,	1,	0.187500000000000,
   0.875000000000000,	1,	0.125000000000000,
   0.937500000000000,	1,	0.0625000000000000,
   1,	1,	0,
   1,	0.937500000000000,	0,
   1,	0.875000000000000,	0,
   1,	0.812500000000000,	0,
   1,	0.750000000000000,	0,
   1,	0.687500000000000,	0,
   1,	0.625000000000000,	0,
   1,	0.562500000000000,	0,
   1,	0.500000000000000,	0,
   1,	0.437500000000000,	0,
   1,	0.375000000000000,	0,
   1,	0.312500000000000,	0,
   1,	0.250000000000000,	0,
   1,	0.187500000000000,	0,
   1,	0.125000000000000,	0,
   1,	0.062500000000000,	0,
   1,	0,	0,
   0.937500000000000,	0,	0,
   0.875000000000000,	0,	0,
   0.812500000000000,	0,	0,
   0.750000000000000,	0,	0,
   0.687500000000000,	0,	0,
   0.625000000000000,	0,	0,
   0.562500000000000,	0,	0,
   0.500000000000000,	0,	0};
  
/*
const int TRACKLET_BD_REGION_SIZE = 30;
//the size of sqrt(row*col)
const double TRACKLET_BD_REGION_RANGE[30] = {
   26.9131,  36.9215,  46.9300,  56.9384,  66.9468, 
   76.9552,  86.9636,  96.9720, 106.9805, 116.9889, 
  126.9973, 137.0057, 147.0141, 157.0225, 167.0310, 
  177.0394, 187.0478, 197.0562, 207.0646, 217.0731,
  227.0815, 237.0899, 247.0983, 257.1067, 267.1151, 
   277.1236, 287.1320, 297.1404, 307.1488, 317.1572};
*/
/*
const int TRACKLET_BD_REGION_SIZE = 20;
//the size of sqrt(row*col)
const double TRACKLET_BD_REGION_RANGE[20] = {
  29.415, 44.428, 59.440, 74.453, 89.466, 104.478, 119.491, 134.504, 149.516, 164.529, 	
179.541, 194.554, 209.567, 224.579, 239.592, 254.605, 269.617, 284.630, 299.643, 314.655};

*/

///*
const int TRACKLET_BD_REGION_SIZE = 15;
//the size of sqrt(row*col)
const double TRACKLET_BD_REGION_RANGE[15] = {
 31.917, 51.934, 71.951, 91.968, 111.985, 132.002, 152.018, 172.035, 192.052, 212.069, 
 232.086, 252.103, 272.119, 292.136, 312.153};
//*/

/*
const int TRACKLET_BD_REGION_SIZE = 10;
//the size of sqrt(row*col)
const double TRACKLET_BD_REGION_RANGE[10] = {
36.922, 66.947, 96.972, 126.997, 157.023, 187.048, 217.073, 247.098, 277.124, 307.149};

*/

#endif
