/* 
 * Compute naive depth potential
 * Directly work on region of interest!
 *
 *
 * Liang-Chieh @ TTIC, August 2013
 *
 */

#ifndef _DEPTH_POTENTIAL_H
#define _DEPTH_POTENTIAL_H

#include <vector>
#include <opencv2/core/core.hpp>

using namespace std;
using namespace cv;

class DepthPotential
{
 public:
  DepthPotential();
  ~DepthPotential();

  void compute_unary_term(const Mat& mask, const vector<double>& lambda, Mat& ue);
  void compute_unary_term(const Mat& mask, const Mat& soft_mask, Mat& ue);

 private:
  //double lambda_f;  //penalty if assign bkg to frg point cloud
  //double lambda_b;  //penalty if assing frg to bkg point cloud


};



#endif
