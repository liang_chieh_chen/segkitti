#ifndef _CONTRAST_SENSITIVE_PM_H
#define _CONTRAST_SENSITIVE_PM_H

/*
 * Contrast Sensitive Potts Model
 *
 * Liang-Chieh@UCLA, April 2013
 *
 */

#include <opencv2/core/core.hpp>
#include <vector>

using namespace cv;
using namespace std;

class ContrastSensitivePM
{
  public:
    ContrastSensitivePM(const Rect& _roi, int conn=4);
    ~ContrastSensitivePM();

    //void compute_pe(const Mat& img, const Mat& mask, const vector<double>& lambda, Mat& PE, Mat& PI);
    void compute_pe(const Mat& img, const Mat& mask, const vector<double>& lambda, Mat& PE1, Mat& PE2, Mat& PI);

    const Rect& get_roi() const {return roi;}

    //void compute_img_diff(const Mat& fimg, const vector<double>& lambda, vector<Mat>& img_diff, double& total_diff);    
    void compute_img_diff(const Mat& fimg, vector<Mat>& img_diff, double& total_diff);    

  private:
    Rect roi;  //region of interest

    int ngh_conn;    //neighbor connection. 2 for 4-connected neighbors, and 4 for 8-connected ones
                    //this value is computed by dividing the input argument by 2

   };


#endif
