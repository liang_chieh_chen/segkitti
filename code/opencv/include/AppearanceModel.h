#ifndef _APPEARANCE_MODEL_H
#define _APPEARANCE_MODEL_H

/*
 *
 * Base class for appearance model
 *
 *
 * Liang-Chieh@UCLA, April 2013
 *
 */

#include <opencv2/core/core.hpp>
#include <string>

using namespace std;
using namespace cv;


class AppearanceModel
{
  public:
    AppearanceModel();

    virtual ~AppearanceModel();

    AppearanceModel(const Mat& mask, const Rect& _roi, const Rect& _wb_roi);

    //interface
    virtual void update_model_parameters(Mat& img, Mat& mask) = 0;

    //note the node index for ue is in row-scan-order within the image
    virtual void compute_unary_term(const Mat& img, const Mat& mask, Mat& ue) = 0;

    virtual void save_parameters(const int itr, const string& file_name) const = 0;

    //get and set data members
    void update_map_result_within_roi(Mat& x_opt) {x_opt.copyTo(map_result(roi));}
    const Mat& get_map_result() const {return map_result;}

    const Rect& get_roi() const {return roi;}
    const Rect& get_roi_wb() const {return roi_wb;}

    void setup_save_path(const string& s) {save_path = s;}
    const string& get_save_path() const {return save_path;}

  private:
    Mat map_result;   //map result for the undefined region
    Rect roi;         //region of interest (i.e.,the undefined region)
    Rect roi_wb;      //region within the Weak background rectangle

    string save_path;           //the path where to save the results

};



#endif
