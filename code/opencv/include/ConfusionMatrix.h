/*
 * This class is copied/modified from Stephen Gould's library: darwin
 * See their drwnConfusionMatrix for clear explanation
 *
 * 
 *
 */

#ifndef _CONFUSION_MATRIX_H
#define _CONFUSION_MATRIX_H

#include <vector>
#include <iostream>
#include <string>

class ConfusionMatrix
{
 public:
  //create a m-by-m confusion matrix
  ConfusionMatrix(const int m);
  ~ConfusionMatrix();

  void accumulate(const int actual, const int predicted);
  void accumulate(const ConfusionMatrix& conf);
 
  int numRows() const;
  int numCols() const;
  
  void clear();
  
  void printCounts(std::ostream &os = std::cout, const char *header = NULL) const;
  void printRowNormalized(std::ostream &os = std::cout, const char *header = NULL) const;
  void printColNormalized(std::ostream &os = std::cout, const char *header = NULL) const;
  void printNormalized(std::ostream &os = std::cout, const char *header = NULL) const;
  void printPrecisionRecall(std::ostream &os = std::cout, const char *header = NULL) const;
  void printF1Score(std::ostream &os = std::cout, const char *header = NULL) const;
  void printJaccard(std::ostream &os = std::cout, const char *header = NULL) const;
  
  double rowSum(int n) const;
  double colSum(int m) const;
  double diagSum() const;
  double totalSum() const;
  double accuracy() const;
  double avgPrecision() const;
  double avgRecall() const;
  double avgJaccard() const;
 
  double precision(int n) const;
  double recall(int n) const;
  double jaccard(int n) const;
 
  const unsigned& operator()(int x, int y) const;
  unsigned& operator()(int x, int y);

 private:
  std::vector< std::vector<unsigned int> > _matrix;
  
 public:
  std::string ROW_BEGIN;
  std::string ROW_END;
  std::string COL_SEP;


};


#endif
