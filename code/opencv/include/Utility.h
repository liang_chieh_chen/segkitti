#ifndef _MY_UTILITY_H
#define _MY_UTILITY_H

/*
 *
 * Utiltiy functions
 *
 *
 * Liang-Chieh@UCLA, April 2013
 *
 */

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <iostream>
#include <string>
#include <vector>
#include <sstream>

using namespace cv;
using namespace std;


class ExpConfiguration;


//visualization functions
void visualize_ue(const Mat& ue, const Rect& roi_rect, const string& file_name="");
void visualize_binary_img(const Mat& img, const Rect& roi, const string& file_name="");
void visualize_map_result(const Mat& img, const Mat& mask, const Rect& roi, const string& file_name="", const bool show_result=false);
void imagesc_bw(const string& name, const Mat& fimg, const string& filename="");
void visualize_position(const string& name, const Mat& pos, const int rows, const int cols);
void color_imshow_bw(const string& name, const Mat& fimg, const string& filename="");


//set the value to true if the element's "which_dim" satisfy the condition
void setup_select_indicator(vector<bool>& ind, const Mat& source, const int dim, const bool larger, const float val);

void select_by_indicator(const vector<bool>& source, const vector<bool>& ind, vector<bool>& dest);
void select_by_indicator(const Mat& source, const vector<bool>& ind, Mat& dest);
void remove_by_indicator(const Mat& source, const vector<bool>& ind, Mat& dest);
void negate_indicator(vector<bool>& dest, const vector<bool>& source);
void and_indicator(vector<bool>& dest, const vector<bool>& source1, const vector<bool>& source2); 

//save/load opencv rect
void load_bounding_boxes_from_file(const string& file_name, vector<Rect>& bboxes);

void save_a_bounding_box(const string& file_name, const Rect& bbox, const bool rewrite = true);

void load_vector_double(const string& file_name, vector<double>& w);

//save/load opencv Mat
void save_sparse_float_mat(const string& name, const Mat& f_mat, const bool rewrite = true);
void load_sparse_float_mat(const string& name, Mat& f_mat);
void save_float_mat(const string& name, const Mat& f_mat, const bool rewrite = true);

void save_uchar_mat(const string& name, const Mat& u_mat, const bool rewrite = true);

void load_int_mat_bin(const string& name, Mat& i_mat);
void save_int_mat_bin(const string& name, const Mat& i_mat, const bool rewrite = true);

int load_float_mat_bin(const string& name, Mat& i_mat);
void save_float_mat_bin(const string& name, const Mat& i_mat, const bool rewrite = true);

//load the weight from structural svm output
void load_weight_from_ssvm_output(const string& filename, vector<double>& w);

//string processing
string extract_first_token(const string& name, const string& pattern);

string replace_string(string subject, const string& search, const string& replace);

//load all needed data for one specific image
//such as loading computed mask, computed depth_img
void load_all_data(const string& name, const ExpConfiguration* config, Mat& fimg, vector<Rect>& bboxes, Mat& mask, Mat& soft_mask, Mat& inv_mask, Mat& depth_img, Mat& recon_depth, Mat& stereo_depth, float& frg_median_depth);

void load_all_data(const string& name, Mat& fimg, vector<Rect>& bboxes, Mat& mask, Mat& soft_mask, Mat& inv_mask, Mat& depth_img, Mat& recon_depth, Mat& stereo_depth, float& frg_median_depth);

void load_all_data_and_star_energy(const string& name, Mat& fimg, vector<Rect>& bboxes, Mat& mask, Mat& soft_mask, Mat& inv_mask, Mat& depth_img, Mat& recon_depth, Mat& stereo_depth, float& frg_median_depth, Mat& s_pe, Mat& s_pi, const int num_star);


//load filenames from a txt file
void read_filenames(const string& filename, vector<string>& filenames);

//combine undirectional and directional energy terms
void combine_binary_energy(const Mat& pe1, const Mat& pi1, const Mat& pe2, const Mat& pi2, Mat& pe, Mat& pi);

//extract the experimental id and svm c from path name
void extract_expID_and_C(const string& map_path, string& exp_id, string& train_c);


//output cv rectangle
ostream& operator<<(ostream& os, const Rect& rect);

//find closes element in an array
void find_closest_element(const double query, const double* source, const int source_size, double* val, int *index);


//change int, float, ... to string
template <typename T>
string NumberToString(T pNumber)
{
 ostringstream oOStrStream;
 oOStrStream << pNumber;
 return oOStrStream.str();
}




#endif
