function [modelfiles, outdirclass] = fitCADtoKITTIapp_Jay(which, params)

if nargin < 2
    params.usedims = 1;
    params.useapp = 0;
    params.useocc = 1;
    params.storeall = 1;
    params.delete_folder = 0;
    params.display_progress = 1;
    params.lambda_2d = 0.02;  % weight for 2D box match
    params.lambda_dims = 0.002 * [1, 2, 1];  % weight for 3D dim match
    params.dist2cad_thresh = 1.5;   % weight for occlusion (if a 3d point is closer than this value from the CAD depth, it's occlusion)
    params.version = 'v3';
end;
if nargin < 1 || isempty(which)
which = 'all_list';
end;
%if usedims == 0
%    version = 'v1';
%else
%    version = 'v2';
%end;
%if useapp == 1
%    version = 'v3';
%end;
leftright = 'left';
segkitti_globals;
imdir = fullfile(DATASET_ROOT, ['Images_' leftright]);
annotdir = fullfile(DATASET_ROOT, ['Annotations_' leftright]);
outroot = fullfile(DATASET_ROOT, 'Segmentation_CAD', params.version);
annotoutdir = fullfile(outroot, ['Annotations_' leftright '_seg']);
outdirclass = fullfile(outroot, ['SegmentationClass_' leftright]);
outdirobj = fullfile(outroot, ['SegmentationObject_' leftright]);
depthdirobj = fullfile(outroot, ['DepthObject_' leftright]);
segvizdir = fullfile(outroot, ['SegViz_' leftright]);
objvizdir = fullfile(outroot, ['ObjViz_' leftright]);
spdir = fullfile(DATASET_ROOT, 'cpmc', ['ucm_' leftright]);

if exist(outroot, 'dir') && params.delete_folder
    cmd = sprintf('rm -fR %s', outroot);
    unix(cmd);
end;

mkdirsanja(outdirclass);
mkdirsanja(outdirobj);
mkdirsanja(segvizdir);
mkdirsanja(objvizdir);
mkdirsanja(annotoutdir);
mkdirsanja(depthdirobj);
snap2sp = 0;
map = colormap('jet');
close all;

cd(DATASET_ROOT);
if ischar(which)
    % read the full image file and take all objects!
    imfiles = textread([which '.txt'], '%s');
    objects = struct('ind_objects', cell(length(imfiles), 1));
    for i = 1 : length(imfiles)
        [~,name] = fileparts(imfiles{i});
        imfiles{i} = name;
        objects(i).ind_objects = [1:10000]';
    end;
else
    % take only a selected set of images and objects!
    imfiles = which.imfiles;
    objects = which.objects;
end;
%imfiles = textread(fullfile(DATASET_ROOT, [which '.txt']), '%s');

if ~isfield(params, 'modelfiles')
    allmodelsfile = fullfile(model_dir, 'allmodels.mat');
    if exist(allmodelsfile, 'file')
       load(allmodelsfile);
    else
       modelfiles = loadallmodels(allmodelsfile, model_dir);
    end;
else
    modelfiles = params.modelfiles;
    params.modelfiles = [];
end;

for i = 1 : length(modelfiles)
    [~,name] = fileparts(modelfiles(i).name);
    modelfiles(i).filename = name;
end;


for i = 1 : length(imfiles)
    annotfile = fullfile(annotdir, [imfiles{i} '.mat']);
    annotoutfile = fullfile(annotoutdir, [imfiles{i} '.mat']);
    outfileclass = fullfile(outdirclass, [imfiles{i} '.png']);
    outfileobj = fullfile(outdirobj, [imfiles{i} '.png']);
    depthfileobj = fullfile(depthdirobj, [imfiles{i} '.mat']);
    spfile = fullfile(spdir, [imfiles{i} '.mat']);
    txtfile = fullfile(outdirobj, [imfiles{i} '.txt']);
    if exist(txtfile, 'file')
        continue;
    end;
    if exist(outfileobj, 'file')
        continue;
    end;
    dlmwrite(txtfile, 'doing');
    im = imread(fullfile(imdir, [imfiles{i} '.png']));
    outimvizfile = fullfile(segvizdir, [imfiles{i} '.png']);
    if params.display_progress, fprintf('%d/%d\n', i, length(imfiles)); end;
    data = load(annotfile);
    an = data.annotation;
    inds = strmatch(CLASS_NAME, an.class, 'exact');
    if strcmp(CLASS_NAME, 'Car')
       inds1 = strmatch('Van', an.class, 'exact');
       inds = [inds; inds1];
    end;
    inds = intersect(inds, objects(i).ind_objects);
    trunc = cell2mat(an.truncated(inds));
    ind = find(trunc <= 1);
    inds = inds(ind);
    if ~isempty(inds)
       h = an.bboxes(inds, 4);
       w = an.bboxes(inds, 3);
       ind = find(h >= 5 & w >= 5);
       inds = inds(ind);
    end;
    
    v = get_depth_order(an.box3D(inds));
    inds = inds(v);
    segim = zeros(an.imsize(1), an.imsize(2));
    depthim = inf * ones(an.imsize(1), an.imsize(2));
    supim = [];
    supdata = [];
    supsize = [];
    if exist(spfile, 'file') && snap2sp
       data = load(spfile);
       [supim, supdata, supsize] = getsupdata_main(data.ucm2);
    elseif snap2sp
        fprintf('spfile doesnt exist\n');
    end;
    cadboxes = zeros(size(an.bboxes));
    masks = cell(size(an.bboxes, 1), 1);
    depths = cell(size(an.bboxes, 1), 1);
    cadinfo = struct('filename', cell(size(an.bboxes, 1), 1), ...
                     'id', cell(size(an.bboxes, 1), 1), 'transform', cell(size(an.bboxes, 1), 1), ...
                     'instance_name', cell(size(an.bboxes, 1), 1), 'error', cell(size(an.bboxes, 1), 1));
    
    K = [];
    for j = 1 : length(an.box3D)
        if isfield(an.box3D{j}, 'K') && isempty(K)
            K = an.box3D{j}.K;
        end;
    end;
    
    sceneDepth = [];
    if params.useocc
        try
        %sceneDepth = readBinFile(fullfile(depth_dir, [imfiles{i} '.bin']));
        scenedata = load(fullfile(depth_dir, [imfiles{i} '.mat']));
        sceneDepth = scenedata.recon_depth;
        ind = find(sceneDepth == 0);
        sceneDepth(ind) = inf;
        catch
            sceneDepth = inf * ones(size(im, 1), size(im, 2));
        end;
    end;
    %[depth,disp] = getDepthfromFile(imfiles{i});
    %sceneDepth = depth;

        boxes = an.bboxes;
        boxes(:, 3:4) = boxes(:, 1:2) + boxes(:, 3:4);
        boxesim = round(boxes);
        boxesim(:, 1:2) = max(boxesim(:, 1:2), ones(size(boxesim, 1), 2));
        boxesim(:, 3) = min(boxesim(:, 3), an.imsize(2));
        boxesim(:, 4) = min(boxesim(:, 4), an.imsize(1));
    for j = 1 : length(inds)
        if params.display_progress, fprintf('   obj %d ', j); end;
        %box3D = an.box3D{inds(j)};
        box3D = an.box3Dor{inds(j)};
        vertices = box3D.vertices;
        P = box3D.P;
        %[mesh, best_id, error] = matchCADmodels(modelfiles, vertices, K, boxesim(inds(j), :), an.imsize, usedims, useapp);
        [mesh, best_id, error] = matchCADmodels(modelfiles, vertices, P, boxesim(inds(j), :), an.imsize, params);
        if params.display_progress, fprintf('(%0.4f)\n', error); end;
        if error < 100
           [segim, segimj, depthim, depthimj, fitboxes] = getsilhCAD(boxesim(inds(j), :), box3D, segim, depthim, inds(j), P, mesh, sceneDepth, params);
           fitboxes(:, 3:4) = fitboxes(:, 3:4)-fitboxes(:,1:2);
           cadboxes(inds(j), :) = fitboxes;
           segind = find(segimj);
           masks{inds(j)} = segind;
           depths{inds(j)} = depthimj(segind);
           cadinfo(inds(j)).filename = modelfiles(best_id).filename;
           cadinfo(inds(j)).id = best_id;
           cadinfo(inds(j)).transform = mesh.transform{1};
           cadinfo(inds(j)).instance_name = mesh.instance_name;
           cadinfo(inds(j)).error = error;
        end;
    end;
    inds = strmatch('DontCare', an.class, 'exact');
    if ~isempty(inds)
     %   [segim] = getdontcare(segim, boxesim(inds, :), supim, supdata, supsize);
    end;
    %im = getimviz(map, im, segim, cadboxes, an.bboxes);
    if length(find(sum(cadboxes, 2) >0))
       %export_fig(outobjvizfile)
    end;
   % close all;
    if params.storeall
       imwrite(im, outimvizfile);
       imwrite(uint8(segim), VOClabelcolormap(255), outfileobj);
    end;
    ind = find(segim > 0 & segim < 255);
    segim(ind) = 1;
    imwrite(uint8(segim), VOClabelcolormap(255), outfileclass);
    
    an.cadboxes = cadboxes;
    an.masks = masks;
    an.depths = depths;
    an.cadinfo = cadinfo;
    annotation = an;
    if params.storeall
       save(annotoutfile, 'annotation');
       depthcad = depthim;
       save(depthfileobj, 'depthcad')
    end;
    unix(sprintf('rm %s', txtfile));
end;


function v = get_depth_order(boxes3D)

d = zeros(length(boxes3D), 1);

for i = 1 : length(boxes3D)
    di = dist2(boxes3D{i}.vertices, [0,0,0]);
    d(i) = min(di);
end;

[u,v] = sort(d, 'descend');


function [mesh, best_id, error] = matchCADmodels(allmodelfiles, vertices, K, bbox, imsize, params)

error = inf;
best_id = 0;
[dx, dy, dz] = getboxdimensions(vertices);
dims = [dx, dy, dz] * 1000;
lambda_2d = params.lambda_2d;
thresh = 500;
if params.usedims
    lambda_dims = params.lambda_dims;
else
    lambda_dims = zeros(1, 3);
end;

for i = 1 : length(allmodelfiles)
    if isempty(allmodelfiles(i).mesh)
        continue;
    end;
    compare = shouldCompare(allmodelfiles(i).mesh, params.usedims);
    if ~compare
        continue;
    end;
    [mesh, vert_ind] = findtransform(allmodelfiles(i), vertices);
    bbox3D = mesh.bbox;
    diff = bbox3D.vertices(vert_ind, :) - vertices;
    error_i = sqrt(sum(diff(:).^2)) / 8;
    %[x, y] = project_points(K, mesh.vertices);
    pp = projectToImage(mesh.vertices',K);
    pp = pp';
    x = pp(:, 1); y = pp(:, 2);
    box = [max(1, min(x)), max(1, min(y)), min(max(x), imsize(2)), min(max(y), imsize(1))];
    error_i = error_i + lambda_2d*sqrt(sum((box-bbox).^2))/2;
    if params.usedims
        if abs(mesh.dims(2)-dims(2)) > thresh
            error_i = 200;
        end;
        error_i = error_i + sum(lambda_dims .* abs(mesh.dims - dims));
    end;
    if error_i < error
        error = error_i;
        best_id = i;
    end;
end;

mesh = findtransform(allmodelfiles(best_id), vertices);

if params.useapp
    v = getfacedist(mesh);
    mesh.faces = mesh.faces(v, :);
end;

function compare = shouldCompare(mesh, usedims)

compare = 1;
if ~usedims
    %return;
else
    if ~isfield(mesh, 'dims')
        compare = 0;
        return;
    end;
    if length(mesh.dims) < 3
        compare = 0;
        return;
    end;
    
end;


function [mesh, ind] = findtransform(caddata, vertices)

    vertices_cad = caddata.mesh.bbox.vertices;
    [pointsCAD, ind] = getboxpoints(vertices_cad, caddata.mesh.bbox.faces);
    
    pointsdata = vertices;
    [d, p, transf] = procrustes(pointsdata,pointsCAD);
    transform = struct('t', transf.c(1, :), 'R', transf.T, 'sc', transf.b);
    %transform.t = point3D;
    %transform.sc = 1;
    %transform.R = pinv(pointsCAD(2:end,:)') * pointsdata(2:end,:)';
    mesh = caddata.mesh;
    mesh.transform{1} = transform;
    mesh = transform_mesh2(mesh);


function [segim, segimout, depthim, depthimout, fitbox] = getsilhCAD(box, box3D, segim, depthim, objid, K, mesh, sceneDepth, params)

box = bigbox(box, size(segim));
%[x, y] = project_points(K, mesh.vertices); 

% old version
%segimout = renderimage(segimout, mesh.faces, [x,y], [], 0);
tic
[segimout, depthimout, points] = getMaskFromCAD(K, mesh, size(segim));
box3D = getBox3D(box3D);
%[~, depthbox, ~] = getMaskFromCAD(K, box3D, size(segim));
depthbox = depthimout;
e=toc;
x = points(:, 1);
y = points(:, 2);

x = x(x>0.5 & x<= size(segim, 2)+0.4);% & x>=box(1) & x<=box(3));
y = y(y>0.5 & y<=size(segim, 1)+0.4);% & y>=box(2) & y<=box(4));
fitbox = [min(x),min(y),max(x), max(y)];
box = round(fitbox);

if 0
    M = size(segim, 1);
    N = size(segim, 2);
for i = 1 : size(mesh.faces, 1)
    ind = mesh.faces(i, :)';
    ind = [ind; ind(1)];
    %segimouti = roipoly(segimout, x(ind), y(ind));
    [xe,ye] = poly2edgelist(x(ind),y(ind));
    segimouti = edgelist2mask(M,N,xe,ye);
    segimout = segimout + segimouti;
end;
end;
segimout = double(segimout > 0);
%dim = min(box(3)-box(1), box(4)-box(2));
if 0%~isempty(supim) && dim >= 40
   dim = min(box(3)-box(1), box(4)-box(2));
   level = mapdim2sp(dim);
   ind = getSPind(box, segimout, supim{level}, supdata{level}, supsize{level}, 0.7);
   segimout = zeros(size(segimout));
   segimout(ind) = 1;
else
    segbox = zeros(size(segimout));
    segbox(box(2):box(4),box(1):box(3)) = 1;
    segimout = segimout .* segbox;
    ind = find(segbox == 0);
    depthimout(ind) = inf;
    if params.useocc
        [ind] = find((sceneDepth < depthbox - params.dist2cad_thresh) & segimout > 0);
        if ~isempty(ind)
           segimout(ind) = 0;
        end;
    end;
end;
ind = find(segimout);
segim(ind) = objid;
depthim = min(depthim, depthimout);


function i = mapdim2sp(dim)

i = floor(dim / 80) + 1;

function segim = getdontcare(segim, boxes, supim, supdata, supsize)

col = 255;
for i = 1 : size(boxes, 1)
    box = boxes(i, :);
    box = bigbox(box, size(segim));
    if nargin < 3 || isempty(supim)
        segim(box(2):box(4), box(1):box(3)) = col;
    else
       dim = min(box(3)-box(1), box(4)-box(2));
       level = mapdim2sp(dim);
       if level ==0, level=1; end;
       if level > length(supim), level = length(supim); end;
       ind = getSPind(box, [], supim{level}, supdata{level}, supsize{level}, 0.5);
       segim(ind) = col;
    end;
end;


function [supdata, supsize] = getsupdata(supim)

u = unique(supim(:));
n = length(u);
supdata = struct('ind', cell(n, 1));
supsize = zeros(n, 1);

for i = 1 : length(u)
    j = u(i);
    ind = find(supim == j);
    supdata(j).ind = ind;
    supsize(j) = length(ind);
end;



function ind = getSPind(box, mask, supim, supdata, supsize, thresh)

if isempty(mask)
   boxsup = supim(box(2):box(4),box(1):box(3));
else
    boxsup = supim(find(mask));
end;
boxsup = boxsup(:);
c = histc(boxsup, [1:length(supdata)]');
ind_sup = find(c >= thresh * supsize);
supbox = zeros(size(supim));
supboxtemp = supbox;
supboxtemp(box(2):box(4),box(1):box(3)) = 1;

for i = 1 : length(ind_sup)
    j = ind_sup(i);
    ind = supdata(j).ind;
    ind = ind(supboxtemp(ind) > 0);
    supbox(ind) = 1;
end;
ind = find(supbox);



function [vertices, vert_ind] = getboxpoints(vertices, faces)

faces_annot = faces_for_box();
vert_ind = zeros(size(vertices, 1), 1);

for i = 1 : size(vertices, 1)
    [y,x] = find(faces_annot == i);
    y = unique(y);
    
    vert = [1:size(vertices, 1)]';
    for j = 1 : length(y)
        vert = intersect(vert, faces(y(j), :));
    end;
    vert_ind(i) = vert;
end;

vertices = vertices(vert_ind, :);


function [supim, supdata, supsize] = getsupdata_main(ucm2)

thresh = [1:3:20];
supdata = cell(length(thresh), 1);
supsize = cell(length(thresh), 1);
supim = cell(length(thresh), 1);
%ucm2 = ucm2(3:2:end, 3:2:end);

for i = 1 : length(thresh)
   supim_i = bwlabel(ucm2 <= thresh(i)); % 5 was used to create sp_app
   %supim_i = supim_i+1;
   supim_i = supim_i(2:2:end, 2:2:end);
   [supdata_i, supsize_i] = getsupdata(supim_i);
   supdata{i} = supdata_i;
   supsize{i} = supsize_i;
   supim{i} = supim_i;
end;

function im = getimviz(map, im, segim, cadboxes, bboxes)

inc = 5;
doplot = 1;

colim = zeros(size(segim, 1), size(segim, 2), 3);

u = unique(segim(:));
u = u(u>0);

for i = 1 : length(u)
    ind = find(segim == u(i));
    if u(i) < 255
       col = map(mod((u(i) - 1) * inc, size(map, 1)) + 1, :);
    else
       col = [1,1,1]; 
    end;
    for j = 1 : 3
        indj = ind + (j-1) * size(segim, 1) * size(segim, 2);
        colim(indj) = col(j);
    end;
end;

lambda = 0.5;
colim = colim * 255;
imor = im;
im = double(im) * (1-lambda) + colim * lambda;
im = uint8(im);

if doplot
    imshow(imor);
hold on;
ind = find(sum(cadboxes, 2) > 0);
showdetection(bboxes(ind, :), 2)
showdetection(cadboxes(ind, :), 1)
hold off;
end;

function showdetection(boxes, which)

if which==1
    col = [1,0,0];
elseif which==2
    col = [1,0.7,0];
else
    col = [0.5,0,1];
end;

%imshow(im);
%hold on;
for i = 1 : size(boxes, 1)
    try
    rectangle('position', boxes(i, :), 'linewidth', 1.2, 'edgecolor', col);
    end;
end;


function segimout = renderimage2(segim, faces, x, y)

    M = size(segim, 1);
    N = size(segim, 2);
    segimout = zeros(M, N);
for i = 1 : size(faces, 1)
    ind = faces(i, :)';
    ind = [ind; ind(1)];
    %segimouti = roipoly(segimout, x(ind), y(ind));
    [xe,ye] = poly2edgelist(x(ind),y(ind));
    segimouti = edgelist2mask(M,N,xe,ye);
    segimout = segimout + segimouti;
end;
segimout = double(segimout > 0);


function template = renderimage(template, faces, vertices, meshtransf, rendertexture)

figure('position', [5,5,size(template,2), size(template, 1)]);
subplot('position',[0,0,1,1]);
imshow(template);
hold on;
if ~rendertexture
   h=trisurf(faces,vertices(:,1),vertices(:,2),ones(size(vertices, 1), 1),'EdgeColor','none','FaceColor',[1,1,1]);
else
   h=trisurf(faces,vertices(:,1),vertices(:,2),ones(size(vertices, 1), 1),'facevertexcdata',meshtransf.colors,'EdgeColor','none');
end;
%a = get(h);
%set(h, 'XData', round(a.XData));
%set(h, 'YData', round(a.YData));
%axis off; axis equal;
hold off;
stamp = rand(1);
file=fullfile(pwd, [strrep(sprintf('%0.8f',stamp),'.','') '.png']);
%saveas(gcf, file);
export_fig(file)
template = imread(file);
template = double(template > 0);
delete(file);
close all;


function [t, bb] = getobjectmask(template, bckgr)

temp = ones(size(template, 1), size(template, 2));
for i = 1 : 3, temp = temp .* double(abs(template(:,:,i)-bckgr(i))<5); end;
[y,x] = find(temp);
bb = [min(x),min(y),max(x),max(y)];
template = template(bb(2):bb(4),bb(1):bb(3),:);
t = zeros(size(template,1),size(template,2));
ind = find(template(:,:,3)==0);
t(ind) = 1;
t = repmat(t, [1,1,size(template,3)]);


function box = bigbox(box, imsize)

%box(1:2) = box(1:2)-1;
%box(3:4) = box(3:4)+1;
%box(1:2) = max(box(1:2), ones(1,2));
%box(3:4) = [min(box(3), imsize(2)), min(box(4), imsize(1))];


function v = getfacedist(mesh)

vertices = mesh.vertices;
faces = mesh.faces;
d = sqrt(vertices(:, 1).^2 + vertices(:, 2).^2 + vertices(:, 3).^2);
ind = find(faces);
dfaces = zeros(size(faces));
dfaces(ind) = d(faces(ind));
dfaces = max(dfaces, [], 2);
%dfaces = mean(dfaces, 2);
[u, v] = sort(dfaces, 'descend');


function box3D = getBox3D(box3D)

mesh = box3D;
if size(mesh.faces, 2) > 3
    sides = zeros(size(mesh.faces, 1), 3);
    for i = 1 : 3
        temp = mesh.vertices(mesh.faces(:, i + 1), :) - mesh.vertices(mesh.faces(:, 1), :);
        sides(:, i) = sqrt(sum(temp .* temp, 2));
    end;
    [u, v] = sort(sides, 2);
    v = v + 1;
    temp = [mesh.faces(:, 1), zeros(size(mesh.faces, 1), 3)];
    for i = 1 : 3
       u = v(:, i);
       x = sub2ind(size(mesh.faces), [1 : size(mesh.faces, 1)]', u);
       temp(:, i + 1) = mesh.faces(x);
    end;
    mesh.faces=  temp;
    mesh.faces = [mesh.faces(:, 1 : 3); mesh.faces(:, [2,3,4])];
end;
box3D = mesh;