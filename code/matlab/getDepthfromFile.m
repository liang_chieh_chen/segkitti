function [depth, disp] = getDepthfromFile(imname)

segkitti_globals;
disp = imread(fullfile(stereo_dir, [imname '_disparity.png']));
camdata = getCamParams(imname);
depth = getDepthfromDisp(double(disp), camdata);


