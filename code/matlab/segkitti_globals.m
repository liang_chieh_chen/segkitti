DATASET_NAME = 'LKITTI';
global CLASS_NAME;
CLASS_NAME = 'Car';
%dataset_globals;
%CLASS_NAME = 'Car';

calib_which = 'train';

machine = 'server';
switch machine
    case 'my'
        %DATA_PATH = '~/science/data/3D/';
        CAD_MODELS_PATH = fullfile(DATA_PATH, 'CAD', CLASS_NAME, 'matlab');
        model_dir = CAD_MODELS_PATH;
        DATASET_ROOT = '~/science/data/3D/real_objects/KITTI';
        depth_dir = fullfile(DATASET_ROOT, 'jay/recon_depth_LapMRF');
    otherwise
        %DATASET_ROOT = '/share/data/segkitti/'; 
        DATA_PATH = '~/dataset/segkitti/';
        DATASET_ROOT = '~/dataset/segkitti';
        
        model_dir = fullfile(DATASET_ROOT, 'CAD', 'v2', lower(CLASS_NAME));
        depth_dir = fullfile(DATASET_ROOT, 'tracklets/recon_depth_LapMRF');
        stereo_dir = fullfile(DATASET_ROOT, 'Images_left_disp_pcbp');
        calib_dir = fullfile(DATASET_ROOT, 'calib', calib_which);
end;