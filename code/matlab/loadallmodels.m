function modelfiles = loadallmodels(allmodelsfile, model_dir)

files = dir(fullfile(model_dir, '*_mesh.mat'));
modelfiles = struct('name', cell(length(files), 1), 'mesh', cell(length(files), 1));
pntr = 1;

for i = 1 : length(files)
    fprintf('model %d/%d\n', i, length(files));
    filename = fullfile(model_dir, files(i).name);
    modelfiles(i).name = filename;
    meshdata = load(filename);
    if ~isfield(meshdata, 'mesh')
        fprintf('no mesh\n');
        continue;
    end;
    mesh = meshdata.mesh;
    try
    mesh = rmfield(mesh, 'm');
    end;
    mesh = preprocessmesh(mesh);
    modelfiles(pntr).mesh = mesh;
    pntr = pntr + 1;
end;


save(allmodelsfile, 'modelfiles')