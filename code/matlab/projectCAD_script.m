% CROSS-VALIDATION of PARAMS
clear all; close all; clc;

addpath(genpath(pwd));

segkitti_globals;
params.usedims = 1;
params.useapp = 0;
params.useocc = 0;   % faster if we don't load Velodyne  
params.storeall = 1;
params.display_progress = 1;
params.delete_folder = 1;

for s = 1:5
    split_folder = sprintf('split%d', s);
    
    %for p = [2 3 4 5 10 15 20 25 30 35 40 45 50 100 200 300 400 500]
    for p = [2 3 4 5 10 20 50 100 200 500]
        partition = sprintf('p%03d', p);

        params.version = ['v4_trained_', split_folder, partition];
        trainfilename = sprintf('cad2tracklet_trainval_list_%s', partition);

        trainfile = fullfile(DATASET_ROOT, 'tracklets', split_folder, [trainfilename '.txt'])
        traininfo = getTrackletIds2AnnotationIds(trainfile);

        lambda_2d = [0: 0.02 : 0.2];  % weight for 2D box match
        factor = [0:0.002 : 0.02];
        lambda_dims = [1, 2, 1];  % weight for 3D dim match
        params.dist2cad_thresh = 1.5;   % weight for occlusion (if a 3d point is closer than this value from the CAD depth, it's occlusion)
        bestparams = [];
        bestacc = -inf;
        pntr = 0;


        if 0
        for i = 1 : length(lambda_2d)
            params.lambda_2d = lambda_2d(i);
            for j = 1 : length(factor)
                pntr = pntr + 1;
                fprintf('iter: %d/%d\n', pntr, length(lambda_2d) * length(factor));
                params.lambda_dims = factor(j) * lambda_dims;
                [modelfiles, outdirclass] = fitCADtoKITTIapp_Jay(traininfo, params);
                if ~isfield(params, 'modelfiles')
                    params.modelfiles = modelfiles;
                end;
                [accuracies,avacc,conf,rawcounts] = evalseg(outdirclass);
                if avacc > bestacc
                    bestacc = avacc;
                    bestparams = params;
                    fprintf('accuracy: %0.2f\n', bestacc)
                end
            end;
        end;
        else
            params.lambda_dims = mean(factor) * lambda_dims;
            for j = 1 : length(factor)
                pntr = pntr + 1;
                fprintf('iter: %d/%d\n', pntr, length(factor) + length(lambda_2d));
                params.lambda_2d = lambda_2d(j);
                [modelfiles, outdirclass] = fitCADtoKITTIapp_Jay(traininfo, params);
                if ~isfield(params, 'modelfiles')
                    params.modelfiles = modelfiles;
                end;
                [accuracies,avacc,conf,rawcounts] = evalseg(outdirclass);
                if avacc > bestacc
                    bestacc = avacc;
                    bestparams = params;
                    fprintf('accuracy: %0.2f\n', bestacc)
                end
            end; 

            params.lambda_2d = bestparams.lambda_2d;
            for j = 1 : length(factor)
                pntr = pntr + 1;
                fprintf('iter: %d/%d\n', pntr, length(factor) + length(lambda_2d));
                params.lambda_dims = factor(j) * lambda_dims;
                [modelfiles, outdirclass] = fitCADtoKITTIapp_Jay(traininfo, params);
                if ~isfield(params, 'modelfiles')
                    params.modelfiles = modelfiles;
                end;
                [accuracies,avacc,conf,rawcounts] = evalseg(outdirclass);
                if avacc > bestacc
                    bestacc = avacc;
                    bestparams = params;
                    fprintf('accuracy: %0.2f\n', bestacc)
                end
            end;    
        end;



        bestparams.useocc = 1;
        outfile = fullfile(outdirclass, '..', '..', ['learned_params_' params.version '.mat']);
        save(outfile, 'bestparams', 'bestacc', 'traininfo')

        % TESTING ? 
        fitCADtoKITTIapp_Jay([], bestparams)
        [accuracies,avacc,conf,rawcounts] = evalseg(outdirclass);
        

    end
    
end
