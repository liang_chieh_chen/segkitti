function depth = getDepthfromDisp(disp, camdata)

       disp = double(disp) / 256;
       disp(disp == 0) = -1;
       depth = camdata.f * camdata.baseline ./ double(disp);