function data = readBinFile(filename)

fid = fopen(filename,'rb');
data = fread(fid,[2 1],'int');
dy = data(1);
dx = data(2);
data = fread(fid,[dx*dy 1],'float');
%data = data(1:5:end,:); % remove every 5th point for display speed
data = reshape(data', [dx, dy])';
fclose(fid);