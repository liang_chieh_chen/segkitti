function testCADmodels(version)

segkitti_globals;
outroot = fullfile(DATASET_ROOT, 'Segmentation_CAD');
outfile = fullfile(outroot, ['learned_params_' version '.mat']);
load(outfile, 'bestparams', 'bestacc', 'traininfo')

% TESTING ?  
bestparams.display_progress = 1;
bestparams.storeall = 1;
bestparams.delete_folder = 0;
fitCADtoKITTIapp_Jay([], bestparams)