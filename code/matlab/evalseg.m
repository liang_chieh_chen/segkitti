%VOCEVALSEG Evaluates a set of segmentation results.
% VOCEVALSEG(VOCopts,ID); prints out the per class and overall
% segmentation accuracies. Accuracies are given using the intersection/union 
% metric:
%   true positives / (true positives + false positives + false negatives) 
%
% [ACCURACIES,AVACC,CONF] = VOCEVALSEG(VOCopts,ID) returns the per class
% percentage ACCURACIES, the average accuracy AVACC and the confusion
% matrix CONF.
%
% [ACCURACIES,AVACC,CONF,RAWCOUNTS] = VOCEVALSEG(VOCopts,ID) also returns
% the unnormalised confusion matrix, which contains raw pixel counts.
function [accuracies,avacc,conf,rawcounts] = evalseg(resdir)

% image test set
%DATASET_ROOT = '/share/data/segkitti/';
DATASET_ROOT = '~/dataset/segkitti/';

annot_dir = fullfile(DATASET_ROOT, 'Annotations_left');
gt_dir = fullfile(DATASET_ROOT, 'AnnotMasks_left');
testset = 'all_list';

gt_filename = fullfile(DATASET_ROOT, [testset, '.txt']);
fid = fopen(gt_filename);
gtids = textscan(fid, '%s');
fclose(fid);
gtids = gtids{1};

%[gtids]=textread(fullfile(DATASET_ROOT, [testset '.txt']),'%s');

classes{1} = 'car';

% number of labels = number of classes plus one for the background
num = length(classes) + 1; 
nclasses = length(classes);
confcounts = zeros(num);
count=0;
tic;
for i=1:length(gtids)
    % display progress
    if toc>1
        fprintf('test confusion: %d/%d\n',i,length(gtids));
        drawnow;
        tic;
    end
        
    imname = gtids{i};
    
    % ground truth label file
    [~,name] = fileparts(imname);
    data = load(fullfile(annot_dir, [name '.mat']));
    annotation = data.annotation;
    gtfile = fullfile(gt_dir, imname);
    if ~exist(gtfile, 'file')
        fprintf('gt file doesnt exist, skipping\n');
        continue;
    end;
    [gtim,map] = imread(gtfile);    
    gtim = double(gtim);
    gtim = getBBgt(gtim, annotation);
    
    % results file
    resfile = fullfile(resdir, imname);
    if exist(resfile, 'file')
       [resim,map] = imread(resfile);
    else
        resim = zeros(size(gtim));
    end;
    resim = double(resim > 0);

    szgtim = size(gtim); szresim = size(resim);
    if any(szgtim~=szresim)
        error('Results image ''%s'' is the wrong size, was %d x %d, should be %d x %d.',imname,szresim(1),szresim(2),szgtim(1),szgtim(2));
    end
    
    %pixel locations to include in computation
    locs = gtim<255;
    
    % joint histogram
    sumim = 1+gtim+resim*num; 
    hs = histc(sumim(locs),1:num*num); 
    count = count + numel(find(locs));
    confcounts(:) = confcounts(:) + hs(:);
end

% confusion matrix - first index is true label, second is inferred label
%conf = zeros(num);
conf = 100*confcounts./repmat(1E-20+sum(confcounts,2),[1 size(confcounts,2)]);
rawcounts = confcounts;

% Percentage correct labels measure is no longer being used.  Uncomment if
% you wish to see it anyway
%overall_acc = 100*sum(diag(confcounts)) / sum(confcounts(:));
%fprintf('Percentage of pixels correctly labelled overall: %6.3f%%\n',overall_acc);

accuracies = zeros(nclasses,1);
fprintf('Accuracy for each class (intersection/union measure)\n');
for j=1:num
   
   gtj=sum(confcounts(j,:));
   resj=sum(confcounts(:,j));
   gtjresj=confcounts(j,j);
   % The accuracy is: true positive / (true positive + false positive + false negative) 
   % which is equivalent to the following percentage:
   accuracies(j)=100*gtjresj/(gtj+resj-gtjresj);   
   
   clname = 'background';
   if (j>1), clname = classes{j-1};end;
   fprintf('  %14s: %6.3f%%\n',clname,accuracies(j));
end
accuracies = accuracies(1:end);
avacc = mean(accuracies);
fprintf('-------------------------\n');
fprintf('Average accuracy: %6.3f%%\n',avacc);


function gtim = getBBgt(gtim, annotation)

mask = zeros(size(gtim));
u = unique(gtim(:));
u = u(u>0);

for i = 1 : length(u)
    box = annotation.bboxes(u(i), :);
    box(3:4) = box(1:2) + box(3:4);
    box = round(box);
    box(1:2) = max(1, box(1:2));
    box(3) = min(size(mask, 2), box(3));
    box(4) = min(size(mask, 1), box(4));
    mask(box(2):box(4),box(1):box(3)) = 1;
end;

ind = find(mask == 0 & gtim == 0);
gtim = double(gtim > 0);
gtim(ind) = 255;