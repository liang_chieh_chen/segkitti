function [x, y] = project_points(T, points)  

% gets 2D projection of 'points' given 3x3 camera matrix T
% INPUT:
% T ... 3x3 camera matrix 
% points ... n 3D points in rows
% OUTPUT
% x, y coordinates of the projection

if size(points, 2) == 3
    points = points';
end;
[~, n] = size(points);
   
   xt = T*points;
   x = zeros(1,n); y = zeros(1,n);
   x(:) = xt(1,:)./xt(3,:);
   y(:) = xt(2,:)./xt(3,:);
   
   x = x';
   y = y';