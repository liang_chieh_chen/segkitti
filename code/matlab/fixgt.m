function gt = fixgt(gt, trunc, box, imsize)

if trunc == 0
    return;
end;
gt = double(gt);

gt = fixgt_main(gt, box, imsize);
gt = fixgt_main(gt, box, imsize);

function gt = fixgt_main(gt, box, imsize)

hc = histc(gt(:), [1:max(10,max(gt(:)))]);
[~,c] = max(hc);
[y,x] = find(gt);
xmin = min(x);
xmax = max(x);
ymin = min(y);
ymax = max(y);
w = box(3) - box(1);
h = box(4) - box(1);
p = 0.05;
thresh_x = max(5, w *p);
slack_x = max(5, w * p);
thresh_y = max(5, h *p);
slack_y = max(5, h * p);

if box(1) <= slack_x
    % box is on the left side
    for i = ymin : ymax
        xi = find(gt(i, :));
        if ~isempty(xi) && xi(1) <= thresh_x
            gt(i, 1:xi(1)) = c;
        end;
    end;
end;

if box(3) >= imsize(2) - slack_x + 1
    % box is on the right side
    for i = ymin : ymax
        xi = find(gt(i, :));
        if ~isempty(xi) && size(gt, 2) - xi(end) <= thresh_x
            gt(i, xi(end) : end) = c;
        end;
    end;
end;

if box(2) <= slack_y
    % box is on the top
    for i = xmin : xmax
        yi = find(gt(:, i));
        if ~isempty(yi) && yi(1) <= thresh_y
            gt(1:yi(1), i) = c;
        end;
    end;
end;

if box(4) >= imsize(1) - slack_y + 1
    % box is on the bottom
    for i = xmin : xmax
        yi = find(gt(:, i));
        if ~isempty(yi) && size(gt, 1) - yi(end) <= thresh_y
            gt(yi(end) : end, i) = c;
        end;
    end;
end;
