function velo_img = readVelodynePC(imname, im2video)

if nargin < 2
    [videos, im2video] = getKITTIvideonames_Jay();
end;

imlist = arrayfun(@(x)x.name, im2video, 'UniformOutput', 0);
ind = strmatch(imname, imlist, 'exact');

segkitti_globals;
calib_dir = fullfile(DATASET_ROOT, im2video(ind).seq);
calib = loadCalibrationCamToCam(fullfile(calib_dir,'calib_cam_to_cam.txt'));
Tr_velo_to_cam = loadCalibrationRigid(fullfile(calib_dir,'calib_velo_to_cam.txt'));

% compute projection matrix velodyne->image plane
cam = 2;
R_cam_to_rect = eye(4);
R_cam_to_rect(1:3,1:3) = calib.R_rect{1};
P_velo_to_img = calib.P_rect{cam+1}*R_cam_to_rect*Tr_velo_to_cam;

% load velodyne points
fid = fopen(sprintf('%s/velodyne_points/data/%s.bin',fullfile(DATASET_ROOT, im2video(ind).seq, im2video(ind).videoname),im2video(ind).frame),'rb');
velo = fread(fid,[4 inf],'single')';
%velo = velo(1:5:end,:); % remove every 5th point for display speed
fclose(fid);

% remove all points behind image plane (approximation
idx = velo(:,1)<5;
velo(idx,:) = [];

% project to image plane (exclude luminance)
velo_img = project(velo(:,1:3),P_velo_to_img);

% plot points
cols = jet;
for i=1:size(velo_img,1)
  col_idx = round(64*5/velo(i,1));
  plot(velo_img(i,1),velo_img(i,2),'o','LineWidth',4,'MarkerSize',1,'Color',cols(col_idx,:));
end