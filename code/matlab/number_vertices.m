function num = number_vertices(box)

%     8-----7
%    /|    /|
%   4-----3 |
%   | 5---|-6
%   1-----2/
%
% function labels the vertices so that:
%         box.faces(1, :) is defined by vertices 1-2-3-4
%                  (2, :)                        1-2-6-5
%                  (3, :)                        2-6-7-3
%                  (4, :)                        1-5-8-4
%                  (5, :)                        4-3-7-8
%                  (6, :)                        5-6-7-8


faces = box.faces;
box = number_box_faces(box, [1,2]');  % make sure of an ordering
num = zeros(size(box.vertices, 1), 1);
num(1) = intersect(intersect(faces(1, :), faces(2, :)), faces(4, :));
num(2) = intersect(intersect(faces(1, :), faces(2, :)), faces(3, :));
num(3) = intersect(intersect(faces(1, :), faces(3, :)), faces(5, :));
num(4) = intersect(intersect(faces(1, :), faces(4, :)), faces(5, :));
num(5) = intersect(intersect(faces(2, :), faces(4, :)), faces(6, :));
num(6) = intersect(intersect(faces(2, :), faces(3, :)), faces(6, :));
num(7) = intersect(intersect(faces(3, :), faces(5, :)), faces(6, :));
num(8) = intersect(intersect(faces(4, :), faces(5, :)), faces(6, :));