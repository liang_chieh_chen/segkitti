function data = getCamParams(imname)

segkitti_globals;

calibfile = fullfile(calib_dir, [imname '.txt']);
P_left = getcam(calibfile, 2, 1);
P_right = getcam(calibfile, 3, 1);


    [K_left, R_left, t_left] = art(P_left);
    [K_right, R_right, t_right] = art(P_right);
    
    baseline = abs(t_right(1,1) - t_left(1,1));
    f = K_left(1,1);
    
    data.baseline = baseline;
    data.f = f;
    data.P_left = P_left;
    data.P_right = P_right;
    data.K_left = K_left;
    data.K_right = K_right;
    data.t_left = t_left;
    data.t_right = t_right;