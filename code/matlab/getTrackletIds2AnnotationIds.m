function outdata = getTrackletIds2AnnotationIds(filename)

%[imfiles, object_ids] = textread(filename, '%s %s');

fid = fopen(filename, 'r');
mapping = textscan(fid, '%s %s');
fclose(fid);

imfiles    = mapping{1};
object_ids = mapping{2};

txt = [];
for i = 1 : length(object_ids)
    comma = ',';
    if i == 1, comma = []; end;
    txt = [txt ',' object_ids{i}];
end;
object_ids = str2num(txt)' + 1;

for i = 1 : length(imfiles)
    p = findstr(imfiles{i}, '_');
    imfiles{i} = imfiles{i}(1 : p(1) - 1);
end;

[~, v, ids] = unique(imfiles);

imfiles = imfiles(v);
objects = struct('ind_objects', cell(length(imfiles), 1));

for i = 1 : length(object_ids)
   id = ids(i);
   objects(id).ind_objects = [objects(id).ind_objects; object_ids(i)];
end;

outdata.imfiles = imfiles;
outdata.objects = objects;