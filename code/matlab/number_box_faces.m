function box = number_box_faces(box, nums)

% INPUT
% box in the format of either cornerpoints or as a mesh (mesh.vertices are
% cornerpoints and mesh.face are current faces)
% nums ... [i_first, i_ground], where i_first indicating the sequential
% number of the frontal face in the current arrangement, and i_ground the
% number of the ground face
% nums can also be empty, and there will be a 'best guess' for them

% OUTPUT
% a box as a mesh with faces arranged as:
% first face = front
% second face ground face
% 3 : right
% 4 : left
% 5 : top
% 6 : back

% get sides of the box
[sides, face_sides] = get_sides(box);
[x, y, val] = find(face_sides);
N = sparse(x, val, ones(size(x)), size(face_sides, 1), size(sides, 1));
N_f = N * N';
for i = 1 : size(N_f, 1), N_f(i,i) = 0; end;
if nums(2) ~= 2 & nums(1) ~= 1
    [ind] = find(N_f(nums(1), :));
    if isempty(intersect(nums(2), ind))
        nums(2) = ind(1);
    end;
end;
[ind] = find(N_f(nums(1), :));
if isempty(intersect(nums(2), ind))
    nums(2) = ind(1);
end;
num = nums(1); % front face
% ground face
if N_f(num, nums(2)), num = [num; nums(2)]; else [ind] = find(N_f(num, :)); num = [num; ind(1)]; end; 
% third face
[ind] = find(N_f(num(1), :) & N_f(num(2), :));   % should be of size 2
face_f = box.faces(num(1), :);
face_g = box.faces(num(2), :);
err = zeros(length(ind), 1);
for i = 1 : length(ind)
    s = intersect(face_f, face_g);
    a = intersect(s, box.faces(ind(i), :)); % corner point of all three faces
    d = intersect(s, box.faces(ind(mod(i, length(ind)) + 1), :)); % other corner point of all three faces
    [x, y] = find(sides == a);
    s1 = intersect(setdiff(face_sides(num(2), :), face_sides(num(1), :)), x); s1 = s1';
    b = sides(s1, find(sides(s1, :) ~= a));
    s2 = intersect(setdiff(face_sides(num(1), :), face_sides(num(2), :)), x); s2 = s2';
    c = sides(s2, find(sides(s2, :) ~= a));
    sb = box.vertices(b, :) - box.vertices(a, :);
    sc =  box.vertices(c, :) - box.vertices(a, :);
    sd = box.vertices(d, :) - box.vertices(a, :);
    v = box.vertices(a, :) + cross(sb / norm(sb), sc / norm(sc)) * norm(sd);
    err(i) = norm(box.vertices(d, :) - v);
end;
[e, j] = max(err);
num = [num; ind(j)];
% fourth face
num = [num; ind(mod(j, length(ind)) + 1)];
% fifth face
[ind] = find(N_f(num(1), :) & N_f(num(3), :) & N_f(num(4), :));
num = [num; ind(find(ind ~= num (2)))];
% last face
num = [num; setdiff([1:6]', num)];
box.faces = box.faces(num, :);




function [sides, face_sides] = get_sides(box)

sides = [];
n = size(box.faces, 1);
m = size(box.faces, 2);
for i = 1 : m
    sides = [sides; box.faces(:, [i , mod(i, m) + 1])];
end;

sides = sort(sides, 2);
sides = unique(sides, 'rows');
face_sides = zeros(size(box.faces));
for j = 1 : n
    for i = 1 : m
        s = box.faces(j, [i , mod(i, m) + 1]);
        s = sort(s, 2);
        [ind] = find(all(sides == repmat(s, [size(sides, 1), 1]), 2));
        face_sides(j, i) = ind;
    end;
end;


